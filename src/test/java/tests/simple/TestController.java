package tests.simple;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.dao.RepositorioEstadisticas;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.turnos.Estado;
import com.aplicacion.spring.negocio.entidades.turnos.Horario;
import com.aplicacion.spring.negocio.entidades.turnos.Turno;
import com.aplicacion.spring.negocio.servicios.ServiciosTurnos;
import com.aplicacion.spring.utilitarios.Constantes;
import com.aplicacion.spring.utilitarios.controladores.Conversion;

import de.felixroske.jfxsupport.FXMLController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import tests.simple.beans.BeanConsulta;
import tests.simple.beans.Cronograma;
import tests.simple.beans.ListadoTurnos;
import tests.simple.beans.PacienteLite;
import tests.simple.beans.ProfesionalLite;
import tests.simple.beans.TurnoProfesional;

@FXMLController
public class TestController implements Initializable {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(TestController.class);
	@Autowired
	ServiciosTurnos serviciosTurnos;

	private static final String XML_FILE = "input.xml";
	private static final String XSLT_FILE = "template.xsl";
	private static final String HTML_FILE = "output.html";

	@FXML
	Button boton;
	@FXML
	Label etiqueta;
	@Autowired
	Aplicacion aplicacion;
	@Autowired
	BeanConsulta beanConsulta;
	@Autowired
	RepositorioEstadisticas repositorioEstadisticas;
	static int tipo = 0;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		test4();
	}

	private void test4() {
		boton.setOnAction(e -> {

			List<Turno> listaTurnos = crearListaTurnos(new ArrayList<>());
			for (Turno turnoRecuperado : serviciosTurnos.obtenerTurnos(9, LocalDate.now().minusDays(1))) {
				for (Turno turnoVacio : listaTurnos) {
					if (turnoRecuperado.getHorario().getId() == turnoVacio.getHorario().getId()) {
						listaTurnos.set(listaTurnos.indexOf(turnoVacio), turnoRecuperado);
					}
				}
			}

			List<TurnoProfesional> turnosProfesional = new ArrayList<>();
			for (Turno turno : listaTurnos) {
				TurnoProfesional turnoProfesional = new TurnoProfesional();
				turnoProfesional.setHorario(turno.getHorario());
				turnoProfesional.setEstado(turno.getEstado());
				turnoProfesional.setPaciente(new PacienteLite(turno.getPaciente()));
				turnoProfesional.setObservaciones(turno.getObservaciones());
				turnosProfesional.add(turnoProfesional);
			}

			Cronograma cronograma = new Cronograma();
			cronograma.setProfesional(new ProfesionalLite(9, "jprez", "Jorge Prez"));
			cronograma.setFecha(LocalDate.now().minusDays(1).format(Constantes.FECHA_DIA_DD_MM_YY));
			cronograma.setListadoTurnos(new ListadoTurnos(turnosProfesional));

			Conversion.formarXML(cronograma, XML_FILE);
			Conversion.convertirXMLaHTML(XML_FILE, XSLT_FILE, HTML_FILE);

		});
	}

	/**
	 * Obtiene la lista de turnos para cada dia en base a los horarios
	 * 
	 * @param listaTurnos
	 * @return
	 */
	private List<Turno> crearListaTurnos(List<Turno> listaTurnos) {
		for (Horario horario : aplicacion.getRepositorios().getListaHorarios()) {
			listaTurnos.add(new Turno(LocalDate.now().minusDays(1), horario, Estado.LIBRE, new Usuario(),
					new Paciente(), null));
		}
		return listaTurnos;
	}

	// private void test3() {
	//
	// boton.setOnAction(e -> {
	// // for (EstadisticasConsultaMedicina valor : repositorioEstadisticas
	// // .obtenerEstadisticasConsultasPaciente(8344)) {
	// // System.out.println(valor);
	// // }
	// for (EstadisticasPatologia valor :
	// repositorioEstadisticas.obtenerEstadisticasPatologiasPaciente(8344,
	// Patologia.HEMOGLOBINA_GLICOCILADA)) {
	// System.out.println(valor);
	// }
	// });
	//
	// }
	//
	// private void test2() {
	//
	// boton.setOnAction(e -> {
	// beanConsulta.setTipo(tipo++);
	// System.out.println(beanConsulta.nuevaConsulta());
	// });
	//
	// }
	//
	// private void test1() {
	// Paciente pac1 = new Paciente(1, "Flores", "Pepito");
	// Paciente pac2 = new Paciente(2, "Malena", "Cabeza");
	//
	// aplicacion.getSesion().pacienteProperty()
	// .addListener((o, p, pacienteActual) -> System.out.println(pacienteActual));
	// aplicacion.getSesion().usuarioProperty()
	// .addListener((o, p, usuarioActual) -> System.out.println(usuarioActual));
	// aplicacion.getSesion().turnoProperty().addListener((o, p, turnoActual) ->
	// System.out.println(turnoActual));
	//
	// etiqueta.visibleProperty().bind(aplicacion.getSesion().pacienteProperty().isNull());
	// boton.setOnAction(e -> {
	// if (aplicacion.getSesion().getPaciente() == null) {
	// aplicacion.getSesion().setPaciente(new Paciente());
	// } else {
	// aplicacion.getSesion().setPaciente(null);
	// }
	// });
	// aplicacion.getSesion().setPaciente(new Paciente(pac1));
	// aplicacion.getSesion().setPaciente(new Paciente());
	// aplicacion.getSesion().setPaciente(new Paciente(pac2));
	// aplicacion.getSesion().setUsuario(new Usuario());
	// aplicacion.getSesion().setTurno(new Turno());
	// }
}
