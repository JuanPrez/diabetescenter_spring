package tests.simple;

import java.util.Arrays;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.aplicacion.spring.Ventanas;
import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Parametros.Ambiente;

import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

@SpringBootApplication
@ComponentScan(basePackages = { "tests.simple", "com.aplicacion.spring" })
public class TestMain extends AbstractJavaFxApplicationSupport {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(TestMain.class);

	Aplicacion aplicacion;

	/**
	 * Inicio del programa
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		launch(TestMain.class, TestView.class, args);
	}

	/**
	 * Ejecucion de los procesos de configuracion y pre inicializacion de las vistas
	 */
	@Override
	public void beforeInitialView(Stage stage, ConfigurableApplicationContext ctx) {
		super.beforeInitialView(stage, ctx);

		// Union manual del bean de aplicacion
		aplicacion = ctx.getBean(Aplicacion.class);

		// Manejo de parametros
		bitacora.info(aplicacion.getParametros().manejarParametros(getParameters().getRaw()));

		// Configuramos el ambiente a usar
		aplicacion.getPropiedades().setAmbiente(Ambiente.DESARROLLO);

		// Lectura de propiedades
		if (aplicacion.getPropiedades().probar()) {
			bitacora.info("Archivo de propiedades leido exitosamente");
		} else {
			Alert sinPropiedades = Ventanas.crearAlerta(AlertType.ERROR, "Error critico",
					"Imposible leer el archivo de propiedades",
					"No se puede encontrar o abrir el archivo de propiedades", Arrays.asList(ButtonType.OK));
			Optional<ButtonType> respuesta = sinPropiedades.showAndWait();

			if (respuesta.isPresent())
				System.exit(0);
		}

		// Creacion del pool de conexiones
		aplicacion.iniciarBaseDeDatos(3);

		// Inicializacion de los repositorios
		aplicacion.getRepositorios().iniciarRepositorios();

//		validarInicio();
	}


}
