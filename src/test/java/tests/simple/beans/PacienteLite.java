package tests.simple.beans;

import com.aplicacion.spring.negocio.entidades.paciente.Paciente;

public class PacienteLite {
	int id;
	String apellido;
	String nombre;
	String documento;

	public PacienteLite() {

	}

	public PacienteLite(Paciente paciente) {
		this.id = paciente.getId();
		this.apellido = paciente.getApellido();
		this.nombre = paciente.getNombre();
		this.documento = paciente.getDatosPersonales().getDni();
	}

	public PacienteLite(int id, String apellido, String nombre, String documento) {
		this.id = id;
		this.apellido = apellido;
		this.nombre = nombre;
		this.documento = documento;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	@Override
	public String toString() {
		return "PacienteLite [id=" + id + ", apellido=" + apellido + ", nombre=" + nombre + ", documento=" + documento
				+ "]";
	}

}
