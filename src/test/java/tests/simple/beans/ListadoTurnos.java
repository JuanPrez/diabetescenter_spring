package tests.simple.beans;

import java.util.List;

public class ListadoTurnos {
	private List<TurnoProfesional> turnos;

	public ListadoTurnos(List<TurnoProfesional> turnos) {
		this.turnos = turnos;
	}

	public List<TurnoProfesional> getTurnos() {
		return turnos;
	}

	public void setTurnos(List<TurnoProfesional> turnos) {
		this.turnos = turnos;
	}

}
