package tests.simple.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.negocio.entidades.registros.consultas.Consulta;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaMedicina;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaNutricion;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaPodologica;

@Component
public class BeanConsulta {

	@Autowired
	Aplicacion aplicacion;

	int tipo = 1;

	@Bean
	@Scope("prototype")
	public Consulta nuevaConsulta() {
		switch (tipo) {
		case 1:
			return new ConsultaMedicina();
		case 2:
			return new ConsultaNutricion();
		case 3:
			return new ConsultaPodologica();
		default:
			return new ConsultaMedicina();
		}
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
}
