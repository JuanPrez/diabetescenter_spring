package tests.simple.beans;

import java.time.LocalDate;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.aplicacion.spring.negocio.entidades.turnos.AdapterEstado;
import com.aplicacion.spring.negocio.entidades.turnos.Estado;
import com.aplicacion.spring.negocio.entidades.turnos.Horario;

public class TurnoProfesional {
	private LocalDate fecha;
	private Horario horario;
	private Estado estado;
	private PacienteLite paciente;
	private String observaciones;

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public Horario getHorario() {
		return horario;
	}

	public void setHorario(Horario horario) {
		this.horario = horario;
	}

	public Estado getEstado() {
		return estado;
	}

	@XmlJavaTypeAdapter(AdapterEstado.class)
	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public PacienteLite getPaciente() {
		return paciente;
	}

	public void setPaciente(PacienteLite paciente) {
		this.paciente = paciente;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + ((horario == null) ? 0 : horario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof TurnoProfesional))
			return false;
		TurnoProfesional other = (TurnoProfesional) obj;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (horario == null) {
			if (other.horario != null)
				return false;
		} else if (!horario.equals(other.horario))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Turno [fecha=" + fecha + ", horario=" + horario + ", estado=" + estado + ", paciente=" + paciente
				+ ", observaciones=" + observaciones + "]";
	}

}
