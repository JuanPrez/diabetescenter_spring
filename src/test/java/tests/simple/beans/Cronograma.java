package tests.simple.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Cronograma {
	private String fecha;
	private ListadoTurnos listadoTurnos;
	private ProfesionalLite profesional;

	public String getFecha() {
		return fecha;
	}

	@XmlElement
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public ListadoTurnos getListadoTurnos() {
		return listadoTurnos;
	}

	@XmlElement
	public void setListadoTurnos(ListadoTurnos listadoTurnos) {
		this.listadoTurnos = listadoTurnos;
	}

	public ProfesionalLite getProfesional() {
		return profesional;
	}

	@XmlElement
	public void setProfesional(ProfesionalLite profesional) {
		this.profesional = profesional;
	}

}
