package tests.simple;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

@FXMLView(value = "/TestView.fxml", encoding = "UTF-8")
public class TestView extends AbstractFxmlView {
	
}
