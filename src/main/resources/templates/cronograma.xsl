<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:template match="/">
		<html>
			<head>
				<style type="text/css">
					.tabla {
					font-size: 11px;
					border-style: none;
					}

					.celda {
					padding: 2;
					border-bottom: 1px solid gray;
					}
					.estado {
					text-align: center;
					}
					.libre {
					background-color:
					lightblue;
					}

					.reservado {
					background-color: Orange;
					}

					.asistido {
					background-color: Tomato;
					}

					.cerrado {
					background-color:
					MediumSeaGreen;
					}
				</style>
			</head>
			<body>
				<xsl:variable name="conteo"
					select="count(cronograma/listadoTurnos/turno)" />
				<h1>
					<xsl:value-of select="cronograma/fecha" />
				</h1>
				<h2>
					<xsl:value-of select="cronograma/profesional/nombre" />
					(
					<xsl:value-of select="cronograma/profesional/usuario" />
					)
				</h2>
				<table>
					<tr>
						<td>
							<table class="tabla">
								<tr>
									<th>Hora</th>
									<th>Paciente</th>
									<th>Estado</th>
								</tr>
								<xsl:for-each
									select="cronograma/listadoTurnos/turno[position() &lt; ceiling($conteo div 2)]">
									<tr>
										<!-- LIBRE(1), RESERVADO(2), CONSUMIDO(3), CERRADO(4) -->
										<xsl:choose>
											<xsl:when test="estado = '2'">
												<xsl:attribute name="class">reservado
							</xsl:attribute>
											</xsl:when>
											<xsl:when test="estado = '3'">
												<xsl:attribute name="class">asistido
							</xsl:attribute>
											</xsl:when>
											<xsl:when test="estado = '4'">
												<xsl:attribute name="class">cerrado
							</xsl:attribute>
											</xsl:when>
											<xsl:otherwise>
												<xsl:attribute name="class">libre
							</xsl:attribute>
											</xsl:otherwise>
										</xsl:choose>
										<td class="celda">
											<xsl:value-of select="horario/hora" />
										</td>
										<td class="celda">
											<xsl:choose>
												<xsl:when test="paciente/apellido!=''">
													<xsl:value-of select="paciente/apellido" />
													,
													<xsl:value-of select="paciente/nombre" />
													(
													<xsl:value-of select="paciente/documento" />
													)
												</xsl:when>
												<xsl:otherwise>
													-
													-
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<xsl:choose>
											<xsl:when test="estado = '2'">
												<td class="estado">Reservado</td>
											</xsl:when>
											<xsl:when test="estado = '3'">
												<td class="estado">Asistido</td>
											</xsl:when>
											<xsl:when test="estado = '4'">
												<td class="estado">Cerrado</td>
											</xsl:when>
											<xsl:otherwise>
												<td class="estado">Libre</td>
											</xsl:otherwise>
										</xsl:choose>
									</tr>
								</xsl:for-each>
							</table>
						</td>
						<td>
							<table class="tabla">
								<tr>
									<th>Hora</th>
									<th>Paciente</th>
									<th>Estado</th>
								</tr>
								<xsl:for-each
									select="cronograma/listadoTurnos/turno[position() > ceiling($conteo div 2)]">
									<tr>
										<!-- LIBRE(1), RESERVADO(2), CONSUMIDO(3), CERRADO(4) -->
										<xsl:choose>
											<xsl:when test="estado = '2'">
												<xsl:attribute name="class">reservado
							</xsl:attribute>
											</xsl:when>
											<xsl:when test="estado = '3'">
												<xsl:attribute name="class">asistido
							</xsl:attribute>
											</xsl:when>
											<xsl:when test="estado = '4'">
												<xsl:attribute name="class">cerrado
							</xsl:attribute>
											</xsl:when>
											<xsl:otherwise>
												<xsl:attribute name="class">libre
							</xsl:attribute>
											</xsl:otherwise>
										</xsl:choose>
										<td class="celda">
											<xsl:value-of select="horario/hora" />
										</td>
										<td class="celda">
											<xsl:choose>
												<xsl:when test="paciente/apellido!=''">
													<xsl:value-of select="paciente/apellido" />
													,
													<xsl:value-of select="paciente/nombre" />
													(
													<xsl:value-of select="paciente/documento" />
													)
												</xsl:when>
												<xsl:otherwise>
													-
													-
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<xsl:choose>
											<xsl:when test="estado = '2'">
												<td class="estado">Reservado</td>
											</xsl:when>
											<xsl:when test="estado = '3'">
												<td class="estado">Asistido</td>
											</xsl:when>
											<xsl:when test="estado = '4'">
												<td class="estado">Cerrado</td>
											</xsl:when>
											<xsl:otherwise>
												<td class="estado">Libre</td>
											</xsl:otherwise>
										</xsl:choose>
									</tr>
								</xsl:for-each>
							</table>
						</td>
					</tr>
				</table>
				<hr />
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>