package com.aplicacion.spring.gestion.controladores;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.Ventanas;
import com.aplicacion.spring.gestion.entidades.Especialidad;
import com.aplicacion.spring.gestion.entidades.Perfil;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.gestion.servicios.ServiciosUsuario;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.turnos.Cronograma;
import com.aplicacion.spring.negocio.entidades.turnos.Estado;
import com.aplicacion.spring.negocio.entidades.turnos.Horario;
import com.aplicacion.spring.negocio.entidades.turnos.ListadoTurnos;
import com.aplicacion.spring.negocio.entidades.turnos.PacienteLite;
import com.aplicacion.spring.negocio.entidades.turnos.ProfesionalLite;
import com.aplicacion.spring.negocio.entidades.turnos.Turno;
import com.aplicacion.spring.negocio.entidades.turnos.TurnoProfesional;
import com.aplicacion.spring.negocio.servicios.ServiciosPaciente;
import com.aplicacion.spring.negocio.servicios.ServiciosTurnos;
import com.aplicacion.spring.utilitarios.Constantes;
import com.aplicacion.spring.utilitarios.controladores.Conversion;
import com.aplicacion.spring.utilitarios.controladores.Util;
import com.aplicacion.spring.utilitarios.controladores.ejecutores.EjecutorBusquedaPacientes;
import com.aplicacion.spring.utilitarios.controladores.personalizacion.PseudoBrowser;
import com.aplicacion.spring.utilitarios.controladores.personalizacion.VBoxListaPacientes;

import de.felixroske.jfxsupport.FXMLController;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.StageStyle;

/**
 * @Autor Juan Ignacio Prez
 * @Version 1.0
 * @Fecha 04/07/17 // Sin definir aun
 * 
 * @Uso Controlador de FormularioTurnos.fxml Pantalla para control de la turnos
 */
@FXMLController
public class ControladorTurnos implements Initializable {

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	ServiciosPaciente serviciosPaciente;
	@Autowired
	ServiciosTurnos serviciosTurnos;
	@Autowired
	ServiciosUsuario serviciosUsuario;

	@FXML
	private ScrollPane mainPanel;
	@FXML
	private VBox panelTurno;
	@FXML
	private VBox panelProfesionales;
	@FXML
	private VBox panelBusquedaPaciente;
	@FXML
	private RadioButton radioBuscarNombre;
	@FXML
	private RadioButton radioBuscarDNI;
	@FXML
	private ToggleGroup seleccionBusqueda;
	@FXML
	private TextField campoBuscarPaciente;
	@FXML
	private DatePicker datePickerFecha;
	@FXML
	private Button botonVerTurnos;
	@FXML
	private Button botonImprimirTurnos;
	@FXML
	private ComboBox<String> comboBoxProfesionales;
	// @FXML
	// private TreeView<String> treeViewProfesionales;
	@FXML
	private ScrollPane scrollPaneBusqueda;
	@FXML
	private VBoxListaPacientes<Paciente> tablaPacientes;
	@FXML
	private VBox panelNuevoPaciente;
	@FXML
	private Button botonNuevoPaciente;
	@FXML
	private Button botonGuardarPaciente;
	@FXML
	private StackPane stackPaneBotones;
	@FXML
	private HBox hboxEditarPaciente;
	@FXML
	private TextField campoApellidoNuevoPaciente;
	@FXML
	private TextField campoNombreNuevoPaciente;
	@FXML
	private TextField campoDNINuevoPaciente;
	@FXML
	private HBox hboxTurnos;
	@FXML
	private Label etiquetaProfesional;

	EjecutorBusquedaPacientes ejecutorLista;
	// Carga de la sesion
	Usuario profesionalSeleccionado;
	Paciente pacienteSeleccionado;
	SimpleBooleanProperty editable = new SimpleBooleanProperty(false);
	SimpleBooleanProperty guardable = new SimpleBooleanProperty(false);
	private static final String SELECCIONADO = "vbox-paciente-seleccionado";
	private List<Usuario> listaProfesionales;
	private List<Perfil> listaPerfiles;
	private List<Especialidad> listaEspecialidades;
	List<Turno> listaTurnos;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		listaProfesionales = serviciosTurnos.obtenerProfesionales();
		listaPerfiles = serviciosTurnos.obtenerPerfiles();
		listaEspecialidades = serviciosTurnos.obtenerEspecialidades();

		// Preconfiguraciones iniciales
		// Configuracion de la tabla de pacientes
		configuracionTablaPacientes(tablaPacientes);
		configuracionBusquedaPacientes(radioBuscarNombre, seleccionBusqueda, campoBuscarPaciente);
		configuracionFechaActual();
		configuracionNuevoPaciente();

		// Permite guardar el paciente si los 3 campos tienen datos
		guardable.bind(campoApellidoNuevoPaciente.textProperty().isNotEmpty().and(campoNombreNuevoPaciente
				.textProperty().isNotEmpty().and(campoDNINuevoPaciente.textProperty().isNotEmpty())));

		Util.forzarCampoNumericoEnteroLimitado(campoDNINuevoPaciente, 10);

		construirComboBoxProfesionales();

	}

	private void construirComboBoxProfesionales() {
		comboBoxProfesionales.setItems(FXCollections.observableArrayList(getListaProfesionales()));
		if (Perfil.getAdministrativo() != aplicacion.getSesion().getUsuario().getPerfil().getId()) {
			comboBoxProfesionales.getSelectionModel().select(0);
			comboBoxProfesionales.setDisable(true);
		}
	}

	private List<String> getListaProfesionales() {
		if (Perfil.getAdministrativo() == aplicacion.getSesion().getUsuario().getPerfil().getId()) {
			return listaComboBox();
		} else {
			return Arrays.asList(aplicacion.getSesion().getUsuario().getNombre());
		}
	}

	private List<String> listaComboBox() {
		List<String> lista = new ArrayList<>();
		for (Perfil perfil : listaPerfiles) {
			int cp = 0;
			lista.add(String.format("- %s -", perfil.getDescripcion()));
			// Si el perfil es medico, evalua las especialidades
			if (perfil.getId() == Perfil.getMedicina()) {
				for (Especialidad especialidad : listaEspecialidades) {
					lista.add(String.format("-- %s --", especialidad.getDescripcion()));
					// Recorre la lista de profesiones y los evalue por especialidad
					int c = 0;
					for (Usuario profesional : listaProfesionales) {
						if (especialidad.getId() == profesional.getEspecialidad().getId()) {
							lista.add(profesional.getNombre());
							c++;
						}
					}
					// Quita las especialidades sin profesionales
					if (c == 0) {
						lista.remove(String.format("-- %s --", especialidad.getDescripcion()));
					}
				}
			} else {
				// Recorre la lista de profesiones y los evalue por perfil
				for (Usuario prof : listaProfesionales) {
					if (perfil.getId() == prof.getPerfil().getId()) {
						lista.add(prof.getNombre());
					}
				}
			}

			if (cp == 0) {
				lista.remove(String.format("- %s -", perfil.getDescripcion()));
			}
		}
		return lista;
	}

	/**
	 * Disparador de la actualizacion de la vista de los turnos
	 * 
	 * @param event
	 */
	@FXML
	private void verTurnos(ActionEvent event) {
		hboxTurnos.getChildren().clear();
		// Crea la lista de horarios vacia
		listaTurnos = crearListaTurnos(datePickerFecha.getValue());

		profesionalSeleccionado = obtenerProfesional();

		if (profesionalSeleccionado != null) {
			listaTurnos = obtenerTurnos(listaTurnos, datePickerFecha.getValue(), profesionalSeleccionado);
			// Construye el panel principal de turnos
			construirPanelTurnos(listaTurnos, hboxTurnos);
			etiquetaProfesional.setText(profesionalSeleccionado.getNombre());

			if (listaTurnos != null && !listaTurnos.isEmpty())
				Mensajero.setMsg("Obtenidos los turnos", Mensajero.MSG_EXITO);
		}
	}

	@FXML
	private void imprimirTurnos(ActionEvent event) {
		List<TurnoProfesional> turnosProfesional = new ArrayList<>();

		if (profesionalSeleccionado != null) {
			listaTurnos = obtenerTurnos(listaTurnos, datePickerFecha.getValue(), profesionalSeleccionado);
			for (Turno turno : obtenerTurnos(crearListaTurnos(datePickerFecha.getValue()), datePickerFecha.getValue(),
					profesionalSeleccionado)) {
				TurnoProfesional turnoProfesional = new TurnoProfesional();
				turnoProfesional.setHorario(turno.getHorario());
				turnoProfesional.setEstado(turno.getEstado());
				turnoProfesional.setPaciente(new PacienteLite(turno.getPaciente()));
				turnoProfesional.setObservaciones(turno.getObservaciones());
				turnosProfesional.add(turnoProfesional);
			}

			Cronograma cronograma = new Cronograma();
			cronograma.setProfesional(new ProfesionalLite(obtenerProfesional()));
			cronograma.setFecha(datePickerFecha.getValue().format(Constantes.FECHA_DIA_DD_MM_YY));
			cronograma.setListadoTurnos(new ListadoTurnos(turnosProfesional));

			Conversion.formarXML(cronograma, Cronograma.XML_CRONOGRAMA);
			String htmlContent = Conversion.convertirXMLaHTML(Cronograma.XML_CRONOGRAMA, Conversion.XSLT_CRONOGRAMA);

			Button botonImprimir = new Button("Imprimir");

			// Data
			botonImprimir.setUserData("Imprimir");

			PseudoBrowser visor = new PseudoBrowser(htmlContent, botonImprimir);

			Dialog<ButtonType> cuadroImpresion = Ventanas.crearDialogo(StageStyle.UTILITY, "Impresion de Cronograma",
					visor, ButtonType.CLOSE);

			cuadroImpresion.showAndWait();
		}

	}

	/**
	 * Obtiene la lista de turnos para cada dia en base a los horarios
	 * 
	 * @param listaTurnos
	 * @return
	 */
	private List<Turno> crearListaTurnos(LocalDate fecha) {
		List<Turno> lista = new ArrayList<>();
		for (Horario horario : aplicacion.getRepositorios().getListaHorarios()) {
			lista.add(new Turno(fecha, horario, Estado.LIBRE, new Usuario(), new Paciente(), null));
		}
		return lista;
	}

	/**
	 * Recupera los turnos de la base de datos, y si existe algun turno para ese
	 * horario, reemplaza el turno vacio por el recuperado
	 * 
	 * @param listaTurnos
	 * @param fecha
	 * @param profesional
	 * @return lista de Turnos actualizada con los encontrados de la base de datos.
	 */
	private List<Turno> obtenerTurnos(List<Turno> listaTurnos, LocalDate fecha, Usuario profesional) {
		for (Turno turnoRecuperado : serviciosTurnos.obtenerTurnos(profesional.getUid(), fecha)) {
			for (Turno turnoVacio : listaTurnos) {
				if (turnoRecuperado.getHorario().getId() == turnoVacio.getHorario().getId()) {
					listaTurnos.set(listaTurnos.indexOf(turnoVacio), turnoRecuperado);
				}
			}
		}
		return listaTurnos;
	}

	/**
	 * Configura el valor de la fecha por defecto
	 */
	private void configuracionFechaActual() {
		// Configura el valor por defecto del datepicker si es que no lo tiene
		if (datePickerFecha.getValue() == null) {
			datePickerFecha.setValue(LocalDate.now());
		}
		ImageView refresh = new ImageView(new Image(aplicacion.getPropiedades().get("imagen.refrescar")));
		botonVerTurnos.setGraphic(refresh);
		refresh.setFitWidth(16);
		refresh.setFitHeight(16);
		botonVerTurnos.setContentDisplay(ContentDisplay.LEFT);
	}

	/**
	 * Construye el panel completo de turnos
	 * 
	 * @param horarios
	 * @param hboxTurnos
	 */
	private void construirPanelTurnos(List<Turno> horarios, HBox hboxTurnos) {
		VBox vboxSubLista = new VBox();
		for (int index = 0; index < horarios.size(); index++) {

			Turno turno = horarios.get(index);

			// Separa los items en 2 grupos de 27
			if (index % 27 == 0 && index > 0) {
				vboxSubLista.getStyleClass().add(Util.ESTILO_BORDE_DERECHO);
				vboxSubLista.prefWidthProperty().bind(panelTurno.widthProperty().multiply(0.48));

				hboxTurnos.getChildren().addAll(vboxSubLista);
				vboxSubLista = new VBox();
				vboxSubLista.prefWidthProperty().bind(panelTurno.widthProperty().multiply(0.48));
			}

			HBox hboxLista = new HBox();
			hboxLista.setAlignment(Pos.BASELINE_LEFT);
			hboxLista.getStyleClass().add("borde-inferior-suave");
			hboxLista.setSpacing(10);
			hboxLista.setUserData(turno);

			// Etiqueta del horario
			Label etiquetaHorario = new Label(turno.getHorario().getHora());
			String nombrePaciente;
			if (turno.getPaciente() != null && !turno.getPaciente().getApellido().isEmpty()
					&& !turno.getPaciente().getNombre().isEmpty()) {
				nombrePaciente = turno.getPaciente().getApellido().toUpperCase() + ", "
						+ turno.getPaciente().getNombre().toUpperCase();
			} else {
				nombrePaciente = "";
			}

			// Hipervinculo con el nombre del paciente
			Hyperlink vinculoPacienteTurno = new Hyperlink("Paciente");

			vinculoPacienteTurno.textProperty().addListener((o, p, textoActual) -> {
				if (textoActual != null && !textoActual.isEmpty()) {
					vinculoPacienteTurno.setVisible(true);
				} else {
					vinculoPacienteTurno.setVisible(false);
				}
			});
			vinculoPacienteTurno.setText(nombrePaciente);
			vinculoPacienteTurno
					.setOnAction((e) -> accederTurno((Turno) vinculoPacienteTurno.getParent().getUserData()));

			// Boton de reserva
			Button botonReservarTurno = new Button("Reservar");
			botonReservarTurno.getStyleClass().addAll("boton", "botonVerde", "botonMiniatura");

			ImageView imagenBotonReserva = new ImageView(
					new Image(aplicacion.getPropiedades().get("imagen.turno_reservar")));
			imagenBotonReserva.setFitWidth(16);
			imagenBotonReserva.setFitHeight(16);
			botonReservarTurno.setGraphic(imagenBotonReserva);
			botonReservarTurno.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

			botonReservarTurno.setOnAction((e) -> {

				if (pacienteSeleccionado == null) {
					Mensajero.setMsg("Seleccione un paciente de la lista o añada uno nuevo", Mensajero.MSG_INFO);
					return;
				}
				Usuario profesional = profesionalSeleccionado;
				if (profesional == null) {
					Mensajero.setMsg("Seleccione un profesional primero", Mensajero.MSG_ALERTA);
					return;
				}
				if (reservarTurno((Turno) botonReservarTurno.getParent().getUserData(), pacienteSeleccionado,
						profesional)) {
					vinculoPacienteTurno.setText(turno.getPaciente().getApellido().toUpperCase() + ", "
							+ turno.getPaciente().getNombre().toUpperCase());
				}
			});

			// Boton de consumición
			Button botonConsumirTurno = new Button("Consumir");
			botonConsumirTurno.getStyleClass().addAll("boton", "botonAmarillo", "botonMiniatura");

			ImageView imagenBotonConsume = new ImageView(
					new Image(aplicacion.getPropiedades().get("imagen.turno_consumir")));
			imagenBotonConsume.setFitWidth(16);
			imagenBotonConsume.setFitHeight(16);
			botonConsumirTurno.setGraphic(imagenBotonConsume);
			botonConsumirTurno.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

			botonConsumirTurno.setOnAction((e) -> consumirTurno((Turno) botonConsumirTurno.getParent().getUserData()));
			// Boton de cancelacion
			Button botonCancelarTurno = new Button("Cancelar");
			botonCancelarTurno.getStyleClass().addAll("boton", "botonRojo", "botonMiniatura");

			ImageView imagenBotonCancela = new ImageView(
					new Image(aplicacion.getPropiedades().get("imagen.turno_cancelar")));
			imagenBotonCancela.setFitWidth(16);
			imagenBotonCancela.setFitHeight(16);
			botonCancelarTurno.setGraphic(imagenBotonCancela);
			botonCancelarTurno.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

			botonCancelarTurno.setOnAction((e) -> {
				if (cancelarTurno((Turno) botonCancelarTurno.getParent().getUserData())) {
					vinculoPacienteTurno.setText("");
				}
			});
			hboxLista.getChildren().addAll(botonReservarTurno, botonConsumirTurno, botonCancelarTurno, etiquetaHorario,
					vinculoPacienteTurno);
			vboxSubLista.getChildren().add(hboxLista);

			// Estado inicial del turno
			Estado status = ((Turno) hboxLista.getUserData()).getEstado();
			if (status == Estado.LIBRE) {
				cambiosTurno(hboxLista, botonReservarTurno, botonConsumirTurno, botonCancelarTurno,
						vinculoPacienteTurno, "turnoLibre", true, false, false, false, false);
			} else if (status == Estado.RESERVADO) {
				cambiosTurno(hboxLista, botonReservarTurno, botonConsumirTurno, botonCancelarTurno,
						vinculoPacienteTurno, "turnoReservado", false, true, true, false, false);
			} else if (status == Estado.CONSUMIDO) {
				cambiosTurno(hboxLista, botonReservarTurno, botonConsumirTurno, botonCancelarTurno,
						vinculoPacienteTurno, "turnoReservado", false, true, false, true, false);
			} else if (status == Estado.CERRADO) {
				cambiosTurno(hboxLista, botonReservarTurno, botonConsumirTurno, botonCancelarTurno,
						vinculoPacienteTurno, "turnoTerminado", false, false, false, false, true);
			}

			// Disparador del estado del turno
			((Turno) hboxLista.getUserData()).estadoProperty().addListener((o, p, estadoActual) -> {
				if (estadoActual == Estado.LIBRE) {
					cambiosTurno(hboxLista, botonReservarTurno, botonConsumirTurno, botonCancelarTurno,
							vinculoPacienteTurno, "turnoLibre", true, false, false, false, false);
				} else if (estadoActual == Estado.RESERVADO) {
					cambiosTurno(hboxLista, botonReservarTurno, botonConsumirTurno, botonCancelarTurno,
							vinculoPacienteTurno, "turnoReservado", false, true, true, false, false);
				} else if (estadoActual == Estado.CONSUMIDO) {
					cambiosTurno(hboxLista, botonReservarTurno, botonConsumirTurno, botonCancelarTurno,
							vinculoPacienteTurno, "turnoReservado", false, true, false, true, false);
				} else if (estadoActual == Estado.CERRADO) {
					cambiosTurno(hboxLista, botonReservarTurno, botonConsumirTurno, botonCancelarTurno,
							vinculoPacienteTurno, "turnoTerminado", false, false, false, false, true);
				}
			});
		}
		vboxSubLista.getStyleClass().add(Util.ESTILO_BORDE_DERECHO);

		// Añade el panel vertical al Hbox de turnos
		hboxTurnos.getChildren().add(vboxSubLista);
	}

	/**
	 * Cambia el estilo y los botones a mostrar en el panel de turno
	 * 
	 * @param hbox
	 * @param reservar
	 * @param consumir
	 * @param cancelar
	 * @param vinculo
	 * @param estilo
	 * @param reservaVisible
	 * @param cancelaVisible
	 * @param consumeVisible
	 * @param consumirTurno
	 * @param inhabilitarVinculo
	 */
	private void cambiosTurno(HBox hbox, Button reservar, Button consumir, Button cancelar, Hyperlink vinculo,
			String estilo, boolean reservaVisible, boolean cancelaVisible, boolean consumeVisible,
			boolean consumirTurno, boolean inhabilitarVinculo) {
		hbox.getStyleClass().clear();
		hbox.getStyleClass().add(estilo);
		reservar.setVisible(reservaVisible);
		consumir.setVisible(consumeVisible);
		cancelar.setVisible(cancelaVisible);

		if (consumirTurno) {
			vinculo.getStyleClass().clear();
			vinculo.getStyleClass().add("vinculo-Turno-Consumido");
		} else {
			vinculo.getStyleClass().clear();
			vinculo.getStyleClass().add("vinculo-Turno-No-Consumido");
		}
		// En caso de cierre
		if (inhabilitarVinculo) {
			vinculo.setDisable(inhabilitarVinculo);
			vinculo.setStyle("-fx-text-fill: gray;");
		}
	}

	/**
	 * Ata la propiedad a la tabla de pacientes
	 */
	private void configuracionNuevoPaciente() {
		campoApellidoNuevoPaciente.disableProperty().bind(editable.not());
		campoNombreNuevoPaciente.disableProperty().bind(editable.not());
		campoDNINuevoPaciente.disableProperty().bind(editable.not());

		botonNuevoPaciente.visibleProperty().bind(editable.not());
		hboxEditarPaciente.visibleProperty().bind(editable);

		// TODO
		botonGuardarPaciente.disableProperty().bind(guardable.not());
	}

	/**
	 * Establece los parametros iniciales y el listener para el selector y el campo
	 * de busqueda
	 * 
	 * @param radio,
	 *            seleccionado inicialmente
	 * @param seleccion,
	 *            ToggleGroup a evaluar
	 * @param campo,
	 *            TextField a manipular
	 * 
	 */
	private void configuracionBusquedaPacientes(RadioButton radio, ToggleGroup seleccion, TextField campo) {
		radioBuscarDNI.setToggleGroup(seleccionBusqueda);
		radioBuscarNombre.setToggleGroup(seleccionBusqueda);

		radio.setSelected(true);
		campo.setPromptText("Apellido, Nombre");

		seleccion.selectedToggleProperty().addListener((o, p, a) -> {
			if (a.toString().contains("radioBuscarDNI")) {
				campo.setPromptText("12345678");
			} else if (a.toString().contains("radioBuscarNombre")) {
				campo.setPromptText("Apellido, Nombre");
			}
			campo.requestFocus();
		});
	}

	/**
	 * Configuracion inicial de la tabla Pacientes
	 * 
	 * @param tablaPacientes
	 * 
	 */
	private void configuracionTablaPacientes(VBoxListaPacientes<Paciente> tablaPacientes) {
		tablaPacientes.disableProperty().bind(editable);
		tablaPacientes.setImagenIzquierda(new Image(aplicacion.getPropiedades().get("imagen.persona")));
		tablaPacientes.getStyleClass().add("lista");
		tablaPacientes.getChildren().addListener((ListChangeListener<? super Node>) cambio -> {
			cambio.next();
			for (Node boton : cambio.getAddedSubList()) {
				if (boton != null && boton instanceof Button)
					((Button) boton).setOnAction(e -> {
						for (Node node : tablaPacientes.getChildren())
							node.getStyleClass().remove(SELECCIONADO);
						boton.getStyleClass().add(SELECCIONADO);
						seleccionarPaciente((Paciente) boton.getUserData());
					});
			}
		});
	}

	/**
	 * Seleccionado el paciente
	 * 
	 * @param Paciente
	 */
	private void seleccionarPaciente(Paciente paciente) {
		pacienteSeleccionado = paciente;
	}

	/**
	 * Valida que los datos sean los minimos requeridos
	 * 
	 * @param apellido
	 * @param nombre
	 * @param dni
	 * @return
	 */
	private boolean validarDatosPaciente(String apellido, String nombre, String dni) {
		// Valida nulos
		if (apellido == null || nombre == null || dni == null) {
			return false;
		}
		// Valida vacios
		if (apellido.isEmpty() || nombre.isEmpty() || dni.isEmpty()) {
			return false;
		}
		// Valida datos
		if (apellido.length() < 2 || apellido.length() < 2 || dni.length() < 6) {
			return false;
		}
		return true;
	}

	/**
	 * Accion de buscar paciente
	 * 
	 * @param event
	 */
	@FXML
	void buscarPaciente(ActionEvent event) {
		if (radioBuscarNombre.isSelected()) {
			buscarPacienteNombre();
		} else if (radioBuscarDNI.isSelected()) {
			buscarPacienteDNI();
		}
	}

	/**
	 * Buscar paciente por nombre
	 */
	private void buscarPacienteNombre() {
		if (campoBuscarPaciente.getText().trim().length() >= 2) {
			buscarPorNombre(campoBuscarPaciente);
		} else {
			Mensajero.setMsg("Ingrese al menos 3 caracteres para iniciar la busqueda", Mensajero.MSG_INFO);
		}
	}

	/**
	 * Fragmenta la cadena en apellido y nombre, y la usa para buscar los pacientes
	 * 
	 * @param campoBusqueda
	 * @return
	 */
	private void buscarPorNombre(TextField campoBusqueda) {
		limpiarListaPacientes();
		// Comienzo del servicio
		String nombreApellido;
		String nombre;
		String apellido;

		List<String> listaBusqueda = new ArrayList<>();

		if (campoBusqueda.getText().trim().contains(",")) {
			nombreApellido = campoBusqueda.getText().trim();
		} else {
			nombreApellido = campoBusqueda.getText().trim().replaceFirst(" ", ",");
		}

		// Convierte el array en lista
		for (String s : Arrays.asList(nombreApellido.split(",")))
			listaBusqueda.add(s);

		// Verifica si la lista esta vacia
		if (!listaBusqueda.isEmpty()) {
			// Si hay menos de dos parametros, continua
			if (listaBusqueda.size() <= 2) {
				apellido = listaBusqueda.get(0).trim();
				// Si solo hay un parametro, creamos un segundo vacio
				if (listaBusqueda.size() == 1) {
					nombre = "";
				} else {
					nombre = listaBusqueda.get(1).trim();
				}

				ejecutorLista = new EjecutorBusquedaPacientes(serviciosPaciente, aplicacion.getPanelCarga(),
						aplicacion.getPanelFrontal(), "nombre", Arrays.asList(apellido, nombre));

				ejecutorLista.setOnSucceeded(e -> manejarResultadoListaPaciente(ejecutorLista.getValue()));

				ejecutorLista.setOnFailed(
						e -> Mensajero.setMsg(ejecutorLista.getException().getMessage(), Mensajero.MSG_ALERTA));

				ejecutorLista.correrServicio();

				// Obtiene el valor de la ejecucion del servicio
				// return serviciosPaciente.obtenerPacientesPorNombre(apellido, nombre);
			} else {
				Mensajero.setMsg("Demasiados parametros: Solo use apellido y nombre, separados por una coma",
						Mensajero.MSG_ALERTA);
			}
		} else {
			Mensajero.setMsg("No hay nada en el campo de busqueda", Mensajero.MSG_ALERTA);
		}
	}

	/**
	 * 
	 */
	private void manejarResultadoListaPaciente(List<Paciente> listPacientes) {
		if (!listPacientes.isEmpty()) {
			// Se muestra el contenido
			cargarListaPacientes(listPacientes);
			// Si no se encuentran valores
			Mensajero.setMsg("Encontrados " + listPacientes.size() + " pacientes", Mensajero.MSG_EXITO);
		} else {
			Mensajero.setMsg("No se encontraron pacientes", Mensajero.MSG_INFO);
		}
		limpiarCampoBusquedaPaciente();
	}

	/**
	 * Carga de la lista de pacientes en el contendedor
	 * 
	 * @param lista
	 */
	private void cargarListaPacientes(List<Paciente> lista) {
		// En caso de lista nula
		if (lista == null) {
			lista = FXCollections.observableArrayList();
			Mensajero.setMsg("Problemas al recuperar la lista de pacientes", Mensajero.MSG_ALERTA);
		} else {
			// Reinicia el contenedor
			limpiarListaPacientes();
			// Carga la lista
			for (Paciente p : lista) {
				tablaPacientes.sumarElemento(p);
			}
			// Carga los mensajes
			if (lista.size() == 0) {
				Mensajero.setMsg("No se hallaron pacientes", Mensajero.MSG_INFO);
			} else {
				if (lista.size() == 1) {
					seleccionarPaciente(lista.get(0));
					Mensajero.setMsg("Hallado 1 paciente", Mensajero.MSG_EXITO);
				} else {
					Mensajero.setMsg(String.format("Hallados %s pacientes", lista.size()), Mensajero.MSG_EXITO);
				}
			}
		}
	}

	/**
	 * Limpia los campos de busqueda
	 */
	private void limpiarCampoBusquedaPaciente() {
		campoBuscarPaciente.clear();
		campoBuscarPaciente.requestFocus();
	}

	/**
	 * Limpiador de la lista de pacientes
	 */
	private void limpiarListaPacientes() {
		// Limpia la lista de pacientes encontrados
		tablaPacientes.limpiarElementos();
	}

	/**
	 * Metodo para la busqueda de pacientes por DNI, no puede devolver mas de 1
	 * valor por busqueda
	 */
	private Paciente buscarPacienteDNI() {
		Paciente paciente = null;
		if (campoBuscarPaciente.getText().length() > 6) {

			ejecutorLista = new EjecutorBusquedaPacientes(serviciosPaciente, aplicacion.getPanelCarga(),
					aplicacion.getPanelFrontal(), "dni", Arrays.asList(campoBuscarPaciente.getText()));

			ejecutorLista.setOnSucceeded(e -> {
				cargarListaPacientes(ejecutorLista.getValue());
			});

			ejecutorLista.setOnFailed(e -> {
				Mensajero.setMsg(ejecutorLista.getException().getMessage(), Mensajero.MSG_ALERTA);
			});

			ejecutorLista.correrServicio();

		} else {
			Mensajero.setMsg("Ingrese al menos 6 caracteres para iniciar la busqueda", Mensajero.MSG_INFO);
		}
		return paciente;
	}

	/**
	 * Construye el arbol para un solo profesional
	 * 
	 * @param profesional
	 * @return
	 */
	// private TreeItem<String> construirArbolProfesional(Usuario profesional) {
	// TreeItem<String> raizArbolProfesionales = new TreeItem<>();
	// // Construye el arbol de seleccion de profesionales
	// TreeItem<String> hojaProfesional = new
	// TreeItem<String>(profesional.getNombre());
	// raizArbolProfesionales.getChildren().add(hojaProfesional);
	// raizArbolProfesionales.setExpanded(true);
	// treeViewProfesionales.setRoot(raizArbolProfesionales);
	// treeViewProfesionales.setShowRoot(false);
	//
	// // Lo dejamos seleccionado
	// Platform.runLater(() -> {
	// treeViewProfesionales.getSelectionModel().select(hojaProfesional);
	// });
	// return raizArbolProfesionales;
	// }

	/**
	 * Construye la lista de profesionales
	 * 
	 * @param Lista
	 *            especialidades
	 * @param Lista
	 *            perfiles
	 * @param Lista
	 *            profesionales
	 * @return Rama de Perfiles - > Especialidades -> Profesionales
	 * 
	 */
	private TreeItem<String> construirArbolProfesionales(List<Usuario> profesionales, List<Perfil> perfiles,
			List<Especialidad> especialidades) {
		TreeItem<String> raizArbolProfesionales = new TreeItem<>();
		// Recorremos las especialidades validas
		TreeItem<String> subRamaPerfil;
		// Examina la lista de perfiles
		for (Perfil perfil : listaPerfiles) {
			subRamaPerfil = new TreeItem<>(perfil.getDescripcion());
			// Si el perfil es medico, evalua las especialidades
			if (perfil.getId() == Perfil.getMedicina()) {
				TreeItem<String> subRamaEspecialidad;
				for (Especialidad especialidad : listaEspecialidades) {
					subRamaEspecialidad = new TreeItem<>(especialidad.getDescripcion());
					// Recorre la lista de profesiones y los evalue por especialidad
					for (Usuario profesional : listaProfesionales) {
						if (especialidad.getId() == profesional.getEspecialidad().getId()) {
							TreeItem<String> hojaProfesional = new TreeItem<>(profesional.getNombre());
							subRamaEspecialidad.getChildren().add(hojaProfesional);
						}
					}
					subRamaEspecialidad.setExpanded(true);
					subRamaPerfil.getChildren().add(subRamaEspecialidad);

					// Quita las especialidades sin profesionales
					if (subRamaEspecialidad.isLeaf()) {
						subRamaPerfil.getChildren().remove(subRamaEspecialidad);
					}
				}
			} else {
				// Recorre la lista de profesiones y los evalue por perfil
				for (Usuario prof : listaProfesionales) {
					if (perfil.getId() == prof.getPerfil().getId()) {
						TreeItem<String> hojaProfesional = new TreeItem<>(prof.getNombre());
						subRamaPerfil.getChildren().add(hojaProfesional);
					}
				}
			}
			subRamaPerfil.setExpanded(true);
			raizArbolProfesionales.getChildren().add(subRamaPerfil);

			// Quita los perfiles sin profesionales
			if (subRamaPerfil.isLeaf()) {
				raizArbolProfesionales.getChildren().remove(subRamaPerfil);
			}
		}

		// Finalmente dejamos la raiz expandida y la escondemos
		raizArbolProfesionales.setExpanded(true);
		return raizArbolProfesionales;

	}

	/**
	 * Obtiene el profesional seleccionado
	 * 
	 * @return
	 */
	// private Usuario obtenerProfesional() {
	// // Si es administrativo
	// if (Perfil.getAdministrativo() ==
	// aplicacion.getSesion().getUsuario().getPerfil().getId()) {
	// if (!treeViewProfesionales.getSelectionModel().isEmpty()) {
	// if (treeViewProfesionales.getSelectionModel().getSelectedItem().isLeaf()) {
	// return serviciosUsuario.buscarUsuarioNombre(
	// treeViewProfesionales.getSelectionModel().getSelectedItem().getValue());
	// } else {
	// Mensajero.setMsg("Seleccione un profesional primero", Mensajero.MSG_ALERTA);
	// return null;
	// }
	// } else {
	// Mensajero.setMsg("Seleccione un profesional primero", Mensajero.MSG_ALERTA);
	// return null;
	// }
	// // Si es profesional
	// } else {
	// return aplicacion.getSesion().getUsuario();
	// }
	// }

	/**
	 * Obtiene el profesional seleccionado
	 * 
	 * @return
	 */
	private Usuario obtenerProfesional() {
		String nombre = comboBoxProfesionales.getSelectionModel().getSelectedItem();

		Usuario u = listaProfesionales.stream().filter(p -> p.getNombre().equals(nombre)).findFirst().orElse(null);
		return u;
	}

	/**
	 * Accede al turno y lo conserva en sesion
	 * 
	 * @param turno
	 */
	private void accederTurno(Turno turno) {
		aplicacion.getSesion().setTurno(turno);
		aplicacion.getSesion().setPaciente(serviciosPaciente.obtenerPacientePorID(turno.getPaciente().getId()).get(0));
		aplicacion.getPantallaPrincipal().cambiarPantalla(ControladorDiabetesCenter.HC);
	}

	/**
	 * Cierra la consulta guardada en sesion
	 */
	public Turno cerrarTurno(Turno turno) {
		if (turno != null) {
			if (!serviciosTurnos.cerrar(turno))
				Mensajero.setMsg("No se pudo cerrar el turno");
		}
		return null;
	}

	/**
	 * Consume (confirma presencia) el turno
	 * 
	 * @param turno
	 * @return
	 *
	 */
	private boolean consumirTurno(Turno turno) {
		// Consumir (confirma la asistencia) del turno
		if (serviciosTurnos.consumir(turno)) {
			Mensajero.setMsg("Paciente confirmado presente para el turno", Mensajero.MSG_EXITO);
			return true;
		} else {
			Mensajero.setMsg("No se pudo consumir el turno", Mensajero.MSG_FALLO);
			return false;
		}
	}

	/**
	 * Cancela (libera) el turno
	 * 
	 * @param turno
	 * @return
	 */
	private boolean cancelarTurno(Turno turno) {
		// Crea el cuadro de confirmacion
		Alert confirmacion = Ventanas.crearAlerta(AlertType.CONFIRMATION, "Cancelar turno", null,
				"¿Desea cancelar el turno?", null);
		Optional<ButtonType> respuesta = confirmacion.showAndWait();

		if (respuesta.isPresent() && respuesta.get().equals(ButtonType.OK)) {
			// Cancela (Libera) el turno cuando se confirma la accion
			if (serviciosTurnos.liberar(turno)) {
				Mensajero.setMsg("El Turno se ha cancelado", Mensajero.MSG_INFO);
				return true;
			} else {
				Mensajero.setMsg("No se pudo cancelar el turno", Mensajero.MSG_FALLO);
				return false;
			}
		}
		return false;
	}

	/**
	 * Reserva el turno
	 * 
	 * @param turno
	 * @param paciente
	 * @param profesional
	 * @return
	 */
	private boolean reservarTurno(Turno turno, Paciente paciente, Usuario profesional) {
		// Anexa el paciente y el profesional al turno
		turno.setPaciente(paciente);
		turno.setProfesional(profesional);
		// Reserva el turno
		if (serviciosTurnos.reservar(turno, aplicacion.getSesion().getUsuario())) {
			Mensajero.setMsg("El Turno se ha reservado", Mensajero.MSG_INFO);
			return true;
		} else {
			Mensajero.setMsg("No se pudo reservar el turno", Mensajero.MSG_FALLO);
			return false;
		}
	}

	/**
	 * 
	 * 
	 * @param event
	 */
	@FXML
	void nuevoPaciente(ActionEvent event) {
		pacienteSeleccionado = null;
		editable.set(true);
	}

	@FXML
	private void guardarPaciente(ActionEvent event) {
		String a = campoApellidoNuevoPaciente.getText();
		String n = campoNombreNuevoPaciente.getText();
		String d = campoDNINuevoPaciente.getText();
		if (validarDatosPaciente(a, n, d)) {
			Paciente nuevo = serviciosPaciente.nuevoPaciente(a, n, d);
			if (nuevo != null && nuevo.getId() > 0) {
				cargarListaPacientes(Arrays.asList(nuevo));
				editable.set(false);
			}
		}
	}

	@FXML
	private void cancelarPaciente(ActionEvent event) {
		campoApellidoNuevoPaciente.clear();
		campoNombreNuevoPaciente.clear();
		campoDNINuevoPaciente.clear();
		editable.set(false);
	}
}
