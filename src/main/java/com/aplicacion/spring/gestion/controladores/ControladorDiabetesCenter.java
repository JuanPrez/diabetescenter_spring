/* ************************************************************************************* *
 *                                                                                       *
 *  Licensed to the Apache Software Foundation (ASF) under one                           *
 *  or more contributor license agreements.  See the NOTICE file                         *
 *  distributed with this work for additional information                                *
 *  regarding copyright ownership.  The ASF licenses this file                           *
 *  to you under the Apache License, Version 2.0 (the                                    *
 *  "License"); you may not use this file except in compliance                           *
 *  with the License.  You may obtain a copy of the License at                           *
 *                                                                                       *
 *    http://www.apache.org/licenses/LICENSE-2.0                                         *
 *                                                                                       *
 *  Unless required by applicable law or agreed to in writing,                           *
 *  software distributed under the License is distributed on an                          *
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY                               *
 *  KIND, either express or implied.  See the License for the                            *
 *  specific language governing permissions and limitations                              *
 *  under the License.                                                                   *
 *                                                                                       *
 * ************************************************************************************* */

package com.aplicacion.spring.gestion.controladores;

import java.net.URL;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.gestion.vistas.VistaAgenda;
import com.aplicacion.spring.gestion.vistas.VistaAjustes;
import com.aplicacion.spring.gestion.vistas.VistaHistClinica;
import com.aplicacion.spring.gestion.vistas.VistaTurnos;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.utilitarios.Conexion.Estado;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLController;
import de.felixroske.jfxsupport.GUIState;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * @Autor Juan Ignacio Prez
 * @Version 1.0
 * @Fecha 16/08/17
 * 
 * @Uso Controlador del formulario DiabetesCenter.fxml, Pantalla principal de la
 *      GUI
 */
@FXMLController
public class ControladorDiabetesCenter implements Initializable {

	// Cargamos la bitacora
	static final Logger bitacora = Logger.getLogger(ControladorDiabetesCenter.class);

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	VistaHistClinica vistaHistClinica;
	@Autowired
	VistaAgenda vistaAgenda;
	@Autowired
	VistaTurnos vistaTurnos;
	@Autowired
	VistaAjustes vistaAjustes;

	@FXML
	private StackPane panelPrincipal;
	@FXML
	private BorderPane panelFrontal;

	@FXML
	private HBox cuerpoCentral;
	@FXML
	private ImageView botonHCIcono;
	@FXML
	private ImageView botonTurnosIcono;
	@FXML
	private ImageView botonAgendaIcono;
	@FXML
	private ImageView botonAjustesIcono;
	@FXML
	private ImageView iconoConexion;

	@FXML
	public ToggleButton botonHC;
	@FXML
	public ToggleButton botonAgenda;
	@FXML
	public ToggleButton botonTurnos;
	@FXML
	public ToggleButton botonAjustes;

	// Etiquetas
	@FXML
	private Label etiquetaUsuario;
	@FXML
	private Button botonOpcionesUsuario;
	@FXML
	private ImageView botonOpcionesIcono;
	@FXML
	private ScrollPane cuerpoIntercambiable;

	// Cargador
	@FXML
	private VBox panelCarga;
	@FXML
	private ProgressIndicator indicadorCarga;
	@FXML
	private Label etiquetaCarga;

	// Creates the toggle menu
	ToggleGroup alternador = new ToggleGroup();

	public static final String HC = "HistoriaClinica";
	public static final String AGENDA = "Agenda";
	public static final String TURNOS = "Turnos";
	public static final String AJUSTES = "Ajustes";
	public static final String OPCIONES = "Opciones";

	/**
	 * Metodo para cambiar entre pantallas
	 * 
	 * @param pantalla
	 */
	public void cambiarPantalla(String pantalla) {
		switch (pantalla) {
		case HC: {
			botonHC.fire();
			break;
		}
		case AGENDA: {
			botonAgenda.fire();
			break;
		}
		case TURNOS: {
			botonTurnos.fire();
			break;
		}
		case AJUSTES: {
			botonAjustes.fire();
			break;
		}
		case OPCIONES: {
			botonOpcionesUsuario.fire();
			break;
		}
		default: {
			break;
		}
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		aplicacion.setPantallaPrincipal(this);
		Paciente.setImagenDefecto(new Image(aplicacion.getPropiedades().get("imagen.pacientePorDefecto")));

		configurarCargador(panelFrontal, panelCarga);

		// Configuracion inicial de la cabecera
		configuracionIconoMonitor();
		configuracionEtiquetaUsuario(aplicacion.getSesion().getUsuario());

		configuracionBotones();
		configurarAccionBoton(panelFrontal);

		// Cambiamos la regla de redimencionar la pantalla
		GUIState.getStage().setResizable(true);

		// Configuracion inicial del cuerpo
		cuerpoIntercambiable.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
		cuerpoIntercambiable.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		cargarPantallaInicio();

		// Modo de depuracion usando Scenic Viewer
		// if (Aplicacion.activarScenicViewer) {
		// Platform.runLater(() -> {
		// ScenicView.show(panelPrincipal);
		// });
		// }
	}

	/**
	 * 
	 * @param panelFrontal
	 * @param panelCarga
	 */
	private void configurarCargador(BorderPane panelFrontal, VBox panelCarga) {

		panelFrontal.opacityProperty().bind(new SimpleDoubleProperty(1).subtract(panelCarga.opacityProperty()));
		panelCarga.opacityProperty().addListener((o, p, a) -> {
			if (a.doubleValue() < 0.5) {
				panelCarga.setVisible(false);
			} else {
				panelCarga.setVisible(true);
			}
		});

		aplicacion.setPanelCarga(panelCarga);
		aplicacion.setPanelFrontal(panelFrontal);
	}

	/**
	 * Volver a la imagen de inicio
	 */
	private void cargarPantallaInicio() {
		ImageView imageView = new ImageView(new Image(aplicacion.getPropiedades().get("imagen.fondo")));
		imageView.setFitWidth(600);
		imageView.setOpacity(0.3);
		imageView.setPreserveRatio(true);

		BorderPane borderPane = new BorderPane();
		BorderPane.setAlignment(imageView, Pos.CENTER);
		borderPane.setCenter(imageView);

		borderPane.getStyleClass().add("imagenCentral");

		cuerpoIntercambiable.setContent(borderPane);
	}

	/**
	 * 
	 */
	private void configuracionEtiquetaUsuario(Usuario usuario) {
		etiquetaUsuario.setText(usuario.getProfesion().getAcronimo() + " " + usuario.getNombre());
		switch (aplicacion.getPropiedades().getAmbiente()) {
		case PRODUCCION: {
			etiquetaUsuario.getStyleClass().add("texto-negro");
			break;
		}
		case PRUEBA: {
			etiquetaUsuario.getStyleClass().add("texto-naranja");
			break;
		}
		case DESARROLLO: {
			etiquetaUsuario.getStyleClass().add("texto-verde");
			break;
		}
		default:
			break;
		}
	}

	/**
	 * Configura el icono que muestra el estaod de conexion
	 */
	private void configuracionIconoMonitor() {
		iconoConexion.setImage(new Image(aplicacion.getPropiedades().get("imagen.buenaConexion")));
		aplicacion.getMonitor().calidadConexionProperty().addListener((o, p, a) -> {
			if (a != null)
				if (a.equals(Estado.BUENA_CONEXION)) {
					iconoConexion.setImage(new Image(aplicacion.getPropiedades().get("imagen.buenaConexion")));
				} else if (a.equals(Estado.MALA_CONEXION)) {
					iconoConexion.setImage(new Image(aplicacion.getPropiedades().get("imagen.malaConexion")));
				} else {
					iconoConexion.setImage(new Image(aplicacion.getPropiedades().get("imagen.sinConexion")));
				}

		});
	}

	/**
	 * Cambia la vista
	 */
	private void configurarAccionBoton(BorderPane pane) {
		alternador.selectedToggleProperty().addListener((o, p, a) -> {
			if (a != null && a.getUserData() != null) {
				pane.setCenter(cargarVista(((AbstractFxmlView) a.getUserData())));
			}
		});
	}

	/**
	 * Configura los botones para
	 */
	private void configuracionBotones() {
		configurarBoton(botonHC, alternador, vistaHistClinica);
		configurarBoton(botonAgenda, alternador, vistaAgenda);
		configurarBoton(botonTurnos, alternador, vistaTurnos);
		configurarBoton(botonAjustes, alternador, vistaAjustes);

	}

	/**
	 * Configures the button, binds it to the toggle and adds the view to the button
	 * 
	 * @param boton
	 * @param alternador
	 * @param vista
	 */
	private void configurarBoton(ToggleButton boton, ToggleGroup alternador, AbstractFxmlView vista) {
		boton.setToggleGroup(alternador);
		boton.setOnAction(evento -> alternador.selectToggle(boton));
		boton.setUserData(vista);
	}

	/**
	 * Extrae la vista
	 * 
	 * @param vista
	 * @return Nodo
	 */
	private static Node cargarVista(AbstractFxmlView vista) {
		return vista.getView();
	}
}
