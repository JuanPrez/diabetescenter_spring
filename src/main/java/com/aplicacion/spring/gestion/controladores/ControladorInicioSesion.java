package com.aplicacion.spring.gestion.controladores;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Ventanas;
import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.excepciones.ExcepcionCampoFueraDePatron;
import com.aplicacion.spring.excepciones.ExcepcionCampoMuyCorto;
import com.aplicacion.spring.excepciones.ExcepcionCampoMuyLargo;
import com.aplicacion.spring.gestion.servicios.ServiciosUsuario;
import com.aplicacion.spring.gestion.vistas.VistaDiabetesCenter;
import com.aplicacion.spring.utilitarios.Constantes;
import com.aplicacion.spring.utilitarios.Validador;

import de.felixroske.jfxsupport.FXMLController;
import de.felixroske.jfxsupport.GUIState;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

@FXMLController
public class ControladorInicioSesion implements Initializable {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(ControladorInicioSesion.class);

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	ServiciosUsuario serviciosUsuario;
	@Autowired
	VistaDiabetesCenter vistaDiabetesCenter;

	@FXML
	private TextField textoUsuario;
	@FXML
	private PasswordField textoContrasena;
	@FXML
	private VBox panelIngreso;
	@FXML
	private Button botonIngreso;
	@FXML
	private Button botonRecuperar;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		GUIState.getStage().centerOnScreen();
		GUIState.getStage().setOnCloseRequest(event -> aplicacion.cerrarPrograma(event, GUIState.getStage()));

		botonIngreso.disableProperty().bind(textoUsuario.textProperty().isEmpty());
		botonRecuperar.disableProperty().bind(textoUsuario.textProperty().isEmpty());

		// Si existe sesion previa
		// Movemos el foco al campo de contraseña
		if (buscarSesionPrevia(textoUsuario))
			Platform.runLater(() -> textoContrasena.requestFocus());

	}

	/**
	 * Metodo para limpiar el formulario de ingreso
	 */
	private void limpiarFormulario() {
		// Limpiamos el formulario
		textoUsuario.clear();
		textoContrasena.clear();
	}

	/**
	 * Crea la pantalla principal
	 */
	public void mostrarPantallaPrincipal(Stage escenario) {

		Scene diabetesCenterScene = new Scene(vistaDiabetesCenter.getView());
		escenario.setScene(diabetesCenterScene);

		escenario.setMaximized(true);

	}

	/**
	 * Futura implementacion del metodo
	 */
	@FXML
	private void recuperarContrasena() {

		Dialog<String> dialogo = new TextInputDialog();
		dialogo.setHeaderText(String.format("Recuperacion de contraseña para %s", textoUsuario.getText()));
		dialogo.setContentText("Ingrese el correo electronico del usuario");

		Optional<String> respuesta = dialogo.showAndWait();

		if (respuesta.isPresent()) {
			serviciosUsuario.recuperarContrasena(textoUsuario.getText(), respuesta.get());
			Alert alerta = Ventanas.crearAlerta(AlertType.INFORMATION, "Recuperación en curso", respuesta.get(),
					"En breve recibira un correo en su correo con una contraseña temporal", null);
			alerta.show();
		}
	}

	/**
	 * Obtenemos la sesion previa del usuario
	 * 
	 * @param campoTexto
	 * @return
	 */
	private boolean buscarSesionPrevia(TextField campoTexto) {
		// Recuperamos el archivo de la sesion pasada
		// Si el usuario ya esta definido, cambiamos el foco a la contraseña
		String sPrev = null;
		sPrev = serviciosUsuario.recuperarUsuario();
		if (sPrev != null)
			campoTexto.setText(sPrev);
		return (textoUsuario.getText().length() != 0);
	}

	/**
	 * Proceso de verificacion de ingreso
	 */
	@FXML
	protected void verificarIngreso() {
		boolean userOk = false;
		boolean passOk = false;

		try {
			// Validacion del texto en el campo de usuario
			userOk = Validador.validarTextoContraPatronMinimoMaximo(textoUsuario.getText(), Constantes.PATRON_USUARIO,
					4, 15);
			// Validacion del texto en el campo de contraseña
			passOk = Validador.validarTextoContraPatronMinimoMaximo(textoContrasena.getText().trim(),
					Constantes.PATRON_CONTRA, 7, 20);

			// Pasada la validacion del controlador, pasa a la validacion en BD
			if (userOk && passOk) {
				String campoTextoUsuario = textoUsuario.getText();
				String campoTextoContr = textoContrasena.getText().trim();

				if (serviciosUsuario.verificarUsuario(campoTextoUsuario, campoTextoContr)) {
					// Guarda el nombre de usuario
					serviciosUsuario.salvarUsuario(aplicacion.getSesion().getUsuario());

					// Muestra la pantalla principal
					mostrarPantallaPrincipal((Stage) panelIngreso.getScene().getWindow());
				} else {
					// Borramos el archivo de sesion
					// s.borraArchivoSesion()
					bitacora.warn(String.format(Mensajero.USUARIO_FALLO_VERIFICACION.toString(), campoTextoUsuario));
				}
			} else {
				// En caso de que la validacion de patron falle
				Mensajero.setMsg(Mensajero.USUARIO_INVALIDO, Mensajero.MSG_ALERTA);
			}
		} catch (ExcepcionCampoFueraDePatron | ExcepcionCampoMuyCorto | ExcepcionCampoMuyLargo e) {
			Mensajero.setMsg(Mensajero.USUARIO_INVALIDO, Mensajero.MSG_ALERTA);
		}
		limpiarFormulario();
	}
}
