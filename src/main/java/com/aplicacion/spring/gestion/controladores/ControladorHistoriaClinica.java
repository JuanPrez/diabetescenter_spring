/* ************************************************************************************* *
 *                                                                                       *
 *  Licensed to the Apache Software Foundation (ASF) under one                           *
 *  or more contributor license agreements.  See the NOTICE file                         *
 *  distributed with this work for additional information                                *
 *  regarding copyright ownership.  The ASF licenses this file                           *
 *  to you under the Apache License, Version 2.0 (the                                    *
 *  "License"); you may not use this file except in compliance                           *
 *  with the License.  You may obtain a copy of the License at                           *
 *                                                                                       *
 *    http://www.apache.org/licenses/LICENSE-2.0                                         *
 *                                                                                       *
 *  Unless required by applicable law or agreed to in writing,                           *
 *  software distributed under the License is distributed on an                          *
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY                               *
 *  KIND, either express or implied.  See the License for the                            *
 *  specific language governing permissions and limitations                              *
 *  under the License.                                                                   *
 *                                                                                       *
 * ************************************************************************************* */

package com.aplicacion.spring.gestion.controladores;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.consultas.Consulta;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaMedicina;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaNutricion;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaPodologica;
import com.aplicacion.spring.negocio.vistas.VistaAntecedentesMedicina;
import com.aplicacion.spring.negocio.vistas.VistaAntecedentesNutricion;
import com.aplicacion.spring.negocio.vistas.VistaAntecedentesPodologia;
import com.aplicacion.spring.negocio.vistas.VistaArchivos;
import com.aplicacion.spring.negocio.vistas.VistaConsultaMedicina;
import com.aplicacion.spring.negocio.vistas.VistaConsultaNutricion;
import com.aplicacion.spring.negocio.vistas.VistaConsultaPodologia;
import com.aplicacion.spring.negocio.vistas.VistaEstadisticas;
import com.aplicacion.spring.negocio.vistas.VistaEstimulos;
import com.aplicacion.spring.negocio.vistas.VistaImagenes;
import com.aplicacion.spring.negocio.vistas.VistaPaciente;
import com.aplicacion.spring.negocio.vistas.VistaPatologia;
import com.aplicacion.spring.utilitarios.controladores.Util;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLController;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @Autor Juan Ignacio Prez
 * @Version 1.0
 * @Fecha 04/09/17
 * 
 * @Uso Controlador de la Pantalla de Historia Clinica
 */
@FXMLController
public class ControladorHistoriaClinica implements Initializable {

	private static final String PACIENTE = "Datos Personales";
	private static final String MEDICINA = "Medicina";
	private static final String NUTRICION = "Nutricion";
	private static final String PODOLOGIA = "Podologia";
	private static final String IMAGENES = "Imagenes";
	private static final String PATOLOGIAS = "Patologias";
	private static final String ESTIMULOS = "Estimulos";
	private static final String ESTADISTICAS = "Estadisticas";
	private static final String ARCHIVOS = "Archivos";
	private static final String NUEVA_CONSULTA = "Nueva Consulta";

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	VistaPaciente vistaPaciente;
	@Autowired
	VistaAntecedentesMedicina vistaAntecedentesMedicina;
	@Autowired
	VistaAntecedentesNutricion vistaAntecedentesNutricion;
	@Autowired
	VistaAntecedentesPodologia vistaAntecedentesPodologia;
	@Autowired
	VistaPatologia vistaPatologia;
	@Autowired
	VistaEstimulos vistaEstimulos;
	@Autowired
	VistaEstadisticas vistaEstadisticas;
	@Autowired
	VistaImagenes vistaImagenes;
	@Autowired
	VistaArchivos vistaArchivos;

	@Autowired
	VistaConsultaMedicina vistaConsultaMedicina;
	@Autowired
	VistaConsultaNutricion vistaConsultaNutricion;
	@Autowired
	VistaConsultaPodologia vistaConsultaPodologia;

	// Barra de informacion superior
	@FXML
	private BorderPane panelCentral;
	@FXML
	private HBox hboxCabecera;
	@FXML
	private HBox hBoxContenedorStatus;
	@FXML
	private HBox hBoxDatosPaciente;
	@FXML
	private ImageView imagenRetrato;
	@FXML
	private Label etiquetaApellido;
	@FXML
	private Label etiquetaNombre;
	@FXML
	private Label etiquetaEdad;
	@FXML
	private Label etiquetaOS;

	// Panel de pestañas
	@FXML
	private TabPane tabPaneHistClinica;
	@FXML
	private Tab tabPaciente;
	@FXML
	private Tab tabMedicina;
	@FXML
	private Tab tabNutric;
	@FXML
	private Tab tabPodo;
	@FXML
	private Tab tabImagenes;
	@FXML
	private Tab tabPatologias;
	@FXML
	private Tab tabEstimulos;
	@FXML
	private Tab tabEstadisticas;
	@FXML
	private Tab tabArchivos;

	private Tab tabConsulta;

	ChangeListener<Paciente> listenerPaciente = null;
	private Image imagenDefectoPaciente;
	private SimpleObjectProperty<Consulta> consulta = new SimpleObjectProperty<>(null);

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// Si no hay pestañas, las construye
		// Construye las pestañas y las añade al panel
		prepararPanelPestanas(tabPaneHistClinica);
		imagenDefectoPaciente = new Image(aplicacion.getPropiedades().get("imagen.pacientePorDefecto"));

		// En caso de que el listener no este activo, hace una llamada manual
		if (listenerPaciente == null && aplicacion.getSesion().getPaciente() != null)
			mostrarDatosPaciente(aplicacion.getSesion().getPaciente());

		// Configura el listener
		listenerPaciente = new ChangeListener<Paciente>() {
			@Override
			public void changed(ObservableValue<? extends Paciente> observable, Paciente pacientePrevio,
					Paciente pacienteActual) {
				if (pacienteActual != null)
					mostrarDatosPaciente(pacienteActual);
				else
					limpiarCabecera();
			}
		};

		// Añade el listener
		aplicacion.getSesion().pacienteProperty().addListener(listenerPaciente);

		consulta.addListener((o, consultaPrevia, consultaActual) -> {
			if (consultaActual != null) {
				abrirPestanaConsulta(consultaActual);
			} else {
				quitarPestanaConsulta();
			}
		});

		Platform.runLater(() -> {
			tabArchivos.setDisable(true);
			tabEstimulos.setDisable(true);
		});
	}

	/**
	 * Configura el panel para cambiar la vista e inicialmente seleccionar la
	 * pestaña relevante, inicializa con la pestaña pacientes
	 * 
	 * @param principal
	 */
	private void configurarAccionPanelPestanas(TabPane tabPaneHistClinica) {
		// Disparador de la selección de las pestañas
		tabPaneHistClinica.getSelectionModel().selectedItemProperty().addListener((o, tabPrevia, tabActual) -> {
			if (tabActual != null) {
				cambiarPanel(tabActual);
			}
		});
		cambiarPanel(tabPaciente);
	}

	/**
	 * Construye las pestañas del tabPane
	 * 
	 * @param tabPaneConsultas,
	 *            TabPane
	 * @param principal,
	 *            pestaña principal
	 * 
	 */
	private void prepararPanelPestanas(TabPane tabPaneHistClinica) {
		// Añade el contenido a las pestañas
		tabPaciente.setUserData(vistaPaciente);
		tabMedicina.setUserData(vistaAntecedentesMedicina);
		tabNutric.setUserData(vistaAntecedentesNutricion);
		tabPodo.setUserData(vistaAntecedentesPodologia);
		tabImagenes.setUserData(vistaImagenes);
		tabPatologias.setUserData(vistaPatologia);
		tabEstimulos.setUserData(vistaEstimulos);
		tabEstadisticas.setUserData(vistaEstadisticas);
		tabArchivos.setUserData(vistaArchivos);

		// Crea las cabeceras para cada pestañaF
		tabPaciente.setGraphic(crearCabecera(PACIENTE, aplicacion.getPropiedades().get("imagen.paciente")));
		tabMedicina.setGraphic(crearCabecera(MEDICINA, aplicacion.getPropiedades().get("imagen.medicina")));
		tabNutric.setGraphic(crearCabecera(NUTRICION, aplicacion.getPropiedades().get("imagen.nutric")));
		tabPodo.setGraphic(crearCabecera(PODOLOGIA, aplicacion.getPropiedades().get("imagen.podo")));
		tabImagenes.setGraphic(crearCabecera(IMAGENES, aplicacion.getPropiedades().get("imagen.imagenes")));
		tabPatologias.setGraphic(crearCabecera(PATOLOGIAS, aplicacion.getPropiedades().get("imagen.patologias")));
		tabEstimulos.setGraphic(crearCabecera(ESTIMULOS, aplicacion.getPropiedades().get("imagen.estimulos")));
		tabEstadisticas.setGraphic(crearCabecera(ESTADISTICAS, aplicacion.getPropiedades().get("imagen.estadisticas")));
		tabArchivos.setGraphic(crearCabecera(ARCHIVOS, aplicacion.getPropiedades().get("imagen.imagenes")));

		// Ata las pestañas a la propiedad de paciente activo
		tabMedicina.disableProperty().bind(aplicacion.getSesion().pacienteProperty().isNull());
		tabNutric.disableProperty().bind(aplicacion.getSesion().pacienteProperty().isNull());
		tabPodo.disableProperty().bind(aplicacion.getSesion().pacienteProperty().isNull());
		tabPatologias.disableProperty().bind(aplicacion.getSesion().pacienteProperty().isNull());
		tabEstadisticas.disableProperty().bind(aplicacion.getSesion().pacienteProperty().isNull());
		tabImagenes.disableProperty().bind(aplicacion.getSesion().pacienteProperty().isNull());

//		tabEstimulos.disableProperty().bind(aplicacion.getSesion().pacienteProperty().isNull());
//		tabArchivos.disableProperty().bind(aplicacion.getSesion().pacienteProperty().isNull());

		// Configura el disparador
		configurarAccionPanelPestanas(tabPaneHistClinica);
	}

	private HBox crearCabecera(String titulo, String urlImagen) {
		HBox cabecera = new HBox();
		cabecera.getStyleClass().add("spacing4");
		ImageView imagen = new ImageView(new Image(urlImagen));
		imagen.setFitWidth(16);
		imagen.setFitHeight(16);
		Label nombre = new Label(titulo);
		cabecera.getChildren().addAll(imagen, nombre);
		return cabecera;
	}

	/**
	 * Abre la consulta de nueva consulta
	 */
	public void abrirPestanaConsulta(Consulta consulta) {
		cambiarPestana(agregarPestanaConsulta(consulta));
	}

	/**
	 * Cambia la pestaña a la seleccionada
	 * 
	 * @param tab
	 */
	public void cambiarPestana(Tab tab) {
		this.tabPaneHistClinica.getSelectionModel().select(tab);
	}

	/**
	 * Construye la pestaña de consulta nueva
	 * 
	 * @param tabPane
	 * @return
	 */
	public Tab agregarPestanaConsulta(Consulta consulta) {
		// Evalua si la pestaña existe, si es asi, la selecciona,
		if (!tabPaneHistClinica.getTabs().contains(tabConsulta)) {
			// Crea una nueva pestaña
			tabConsulta = new Tab();
			String estilo = "tabHistClinica";

			// Seleccion de nueva consulta a partir del perfil
			AbstractFxmlView vistaConsulta;
			if (consulta instanceof ConsultaMedicina) {
				vistaConsulta = vistaConsultaMedicina;
			} else if (consulta instanceof ConsultaNutricion) {
				vistaConsulta = vistaConsultaNutricion;
			} else if (consulta instanceof ConsultaPodologica) {
				vistaConsulta = vistaConsultaPodologia;
			} else {
				vistaConsulta = null;
			}

			if (vistaConsulta != null) {
				tabConsulta.setId("tabConsulta");
				tabConsulta.getStyleClass().add(estilo);

				tabConsulta.setGraphic(
						crearCabecera(NUEVA_CONSULTA, aplicacion.getPropiedades().get("imagen.nuevaConsulta")));

				tabConsulta.setUserData(vistaConsulta);

				tabPaneHistClinica.getTabs().add(tabConsulta);
				tabPaneHistClinica.getSelectionModel().select(tabConsulta);
			} else {
				Mensajero.setMsg("No tiene acceso para la acción", Mensajero.MSG_ALERTA);
			}
		}
		return tabConsulta;
	}

	/**
	 * Remueve la pestaña de consulta nueva
	 * 
	 * @param tabPane
	 */
	private void quitarPestanaConsulta() {
		tabPaneHistClinica.getSelectionModel().select(tabPaciente);
		if (tabPaneHistClinica.getTabs().contains(tabConsulta)) {
			tabPaneHistClinica.getTabs().remove(tabConsulta);
		}
	}

	/**
	 * Cambia el contenido del tabPane tomando la pestaña que fue seleccionada
	 * 
	 * @param tab
	 */
	private void cambiarPanel(Tab tab) {
		tab.setContent(((AbstractFxmlView) tab.getUserData()).getView());
	}

	/**
	 * Muestra los datos del paciente
	 * 
	 * @param paciente
	 */
	public void mostrarDatosPaciente(Paciente paciente) {
		// Limpiar panel de datos
		limpiarCabecera();
		// Muestra los valores
		etiquetaApellido.setText(paciente.getApellido() + ",");
		etiquetaNombre.setText(paciente.getNombre());

		if (paciente.getDatosPersonales() != null) {
			imagenRetrato.setImage(Util.recortarImagen(paciente.getDatosPersonales().getFoto()));
			etiquetaEdad.setText(String.format("%s años", paciente.getDatosPersonales().getEdad()));
			etiquetaOS.setText(String.format("OS: %s", paciente.getDatosPersonales().getIdObraSocial()));
		}

		if (imagenRetrato.getImage() == null)
			imagenRetrato.setImage(imagenDefectoPaciente);

		List<String> listStatus = new ArrayList<>();

		// Desglosado de la cadena de Status Patologico en partes
		if (paciente.getDatosClinicos().getStatusPatologico() != null)
			listStatus = Arrays.asList(paciente.getDatosClinicos().getStatusPatologico().split("\r\n|\r|\n"));

		// Construye un vbox y 3 etiquetas por cada 3 lineas encontradas
		VBox vbox = new VBox();
		vbox.setAlignment(Pos.CENTER_LEFT);
		for (int i = 0; i < listStatus.size(); i++) {
			if ((i % 3) == 0 && i > 0) {
				vbox.getStyleClass().add("statusPaciente");
				hBoxContenedorStatus.getChildren().addAll(Util.espacioHLimitado(25, Util.ESTILO_BORDE_IZQUIERDO), vbox,
						Util.espacioHLimitado(25, ""));
				vbox = new VBox();
				vbox.setAlignment(Pos.CENTER_LEFT);
			}
			Label etiqueta = new Label(listStatus.get(i));
			etiqueta.getStyleClass().addAll("borde-inferior-suave");
			vbox.getChildren().add(etiqueta);
		}
		vbox.getStyleClass().add("statusPaciente");

		hBoxContenedorStatus.getChildren().addAll(Util.espacioHLimitado(25, Util.ESTILO_BORDE_IZQUIERDO), vbox,
				Util.espacioHLimitado(25, ""));
		List<String> styleList = new ArrayList<>();
		styleList.add("spacing4");
		styleList.add("fondo-blanco");
		if (!hBoxContenedorStatus.getStyleClass().containsAll(styleList)) {
			hBoxContenedorStatus.getStyleClass().addAll(styleList);
		}

		// Una vez cargado todo el contenido, organiza el ancho de los elementos
		Platform.runLater(() -> {
			organizarAnchos(hBoxContenedorStatus);
		});
	}

	/**
	 * Distribuye el ancho de los elementos
	 * 
	 * @param hBox
	 */
	private void organizarAnchos(HBox hBox) {
		double ancho = 0;
		VBox vNode;
		int elementos = 0;

		// Obtiene la cantidad de elementos (VBox) contenidos
		for (Node n : hBox.getChildrenUnmodifiable()) {
			if (n instanceof VBox) {
				elementos++;
			}
		}

		// Obtiene el valor del mayor de los anchos entre los nodos
		for (int idx = 0; idx < hBox.getChildrenUnmodifiable().size(); idx++) {
			if (hBox.getChildrenUnmodifiable().get(idx) instanceof VBox) {
				vNode = (VBox) hBox.getChildrenUnmodifiable().get(idx);
				if (ancho < vNode.getWidth()) {
					ancho = vNode.getWidth();
				}
			}
		}
		// Reduce en 4 el ancho hasta que el ancho sea menor que el ancho del contenedor
		// (60% de la cabecera)
		while ((ancho * elementos) > hBoxContenedorStatus.getPrefWidth()) {
			ancho = ancho - 4;
		}
		// Luego redimenciona a todos los nodos para que tengan el mismo ancho
		for (int idx = 0; idx < hBox.getChildren().size(); idx++) {
			if (hBox.getChildrenUnmodifiable().get(idx) instanceof VBox) {
				VBox n = (VBox) hBox.getChildrenUnmodifiable().get(idx);
				n.setPrefWidth(ancho);
			}
		}
	}

	private void limpiarCabecera() {
		imagenRetrato.setImage(null);
		etiquetaApellido.setText(null);
		etiquetaNombre.setText(null);
		etiquetaEdad.setText(null);
		etiquetaOS.setText(null);
		hBoxContenedorStatus.getChildren().clear();
	}

	public final Tab getTabConsulta() {
		return tabConsulta;
	}

	public final Tab getTabPaciente() {
		return tabPaciente;
	}

	public final Tab getTabMedicina() {
		return tabMedicina;
	}

	public final Tab getTabNutric() {
		return tabNutric;
	}

	public final Tab getTabPodo() {
		return tabPodo;
	}

	public final Tab getTabImagenes() {
		return tabImagenes;
	}

	public final Tab getTabPatologias() {
		return tabPatologias;
	}

	public final Tab getTabEncuestas() {
		return tabEstimulos;
	}

	public final Tab getTabEstadisticas() {
		return tabEstadisticas;
	}

	public final Tab getTabArchivos() {
		return tabArchivos;
	}

	public final SimpleObjectProperty<Consulta> consultaProperty() {
		return this.consulta;
	}

	public final Consulta getConsulta() {
		return this.consultaProperty().get();
	}

	public final void setConsulta(final Consulta consulta) {
		this.consultaProperty().set(consulta);
	}

}
