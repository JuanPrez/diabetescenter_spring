package com.aplicacion.spring.gestion.controladores;

import java.net.URL;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.servicios.ServiciosPaciente;

import de.felixroske.jfxsupport.FXMLController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

@FXMLController
public class ControladorAgenda implements Initializable {

	// Constantes
	public static final String COMBO_ANIV = "Aniversarios";
	public static final String COMBO_CITAS = "Proximas Citas";
	private static final String TITULO_ANIV = " - Aniversarios - ";
	private static final String TITULO_CITAS = " - Proximas Citas - ";

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	ServiciosPaciente serviciosPaciente;

	@FXML
	private HBox hboxCabecera;
	@FXML
	private VBox vboxPrincipal;
	@FXML
	private DatePicker datePickerFechaDesde;
	@FXML
	private Button botonRefrescar;
	@FXML
	private ComboBox<String> comboBoxPantalla;
	@FXML
	private Label subTitulo;
	@FXML
	private HBox hboxCuerpo;

	List<Paciente> listaPacientes = new ArrayList<>();

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		comboBoxPantalla.getItems().addAll(COMBO_ANIV, COMBO_CITAS);

		Platform.runLater(() -> {
			if (listaPacientes == null || listaPacientes.isEmpty()) {
				if (datePickerFechaDesde.getValue() == null) {
					datePickerFechaDesde.setValue(LocalDate.now());
				}
				comboBoxPantalla.getSelectionModel().clearAndSelect(0);
				botonRefrescar.fire();
			}
		});
	}

	/**
	 * 
	 * Construye las columnas de citas
	 * 
	 * @param hbox,
	 *            nodo donde se incluyen los elementos
	 * @param lista,
	 *            lista de elementos para llenar las columnas
	 * @param fecha
	 *            inicial
	 * 
	 */
	private void mostrarColumnasAniversarios(HBox hbox, List<Paciente> lista, LocalDate fechaDesde) {
		subTitulo.setText(TITULO_ANIV);
		subTitulo.getStyleClass().addAll("text18", "borde-superior-inferior", "borde-negro", "negrita");

		hbox.getChildren().clear();
		for (int i = 0; i < 7; i++) {
			VBox vbox = new VBox();
			LocalDate fecha = fechaDesde.plusDays(i);
			Label tituloFecha = new Label(fecha.toString());
			tituloFecha.getStyleClass().addAll("text13", "negrita");
			ListView<Paciente> listView = new ListView<>();

			listView.setCellFactory((ListView<Paciente> l) -> new CeldaPaciente());

			listView.setOnMouseClicked(event -> {
				if (event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2) {
					aplicacion.getSesion().setPaciente(
							// Toma el primer elemento de la lista
							serviciosPaciente
									.obtenerPacientePorID(listView.getSelectionModel().getSelectedItem().getId())
									.get(0));
					aplicacion.getPantallaPrincipal().cambiarPantalla(ControladorDiabetesCenter.HC);
				}
			});

			for (Paciente paciente : lista) {
				Month m = paciente.getDatosPersonales().getFechaNacimiento().getMonth();
				int d = paciente.getDatosPersonales().getFechaNacimiento().getDayOfMonth();
				if (m.equals(fecha.getMonth()) && d == fecha.getDayOfMonth()) {
					listView.getItems().add(paciente);
				}
			}
			if (listView.getItems().isEmpty()) {
				listView.getItems().add(new Paciente());
			}

			vbox.setAlignment(Pos.TOP_LEFT);
			vbox.getChildren().addAll(tituloFecha, listView);
			hbox.getChildren().add(vbox);

			tituloFecha.setPrefWidth(vbox.getPrefWidth());

			VBox.setVgrow(listView, Priority.ALWAYS);
			hbox.setSpacing(2);
		}
	}

	/**
	 * 
	 * Construye las columnas de citas
	 * 
	 * @param hbox,
	 *            nodo donde se incluyen los elementos
	 * @param lista,
	 *            lista de elementos para llenar las columnas
	 * @param fecha
	 *            inicial
	 */
	private void mostrarColumnasCitas(HBox hbox, List<Paciente> lista, LocalDate fechaDesde) {
		if (lista != null) {
			subTitulo.setText(TITULO_CITAS);

			hbox.getChildren().clear();
			for (int i = 0; i < 7; i++) {
				VBox vbox = new VBox();
				LocalDate fecha = fechaDesde.plusDays(i);
				Label tituloFecha = new Label(fecha.toString());
				tituloFecha.getStyleClass().addAll("text13", "negrita");
				ListView<Paciente> listView = new ListView<>();

				listView.setCellFactory((ListView<Paciente> l) -> new CeldaPaciente());

				listView.setOnMouseClicked(event -> {
					if (event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2) {
						aplicacion.getSesion().setPaciente(
								// Toma el primer elemento de la lista
								serviciosPaciente
										.obtenerPacientePorID(listView.getSelectionModel().getSelectedItem().getId())
										.get(0));
						aplicacion.getPantallaPrincipal().cambiarPantalla(ControladorDiabetesCenter.HC);
					}
				});

				for (Paciente paciente : lista) {
					Month m = paciente.getDatosPersonales().getFechaNacimiento().getMonth();
					int d = paciente.getDatosPersonales().getFechaNacimiento().getDayOfMonth();
					if (m.equals(fecha.getMonth()) && d == fecha.getDayOfMonth()) {
						listView.getItems().add(paciente);
					}
				}
				if (listView.getItems().isEmpty()) {
					listView.getItems().add(new Paciente());
				}
				vbox.getChildren().addAll(tituloFecha, listView);
				hbox.getChildren().add(vbox);
				VBox.setVgrow(listView, Priority.ALWAYS);
				hbox.setSpacing(2);
			}
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param event
	 */
	@FXML
	void armarPantalla(ActionEvent event) {
		LocalDate fecha = null;
		if (datePickerFechaDesde.getValue() != null) {
			fecha = datePickerFechaDesde.getValue();
		} else {
			fecha = LocalDate.now();
		}

		if (comboBoxPantalla.getValue().equals(COMBO_ANIV)) {
			listaPacientes = serviciosPaciente.obtenerPacientesPorFechaNacimiento(fecha);
			mostrarColumnasAniversarios(hboxCuerpo, listaPacientes, fecha);
		} else if (comboBoxPantalla.getValue().equals(COMBO_CITAS)) {
			listaPacientes = serviciosPaciente.obtenerPacientesPorCitas(fecha);
			mostrarColumnasCitas(hboxCuerpo, listaPacientes, fecha);
		}

	}

	/**
	 * Celda que muestra Apellido/Nombre del paciente
	 * 
	 * @author Juan Prez
	 *
	 */
	static class CeldaPaciente extends ListCell<Paciente> {
		@Override
		public void updateItem(Paciente item, boolean empty) {
			super.updateItem(item, empty);
			if (item != null) {
				setText(item.getApellido() + ", " + item.getNombre());
			}
		}
	}
}
