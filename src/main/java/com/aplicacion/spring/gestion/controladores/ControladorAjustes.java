package com.aplicacion.spring.gestion.controladores;

import java.net.URL;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.gestion.vistas.VistaAjustesPatologias;
import com.aplicacion.spring.gestion.vistas.VistaAjustesUsuarios;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

@FXMLController
public class ControladorAjustes implements Initializable {

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	VistaAjustesUsuarios vistaAjustesUsuarios;
	@Autowired
	VistaAjustesPatologias vistaAjustesPatologias;

	@FXML
	private ScrollPane mainPanel;
	@FXML
	private TabPane tabPaneAjustes;
	@FXML
	private Tab tabPatologias;
	@FXML
	private Tab tabUsuarios;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		tabPatologias.setUserData(vistaAjustesPatologias);
		tabUsuarios.setUserData(vistaAjustesUsuarios);

		tabPaneAjustes.getSelectionModel().selectedItemProperty().addListener((o, tabPrevia, tabActual) -> {
			if (tabActual != null) {
				cambiarPanel(tabActual);
			}
		});
		cambiarPanel(tabUsuarios);
	}

	/**
	 * Cambia el contenido del tabPane tomando la pestaña que fue seleccionada
	 * 
	 * @param tab
	 */
	private void cambiarPanel(Tab tab) {
		tab.setContent(((AbstractFxmlView) tab.getUserData()).getView());
	}

}
