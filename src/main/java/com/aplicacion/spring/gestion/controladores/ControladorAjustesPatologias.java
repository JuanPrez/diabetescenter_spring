package com.aplicacion.spring.gestion.controladores;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.negocio.dao.RepositorioPatologias;
import com.aplicacion.spring.negocio.entidades.registros.patologias.Examen;
import com.aplicacion.spring.negocio.entidades.registros.patologias.Patologia;
import com.aplicacion.spring.negocio.entidades.registros.patologias.PatologiaEnlaceExamen;

import de.felixroske.jfxsupport.FXMLController;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;

@FXMLController
public class ControladorAjustesPatologias implements Initializable {

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	RepositorioPatologias patologiasDAO;

	@FXML
	private BorderPane panelCentral;

	@FXML
	private TableView<Patologia> tablaPatologias;
	@FXML
	private TextField campoTextoPatologia;
	@FXML
	private Button botonInsertarPatologia;
	@FXML
	private Button botonModificarPatologia;
	@FXML
	private Button botonQuitarPatologia;
	@FXML
	private TextField campoTextoAbrevPatologia;

	@FXML
	private TableView<Examen> tablaExamenes;
	@FXML
	private TextField campoTextoExamen;
	@FXML
	private Button botonInsertarExamen;
	@FXML
	private Button botonModificarExamen;
	@FXML
	private Button botonQuitarExamen;

	@FXML
	private TableView<Patologia> listaPatologias;
	@FXML
	private TableView<Examen> listaExamenesRelacionados;
	@FXML
	private Button botonSumarRelacionExamenPatologia;
	@FXML
	private ImageView botonSumarRelacionIcono;
	@FXML
	private Button botonQuitarRelacionExamenPatologia;
	@FXML
	private ImageView botonQuitarRelacionIcono;
	@FXML
	private TableView<Examen> listaExamenesDisponibles;

	ChangeListener<Examen> listenerTablaExamenes = null;
	ReadOnlyObjectProperty<Examen> propiedadTablaExamenes;

	ChangeListener<Patologia> listenerTablaPatologias = null;
	ReadOnlyObjectProperty<Patologia> propiedadTablaPatologias;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		// Armamos la tabla de patologias
		armarTablaPatologias();

		// Armamos la tabla de examenes
		armarTablaExamenes();

		// Armamos la lista de patologias
		armarListaPatologias();

		// Listeners de seleccion de la lista de Patologias
		seleccionListaPatologias();

		// Configura la situacion inicial de los botones de relacion
		armarBotonesRelacion();

		// Ata la propiedad "desabilitada" a la seleccion de las tablas relacionadas
		habilitarBotonesRelacion();
	}

	/**
	 * 
	 * Configura la situacion inicial de los botones de relacion
	 * 
	 */
	private void armarBotonesRelacion() {
		// Da imagen a los botones de relacion
		botonSumarRelacionIcono.setImage(new Image(aplicacion.getPropiedades().get("imagen.botonSumarRelacion")));
		botonQuitarRelacionIcono.setImage(new Image(aplicacion.getPropiedades().get("imagen.botonQuitarRelacion")));

		// Cambia la condicion inicial de los botones a deshabilitado
		botonQuitarRelacionExamenPatologia.setDisable(true);
		botonSumarRelacionExamenPatologia.setDisable(true);
	}

	/**
	 * 
	 * Ata la propiedad "desabilitada" a la seleccion de las tablas relacionadas
	 * 
	 */
	private void habilitarBotonesRelacion() {
		// Bind del boton de sumar relaciones, solo se activa cuando hay elementos de
		// la listas de examenes disponibles y patologias seleccionados.
		botonSumarRelacionExamenPatologia.disableProperty()
				.bind(Bindings.or(listaPatologias.getSelectionModel().selectedItemProperty().isNull(),
						listaExamenesDisponibles.getSelectionModel().selectedItemProperty().isNull()));

		// Bind del boton de quitar relaciones, solo se activa cuando hay elementos de
		// la listas de examenes relacionados y patologias seleccionados.
		botonQuitarRelacionExamenPatologia.disableProperty()
				.bind(Bindings.or(listaPatologias.getSelectionModel().selectedItemProperty().isNull(),
						listaExamenesRelacionados.getSelectionModel().selectedItemProperty().isNull()));
	}

	/**
	 * 
	 * Listener de seleccion de lista Patologicas
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void seleccionListaPatologias() {
		listaPatologias.getSelectionModel().selectedItemProperty().addListener((o, p, a) -> {

			if (a != null) {

				List<Examen> examenesRelacionados = new ArrayList<>();

				// Limpiamos la tabla y la lista
				examenesRelacionados.clear();
				listaExamenesRelacionados.getItems().clear();
				listaExamenesRelacionados.getColumns().clear();

				// Tomamos la lista de nexos
				for (PatologiaEnlaceExamen ln : aplicacion.getRepositorios().getListaEnlacesPatologiaExamen()) {
					// Buscamos el nexo que una con la patologia seleccionada
					if (ln != null && a.getId() == ln.getPatologia().getId()) {
						// Recorremos la lista de examenes
						for (Examen le : aplicacion.getRepositorios().getListaExamenes()) {
							// Seleccionamos solo los que esten relacionados con el nexo
							if (ln.getExamen().getId() == le.getId()) {
								// Armamos una nueva lista
								examenesRelacionados.add(le);
							}
						}
					}
				}
				// Titulo
				TableColumn<Examen, String> examenRelacionadoColumna = new TableColumn<>("Examen");
				examenRelacionadoColumna.setMinWidth(250);
				examenRelacionadoColumna.setCellValueFactory(new PropertyValueFactory<Examen, String>("nombre"));

				listaExamenesRelacionados.setItems(FXCollections.observableArrayList(examenesRelacionados));
				listaExamenesRelacionados.getColumns().addAll(examenRelacionadoColumna);
			}

		});
	}

	/**
	 * 
	 * Listener de seleccion de la tabla Patologias
	 * 
	 */
	private void escucharSeleccionPatologia() {
		propiedadTablaPatologias = tablaPatologias.getSelectionModel().selectedItemProperty();
		listenerTablaPatologias = (o, p, a) -> {
			if (a != null) {
				campoTextoPatologia.setText(a.getNombre());
				campoTextoAbrevPatologia.setText(a.getAbreviatura());
			}
		};
		propiedadTablaPatologias.addListener(listenerTablaPatologias);
	}

	private void enmudecerSeleccionPatologia() {
		propiedadTablaExamenes.removeListener(listenerTablaExamenes);
	}

	/**
	 * 
	 * Listener de seleccion de la tabla Examen
	 * 
	 */
	private void escucharSeleccionExamen() {
		// Tomamos la propiedad "elemento seleccionado"
		propiedadTablaExamenes = tablaExamenes.getSelectionModel().selectedItemProperty();
		// Mostramos el texto
		listenerTablaExamenes = (o, p, a) -> {
			if (a != null) {
				campoTextoExamen.setText(a.getNombre());
			}
		};
		// Añadimos el listener
		propiedadTablaExamenes.addListener(listenerTablaExamenes);
	}

	/**
	 * 
	 * Quitamos el listener
	 * 
	 */
	private void enmudecerSeleccionExamen() {
		propiedadTablaExamenes.removeListener(listenerTablaExamenes);
	}

	/**
	 * 
	 * Construye la tabla de examenes
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void armarTablaExamenes() {

		List<Examen> lexamen1 = new ArrayList<>();
		List<Examen> lexamen2 = new ArrayList<>();

		if (listenerTablaExamenes != null) {
			enmudecerSeleccionExamen();
		}

		tablaExamenes.getColumns().clear();
		listaExamenesDisponibles.getColumns().clear();

		// Titulo
		TableColumn<Examen, String> tituloColumna = new TableColumn<>("Examen");
		tituloColumna.setCellValueFactory(new PropertyValueFactory<Examen, String>("nombre"));

		TableColumn<Examen, String> tituloColumna2 = new TableColumn<>("Examen");
		tituloColumna2.setCellValueFactory(new PropertyValueFactory<Examen, String>("nombre"));

		for (Examen le : aplicacion.getRepositorios().getListaExamenes()) {
			lexamen1.add(le);
			lexamen2.add(le);
		}

		// Construye las tablas a partir de los valores de la listaExamenes
		tablaExamenes.setItems(FXCollections.observableArrayList(lexamen1));
		listaExamenesDisponibles.setItems(FXCollections.observableArrayList(lexamen2));

		// Añade la columna2
		tablaExamenes.getColumns().addAll(tituloColumna);
		listaExamenesDisponibles.getColumns().addAll(tituloColumna2);

		escucharSeleccionExamen();
	}

	/**
	 * 
	 * Arma la tabla de patologias
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void armarTablaPatologias() {

		if (listenerTablaPatologias != null) {
			enmudecerSeleccionPatologia();
		}

		tablaPatologias.getColumns().clear();
		// ID
		TableColumn<Patologia, Integer> idColumna = new TableColumn<>("Id");
		idColumna.setCellValueFactory(new PropertyValueFactory<Patologia, Integer>("id"));

		// Titulo
		TableColumn<Patologia, String> nombreColumna = new TableColumn<>("Patologia");
		nombreColumna.setCellValueFactory(new PropertyValueFactory<Patologia, String>("nombre"));

		tablaPatologias.setItems(FXCollections.observableArrayList(aplicacion.getRepositorios().getListaPatologias()));
		tablaPatologias.getColumns().addAll(idColumna, nombreColumna);

		escucharSeleccionPatologia();
	}

	/**
	 * 
	 * Arma la tabla de patologias
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void armarListaPatologias() {

		listaPatologias.getColumns().clear();

		// ID
		TableColumn<Patologia, Integer> idListaPatologiaColumna = new TableColumn<>("Id");
		idListaPatologiaColumna.setCellValueFactory(new PropertyValueFactory<Patologia, Integer>("id"));

		// Titulo
		TableColumn<Patologia, String> tituloListaPatologiaColumna = new TableColumn<>("Patologia");
		tituloListaPatologiaColumna.setCellValueFactory(new PropertyValueFactory<Patologia, String>("nombre"));

		listaPatologias.setItems(FXCollections.observableArrayList(aplicacion.getRepositorios().getListaPatologias()));
		listaPatologias.getColumns().addAll(idListaPatologiaColumna, tituloListaPatologiaColumna);
	}

	/**
	 * 
	 * Limpia las selecciones de las tablas
	 * 
	 */
	private void limpiarSelecciones() {
		if (!tablaPatologias.getItems().isEmpty())
			tablaPatologias.getSelectionModel().select(-1);
		if (!tablaExamenes.getItems().isEmpty())
			tablaExamenes.getSelectionModel().select(-1);
		if (!listaPatologias.getItems().isEmpty())
			listaPatologias.getSelectionModel().select(-1);
		if (!listaExamenesDisponibles.getItems().isEmpty())
			listaExamenesDisponibles.getSelectionModel().select(-1);
		if (!listaExamenesRelacionados.getItems().isEmpty())
			listaExamenesRelacionados.getSelectionModel().select(-1);
	}

	/**
	 * 
	 * Dispara la accion de sumar relacion Patologia - Examen
	 * 
	 * @param e
	 */
	@FXML
	private void botonSumarRelacion(ActionEvent e) {
		if (patologiasDAO.sumarRelacionPatExa(listaPatologias.getSelectionModel().getSelectedItem().getId(),
				listaExamenesDisponibles.getSelectionModel().getSelectedItem().getId())) {
			limpiarSelecciones();
			try {
				aplicacion.getRepositorios().iniciarRepositorios();
			} catch (Exception exc) {
				Mensajero.setMsg(Mensajero.APP_REPOSITORIOS_NO_RECUPERADOS, Mensajero.MSG_ALERTA);
			}

			// Intento de refresco
			int index = listaPatologias.getSelectionModel().getSelectedIndex();
			if (index == 0) {
				listaPatologias.getSelectionModel().selectLast();
			} else {
				listaPatologias.getSelectionModel().select(index);
			}
		}
	}

	/**
	 * 
	 * Dispara la accion de quitar relacion Patologia - Examen
	 * 
	 * @param e
	 */
	@FXML
	private void botonQuitarRelacion(ActionEvent e) {
		if (patologiasDAO.quitarRelacionPatExa(listaPatologias.getSelectionModel().getSelectedItem().getId(),
				listaExamenesRelacionados.getSelectionModel().getSelectedItem().getId())) {
			limpiarSelecciones();
			try {
				aplicacion.getRepositorios().iniciarRepositorios();
			} catch (Exception exc) {
				Mensajero.setMsg(Mensajero.APP_REPOSITORIOS_NO_RECUPERADOS, Mensajero.MSG_ALERTA);
			}
			int index = listaPatologias.getSelectionModel().getSelectedIndex();
			listaPatologias.getSelectionModel().clearAndSelect(-1);
			listaPatologias.getSelectionModel().select(index);
		}
	}

	/**
	 * 
	 * Dispara la accion de sumar una patologia nueva
	 * 
	 * @param event
	 */
	@FXML
	private void insertarPatologia(ActionEvent event) {
		String c1 = campoTextoPatologia.getText().trim();
		String c2 = campoTextoAbrevPatologia.getText().trim();
		if (patologiasDAO.sumarPatologia(c1, c2)) {
			Mensajero.setMsg("Añadida nueva patologia", Mensajero.MSG_EXITO);
			limpiarSelecciones();
			try {
				aplicacion.getRepositorios().iniciarRepositorios();
			} catch (Exception exc) {
				Mensajero.setMsg(Mensajero.APP_REPOSITORIOS_NO_RECUPERADOS, Mensajero.MSG_ALERTA);
			}
			int index = tablaPatologias.getSelectionModel().getSelectedIndex();
			armarTablaPatologias();
			tablaPatologias.getSelectionModel().clearAndSelect(-1);
			tablaPatologias.getSelectionModel().select(index);
		}
	}

	/**
	 * 
	 * Modifica la patologia seleccionada
	 * 
	 * @param event
	 * 
	 */
	@FXML
	private void modificarPatologia(ActionEvent event) {
		int id = tablaPatologias.getSelectionModel().getSelectedItem().getId();
		String c1 = campoTextoPatologia.getText().trim();
		String c2 = campoTextoAbrevPatologia.getText().trim();
		if (patologiasDAO.modificarPatologia(id, c1, c2)) {
			Mensajero.setMsg("Modificada la patologia", Mensajero.MSG_EXITO);
			try {
				aplicacion.getRepositorios().iniciarRepositorios();
			} catch (Exception exc) {
				Mensajero.setMsg(Mensajero.APP_REPOSITORIOS_NO_RECUPERADOS, Mensajero.MSG_ALERTA);
			}
			int index = tablaPatologias.getSelectionModel().getSelectedIndex();
			armarTablaPatologias();
			tablaPatologias.getSelectionModel().clearAndSelect(-1);
			tablaPatologias.getSelectionModel().select(index);
		}
	}

	/**
	 * 
	 * Remueve la patologia seleccionada, para poder hacer esto, es necesario que no
	 * haya ninguna relacion con la patologia
	 * 
	 * @param event
	 * 
	 */
	@FXML
	private void quitarPatologia(ActionEvent event) {
		int id = tablaPatologias.getSelectionModel().getSelectedItem().getId();
		if (patologiasDAO.quitarPatologia(id)) {
			Mensajero.setMsg("Quitada la patologia", Mensajero.MSG_EXITO);
			try {
				aplicacion.getRepositorios().iniciarRepositorios();
			} catch (Exception exc) {
				Mensajero.setMsg(Mensajero.APP_REPOSITORIOS_NO_RECUPERADOS, Mensajero.MSG_ALERTA);
			}
			armarTablaPatologias();
			tablaPatologias.getSelectionModel().clearAndSelect(-1);
		}
	}

	/**
	 * 
	 * Agrega un nuevo examen
	 * 
	 * @param event
	 * 
	 */
	@FXML
	private void insertarExamen(ActionEvent event) {
		String c1 = campoTextoExamen.getText().trim();
		if (patologiasDAO.sumarExamen(c1)) {
			Mensajero.setMsg("Añadido nuevo examen", Mensajero.MSG_EXITO);
			try {
				aplicacion.getRepositorios().iniciarRepositorios();
			} catch (Exception exc) {
				Mensajero.setMsg(Mensajero.APP_REPOSITORIOS_NO_RECUPERADOS, Mensajero.MSG_ALERTA);
			}
			int index = tablaExamenes.getSelectionModel().getSelectedIndex();
			armarTablaExamenes();
			tablaExamenes.getSelectionModel().clearAndSelect(-1);
			tablaExamenes.getSelectionModel().select(index);
		}
	}

	/**
	 * 
	 * Modifica el examen seleccionado
	 * 
	 * @param event
	 * 
	 */
	@FXML
	private void modificarExamen(ActionEvent event) {
		int id = tablaExamenes.getSelectionModel().getSelectedItem().getId();
		String c1 = campoTextoExamen.getText().trim();
		if (patologiasDAO.modificarExamen(id, c1)) {
			Mensajero.setMsg("Modificado el examen", Mensajero.MSG_EXITO);
			try {
				aplicacion.getRepositorios().iniciarRepositorios();
			} catch (Exception exc) {
				Mensajero.setMsg(Mensajero.APP_REPOSITORIOS_NO_RECUPERADOS, Mensajero.MSG_ALERTA);
			}
			int index = tablaExamenes.getSelectionModel().getSelectedIndex();
			armarTablaExamenes();
			tablaExamenes.getSelectionModel().clearAndSelect(-1);
			tablaExamenes.getSelectionModel().select(index);
		}
	}

	/**
	 * 
	 * Remueve el examne seleccionado, para poder hacer esto, es necesario que no
	 * haya ninguna relacion con este
	 * 
	 * @param event
	 * 
	 */
	@FXML
	private void quitarExamen(ActionEvent event) {
		int id = tablaExamenes.getSelectionModel().getSelectedItem().getId();
		if (patologiasDAO.quitarExamen(id)) {
			Mensajero.setMsg("Quitado el examen", Mensajero.MSG_EXITO);
			try {
				aplicacion.getRepositorios().iniciarRepositorios();
			} catch (Exception exc) {
				Mensajero.setMsg(Mensajero.APP_REPOSITORIOS_NO_RECUPERADOS, Mensajero.MSG_ALERTA);
			}
			armarTablaExamenes();
			tablaExamenes.getSelectionModel().clearAndSelect(-1);
		}
	}
}
