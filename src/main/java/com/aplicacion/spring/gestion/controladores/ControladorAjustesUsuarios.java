/* ************************************************************************************* *
 *                                                                                       *
 *  Licensed to the Apache Software Foundation (ASF) under one                           *
 *  or more contributor license agreements.  See the NOTICE file                         *
 *  distributed with this work for additional information                                *
 *  regarding copyright ownership.  The ASF licenses this file                           *
 *  to you under the Apache License, Version 2.0 (the                                    *
 *  "License"); you may not use this file except in compliance                           *
 *  with the License.  You may obtain a copy of the License at                           *
 *                                                                                       *
 *    http://www.apache.org/licenses/LICENSE-2.0                                         *
 *                                                                                       *
 *  Unless required by applicable law or agreed to in writing,                           *
 *  software distributed under the License is distributed on an                          *
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY                               *
 *  KIND, either express or implied.  See the License for the                            *
 *  specific language governing permissions and limitations                              *
 *  under the License.                                                                   *
 *                                                                                       *
 * ************************************************************************************* */

package com.aplicacion.spring.gestion.controladores;

import java.net.URL;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;

import de.felixroske.jfxsupport.FXMLController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;

/**
 * @Autor Juan Ignacio Prez
 */
@FXMLController
public class ControladorAjustesUsuarios implements Initializable {

	@Autowired
	Aplicacion aplicacion;

	@FXML
	private Button botonNuevoUsuario;
	@FXML
	private ComboBox<?> comboListaUsuarios;
	@FXML
	private ScrollPane mainPanel;
	@FXML
	private Button botonNuevoUsuario2;
	@FXML
	private HBox panelCentral;
	@FXML
	private Button botonNuevoUsuario1;

	@FXML
	void nuevoUsuario(ActionEvent event) {
		
	}

	@FXML
	void guardarUsuario(ActionEvent event) {

	}

	@FXML
	void cancelarUsuario(ActionEvent event) {

	}

	@FXML
	void nuevoPerfil(ActionEvent event) {

	}

	@FXML
	void nuevaEspecialidad(ActionEvent event) {

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}
}
