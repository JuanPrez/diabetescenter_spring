/* ************************************************************************************* *
 *                                                                                       *
 *  Licensed to the Apache Software Foundation (ASF) under one                           *
 *  or more contributor license agreements.  See the NOTICE file                         *
 *  distributed with this work for additional information                                *
 *  regarding copyright ownership.  The ASF licenses this file                           *
 *  to you under the Apache License, Version 2.0 (the                                    *
 *  "License"); you may not use this file except in compliance                           *
 *  with the License.  You may obtain a copy of the License at                           *
 *                                                                                       *
 *    http://www.apache.org/licenses/LICENSE-2.0                                         *
 *                                                                                       *
 *  Unless required by applicable law or agreed to in writing,                           *
 *  software distributed under the License is distributed on an                          *
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY                               *
 *  KIND, either express or implied.  See the License for the                            *
 *  specific language governing permissions and limitations                              *
 *  under the License.                                                                   *
 *                                                                                       *
 * ************************************************************************************* */
package com.aplicacion.spring.gestion.servicios;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 * 
 * @Autor Juan Ignacio Prez
 * @Version 1.0
 * @Fecha 04/07/17
 * 
 * @Uso Manejo de las contraseñas del sistema, encriptacion y recuperacion
 * 
 */
@Service
public class ServiciosContrasena {

	// Cargamos la bitacora
	static final Logger bitacora = Logger.getLogger(ServiciosContrasena.class);

	String u = null;
	String correo = null;

	/**
	 * 
	 */
	public boolean actualizarBaseDeDatos(String s) {
		// Crear SQL para actualizar la contrase�a
		return false;
	}

	/**
	 * Encriptar la contrasena.
	 *
	 * @param string
	 *            the password
	 * @return the byte[]
	 */
	public static StringBuilder encriptarContrasena(String p) {
		StringBuilder hexString = new StringBuilder();
		try {
			// Longitud del HASH
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			// Tipo de encoding
			byte[] hash = digest.digest(p.getBytes("UTF-8"));
			for (int i : hash) {
				hexString.append(String.format("%02x", 0XFF & i));
			}
		} catch (NoSuchAlgorithmException exc) {
			bitacora.error("No se puede encriptar la contraseña, algoritmo de encriptacion no valido");
		} catch (UnsupportedEncodingException exc) {
			bitacora.error("No se puede encriptar la contraseña, encoding no soportado");
		}
		return hexString;
	}

	/**
	 * 
	 * 
	 * 
	 */
	public boolean enviarCorreo(String correo, String mensaje, String contrasena) {
		// Crear metodo para enviar el correo
		return false;
	}

	/**
	 * Generar cadena de texto aleatoria usando un patron para recuperar la
	 * contrase�a.
	 *
	 * @return String (Cadena aleatoria)
	 */
	// public String generarCadenaAleatoria() {
	// // Patron para la generacion
	// String c = "([A-Z]{3}[a-z]{1,3}[0-9]{5,8})";
	// Xeger generator = new Xeger(c);
	// String result = generator.generate();
	// // Compilacion del String
	// assert result.matches(c);
	// return c;
	// }

	/**
	 * 
	 * 
	 * 
	 */
	public void recuperarContrasena(String s) {
		// Si existe mas de un usuario con ese correo, cancelar el proceso.
		if (verificarCorreo(s)) {
			// Enviar el correo
			enviarCorreo("Correo", "Mensaje", "Contrasena");
			// Actualizamos la BD
			actualizarBaseDeDatos("Contrasena");
		}
	}

	/**
	 * 
	 * 
	 * 
	 */
	public boolean verificarCorreo(String correo) {
		// Crear SQL para verificar que el correo
		return false;
	}
}
