package com.aplicacion.spring.gestion.servicios;

import com.aplicacion.spring.gestion.entidades.Usuario;

public interface IServiciosUsuario {

	/**
	 * 
	 * @param usuario
	 * @return
	 */
	boolean isActivo(Usuario usuario);

	/**
	 * 
	 * @param usuario
	 * @return
	 */
	boolean isDesbloqueado(Usuario usuario);

	/**
	 * 
	 * @param nombre
	 * @return
	 */
	Usuario buscarUsuarioNombre(String nombre);

	/**
	 * 
	 * @param nombreUsuario
	 * @param contr
	 * @return
	 */
	boolean verificarUsuario(String nombreUsuario, String contr);

	/**
	 * 
	 * @param uid
	 * @param passWord
	 * @return
	 */
	boolean verificarContrasena(int uid, StringBuilder passWord);

	/**
	 * Obtiene el usuario desde Archivo Persistente
	 * 
	 * @return
	 */
	String recuperarUsuario();

	/**
	 * 
	 * @param usuario
	 * @return
	 */
	boolean salvarUsuario(Usuario usuario);

	/**
	 * 
	 */
	void eliminarArchivo();

}