package com.aplicacion.spring.gestion.servicios;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.gestion.dao.RepositorioUsuario;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.gestion.persistencia.PersistenciaUsuario;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ServiciosUsuario implements IServiciosUsuario {

	// Carga de la bitacora
	private static final Logger bitacora = Logger.getLogger(ServiciosUsuario.class);
	private static final String ARCHIVO_SESION = "usuario.dat";

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	ObjectMapper mapeadorJackson;
	@Autowired
	RepositorioUsuario repositorioUsuario;
	@Autowired
	Usuario usuarioV;

	PersistenciaUsuario usuarioPojo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aplicacion.gestion.servicios.IServiciosUsuario#isActivo(com.aplicacion.
	 * gestion.usuario.entidades.Usuario)
	 */
	@Override
	public boolean isActivo(Usuario usuario) {
		// Evita el ingreso si no esta activo
		if (usuario.isInactivo()) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aplicacion.gestion.servicios.IServiciosUsuario#isDesbloqueado(com.
	 * aplicacion.gestion.usuario.entidades.Usuario)
	 */
	@Override
	public boolean isDesbloqueado(Usuario usuario) {
		// Evita el ingreso si esta bloqueado o suspendido
		if (usuario.isBloqueado()) {
			return false;
		} else {
			return true;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aplicacion.gestion.servicios.IServiciosUsuario#buscarUsuarioNombre(java.
	 * lang.String)
	 */
	@Override
	public Usuario buscarUsuarioNombre(String nombre) {
		return repositorioUsuario.buscarUsuarioNombre(nombre);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aplicacion.gestion.servicios.IServiciosUsuario#verificarUsuario(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public boolean verificarUsuario(String nombreUsuario, String contr) {
		// Buscamos el usuario en la BD
		usuarioV = repositorioUsuario.obtenerDatosUsuario(nombreUsuario);

		// Verifa si el usuario existe, no esta bloqueado y no esta inactivo
		if (usuarioV != null && isActivo(usuarioV) && isDesbloqueado(usuarioV)) {
			// Valida la contraseña
			if (verificarContrasena(usuarioV.getUid(), ServiciosContrasena.encriptarContrasena(contr))) {

				aplicacion.getSesion().setUsuario(usuarioV);

				bitacora.info("Usuario " + aplicacion.getSesion().getUsuario().getNombreUsuario() + " verificado");
				repositorioUsuario.actualizarUltimoAcceso(aplicacion.getSesion().getUsuario().getUid());
				return true;
			} else {
				// Completamos las variables estaticas de sesion
				aplicacion.getSesion().setUsuario(null);
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aplicacion.gestion.servicios.IServiciosUsuario#verificarContrasena(int,
	 * java.lang.StringBuilder)
	 */
	@Override
	public boolean verificarContrasena(int uid, StringBuilder passWord) {
		return repositorioUsuario.validarContrasena(uid, passWord);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aplicacion.gestion.servicios.IServiciosUsuario#recuperarUsuario()
	 */
	@Override
	public String recuperarUsuario() {
		return this.leerUsuarioDisco(this.usuarioV, mapeadorJackson).getNombreUsuario();
	}

	/**
	 * 
	 * Obtiene los datos de usuario de archivo
	 * 
	 * @return
	 */
	private Usuario leerUsuarioDisco(Usuario usuario, ObjectMapper mapeador) {
		// Leer archivo de sesion si existe
		if (Paths.get(ARCHIVO_SESION).toFile().exists()) {
			try {
				usuarioPojo = mapeador.readValue(new File(ARCHIVO_SESION), PersistenciaUsuario.class);
				usuario = PersistenciaUsuario.crearFX(usuarioPojo);
			} catch (IOException exc) {
				bitacora.info("No se pudo leer el archivo " + ARCHIVO_SESION, exc);
				usuario = null;
			}
		} else {
			usuario = null;
		}

		// Retorno del objeto usuario
		if (usuario == null) {
			usuario = new Usuario();
		}
		return usuario;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aplicacion.gestion.servicios.IServiciosUsuario#salvarUsuario(com.
	 * aplicacion.gestion.usuario.entidades.Usuario)
	 */
	@Override
	public boolean salvarUsuario(Usuario usuario) {
		return salvarUsuarioDisco(usuario, mapeadorJackson);
	}

	/**
	 * 
	 * Guarda los datos de usuario en archivo
	 * 
	 * @param usuario
	 * @param mapeador
	 * @return
	 */
	private boolean salvarUsuarioDisco(Usuario usuario, ObjectMapper mapeador) {
		usuarioPojo = PersistenciaUsuario.crearPojo(usuario);
		try {
			// Objeto a JSON
			mapeador.writeValue(new File(ARCHIVO_SESION), usuarioPojo);
			return true;
		} catch (IOException exc) {
			bitacora.warn("No se pudo guardar los datos en el archivo " + ARCHIVO_SESION, exc);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aplicacion.gestion.servicios.IServiciosUsuario#eliminarArchivo()
	 */
	@Override
	public void eliminarArchivo() {
		if (Paths.get(ARCHIVO_SESION).toFile().exists()) {
			try {
				Files.delete(Paths.get(ARCHIVO_SESION));
			} catch (IOException exc) {
				bitacora.warn("No se puede leer el archivo, no hay acceso de lectura al archivo", exc);
			}
		}
	}

	public void recuperarContrasena(String usuario, String email) {
		repositorioUsuario.recuperarContrasena(usuario, email);
	}
}
