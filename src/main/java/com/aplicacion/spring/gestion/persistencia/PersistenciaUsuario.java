package com.aplicacion.spring.gestion.persistencia;

import com.aplicacion.spring.gestion.entidades.Ajustes;
import com.aplicacion.spring.gestion.entidades.Especialidad;
import com.aplicacion.spring.gestion.entidades.Perfil;
import com.aplicacion.spring.gestion.entidades.Profesion;
import com.aplicacion.spring.gestion.entidades.Usuario;

public class PersistenciaUsuario {

	private int id;
	private String usuario;
	private String nombre;
	private Perfil perfil;
	private Especialidad especialidad;
	private Profesion profesion;
	private Ajustes ajustes;

	public PersistenciaUsuario() {
		perfil = new Perfil();
		especialidad = new Especialidad();
		profesion = new Profesion();
		ajustes = new Ajustes();
	}

	public PersistenciaUsuario(int id, String usuario, String nombre) {
		this();
		this.id = id;
		this.usuario = usuario;
		this.nombre = nombre;
	}

	public PersistenciaUsuario(int id, String usuario, String nombre, Perfil perfil, Especialidad especialidad, Profesion profesion,
			Ajustes ajustes) {
		this.id = id;
		this.usuario = usuario;
		this.nombre = nombre;
		this.perfil = perfil;
		this.especialidad = especialidad;
		this.profesion = profesion;
		this.ajustes = ajustes;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Especialidad getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(Especialidad especialidad) {
		this.especialidad = especialidad;
	}

	public Profesion getProfesion() {
		return profesion;
	}

	public void setProfesion(Profesion profesion) {
		this.profesion = profesion;
	}

	public Ajustes getAjustes() {
		return ajustes;
	}

	public void setAjustes(Ajustes ajustes) {
		this.ajustes = ajustes;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", usuario=" + usuario + ", nombre=" + nombre + ", perfil=" + perfil
				+ ", especialidad=" + especialidad + ", profesion=" + profesion + ", ajustes=" + ajustes + "]";
	}

	public static PersistenciaUsuario crearPojo(Usuario usuarioFX) {
		return new PersistenciaUsuario(usuarioFX.getUid(), usuarioFX.getNombreUsuario(), usuarioFX.getNombre(),
				usuarioFX.getPerfil(), usuarioFX.getEspecialidad(), usuarioFX.getProfesion(), usuarioFX.getAjustes());
	}

	public static Usuario crearFX(PersistenciaUsuario usuarioPojo) {
		return new Usuario(usuarioPojo.getId(), usuarioPojo.getUsuario(), usuarioPojo.getNombre(),
				usuarioPojo.getAjustes());
	}

}