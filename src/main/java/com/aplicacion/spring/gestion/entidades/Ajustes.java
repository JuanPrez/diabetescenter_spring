package com.aplicacion.spring.gestion.entidades;

import java.util.List;

public class Ajustes {
	private String selectorEspecialidad;
	private String repeticionConsulta;
	private List<Integer> horariosLunes;
	private List<Integer> horariosMartes;
	private List<Integer> horariosMiercoles;
	private List<Integer> horariosJueves;
	private List<Integer> horariosViernes;
	private List<Integer> horariosSabado;

	public Ajustes() {
	}

	public Ajustes(String selectorEspecialidad, String repeticionConsulta, List<Integer> horariosLunes,
			List<Integer> horariosMartes, List<Integer> horariosMiercoles, List<Integer> horariosJueves,
			List<Integer> horariosViernes, List<Integer> horariosSabado) {
		this.selectorEspecialidad = selectorEspecialidad;
		this.repeticionConsulta = repeticionConsulta;
		this.horariosLunes = horariosLunes;
		this.horariosMartes = horariosMartes;
		this.horariosMiercoles = horariosMiercoles;
		this.horariosJueves = horariosJueves;
		this.horariosViernes = horariosViernes;
		this.horariosSabado = horariosSabado;
	}

	public String getSelectorEspecialidad() {
		return selectorEspecialidad;
	}

	public void setSelectorEspecialidad(String selectorEspecialidad) {
		this.selectorEspecialidad = selectorEspecialidad;
	}

	public String getRepeticionConsulta() {
		return repeticionConsulta;
	}

	public void setRepeticionConsulta(String repeticionConsulta) {
		this.repeticionConsulta = repeticionConsulta;
	}

	public List<Integer> getHorariosLunes() {
		return horariosLunes;
	}

	public void setHorariosLunes(List<Integer> horariosLunes) {
		this.horariosLunes = horariosLunes;
	}

	public List<Integer> getHorariosMartes() {
		return horariosMartes;
	}

	public void setHorariosMartes(List<Integer> horariosMartes) {
		this.horariosMartes = horariosMartes;
	}

	public List<Integer> getHorariosMiercoles() {
		return horariosMiercoles;
	}

	public void setHorariosMiercoles(List<Integer> horariosMiercoles) {
		this.horariosMiercoles = horariosMiercoles;
	}

	public List<Integer> getHorariosJueves() {
		return horariosJueves;
	}

	public void setHorariosJueves(List<Integer> horariosJueves) {
		this.horariosJueves = horariosJueves;
	}

	public List<Integer> getHorariosViernes() {
		return horariosViernes;
	}

	public void setHorariosViernes(List<Integer> horariosViernes) {
		this.horariosViernes = horariosViernes;
	}

	public List<Integer> getHorariosSabado() {
		return horariosSabado;
	}

	public void setHorariosSabado(List<Integer> horariosSabado) {
		this.horariosSabado = horariosSabado;
	}

}
