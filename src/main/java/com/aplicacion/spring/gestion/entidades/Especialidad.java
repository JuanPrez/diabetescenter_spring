package com.aplicacion.spring.gestion.entidades;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Especialidad implements Comparable<Especialidad>  {

	SimpleIntegerProperty id = new SimpleIntegerProperty();
	SimpleStringProperty descripcion = new SimpleStringProperty();

	@Override
	public int compareTo(Especialidad o) {
		return this.getDescripcion().compareTo(o.getDescripcion());
	}

	public Especialidad() {
	}

	public Especialidad(int id, String descripcion) {
		this.id.set(id);
		this.descripcion.set(descripcion);
	}

	public String print() {
		return id.get() + " - descripcion: " + descripcion.get();
	}

	@Override
	public String toString() {
		return descripcion.get();
	}

	public final SimpleIntegerProperty idProperty() {
		return this.id;
	}

	public final int getId() {
		return this.idProperty().get();
	}

	public final void setId(final int id) {
		this.idProperty().set(id);
	}

	public final SimpleStringProperty descripcionProperty() {
		return this.descripcion;
	}

	public final String getDescripcion() {
		return this.descripcionProperty().get();
	}

	public final void setDescripcion(final String descripcion) {
		this.descripcionProperty().set(descripcion);
	}

}
