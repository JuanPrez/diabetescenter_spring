package com.aplicacion.spring.gestion.entidades;

import org.springframework.stereotype.Component;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

@Component
public class Usuario implements Comparable<Usuario> {

	private SimpleIntegerProperty uid = new SimpleIntegerProperty(0);
	private SimpleStringProperty nombreUsuario = new SimpleStringProperty();
	private SimpleStringProperty nombre = new SimpleStringProperty();
	private SimpleStringProperty correo = new SimpleStringProperty();
	private SimpleStringProperty matricula = new SimpleStringProperty();
	private Perfil perfil = new Perfil();
	private Especialidad especialidad = new Especialidad();
	private Profesion profesion = new Profesion();
	private SimpleBooleanProperty inactivo = new SimpleBooleanProperty(false);
	private SimpleBooleanProperty bloqueado = new SimpleBooleanProperty(false);
	private Ajustes ajustes = new Ajustes();

	public Usuario() {
	}

	public Usuario(int uid, String nombreUsuario, String nombre) {
		this.uid.set(uid);
		this.nombreUsuario.set(nombreUsuario);
		this.nombre.set(nombre);
	}

	public Usuario(int uid, String nombre, int perfil, int especialidad) {
		this.uid.set(uid);
		this.nombre.set(nombre);
		this.getPerfil().setId(perfil);
		this.getEspecialidad().setId(especialidad);
	}

	public Usuario(int uid, String nombreUsuario, String nombre, String correo, String matricula, Perfil perfil,
			Especialidad especialidad, Profesion profesion, int inactivo, int bloqueado) {
		this.uid.set(uid);
		this.nombreUsuario.set(nombreUsuario);
		this.nombre.set(nombre);
		this.correo.set(correo);
		this.matricula.set(matricula);
		this.perfil = perfil;
		this.especialidad = especialidad;
		this.setProfesion(profesion);
		// TODO CORREGIR POR BOOLEAN!
		this.inactivo.set(inactivo > 0);
		this.bloqueado.set(bloqueado > 0);
	}

	public Usuario(int uid, String nombreUsuario, String nombre, Ajustes ajustes) {
		this.uid.set(uid);
		this.nombreUsuario.set(nombreUsuario);
		this.nombre.set(nombre);
		this.ajustes = ajustes;
	}

	public final SimpleIntegerProperty uidProperty() {
		return this.uid;
	}

	public final int getUid() {
		return this.uidProperty().get();
	}

	public final void setUid(final int uid) {
		this.uidProperty().set(uid);
	}

	public final SimpleStringProperty nombreUsuarioProperty() {
		return this.nombreUsuario;
	}

	public final String getNombreUsuario() {
		return this.nombreUsuarioProperty().get();
	}

	public final void setNombreUsuario(final String nombreUsuario) {
		this.nombreUsuarioProperty().set(nombreUsuario);
	}

	public final SimpleStringProperty nombreProperty() {
		return this.nombre;
	}

	public final String getNombre() {
		return this.nombreProperty().get();
	}

	public final void setNombre(final String nombre) {
		this.nombreProperty().set(nombre);
	}

	public final SimpleStringProperty correoProperty() {
		return this.correo;
	}

	public final String getCorreo() {
		return this.correoProperty().get();
	}

	public final void setCorreo(final String correo) {
		this.correoProperty().set(correo);
	}

	public final SimpleStringProperty matriculaProperty() {
		return this.matricula;
	}

	public final String getMatricula() {
		return this.matriculaProperty().get();
	}

	public final void setMatricula(final String matricula) {
		this.matriculaProperty().set(matricula);
	}

	public Profesion getProfesion() {
		return profesion;
	}

	public void setProfesion(Profesion profesion) {
		this.profesion = profesion;
	}

	public Especialidad getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(Especialidad especialidad) {
		this.especialidad = especialidad;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Ajustes getAjustes() {
		return ajustes;
	}

	public void setAjustes(Ajustes ajustes) {
		this.ajustes = ajustes;
	}

	@Override
	public String toString() {
		return "UsuarioFX [uid=" + uid.get() + ", nombreUsuario=" + nombreUsuario.get() + ", nombre=" + nombre.get()
				+ ", correo=" + correo.get() + ", matricula=" + matricula.get() + ", perfil=" + perfil
				+ ", especialidad=" + especialidad + ", profesion=" + profesion + ", inactivo=" + inactivo
				+ ", bloqueado=" + bloqueado + ", ajustes=" + ajustes + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombreUsuario.get() == null) ? 0 : nombreUsuario.get().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Usuario))
			return false;
		Usuario other = (Usuario) obj;
		if (nombreUsuario.get() == null) {
			if (other.nombreUsuario.get() != null)
				return false;
		} else if (!nombreUsuario.get().equals(other.nombreUsuario.get()))
			return false;
		return true;
	}

	@Override
	public int compareTo(Usuario o) {
		return this.getNombreUsuario().compareTo(o.getNombreUsuario());
	}

	public final SimpleBooleanProperty inactivoProperty() {
		return this.inactivo;
	}

	public final boolean isInactivo() {
		return this.inactivoProperty().get();
	}

	public final void setInactivo(final boolean inactivo) {
		this.inactivoProperty().set(inactivo);
	}

	public final SimpleBooleanProperty bloqueadoProperty() {
		return this.bloqueado;
	}

	public final boolean isBloqueado() {
		return this.bloqueadoProperty().get();
	}

	public final void setBloqueado(final boolean bloqueado) {
		this.bloqueadoProperty().set(bloqueado);
	}

}
