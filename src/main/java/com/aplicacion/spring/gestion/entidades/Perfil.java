package com.aplicacion.spring.gestion.entidades;

public class Perfil implements Comparable<Perfil> {

	private int id;
	private String descripcion;

	private static int medicina;
	private static int nutricion;
	private static int podologia;
	private static int administrativo;
	private static int enfermeria;
	private static int otros;
	private static int administrador;

	@Override
	public int compareTo(Perfil o) {
		return this.getDescripcion().compareTo(o.getDescripcion());
	}

	public Perfil() {
	}

	public Perfil(int id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "Perfil [id=" + id + ", descripcion=" + descripcion + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Perfil))
			return false;
		Perfil other = (Perfil) obj;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		return true;
	}

	public static int getMedicina() {
		return medicina;
	}

	public static void setMedicina(int medicina) {
		Perfil.medicina = medicina;
	}

	public static int getNutricion() {
		return nutricion;
	}

	public static void setNutricion(int nutricion) {
		Perfil.nutricion = nutricion;
	}

	public static int getPodologia() {
		return podologia;
	}

	public static void setPodologia(int podologia) {
		Perfil.podologia = podologia;
	}

	public static int getAdministrativo() {
		return administrativo;
	}

	public static void setAdministrativo(int administrativo) {
		Perfil.administrativo = administrativo;
	}

	public static int getEnfermeria() {
		return enfermeria;
	}

	public static void setEnfermeria(int enfermeria) {
		Perfil.enfermeria = enfermeria;
	}

	public static int getOtros() {
		return otros;
	}

	public static void setOtros(int otros) {
		Perfil.otros = otros;
	}

	public static int getAdministrador() {
		return administrador;
	}

	public static void setAdministrador(int administrador) {
		Perfil.administrador = administrador;
	}

}
