package com.aplicacion.spring.gestion.entidades;

public class Profesion implements Comparable<Profesion> {

	private int id = 0;
	private String descripcion = "";
	private String acronimo = "";

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getAcronimo() {
		return acronimo;
	}

	public void setAcronimo(String acronimo) {
		this.acronimo = acronimo;
	}

	public Profesion() {
	}

	public Profesion(int id, String descripcion, String acronimo) {
		this.id = id;
		this.descripcion = descripcion;
		this.acronimo = acronimo;
	}

	@Override
	public String toString() {
		return id + "- descripcion - " + descripcion;
	}

	@Override
	public int compareTo(Profesion o) {
		return this.getDescripcion().compareTo(o.getDescripcion());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Profesion))
			return false;
		Profesion other = (Profesion) obj;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		return true;
	}

	public String print() {
		return "Profesion [id=" + id + ", descripcion=" + descripcion + ", acronimo=" + acronimo + "]";
	}

}
