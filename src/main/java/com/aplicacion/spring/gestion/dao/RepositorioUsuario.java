/* ************************************************************************************* *
 *                                                                                       *
 * Derechos de autor 2017, Juan Ignacio Prez                                             *
 *                                                                                       *
 * Autorizado en virtud de la Licencia de Apache, Versi�n 2.0 (la "Licencia");           *
 *  se proh�be utilizar este archivo excepto en cumplimiento de la Licencia.             *
 *  Podr� obtener una copia de la Licencia en:                                           *
 *  http://www.apache.org/licenses/LICENSE-2.0                                           *
 *                                                                                       *
 * A menos que lo exijan las leyes pertinentes o se haya establecido por escrito,        *
 *  el software distribuido en virtud de la Licencia se distribuye �TAL CUAL�,           *
 *  SIN GARANT�AS NI CONDICIONES DE NING�N TIPO, ya sean expresas o impl�citas.          *
 *  V�ase la Licencia para consultar el texto espec�fico relativo a los permisos         *
 *  y limitaciones establecidos en la Licencia.                                          *
 *                                                                                       *
 * ************************************************************************************* *
 */
package com.aplicacion.spring.gestion.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.gestion.entidades.Especialidad;
import com.aplicacion.spring.gestion.entidades.Perfil;
import com.aplicacion.spring.gestion.entidades.Profesion;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.utilitarios.C3Pool;
import com.aplicacion.spring.utilitarios.Constantes;

/**
 * 
 * @Autor Juan Ignacio Prez
 * @Version 1.0
 * @Fecha 04/07/17
 * 
 * @Uso Validar el usuario, generar archivos de persistencia y verificar datos.
 * 
 */
@Repository
public class RepositorioUsuario implements IRepositorioUsuario {

	static final Logger bitacora = Logger.getLogger(RepositorioUsuario.class);
	@Autowired
	C3Pool c3Pool;
	@Autowired
	Aplicacion aplicacion;

	private static final String QUERY_SELECT_USUARIO = "SELECT ID, Usuario, Nombre, Email, UltimoAcceso, Inactivo, Bloqueado, Matricula, "
			+ "codPerfil, perfil, codProfesion, profesion, acronimo, codEspecialidad, descEspecialidad FROM "
			+ Constantes.DB_VISTA_ACCESO_USUARIO + " WHERE usuario = ? ;";
	private static final String QUERY_SELECT_USUARIO_NOMBRE = "SELECT ID, Usuario, Nombre, Email, UltimoAcceso, Inactivo, Bloqueado, Matricula, "
			+ "codPerfil, perfil, codProfesion, profesion, acronimo, codEspecialidad, descEspecialidad FROM "
			+ Constantes.DB_VISTA_ACCESO_USUARIO + " WHERE Nombre LIKE ?;";
	private static final String QUERY_SELECT_LISTA_USUARIOS = "SELECT ID, Nombre, usuario FROM "
			+ Constantes.DB_VISTA_ACCESO_USUARIO + "WHERE ID NOT IN (1,2);";
	private static final String QUERY_SELECT_USUARIO_ID = "SELECT ID, Nombre, usuario FROM "
			+ Constantes.DB_VISTA_ACCESO_USUARIO + "WHERE ID = ?";
	private static final String QUERY_SELECT_VERIFICAR_USUARIO = "SELECT ID FROM " + Constantes.DB_VISTA_ACCESO_USUARIO
			+ " WHERE ID = ? AND Pass = ?";
	private static final String QUERY_ACTUALIZAR_ULTIMO_ACCESO = "UPDATE " + Constantes.DB_USUARIO_PRINCIPAL
			+ " SET ultimoAcceso = current_timestamp() WHERE idacc_usuarios = ? ;";

	/**
	 * Actualiza el ultimo acceso del usuario
	 * 
	 * @param String,
	 *            usuario
	 */
	public void actualizarUltimoAcceso(int usuario) {
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_ACTUALIZAR_ULTIMO_ACCESO);) {

			preparedStatement.setInt(1, usuario);

			Mensajero.imprimirQuery(preparedStatement.toString());
			preparedStatement.executeUpdate();

			bitacora.debug("Actualizado el último acceso para usuario "
					+ aplicacion.getSesion().getUsuario().getNombreUsuario());
		} catch (SQLException exc) {
			bitacora.error(exc);
		}
	}

	/**
	 * Obtiene los datos del usuario
	 * 
	 * @param El
	 *            nombre de usuario
	 */
	public Usuario obtenerDatosUsuario(String user) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_USUARIO);) {

			preparedStatement.setString(1, user);
			try (ResultSet dbrsu = preparedStatement.executeQuery();) {

				if (dbrsu.first()) {
					return new Usuario(dbrsu.getInt("ID"), dbrsu.getString("Usuario"), dbrsu.getString("Nombre"),
							dbrsu.getString("Email"), dbrsu.getString("Matricula"),
							new Perfil(dbrsu.getInt("codPerfil"), dbrsu.getString("perfil")),
							new Especialidad(dbrsu.getInt("codEspecialidad"), dbrsu.getString("descEspecialidad")),
							new Profesion(dbrsu.getInt("codProfesion"), dbrsu.getString("profesion"),
									dbrsu.getString("acronimo")),
							dbrsu.getInt("Inactivo"), dbrsu.getInt("Bloqueado"));
				}
			}
		} catch (SQLException exc) {
			bitacora.error(Mensajero.DB_ERROR_GENERICO, exc);
		}
		return null;
	}

	/**
	 * Recuperar toda la lista de usuarios
	 * 
	 * 
	 * @return la lista "completa" de usuarios
	 * 
	 */
	public List<Usuario> recuperarListaUsuario() {

		List<Usuario> listUser = new ArrayList<>();

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_LISTA_USUARIOS);
				ResultSet dbrs = preparedStatement.executeQuery();) {

			while (dbrs.next()) {
				listUser.add(new Usuario(dbrs.getInt("ID"), dbrs.getString("usuario"), null));
			}
		} catch (SQLException exc) {
			bitacora.error(Mensajero.DB_ERROR_GENERICO, exc);
		}

		return listUser;
	}

	/**
	 * Recupera una lista parcial de usuarios
	 * 
	 * @param idUsuario
	 * @return lista de un solo elemento
	 */
	public List<Usuario> recuperarListaUsuario(int idUsuario) {

		List<Usuario> listUser = new ArrayList<>();
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_USUARIO_ID);) {

			preparedStatement.setInt(1, idUsuario);

			try (ResultSet dbrs = preparedStatement.executeQuery();) {
				while (dbrs.next()) {
					listUser.add(new Usuario(dbrs.getInt("ID"), dbrs.getString("usuario"), null));
				}
			}
		} catch (SQLException exc) {
			bitacora.error(Mensajero.DB_ERROR_GENERICO, exc);
		}
		return listUser;
	}

	/**
	 * Valida la contraseña
	 * 
	 * @param uid
	 * @param passWord
	 * @return
	 */
	public boolean validarContrasena(int uid, StringBuilder passWord) {
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_VERIFICAR_USUARIO);) {

			preparedStatement.setInt(1, uid);
			preparedStatement.setString(2, passWord.toString());

			try (ResultSet dbrs = preparedStatement.executeQuery();) {
				if (dbrs.first()) {
					return true;
				}
			}
		} catch (SQLException exc) {
			bitacora.error(Mensajero.DB_ERROR_GENERICO, exc);
		}
		return false;
	}

	/**
	 * Recupera el usuario usando el nombre
	 * 
	 * @param nombre
	 * @return
	 */
	public Usuario buscarUsuarioNombre(String nombre) {
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_USUARIO_NOMBRE);) {

			preparedStatement.setString(1, nombre);
			try (ResultSet dbrsu = preparedStatement.executeQuery();) {

				if (dbrsu.first()) {
					return new Usuario(dbrsu.getInt("ID"), dbrsu.getString("Usuario"), dbrsu.getString("Nombre"),
							dbrsu.getString("Email"), dbrsu.getString("Matricula"),
							new Perfil(dbrsu.getInt("codPerfil"), dbrsu.getString("perfil")),
							new Especialidad(dbrsu.getInt("codEspecialidad"), dbrsu.getString("descEspecialidad")),
							new Profesion(dbrsu.getInt("codProfesion"), dbrsu.getString("profesion"),
									dbrsu.getString("acronimo")),
							dbrsu.getInt("Inactivo"), dbrsu.getInt("Bloqueado"));
				}
			}
		} catch (SQLException exc) {
			bitacora.error(Mensajero.DB_ERROR_GENERICO, exc);
		}
		return null;
	}

	/**
	 * Dispara el procedimiento para reiniciar la contraseña
	 * 
	 * @param usuario
	 * @param email
	 */
	public void recuperarContrasena(String usuario, String email) {
		// Correr procedimiento para reinicio de la base de datos
		try (Connection conn = c3Pool.getConexion();
				CallableStatement callableStatement = conn.prepareCall("{call recuperarContrasena(?, ?)}");) {

			callableStatement.setString(1, usuario);
			callableStatement.setString(2, email);

			callableStatement.execute();

		} catch (SQLException exc) {
			bitacora.error(Mensajero.DB_ERROR_GENERICO, exc);
		}
	}
}
