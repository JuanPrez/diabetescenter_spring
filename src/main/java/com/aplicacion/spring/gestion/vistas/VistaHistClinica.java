package com.aplicacion.spring.gestion.vistas;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

@FXMLView(value = "/spring/vistas/gestion/HistClinica.fxml", encoding = "UTF-8")
public class VistaHistClinica extends AbstractFxmlView {

}
