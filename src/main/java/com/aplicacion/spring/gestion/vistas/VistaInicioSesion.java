package com.aplicacion.spring.gestion.vistas;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

@FXMLView(value = "/spring/vistas/InicioSesion.fxml", encoding = "UTF-8")
public class VistaInicioSesion extends AbstractFxmlView {

}
