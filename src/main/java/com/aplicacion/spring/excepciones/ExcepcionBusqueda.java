package com.aplicacion.spring.excepciones;

public class ExcepcionBusqueda extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExcepcionBusqueda() {
		super();
	}

	public ExcepcionBusqueda(String message) {
		super(message);
	}

	public ExcepcionBusqueda(String message, Throwable cause) {
		super(message, cause);
	}

	public ExcepcionBusqueda(Throwable cause) {
		super(cause);
	}
}