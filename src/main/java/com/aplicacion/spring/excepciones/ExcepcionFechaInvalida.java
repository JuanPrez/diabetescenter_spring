package com.aplicacion.spring.excepciones;

public class ExcepcionFechaInvalida extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExcepcionFechaInvalida() {
		super();
	}

	public ExcepcionFechaInvalida(String message) {
		super(message);
	}

	public ExcepcionFechaInvalida(String message, Throwable cause) {
		super(message, cause);
	}

	public ExcepcionFechaInvalida(Throwable cause) {
		super(cause);
	}
}