package com.aplicacion.spring.excepciones;

public class ExcepcionCampoFueraDePatron extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExcepcionCampoFueraDePatron() {
		super();
	}

	public ExcepcionCampoFueraDePatron(String message) {
		super(message);
	}

	public ExcepcionCampoFueraDePatron(String message, Throwable cause) {
		super(message, cause);
	}

	public ExcepcionCampoFueraDePatron(Throwable cause) {
		super(cause);
	}
}
