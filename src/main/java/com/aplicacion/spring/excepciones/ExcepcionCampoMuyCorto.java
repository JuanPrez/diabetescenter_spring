package com.aplicacion.spring.excepciones;

public class ExcepcionCampoMuyCorto extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExcepcionCampoMuyCorto() {
		super();
	}

	public ExcepcionCampoMuyCorto(String message) {
		super(message);
	}

	public ExcepcionCampoMuyCorto(String message, Throwable cause) {
		super(message, cause);
	}

	public ExcepcionCampoMuyCorto(Throwable cause) {
		super(cause);
	}
}
