package com.aplicacion.spring.excepciones;

public class ExcepcionCampoMuyLargo extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExcepcionCampoMuyLargo() {
		super();
	}

	public ExcepcionCampoMuyLargo(String message) {
		super(message);
	}

	public ExcepcionCampoMuyLargo(String message, Throwable cause) {
		super(message, cause);
	}

	public ExcepcionCampoMuyLargo(Throwable cause) {
		super(cause);
	}
}
