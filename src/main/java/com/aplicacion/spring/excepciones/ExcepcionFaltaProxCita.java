package com.aplicacion.spring.excepciones;

public class ExcepcionFaltaProxCita extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExcepcionFaltaProxCita() {
		super();
	}

	public ExcepcionFaltaProxCita(String message) {
		super(message);
	}

	public ExcepcionFaltaProxCita(String message, Throwable cause) {
		super(message, cause);
	}

	public ExcepcionFaltaProxCita(Throwable cause) {
		super(cause);
	}
}