package com.aplicacion.spring.negocio.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.negocio.entidades.registros.consultas.EstadisticasConsultaMedicina;
import com.aplicacion.spring.negocio.entidades.registros.patologias.EstadisticasPatologia;
import com.aplicacion.spring.utilitarios.C3Pool;
import com.aplicacion.spring.utilitarios.Constantes;

@Service
public class RepositorioEstadisticas {

	@Autowired
	C3Pool c3Pool;

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(RepositorioEstadisticas.class);

	private static final String SQL_CLOSE = ";";
	private static final String QUERY_ESTADISTICAS_CONSULTAS = "SELECT idpaciente, fechaConsulta, peso, presionSistolica, presionDiastolica "
			+ "FROM " + Constantes.DB_CONSULTA_MEDICA + " WHERE idpaciente = ? AND id_hc_clinica_enmienda IS NULL";

	private static final String QUERY_ESTADISTICAS_PATOLOGIAS = "SELECT A.idpaciente AS idPaciente, A.fecha AS fecha, A.resultado AS resultado FROM "
			+ Constantes.DB_PATOLOGIA_CRONICA_RESULTADOS + " AS A LEFT JOIN " + Constantes.DB_PATOLOGIA_CRONICA_EXAMENES
			+ " AS B ON A.idpatCronEx = B.idcom_patologcron_ex " + "WHERE A.idpaciente = ? AND B.nombre = ?";

	/**
	 * 
	 * 
	 * @return
	 */
	public List<EstadisticasConsultaMedicina> obtenerEstadisticasConsultasPaciente(int idPaciente,
			LocalDate fechaDesde) {

		List<EstadisticasConsultaMedicina> listaEstadisticas = new ArrayList<>();

		StringBuilder query = new StringBuilder(QUERY_ESTADISTICAS_CONSULTAS);

		if (fechaDesde != null) {
			query.append(" AND fechaConsulta >= ?");
		}

		query.append(SQL_CLOSE);

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(query.toString());) {
			preparedStatement.setInt(1, idPaciente);

			if (fechaDesde != null) {
				preparedStatement.setString(2, fechaDesde.format(Constantes.FECHA_YYYY_MM_DD));
			}

			try (ResultSet dbrs = preparedStatement.executeQuery();) {
				while (dbrs.next()) {
					listaEstadisticas.add(new EstadisticasConsultaMedicina(dbrs.getLong("idpaciente"),
							dbrs.getDate("fechaConsulta").toLocalDate(), dbrs.getBigDecimal("peso"),
							dbrs.getInt("presionSistolica"), dbrs.getInt("presionDiastolica")));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return listaEstadisticas;
		}
		return listaEstadisticas;
	}

	/**
	 * 
	 * 
	 * @param limiteMenor
	 * @param limiteMayor
	 * @return
	 */
	public List<EstadisticasPatologia> obtenerEstadisticasPatologiasPaciente(int idPaciente, String patologia,
			LocalDate fechaDesde) {

		List<EstadisticasPatologia> listaEstadisticas = new ArrayList<>();

		StringBuilder query = new StringBuilder(QUERY_ESTADISTICAS_PATOLOGIAS);

		if (fechaDesde != null) {
			query.append(" AND A.fecha >= ?");
		}

		query.append(SQL_CLOSE);

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(query.toString());) {

			preparedStatement.setInt(1, idPaciente);
			preparedStatement.setString(2, patologia);
			if (fechaDesde != null) {
				preparedStatement.setString(3, fechaDesde.format(Constantes.FECHA_YYYY_MM_DD));
			}

			try (ResultSet dbrs = preparedStatement.executeQuery();) {
				while (dbrs.next()) {
					Number resultado;
					try {
						resultado = new BigDecimal(dbrs.getString("resultado").replaceAll(",", "."));
					} catch (Exception exc) {
						resultado = 0;
					}
					listaEstadisticas.add(new EstadisticasPatologia(dbrs.getLong("idPaciente"),
							dbrs.getDate("fecha").toLocalDate(), resultado));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return listaEstadisticas;
		}
		return listaEstadisticas;
	}
}
