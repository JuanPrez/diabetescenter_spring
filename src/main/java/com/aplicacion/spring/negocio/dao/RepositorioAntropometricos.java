package com.aplicacion.spring.negocio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.negocio.entidades.paciente.Antropometricos;
import com.aplicacion.spring.utilitarios.C3Pool;
import com.aplicacion.spring.utilitarios.Constantes;

@Repository
public class RepositorioAntropometricos {

	@Autowired
	C3Pool c3Pool;

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(RepositorioPodologia.class);

	// Queries antropometricas
	private static final String QUERY_DATOS_ANTROPOMETRICOS_SELECT = "SELECT idPaciente, numeroCalzado, formulaDigital, antecedentes, idUsuario, fechaCreacion, fechaUltActualizacion "
			+ "FROM " + Constantes.DB_PACIENTE_ANTROPOMETRICOS + " WHERE idPaciente = ?;";
	private static final String QUERY_DATOS_ANTROPOMETRICOS_DELETE = "DELETE FROM "
			+ Constantes.DB_PACIENTE_ANTROPOMETRICOS + " WHERE idPaciente = ?;";
	private static final String QUERY_DATOS_ANTROPOMETRICOS_INSERT_UPDATE = "INSERT INTO "
			+ Constantes.DB_PACIENTE_ANTROPOMETRICOS
			+ " (idpaciente, numeroCalzado, formulaDigital, antecedentes, fechaCreacion, fechaUltActualizacion, idUsuario)"
			+ " VALUES (?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?)"
			+ " ON DUPLICATE KEY UPDATE numeroCalzado = ?, formulaDigital = ?, antecedentes = ?, idUsuario = ?;";

	/**
	 * Obtiene los Datos antropometricos del paciente
	 * 
	 * @param idPaciente
	 * @return DatosAntropometricos
	 */
	public Antropometricos obtenerAntropometricos(int idPaciente) {
		Antropometricos antropometricos = null;
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_DATOS_ANTROPOMETRICOS_SELECT);) {

			preparedStatement.setInt(1, idPaciente);

			try (ResultSet dbrs = preparedStatement.executeQuery()) {

				if (dbrs.first()) {
					antropometricos = new Antropometricos();
					antropometricos.setNumeroCalzado(dbrs.getBigDecimal("numeroCalzado"));
					antropometricos.setFormulaDigital(dbrs.getString("formulaDigital"));
					antropometricos.setAntecedentes(dbrs.getString("antecedentes"));
					antropometricos
							.setFechaCreacion(dbrs.getTimestamp("fechaCreacion").toLocalDateTime().toLocalDate());
					antropometricos
							.setFechaUltAct(dbrs.getTimestamp("fechaUltActualizacion").toLocalDateTime().toLocalDate());
				} else {
					return null;
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return null;
		}
		return antropometricos;
	}

	/**
	 * Insertar nuevos Datos si no existen, o actualizar los que ya estan
	 * antropometricos del paciente
	 * 
	 * @param datosAntropometricos
	 * @return DatosAntropometricos
	 *
	 */
	public boolean actualizarAntropometricos(Antropometricos datosAntropometricos, int idPaciente, int idUsuario) {
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn
						.prepareStatement(QUERY_DATOS_ANTROPOMETRICOS_INSERT_UPDATE);) {

			preparedStatement.setInt(1, idPaciente);
			preparedStatement.setBigDecimal(2, datosAntropometricos.getNumeroCalzado());
			preparedStatement.setString(3, datosAntropometricos.getFormulaDigital());
			preparedStatement.setString(4, datosAntropometricos.getAntecedentes());
			preparedStatement.setInt(5, idUsuario);

			preparedStatement.setBigDecimal(6, datosAntropometricos.getNumeroCalzado());
			preparedStatement.setString(7, datosAntropometricos.getFormulaDigital());
			preparedStatement.setString(8, datosAntropometricos.getAntecedentes());
			preparedStatement.setInt(9, idUsuario);

			Mensajero.imprimirQuery(preparedStatement.toString());

			// Ejecucion
			return (preparedStatement.executeUpdate() > 0);
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	/**
	 * 
	 * Eliminar Datos antropometricos del paciente
	 * 
	 * @param idPaciente
	 * @return
	 */
	public boolean eliminarAntropometricos(int idPaciente) {
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_DATOS_ANTROPOMETRICOS_DELETE);) {

			preparedStatement.setInt(1, idPaciente);

			// Ejecucion
			return (preparedStatement.executeUpdate() > 0);
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

}
