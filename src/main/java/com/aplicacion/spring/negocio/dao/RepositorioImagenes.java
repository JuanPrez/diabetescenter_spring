package com.aplicacion.spring.negocio.dao;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.registros.ImagenPaciente;
import com.aplicacion.spring.utilitarios.C3Pool;
import com.aplicacion.spring.utilitarios.Constantes;

import javafx.scene.image.Image;

@Repository
public class RepositorioImagenes {

	@Autowired
	C3Pool c3Pool;

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(RepositorioImagenes.class);

	int i = 0;
	List<ImagenPaciente> listaImagenes = new ArrayList<>();
	Image imagen = null;

	private static final String QUERY_IMAGENES_PACIENTE_CONTEO = "SELECT COUNT(*) as cantidad FROM "
			+ Constantes.DB_PACIENTE_IMAGENES + " WHERE idpaciente = ?";

	private static final String QUERY_IMAGENES_PACIENTE_SELECT = "SELECT idpac_imagenes AS id, archivo AS imagen, fecha, comentario, idpaciente, tipoConsulta, idhistoriaclinica "
			+ " FROM " + Constantes.DB_PACIENTE_IMAGENES
			+ " WHERE idpaciente = ? ORDER BY fecha DESC, idpac_imagenes DESC LIMIT ?, ?;";

	private static final String QUERY_IMAGENES_PACIENTE_DELETE = "DELETE FROM " + Constantes.DB_PACIENTE_IMAGENES
			+ " WHERE idpac_imagenes = ?;";

	private static final String QUERY_IMAGENES_PACIENTE_INSERT = "INSERT INTO " + Constantes.DB_PACIENTE_IMAGENES
			+ "(idpac_imagenes, archivo, fecha, comentario, idpaciente, tipoConsulta, idhistoriaclinica, id_usuario, tiporegistro) "
			+ "VALUES (default, ?, ?, ?, ?, ?, ?, ?, '2');";

	private static final String QUERY_IMAGENES_PACIENTE_UPDATE = "UPDATE " + Constantes.DB_PACIENTE_IMAGENES
			+ " SET comentario = ?, id_usuario = ? WHERE idpac_imagenes = ?;";

	private static final String QUERY_ID = "SELECT LAST_INSERT_ID();";

	/**
	 * 
	 * 
	 * @return
	 */
	public int contarImagenesPaciente(int idPaciente) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_IMAGENES_PACIENTE_CONTEO);) {
			preparedStatement.setInt(1, idPaciente);

			try (ResultSet dbrs = preparedStatement.executeQuery();) {
				if (dbrs.first())
					return dbrs.getInt("cantidad");
				else
					return 0;
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return 0;
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param limiteMenor
	 * @param limiteMayor
	 * @return
	 */
	public List<ImagenPaciente> recuperarListaImagenesPaciente(int idPaciente, int limiteMenor, int limiteMayor) {

		listaImagenes = new ArrayList<>();

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_IMAGENES_PACIENTE_SELECT);) {

			preparedStatement.setInt(1, idPaciente);
			preparedStatement.setInt(2, limiteMenor);
			preparedStatement.setInt(3, limiteMayor);

			try (ResultSet dbrs = preparedStatement.executeQuery();) {
				while (dbrs.next()) {

					// Verficar si la imagen tiene algun valor, sino pasar nulo
					byte[] imageB = dbrs.getBytes("imagen");
					if (imageB == null || imageB.length <= 1) {
						imagen = null;
					} else {
						imagen = new Image(dbrs.getBlob("imagen").getBinaryStream());
					}

					LocalDate fecha = null;
					if (dbrs.getDate("fecha") != null) {
						fecha = dbrs.getDate("fecha").toLocalDate();
					}

					listaImagenes.add(new ImagenPaciente(dbrs.getInt("id"), dbrs.getString("comentario"), fecha,
							imagen, dbrs.getInt("idpaciente"), dbrs.getInt("tipoConsulta"),
							dbrs.getInt("idhistoriaclinica")));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return listaImagenes;
		}
		return listaImagenes;
	}

	/**
	 * 
	 * Eliminar Imagen seleccionada
	 * 
	 * @param id
	 * @return boolean
	 */
	public boolean eliminarImagen(ImagenPaciente imagen) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_IMAGENES_PACIENTE_DELETE);) {

			preparedStatement.setInt(1, imagen.getId());

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;

		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	/**
	 * Carga la imagen del paciente a la BD, una vez terminada la carga, se
	 * actualiza la imagen en el controlador
	 * 
	 * @param fileName,
	 *            nombre del archivo
	 * 
	 * @param streamImagenPaciente,
	 *            secuencia de bytes del objeto
	 * @param alenght,
	 *            longitud del objeto
	 *
	 * @param idPaciente
	 * @param idconsulta
	 * @param coment
	 * @return
	 */
	public ImagenPaciente guardarImagenNueva(String fileName, InputStream streamImagenPaciente, long alenght,
			int idPaciente, int tipoConsulta, int idconsulta, String coment, Usuario usuario) {

		LocalDate hoy = LocalDate.now();

		ImagenPaciente imagenHistoriaClinica = new ImagenPaciente(0, coment, hoy, new Image("file:" + fileName),
				idPaciente, tipoConsulta, idconsulta);

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_IMAGENES_PACIENTE_INSERT);) {

			/* INSERT INTO pac_imagenes
			(idpac_imagenes, 
			archivo, 
			fecha, 
			comentario, 
			idpaciente, 
			tipoConsulta, 
			idhistoriaclinica, 
			id_usuario, 
			tiporegistro) 
			VALUES (default, ?, ?, ?, ?, ?, ?, ?, '2');"
			*/
			preparedStatement.setBinaryStream(1, streamImagenPaciente, (int) alenght);
			preparedStatement.setDate(2, Date.valueOf(hoy));
			preparedStatement.setString(3, coment);
			preparedStatement.setInt(4, idPaciente);
			preparedStatement.setInt(5, tipoConsulta);
			if (idconsulta > 0) {
				preparedStatement.setInt(6, idconsulta);
			} else {
				preparedStatement.setNull(6, Types.INTEGER);
			}
			preparedStatement.setInt(7, usuario.getUid());

			if (preparedStatement.executeUpdate() > 0) {
				try (ResultSet dbrs = preparedStatement.executeQuery(QUERY_ID);) {
					dbrs.first();
					imagenHistoriaClinica.setId(dbrs.getInt(1));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return null;
		}
		return imagenHistoriaClinica;
	}

	/**
	 * 
	 * Actualiza los comentario de la imagen
	 * 
	 * @param imagen,
	 *            a modificar
	 * @param comentario,
	 *            a guardar
	 * @return imagen
	 */
	public ImagenPaciente actualizarComentarioImagen(ImagenPaciente imagen, String comentario, Usuario usuario) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_IMAGENES_PACIENTE_UPDATE);) {

			preparedStatement.setString(1, comentario);
			preparedStatement.setInt(2, usuario.getUid());
			preparedStatement.setInt(3, imagen.getId());

			Mensajero.imprimirQuery(preparedStatement.toString());

			if (preparedStatement.executeUpdate() > 0) {
				imagen.setComentarios(comentario);
			} else {
				return null;
			}
			
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return null;
		}
		return imagen;
	}
}
