/* ************************************************************************************* *
 *                                                                                       *
 *  Licensed to the Apache Software Foundation (ASF) under one                           *
 *  or more contributor license agreements.  See the NOTICE file                         *
 *  distributed with this work for additional information                                *
 *  regarding copyright ownership.  The ASF licenses this file                           *
 *  to you under the Apache License, Version 2.0 (the                                    *
 *  "License"); you may not use this file except in compliance                           *
 *  with the License.  You may obtain a copy of the License at                           *
 *                                                                                       *
 *    http://www.apache.org/licenses/LICENSE-2.0                                         *
 *                                                                                       *
 *  Unless required by applicable law or agreed to in writing,                           *
 *  software distributed under the License is distributed on an                          *
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY                               *
 *  KIND, either express or implied.  See the License for the                            *
 *  specific language governing permissions and limitations                              *
 *  under the License.                                                                   *
 *                                                                                       *
 * ************************************************************************************* */

package com.aplicacion.spring.negocio.dao;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.paciente.DatosClinicos;
import com.aplicacion.spring.negocio.entidades.paciente.DatosDemograficos;
import com.aplicacion.spring.negocio.entidades.paciente.DatosPersonales;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.EstadisticasPesoIMC;
import com.aplicacion.spring.negocio.entidades.registros.EstadisticasPulsoPresion;
import com.aplicacion.spring.negocio.entidades.registros.Seguimiento;
import com.aplicacion.spring.utilitarios.C3Pool;
import com.aplicacion.spring.utilitarios.Constantes;

import javafx.scene.image.Image;

/**
 * 
 * @Autor Juan Ignacio Prez
 * @Version 1.0
 * @Fecha 17/08/17
 * 
 * @Uso Clase principal de los pacientes
 * 
 */
@Repository
public class RepositorioPaciente {

	@Autowired
	C3Pool c3Pool;

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(RepositorioPaciente.class);

	private static final String QUERY_SELECT_PACIENTES_NOMBRE = "SELECT id, nombre, apellido, sexo, fechaNacimiento, ciudad, domicilio, codigoPostal, telefono, telefono2, email, talla, "
			+ "idObraSocial, NroAfiliado, DNI, foto, observaciones, difunto, fecAlta, ultimaAct, idUsuario, "
			+ "antFamiliares, antQuirurgicos, statusPatologico, idUsuario, usuario, profesional FROM "
			+ Constantes.DB_VISTA_PACIENTE + " WHERE apellido LIKE ? AND nombre LIKE ?"
			+ " GROUP BY id ORDER BY apellido, nombre, ultimaAct;";

	private static final String QUERY_SELECT_PACIENTES_FECHA = "SELECT * FROM " + Constantes.DB_VISTA_PACIENTE
			+ " WHERE DAYOFYEAR(fechaNacimiento) - DAYOFYEAR(?) BETWEEN 0 AND 7 OR	(DAYOFYEAR(? + INTERVAL 7 DAY) < 7"
			+ " AND	DAYOFYEAR(fechaNacimiento) < DAYOFYEAR(? + INTERVAL 7 DAY)) ORDER BY DAYOFYEAR(fechaNacimiento)";

	private static final String QUERY_SELECT_PACIENTES_DNI = "SELECT id, nombre, apellido, sexo, fechaNacimiento, ciudad, domicilio, codigoPostal, telefono, telefono2, email, talla, "
			+ "idObraSocial, NroAfiliado, DNI, foto, observaciones, difunto, fecAlta, ultimaAct, idUsuario, "
			+ "antFamiliares, antQuirurgicos, statusPatologico, idUsuario, usuario, profesional FROM "
			+ Constantes.DB_VISTA_PACIENTE + " WHERE DNI = ?;";

	private static final String QUERY_SELECT_PACIENTE_ID = "SELECT id, nombre, apellido, sexo, fechaNacimiento, ciudad, domicilio, codigoPostal, telefono, telefono2, email, talla, "
			+ "idObraSocial, NroAfiliado, DNI, foto, observaciones, difunto, fecAlta, ultimaAct, idUsuario, "
			+ "antFamiliares, antQuirurgicos, statusPatologico, idUsuario, usuario, profesional FROM "
			+ Constantes.DB_VISTA_PACIENTE + " WHERE id = ?;";

	private static final String QUERY_UPDATE_IMAGEN_PACIENTE = "UPDATE " + Constantes.DB_PACIENTES
			+ " SET foto = ?, idusuario = ?, ultimaAct = current_timestamp() WHERE idpac_paciente = ?;";

	private static final String QUERY_SELECT_LAST_INSERTED_ID = "SELECT LAST_INSERT_ID();";

	private static final String QUERY_INSERT_PACIENTE_TURNO = "INSERT INTO " + Constantes.DB_PACIENTES
			+ " (idpac_paciente,nombre,apellido,DNI,FecAlta,ultimaAct,idusuario,tipoRegistro)"
			+ "VALUES (default,?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,?, '2');";

	private static final String QUERY_INSERT_PACIENTE = "INSERT INTO " + Constantes.DB_PACIENTES
			+ " (idpac_paciente, nombre, apellido, sexo, fechaNacimiento, "
			+ "ciudad, domicilio, codigoPostal, telefono, telefono2, email, talla, idObraSocial, NroAfiliado, "
			+ "DNI, foto, observaciones, Difunto, FecAlta, ultimaAct, idusuario, tipoRegistro) VALUES "
			+ " (default, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, null, ?, ?, CURRENT_DATE, CURRENT_TIMESTAMP, ?, '2');";

	private static final String QUERY_UPDATE_PACIENTE = "UPDATE " + Constantes.DB_PACIENTES
			+ " SET apellido = ?, nombre = ?, sexo = ?, fechaNacimiento = ?, "
			+ "ciudad = ?, domicilio = ?, codigoPostal = ?, telefono = ?, telefono2 = ?, email = ?, "
			+ "talla = ?, idObraSocial = ?, NroAfiliado = ?, DNI = ?, "
			+ "observaciones = ?, ultimaAct = CURRENT_TIMESTAMP(), idusuario = ? " + "WHERE idpac_paciente = ?;";

	private static final String QUERY_INSERT_OR_UPDATE_PACIENTE_STATUS = "INSERT INTO " + Constantes.DB_PACIENTE_STATUS
			+ "(idpaciente, ant_familiares, ant_quirurgicos, statuspatologico, ultimaAct, idusuario, tipoRegistro)"
			+ " VALUES (?, ?, ?, ?, CURRENT_TIMESTAMP, ?, '2') ON DUPLICATE KEY UPDATE "
			+ " ant_familiares = ?, ant_quirurgicos = ?, statuspatologico = ?, ultimaAct = CURRENT_TIMESTAMP, idusuario = ?;";

	private static final String QUERY_SELECT_ULTIMAS_CONSULTAS = "SELECT idPaciente, idEspecialidad, especialidad, proxCita, ultConsulta, idUsuario, usuario, profesional, idUltConsulta FROM "
			+ Constantes.DB_VISTA_CONSULTAS + " WHERE idPaciente = ? AND idEspecialidad != 100";

	// private static final String QUERY_SELECT_ESTADISTICAS_PESO_IMC = "";
	//
	// private static final String QUERY_SELECT_ESTADISTICAS_PULSO_PRESION = "";

	int insertPacienteResult = 0;
	int insertStatusResult = 0;
	int updateResult = 0;
	int updateResult2 = 0;

	/**
	 * 
	 * Metodo para buscar los datos del paciente, ret
	 * 
	 * 
	 * @param dni
	 *            del paciente
	 * @return verdadero si lo encuentra
	 */
	public Paciente obtenerPacientePorDNI(String dni) {
		// Buscamos si existen pacientes con ese numero de documento
		Paciente paciente = null;

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_PACIENTES_DNI);) {

			preparedStatement.setString(1, dni);

			try (ResultSet dbrs = preparedStatement.executeQuery();) {

				// Ponemos los resultados en el Array y unimos Nombre + Apellido
				// Verificamos la cantidad de datos devueltos
				DatosPersonales datosPersonales;
				DatosDemograficos datosDemograficos;
				DatosClinicos datosClinicos;
				Usuario usuario;
				List<Seguimiento> seguimiento = new ArrayList<Seguimiento>();
				if (dbrs.first()) {
					// Ponemos los resultados en el Array y unimos Nombre + Apellido
					// Paciente
					paciente = new Paciente();
					paciente.setId(dbrs.getInt("id"));
					paciente.setNombre(dbrs.getString("nombre"));
					paciente.setApellido(dbrs.getString("apellido"));
					paciente.setFecAlta(dbrs.getDate("fecAlta").toLocalDate());
					paciente.setUltimaAct(dbrs.getDate("ultimaAct").toLocalDate());

					// Datos Personales
					datosPersonales = new DatosPersonales();
					datosPersonales
							.setSexo(dbrs.getString("sexo") != null ? dbrs.getString("sexo").toCharArray()[0] : 'I');
					if (dbrs.getDate("fechaNacimiento") != null) {
						datosPersonales.setEdad(calcularEdad(dbrs.getDate("fechaNacimiento").toLocalDate()));
						datosPersonales.setFechaNacimiento(dbrs.getDate("fechaNacimiento").toLocalDate());
					} else {
						datosPersonales.setEdad(0);
						datosPersonales.setFechaNacimiento(null);
					}
					datosPersonales.setIdObraSocial(dbrs.getString("idObraSocial"));
					datosPersonales.setNroAfiliado(dbrs.getString("NroAfiliado"));
					datosPersonales.setDni(dbrs.getString("DNI"));
					datosPersonales.setFoto(null);
					paciente.setDatosPersonales(datosPersonales);

					// Datos Demograficos
					datosDemograficos = new DatosDemograficos();
					datosDemograficos.setCiudad(dbrs.getString("ciudad"));
					datosDemograficos.setDomicilio(dbrs.getString("domicilio"));
					datosDemograficos.setCodigoPostal(dbrs.getInt("codigoPostal"));
					datosDemograficos.setTelefono(dbrs.getString("telefono"));
					datosDemograficos.setTelefono2(dbrs.getString("telefono2"));
					datosDemograficos.setEmail(dbrs.getString("email"));
					datosDemograficos.setObservaciones(dbrs.getString("observaciones"));
					paciente.setDatosDemograficos(datosDemograficos);

					// Datos Clinicos
					datosClinicos = new DatosClinicos();
					datosClinicos.setTalla(dbrs.getString("talla"));
					datosClinicos.setAntecedentes(dbrs.getString("antQuirurgicos"));
					datosClinicos.setStatusPatologico(dbrs.getString("statusPatologico"));
					datosClinicos.setConsultas(seguimiento);
					datosClinicos.setDifunto(dbrs.getString("difunto").toCharArray()[0]);
					paciente.setDatosClinicos(datosClinicos);
					// Usuario
					usuario = new Usuario(dbrs.getInt("idUsuario"), dbrs.getString("usuario"),
							dbrs.getString("profesional"));
					paciente.setUsuario(usuario);

					// Verficar si la imagen tiene algun valor, sino pasar nulo
					byte[] imageB = dbrs.getBytes("foto");
					if (imageB == null || imageB.length <= 1) {
						paciente.getDatosPersonales().setFoto(null);
					} else {
						paciente.getDatosPersonales()
								.setFoto(new Image(dbrs.getBlob("foto").getBinaryStream(), 220.0, 220.0, true, true));
					}

					if (dbrs.next()) {
						bitacora.warn("Documento duplicado, mostrando el primero encontrado ");
					}

					paciente = obtenerUltimasConsultas(paciente);

				} else {
					return paciente;
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return null;
		}
		return paciente;
	}

	/**
	 * 
	 * Obtiene las ultimas consultas del paciente
	 * 
	 * @param paciente
	 * @return
	 */
	private Paciente obtenerUltimasConsultas(Paciente paciente) {

		// Ejecucion de la query
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_ULTIMAS_CONSULTAS)) {

			preparedStatement.setInt(1, paciente.getId());

			try (ResultSet dbrs = preparedStatement.executeQuery();) {

				while (dbrs.next()) {
					LocalDate ultConsulta = null;
					LocalDate proxCita = null;
					if (dbrs.getDate("ultConsulta") != null) {
						ultConsulta = dbrs.getDate("ultConsulta").toLocalDate();
					}
					if (dbrs.getDate("proxCita") != null) {
						proxCita = dbrs.getDate("proxCita").toLocalDate();
					}

					paciente.getDatosClinicos().getConsultas()
							.add(new Seguimiento(dbrs.getInt("idPaciente"), dbrs.getInt("idEspecialidad"),
									dbrs.getString("especialidad"), ultConsulta, proxCita, dbrs.getInt("idUsuario"),
									dbrs.getString("usuario"), dbrs.getString("profesional"),
									dbrs.getInt("idUltConsulta")));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return null;
		}
		return paciente;
	}

	/**
	 * 
	 * 
	 * 
	 * @param listaPacientes
	 * @return
	 * @throws SQLException
	 */
	private List<Paciente> obtenerUltimasConsultas(List<Paciente> listaPacientes) throws SQLException {

		if (!listaPacientes.isEmpty()) {

			// Construccion de la query
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append(
					"SELECT idPaciente, idEspecialidad, especialidad, proxCita, ultConsulta, idUsuario, usuario, profesional, idUltConsulta FROM ");
			queryBuilder.append(Constantes.DB_VISTA_CONSULTAS);
			queryBuilder.append(" WHERE idPaciente IN (");
			Paciente p;
			for (int i = 0; i < listaPacientes.size(); i++) {
				if (i != 0) {
					queryBuilder.append(", ");
				}
				p = listaPacientes.get(i);
				queryBuilder.append(p.getId());
			}
			queryBuilder.append(");");

			// Ejecucion de la query
			try (Connection conn = c3Pool.getConexion();
					PreparedStatement preparedStatement = conn.prepareStatement(queryBuilder.toString());
					ResultSet dbrs = preparedStatement.executeQuery();) {

				List<Seguimiento> cronoConsultas = new ArrayList<>();

				// Recorre la lista de consultas encontradas
				while (dbrs.next()) {
					LocalDate ultConsulta = null;
					LocalDate proxCita = null;

					if (dbrs.getDate("ultConsulta") != null)
						ultConsulta = dbrs.getDate("ultConsulta").toLocalDate();
					if (dbrs.getDate("proxCita") != null)
						proxCita = dbrs.getDate("proxCita").toLocalDate();

					cronoConsultas.add(new Seguimiento(dbrs.getInt("idPaciente"), dbrs.getInt("idEspecialidad"),
							dbrs.getString("especialidad"), ultConsulta, proxCita, dbrs.getInt("idUsuario"),
							dbrs.getString("usuario"), dbrs.getString("profesional"), dbrs.getInt("idUltConsulta")));
				}

				// Superpone la lista de pacientes con las consultas encontradas
				for (Paciente pac : listaPacientes) {
					for (Seguimiento con : cronoConsultas) {
						if (pac.getId() == con.getIdPaciente()) {
							pac.getDatosClinicos().getConsultas().add(con);
						}
					}
				}
				return listaPacientes;
			} catch (SQLException exc) {
				bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
				return listaPacientes;
			}
		}
		return listaPacientes;
	}

	/**
	 * 
	 * Metodo para recuperar los datos del paciente
	 * 
	 * @param id
	 *            del paciente
	 */
	public Paciente obtenerPacientePorID(int id) {
		Paciente paciente = null;
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_PACIENTE_ID);) {

			preparedStatement.setInt(1, id);

			try (ResultSet dbrs = preparedStatement.executeQuery()) {

				if (dbrs.first()) {
					char sexo = 'I';
					if (dbrs.getString("sexo") != null)
						sexo = dbrs.getString("sexo").toCharArray()[0];

					// Datos personales
					int edad = 0;
					if (dbrs.getDate("fechaNacimiento") != null)
						edad = calcularEdad(dbrs.getDate("fechaNacimiento").toLocalDate());
					LocalDate fNac = null;
					if (dbrs.getDate("fechaNacimiento") != null)
						fNac = dbrs.getDate("fechaNacimiento").toLocalDate();
					String os = dbrs.getString("idObraSocial");
					String afiliado = dbrs.getString("NroAfiliado");
					String dni = dbrs.getString("DNI");

					// Datos demograficos
					String ciudad = dbrs.getString("ciudad");
					String domicilio = dbrs.getString("domicilio");
					int cp = dbrs.getInt("codigoPostal");
					String telefono1 = dbrs.getString("telefono");
					String telefono2 = dbrs.getString("telefono2");
					String email = dbrs.getString("email");
					String observaciones = dbrs.getString("observaciones");

					// Datos Clinicos
					String talla = dbrs.getString("talla");
					String antecedentes = dbrs.getString("antQuirurgicos");
					String status = dbrs.getString("statusPatologico");
					List<Seguimiento> seguimiento = new ArrayList<Seguimiento>();
					char difunto = dbrs.getString("difunto").toCharArray()[0];

					// Datos de gestion
					int idPac = dbrs.getInt("id");
					String nombre = dbrs.getString("nombre");
					String apellido = dbrs.getString("apellido");
					LocalDate fecAlta = null;
					if (dbrs.getDate("fecAlta") != null)
						fecAlta = dbrs.getDate("fecAlta").toLocalDate();
					LocalDate ultAct = null;
					if (dbrs.getDate("ultimaAct") != null)
						ultAct = dbrs.getDate("ultimaAct").toLocalDate();
					Usuario usuario = new Usuario(dbrs.getInt("idUsuario"), dbrs.getString("usuario"),
							dbrs.getString("profesional"));

					paciente = new Paciente(idPac, nombre, apellido,
							new DatosPersonales(sexo, edad, fNac, os, afiliado, dni, null),
							new DatosDemograficos(ciudad, domicilio, cp, telefono1, telefono2, email, observaciones),
							new DatosClinicos(talla, antecedentes, status, seguimiento, difunto), fecAlta, ultAct,
							usuario);

					// Verficar si la imagen tiene algun valor, sino pasar nulo
					byte[] imageB = dbrs.getBytes(16);
					if (imageB == null || imageB.length <= 1) {
						paciente.getDatosPersonales().setFoto(null);
					} else {
						paciente.getDatosPersonales()
								.setFoto(new Image(dbrs.getBlob(16).getBinaryStream(), 220.0, 220.0, true, true));
					}
					paciente = obtenerUltimasConsultas(paciente);
				} else {
					bitacora.info(String.format("No se encontro ningun paciente con el id: %s", id));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return null;
		}
		return paciente;
	}

	/**
	 * 
	 * Metodo para calcular edad
	 * 
	 * @return integer de edad en años
	 * 
	 */
	private int calcularEdad(LocalDate fNac) {
		// Calculamos la edad del paciente
		if (fNac != null) {
			Period edadA = Period.between(fNac, LocalDate.now());
			return edadA.getYears();
		} else {
			return 0;
		}
	}

	/**
	 * 
	 * Metodo para cargar la imagen del paciente a la BD, una vez terminada la
	 * carga, se actualiza la imagen en el controlador
	 * 
	 * @param imagen
	 * @param nombre
	 * @param pacienteId
	 * @param usuarioId
	 * @return
	 */
	public boolean guardarImagenPaciente(InputStream imagen, long longitud, String nombre, int pacienteId,
			int usuarioId) {

		bitacora.debug("Cargando el archivo: " + nombre);
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_UPDATE_IMAGEN_PACIENTE);) {

			/*
			 * UPDATE pac_paciente SET foto = ?, idusuario = ?, ultimaAct =
			 * current_timestamp() WHERE idpac_paciente = ?;"
			 */

			// Cargamos el archivo
			preparedStatement.setBinaryStream(1, imagen, longitud);
			preparedStatement.setInt(2, usuarioId);
			preparedStatement.setInt(3, pacienteId);

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	public int nuevoPaciente(String apellido, String nombre, String dni, Usuario usuario) {
		Paciente nuevo = new Paciente();
		try (Connection conn = c3Pool.getConexion();) {

			try (PreparedStatement preparedStatement = conn.prepareStatement(QUERY_INSERT_PACIENTE_TURNO);) {

				preparedStatement.setString(1, nombre);
				preparedStatement.setString(2, apellido);
				preparedStatement.setString(3, dni);
				preparedStatement.setInt(4, usuario.getUid());

				Mensajero.imprimirQuery(preparedStatement.toString());

				if (preparedStatement.executeUpdate() > 0) {
					bitacora.info(Mensajero.APP_PACIENTE_CARGADO_EXITO);
					// Asigna el nuevo ID creado
					try (ResultSet dbrs = preparedStatement.executeQuery(QUERY_SELECT_LAST_INSERTED_ID)) {
						dbrs.first();
						nuevo.setId(dbrs.getInt(1));
					}
				} else {
					bitacora.info(Mensajero.APP_PACIENTE_CARGADO_FALLO);
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return 0;
		}
		return nuevo.getId();
	}

	/**
	 * Crear un nuevo paciente en la base de datos, si no esta definido el ID del
	 * paciente, uno nuevo es creado, si ya esta definido, se actualiza basandose en
	 * el numero de paciente
	 * 
	 * Si el id == 0, es la creacion de un nuevo paciente Si el id > 0, es una
	 * actualizacion de un paciente existente
	 * 
	 * @param pac,
	 *            Paciente nuevo o actualizacion de uno existente
	 * 
	 * @return int, id del paciente guardado
	 * 
	 */
	public int nuevoPaciente(Paciente pac, Usuario usuario) {

		Date fNac = null;
		if (pac.getDatosPersonales().getFechaNacimiento() != null) {
			fNac = Date.valueOf(pac.getDatosPersonales().getFechaNacimiento());
		}

		// Si el id == 0, es la creacion de un nuevo paciente
		// Si el id > 0, es una actualizacion de un paciente existente
		if (pac.getId() == 0) {
			// Crea un nuevo paciente
			try (Connection conn = c3Pool.getConexion();) {

				try (PreparedStatement preparedStatement = conn.prepareStatement(QUERY_INSERT_PACIENTE);) {

					preparedStatement.setString(1, pac.getNombre());
					preparedStatement.setString(2, pac.getApellido());
					preparedStatement.setString(3, Character.toString(pac.getDatosPersonales().getSexo()));
					preparedStatement.setDate(4, fNac);

					preparedStatement.setString(5, pac.getDatosDemograficos().getCiudad());
					preparedStatement.setString(6, pac.getDatosDemograficos().getDomicilio());
					preparedStatement.setString(7, String.valueOf(pac.getDatosDemograficos().getCodigoPostal()));
					preparedStatement.setString(8, pac.getDatosDemograficos().getTelefono());
					preparedStatement.setString(9, pac.getDatosDemograficos().getTelefono2());
					preparedStatement.setString(10, pac.getDatosDemograficos().getEmail());

					preparedStatement.setString(11, pac.getDatosClinicos().getTalla());
					preparedStatement.setString(12, pac.getDatosPersonales().getIdObraSocial());
					preparedStatement.setString(13, pac.getDatosPersonales().getNroAfiliado());
					preparedStatement.setString(14, pac.getDatosPersonales().getDni());

					preparedStatement.setString(15, pac.getDatosDemograficos().getObservaciones());
					preparedStatement.setString(16, String.valueOf(pac.getDatosClinicos().getDifunto()));
					preparedStatement.setInt(17, usuario.getUid());

					Mensajero.imprimirQuery(preparedStatement.toString());

					if (preparedStatement.executeUpdate() > 0) {
						bitacora.info(Mensajero.APP_PACIENTE_CARGADO_EXITO);
						// Asigna el nuevo ID creado
						try (ResultSet dbrs = preparedStatement.executeQuery(QUERY_SELECT_LAST_INSERTED_ID)) {
							dbrs.first();
							pac.setId(dbrs.getInt(1));
						}
					} else {
						bitacora.info(Mensajero.APP_PACIENTE_CARGADO_FALLO);
					}
				}

				// Actualizacion del status clinico
				actualizarStatusClinico(pac, conn, usuario);

			} catch (SQLException exc) {
				bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
				return 0;
			}
			return pac.getId();
			// Actualizamos un paciente existente
		} else {
			try (Connection conn = c3Pool.getConexion();
					PreparedStatement preparedStatement = conn.prepareStatement(QUERY_UPDATE_PACIENTE);) {

				// Convertimos el LocalDate en SQL DATE
				fNac = Date.valueOf(pac.getDatosPersonales().getFechaNacimiento());

				preparedStatement.setString(1, pac.getApellido());
				preparedStatement.setString(2, pac.getNombre());
				preparedStatement.setString(3, Character.toString(pac.getDatosPersonales().getSexo()));
				preparedStatement.setDate(4, fNac);
				preparedStatement.setString(5, pac.getDatosDemograficos().getCiudad());
				preparedStatement.setString(6, pac.getDatosDemograficos().getDomicilio());
				preparedStatement.setString(7, String.valueOf(pac.getDatosDemograficos().getCodigoPostal()));
				preparedStatement.setString(8, pac.getDatosDemograficos().getTelefono());
				preparedStatement.setString(9, pac.getDatosDemograficos().getTelefono2());
				preparedStatement.setString(10, pac.getDatosDemograficos().getEmail());
				preparedStatement.setString(11, pac.getDatosClinicos().getTalla());
				preparedStatement.setString(12, pac.getDatosPersonales().getIdObraSocial());
				preparedStatement.setString(13, pac.getDatosPersonales().getNroAfiliado());
				preparedStatement.setString(14, pac.getDatosPersonales().getDni());
				preparedStatement.setString(15, pac.getDatosDemograficos().getObservaciones());

				preparedStatement.setInt(16, usuario.getUid());
				preparedStatement.setInt(17, pac.getId());

				Mensajero.imprimirQuery(preparedStatement.toString());

				// Ejecutamos y guardamos el resultado
				if (preparedStatement.executeUpdate() > 0) {
					bitacora.info(Mensajero.APP_PACIENTE_CARGADO_EXITO);
				} else {
					bitacora.info(Mensajero.APP_PACIENTE_CARGADO_FALLO);
				}

				// Actualizacion del status clinico
				if (actualizarStatusClinico(pac, conn, usuario)) {
					bitacora.info(Mensajero.APP_PACIENTE_STATUS_CARGADO_EXITO);
				} else {
					bitacora.info(Mensajero.APP_PACIENTE_STATUS_CARGADO_FALLO);
				}

			} catch (SQLException exc) {
				bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
				return 0;
			}
			return pac.getId();
		}
	}

	/**
	 * 
	 * Actualizacion del status clinico
	 * 
	 * @param pac,
	 *            paciente a actualizar
	 * @param conn,
	 *            conexion activa
	 * @throws SQLException
	 */
	private boolean actualizarStatusClinico(Paciente pac, Connection conn, Usuario usuario) throws SQLException {
		try (PreparedStatement preparedStatement = conn.prepareStatement(QUERY_INSERT_OR_UPDATE_PACIENTE_STATUS);) {

			preparedStatement.setInt(1, pac.getId());
			preparedStatement.setString(2, null);
			preparedStatement.setString(3, pac.getDatosClinicos().getAntecedentes());
			preparedStatement.setString(4, pac.getDatosClinicos().getStatusPatologico());
			preparedStatement.setInt(5, usuario.getUid());

			preparedStatement.setString(6, null);
			preparedStatement.setString(7, pac.getDatosClinicos().getAntecedentes());
			preparedStatement.setString(8, pac.getDatosClinicos().getStatusPatologico());
			preparedStatement.setInt(9, usuario.getUid());

			Mensajero.imprimirQuery(preparedStatement.toString());

			// Ejecutamos y guardamos el resultado
			return (preparedStatement.executeUpdate() > 0);
		}
	}

	/**
	 * 
	 * Busqueda de pacientes por nombre y apellido
	 * 
	 * @param apellido
	 * @param nombre
	 * @return Lista de pacientes
	 * 
	 */
	public List<Paciente> obtenerPacientesPorNombre(String apellido, String nombre) {
		List<Paciente> listPac = new ArrayList<>();
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_PACIENTES_NOMBRE);) {

			preparedStatement.setString(1, "%" + apellido + "%");
			preparedStatement.setString(2, "%" + nombre + "%");

			try (ResultSet dbrs = preparedStatement.executeQuery()) {

				Paciente paciente;
				DatosPersonales datosPersonales;
				DatosDemograficos datosDemograficos;
				DatosClinicos datosClinicos;
				Usuario usuario;
				List<Seguimiento> seguimiento = new ArrayList<Seguimiento>();

				// Ponemos los resultados en el Array y unimos Nombre + Apellido
				while (dbrs.next()) {
					// Paciente
					paciente = new Paciente();
					paciente.setId(dbrs.getInt("id"));
					paciente.setNombre(dbrs.getString("nombre"));
					paciente.setApellido(dbrs.getString("apellido"));
					paciente.setFecAlta(dbrs.getDate("fecAlta").toLocalDate());
					paciente.setUltimaAct(dbrs.getDate("ultimaAct").toLocalDate());

					// Datos Personales
					datosPersonales = new DatosPersonales();
					datosPersonales
							.setSexo(dbrs.getString("sexo") != null ? dbrs.getString("sexo").toCharArray()[0] : 'I');
					if (dbrs.getDate("fechaNacimiento") != null) {
						datosPersonales.setEdad(calcularEdad(dbrs.getDate("fechaNacimiento").toLocalDate()));
						datosPersonales.setFechaNacimiento(dbrs.getDate("fechaNacimiento").toLocalDate());
					} else {
						datosPersonales.setEdad(0);
						datosPersonales.setFechaNacimiento(null);
					}
					datosPersonales.setIdObraSocial(dbrs.getString("idObraSocial"));
					datosPersonales.setNroAfiliado(dbrs.getString("NroAfiliado"));
					datosPersonales.setDni(dbrs.getString("DNI"));
					datosPersonales.setFoto(null);
					paciente.setDatosPersonales(datosPersonales);

					// Datos Demograficos
					datosDemograficos = new DatosDemograficos();
					datosDemograficos.setCiudad(dbrs.getString("ciudad"));
					datosDemograficos.setDomicilio(dbrs.getString("domicilio"));
					datosDemograficos.setCodigoPostal(dbrs.getInt("codigoPostal"));
					datosDemograficos.setTelefono(dbrs.getString("telefono"));
					datosDemograficos.setTelefono2(dbrs.getString("telefono2"));
					datosDemograficos.setEmail(dbrs.getString("email"));
					datosDemograficos.setObservaciones(dbrs.getString("observaciones"));
					paciente.setDatosDemograficos(datosDemograficos);

					// Datos Clinicos
					datosClinicos = new DatosClinicos();
					datosClinicos.setTalla(dbrs.getString("talla"));
					datosClinicos.setAntecedentes(dbrs.getString("antQuirurgicos"));
					datosClinicos.setStatusPatologico(dbrs.getString("statusPatologico"));
					datosClinicos.setConsultas(seguimiento);
					if (dbrs.getString("difunto") != null)
						datosClinicos.setDifunto(dbrs.getString("difunto").toCharArray()[0]);
					else
						datosClinicos.setDifunto('N');
					paciente.setDatosClinicos(datosClinicos);
					// Usuario
					usuario = new Usuario(dbrs.getInt("idUsuario"), dbrs.getString("usuario"),
							dbrs.getString("profesional"));
					paciente.setUsuario(usuario);

					// Verficar si la imagen tiene algun valor, sino pasar nulo
					byte[] imageB = dbrs.getBytes("foto");
					if (imageB == null || imageB.length <= 1) {
						paciente.getDatosPersonales().setFoto(null);
					} else {
						paciente.getDatosPersonales()
								.setFoto(new Image(dbrs.getBlob("foto").getBinaryStream(), 220.0, 220.0, true, true));
					}
					// Añade el paciente recien creado a la lista
					listPac.add(paciente);
				}
				listPac = obtenerUltimasConsultas(listPac);
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return listPac;
		}
		return listPac;
	}

	public List<Paciente> obtenerPacientesPorFechaNacimiento(LocalDate desde) {
		// Armamos un Array para almacenar el ID y Nombre-Apellido
		List<Paciente> listPac = new ArrayList<>();

		// Buscamos el paciente
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_PACIENTES_FECHA);) {

			preparedStatement.setDate(1, Date.valueOf(desde));
			preparedStatement.setDate(2, Date.valueOf(desde));
			preparedStatement.setDate(3, Date.valueOf(desde));

			try (ResultSet dbrs = preparedStatement.executeQuery()) {

				while (dbrs.next()) {
					listPac.add(new Paciente(dbrs.getInt("id"), dbrs.getString("apellido"), dbrs.getString("nombre"),
							dbrs.getDate("fechaNacimiento").toLocalDate()));
				}

			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return new ArrayList<>();
		}
		return listPac;
	}

	public List<Paciente> obtenerPacientesPorCitas(LocalDate desde) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<EstadisticasPesoIMC> obtenerEstadisticasPesoIMC(int idPaciente) {

		List<EstadisticasPesoIMC> listDatos = new ArrayList<>();
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_PACIENTES_FECHA);) {
			preparedStatement.setInt(1, idPaciente);

			try (ResultSet dbrs = preparedStatement.executeQuery()) {
				while (dbrs.next()) {
					// listPac.add(new Paciente(dbrs.getInt("id"), dbrs.getString("apellido"),
					// dbrs.getString("nombre"),
					// dbrs.getDate("fechaNacimiento").toLocalDate()));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return new ArrayList<>();
		}
		return listDatos;
	}

	public List<EstadisticasPulsoPresion> obtenerEstadisticasPulsoPresion(int idPaciente) {

		List<EstadisticasPulsoPresion> listDatos = new ArrayList<>();
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_PACIENTES_FECHA);) {

			preparedStatement.setInt(1, idPaciente);

			try (ResultSet dbrs = preparedStatement.executeQuery()) {
				while (dbrs.next()) {
					// listPac.add(new Paciente(dbrs.getInt("id"), dbrs.getString("apellido"),
					// dbrs.getString("nombre"),
					// dbrs.getDate("fechaNacimiento").toLocalDate()));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return new ArrayList<>();
		}
		return listDatos;
	}

}