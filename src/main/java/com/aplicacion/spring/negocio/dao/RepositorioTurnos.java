package com.aplicacion.spring.negocio.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.gestion.entidades.Especialidad;
import com.aplicacion.spring.gestion.entidades.Perfil;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.turnos.Estado;
import com.aplicacion.spring.negocio.entidades.turnos.Horario;
import com.aplicacion.spring.negocio.entidades.turnos.Turno;
import com.aplicacion.spring.utilitarios.C3Pool;
import com.aplicacion.spring.utilitarios.Constantes;

@Repository
public class RepositorioTurnos implements IRepositorioTurnos {

	@Autowired
	C3Pool c3Pool;

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(RepositorioTurnos.class);

	private static final String QUERY_PERFILES_SELECT = "SELECT idacc_perfil as ID, descripcion AS perfil FROM "
			+ Constantes.DB_USUARIO_PERFIL + " WHERE idacc_perfil IN (1, 2, 3) ORDER BY ID;";

	private static final String QUERY_ESPECIALIDADES_SELECT = "SELECT idacc_perfil_especialidad as ID, descripcion AS especialidad FROM "
			+ Constantes.DB_USUARIO_ESPECIALIDAD
			+ " WHERE idacc_perfil_especialidad != 100 ORDER BY idacc_perfil_especialidad;";

	private static final String QUERY_PROFESIONALES_SELECT = "SELECT idacc_usuarios as id, nombre, idperfil as perfil,  idespecialidad as especialidad FROM "
			+ Constantes.DB_USUARIO_PRINCIPAL + " ORDER BY nombre;";

	private static final String QUERY_TURNOS_SELECT = "SELECT fecha, idhorario, horario, estado, idusuario, usuario, idpaciente, apellidoPaciente, nombrePaciente, observaciones FROM "
			+ Constantes.DB_VISTA_TURNOS + " WHERE idUsuario = ? AND fecha IN (?);";

	private static final String QUERY_TURNO_INSERT = "INSERT INTO " + Constantes.DB_GESTION_TURNOS
			+ "(fecha, idpaciente, idhora, idprofesional, observaciones, estado, idusuario) VALUES (?,?,?,?,?,?,?);";

	private static final String QUERY_TURNO_UPDATE = "UPDATE " + Constantes.DB_GESTION_TURNOS
			+ " SET estado = ? WHERE fecha = ? AND idhora = ? AND idpaciente = ?;";

	private static final String QUERY_TURNO_DELETE = "DELETE FROM " + Constantes.DB_GESTION_TURNOS
			+ " WHERE fecha = ? AND idhora = ?;";

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aplicacion.spring.negocio.dao.ITurnosDAO#obtenerEspecialidades()
	 */
	@Override
	public List<Especialidad> obtenerEspecialidades() {
		List<Especialidad> listaEspecialidades = new LinkedList<>();
		// Abrimos el conector de la base de datos

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_ESPECIALIDADES_SELECT);
				ResultSet dbrs = preparedStatement.executeQuery();) {

			while (dbrs.next()) {
				listaEspecialidades.add(new Especialidad(dbrs.getInt(1), dbrs.getString(2)));
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
		}
		return listaEspecialidades;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aplicacion.spring.negocio.dao.ITurnosDAO#obtenerPerfiles()
	 */
	@Override
	public List<Perfil> obtenerPerfiles() {
		List<Perfil> listaPerfil = new LinkedList<>();
		// Abrimos el conector de la base de datos

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_PERFILES_SELECT);
				ResultSet dbrs = preparedStatement.executeQuery();) {

			while (dbrs.next()) {
				listaPerfil.add(new Perfil(dbrs.getInt(1), dbrs.getString(2)));
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
		}
		return listaPerfil;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aplicacion.spring.negocio.dao.ITurnosDAO#obtenerProfesionales()
	 */
	@Override
	public List<Usuario> obtenerProfesionales() {
		List<Usuario> listaProfesionales = new LinkedList<>();
		// Abrimos el conector de la base de datos

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_PROFESIONALES_SELECT);
				ResultSet dbrs = preparedStatement.executeQuery();) {

			while (dbrs.next()) {
				listaProfesionales.add(new Usuario(dbrs.getInt(1), dbrs.getString(2), dbrs.getInt(3), dbrs.getInt(4)));
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
		}
		return listaProfesionales;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aplicacion.spring.negocio.dao.ITurnosDAO#obtenerTurnosFecha(int,
	 * java.time.LocalDate)
	 */
	@Override
	public List<Turno> obtenerTurnosFecha(int profesional, LocalDate fecha) {

		List<Turno> listaTurnos = new ArrayList<>();

		// Buscamos los turnos para cada dia
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_TURNOS_SELECT);) {

			preparedStatement.setInt(1, profesional);
			preparedStatement.setString(2, fecha.format(Constantes.FECHA_YYYY_MM_DD));

			try (ResultSet dbrs = preparedStatement.executeQuery();) {

				// Por cada resultado, añadimos un registro a listaTurnos
				while (dbrs.next()) {
					Horario horario = new Horario();
					Usuario usuario = new Usuario();
					Paciente paciente = new Paciente();

					horario.setId(dbrs.getInt("idhorario"));
					horario.setHora(dbrs.getString("horario"));
					usuario.setUid(dbrs.getInt("idusuario"));
					usuario.setNombreUsuario(dbrs.getString("usuario"));
					paciente.setId(dbrs.getInt("idpaciente"));
					paciente.setNombre(dbrs.getString("nombrePaciente"));
					paciente.setApellido(dbrs.getString("apellidoPaciente"));

					List<Estado> listaEstados = Arrays.asList(Estado.values());

					Estado status = Estado.LIBRE;
					for (Estado estado : listaEstados) {
						if (dbrs.getInt("estado") == estado.getValor()) {
							status = estado;
						}
					}
					listaTurnos
							.add(new Turno(fecha, horario, status, usuario, paciente, dbrs.getString("observaciones")));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
		}
		return listaTurnos;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aplicacion.spring.negocio.dao.ITurnosDAO#reservarTurno(com.aplicacion.
	 * spring.negocio.entidades.turnos.Turno,
	 * com.aplicacion.spring.gestion.entidades.Usuario)
	 */
	@Override
	public Boolean reservarTurno(Turno turno, Usuario usuario) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_TURNO_INSERT);) {

			preparedStatement.setDate(1, Date.valueOf(turno.getFecha()));
			preparedStatement.setInt(2, turno.getPaciente().getId());
			preparedStatement.setInt(3, turno.getHorario().getId());
			preparedStatement.setInt(4, turno.getProfesional().getUid());
			preparedStatement.setString(5, turno.getObservaciones());
			preparedStatement.setInt(6, Estado.RESERVADO.getValor());
			preparedStatement.setInt(7, usuario.getUid());

			Mensajero.imprimirQuery(preparedStatement.toString());

			if (preparedStatement.executeUpdate() > 0) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aplicacion.spring.negocio.dao.ITurnosDAO#liberarTurno(com.aplicacion.
	 * spring.negocio.entidades.turnos.Turno)
	 */
	@Override
	public Boolean liberarTurno(Turno turno) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_TURNO_DELETE);) {

			preparedStatement.setDate(1, Date.valueOf(turno.getFecha()));
			preparedStatement.setInt(2, turno.getHorario().getId());

			Mensajero.imprimirQuery(preparedStatement.toString());

			if (preparedStatement.executeUpdate() > 0) {
				bitacora.info("Turno liberado");
				return true;
			} else {
				bitacora.info("Turno NO cancelado");
				return false;
			}

		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.aplicacion.spring.negocio.dao.ITurnosDAO#consumirTurno(com.aplicacion.
	 * spring.negocio.entidades.turnos.Turno)
	 */
	@Override
	public Boolean consumirTurno(Turno turno) {
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_TURNO_UPDATE);) {

			preparedStatement.setInt(1, Estado.CONSUMIDO.getValor());
			preparedStatement.setDate(2, Date.valueOf(turno.getFecha()));
			preparedStatement.setInt(3, turno.getHorario().getId());
			preparedStatement.setInt(4, turno.getPaciente().getId());

			Mensajero.imprimirQuery(preparedStatement.toString());

			if (preparedStatement.executeUpdate() > 0) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aplicacion.spring.negocio.dao.ITurnosDAO#cerrarTurno(com.aplicacion.
	 * spring.negocio.entidades.turnos.Turno)
	 */
	@Override
	public Boolean cerrarTurno(Turno turno) {
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_TURNO_UPDATE);) {

			preparedStatement.setInt(1, Estado.CERRADO.getValor());
			preparedStatement.setDate(2, Date.valueOf(turno.getFecha()));
			preparedStatement.setInt(3, turno.getHorario().getId());
			preparedStatement.setInt(4, turno.getPaciente().getId());

			Mensajero.imprimirQuery(preparedStatement.toString());

			if (preparedStatement.executeUpdate() > 0) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}
}
