package com.aplicacion.spring.negocio.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaNutricion;
import com.aplicacion.spring.utilitarios.C3Pool;

@Repository("ConsultaNutricionDAO")
public class RepositorioNutricion implements IRepositorioConsulta<ConsultaNutricion> {

	@Autowired
	C3Pool c3Pool;

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(RepositorioNutricion.class);

	private static final String DB_VISTA = "vw_lista_hc_nutric";
	private static final String DB_CONSULTA = "pac_hc_nutric";
	private static final String DB_PROXCITA = "pac_proxcita_nutric";

	private static final String QUERY_SELECT_CONSULTAS = "SELECT id, idpaciente, idEnmienda, tratamiento, fechaConsulta, proxConsulta, "
			+ "peso, diametroCintura, idUsuario, usuario, nombre FROM " + DB_VISTA
			+ " WHERE idpaciente = ? AND idEnmienda IS NULL ORDER BY fechaConsulta DESC, id DESC;";

	private static final String QUERY_LAST_INSERTED_ID = "SELECT LAST_INSERT_ID();";

	private static final String QUERY_SELECT_CONSULTA_ID = "SELECT id, idpaciente, idEnmienda, tratamiento, fechaConsulta, proxConsulta, "
			+ "peso, diametroCintura, idUsuario, usuario, nombre FROM " + DB_VISTA + " WHERE id = ?;";

	private static final String QUERY_INSERT_CONSULTA = "INSERT INTO " + DB_CONSULTA
			+ "(id_hc_nutric, idpaciente, fechaConsulta, id_hc_nutric_enmienda, id_usuario, tipoRegistro) "
			+ "VALUES (default, ?, ?, ?, ?, '2');";

	private static final String QUERY_UPDATE_CONSULTA = "UPDATE " + DB_CONSULTA + " SET " + "proxConsulta = ?,"
			+ "peso = ?," + "diametroCintura = ?," + "tratamiento = ?," + "id_hc_nutric_enmienda = ?,"
			+ "id_usuario = ?" + " WHERE id_hc_nutric= ?;";

	private static final String QUERY_DELETE_CONSULTA = "DELETE FROM " + DB_CONSULTA + " WHERE id_hc_nutric=?;";

	private static final String QUERY_UPDATE_CONSULTA_ENMIENDA = "UPDATE " + DB_CONSULTA + " SET"
			+ " id_hc_nutric_enmienda = ?, id_usuario = ? WHERE id_hc_nutric =?;";

	private static final String QUERY_ACTUALIZAR_SEGUIMIENTO = "INSERT INTO " + DB_PROXCITA
			+ "  (idPaciente, ultConsulta, proxCita, idUsuario, idUltConsulta) " + "VALUES (?, ?, ?, ?, ?) "
			+ " ON DUPLICATE KEY UPDATE " + "ultConsulta = ?, proxCita = ? , idUsuario = ?, idUltConsulta = ?;";

	@Override
	public List<ConsultaNutricion> obtenerConsultas(Paciente paciente) {
		// Contruye la lista para retornar
		List<ConsultaNutricion> listaConsultas = new ArrayList<>();

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatementc = conn.prepareStatement(QUERY_SELECT_CONSULTAS);) {

			preparedStatementc.setInt(1, paciente.getId());

			try (ResultSet dbrsc = preparedStatementc.executeQuery();) {

				// Recorre el array del ResultSet
				while (dbrsc.next()) {

					// Evita el null en proxConsulta, si no existe
					LocalDate proxCita;
					if (dbrsc.getDate("proxConsulta") != null) {
						proxCita = dbrsc.getDate("proxConsulta").toLocalDate();
					} else {
						proxCita = LocalDate.now();
					}

					listaConsultas.add(new ConsultaNutricion(
							// Consulta
							dbrsc.getInt("id"), paciente, dbrsc.getDate("fechaConsulta").toLocalDate(), proxCita,
							dbrsc.getInt("idEnmienda"),
							new Usuario(dbrsc.getInt("idUsuario"), dbrsc.getString("usuario"),
									dbrsc.getString("nombre")),
							// Consulta Nutricional
							dbrsc.getBigDecimal("peso"), dbrsc.getBigDecimal("diametroCintura"),
							dbrsc.getString("tratamiento")));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return listaConsultas;
		}
		return listaConsultas;
	}

	@Override
	public Integer nuevaConsulta(ConsultaNutricion consulta, Paciente paciente, Usuario usuario) {

		Date fechaConsulta;
		// Si esta es una enmienda, no cambia la fecha de la consulta
		if (consulta.getEnmienda() > 0) {
			fechaConsulta = Date.valueOf(consulta.getFecha());
		} else {
			fechaConsulta = Date.valueOf(LocalDate.now());
		}

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_INSERT_CONSULTA);) {

			preparedStatement.setInt(1, paciente.getId());
			preparedStatement.setDate(2, fechaConsulta);
			preparedStatement.setNull(3, Types.INTEGER);
			preparedStatement.setInt(4, usuario.getUid());

			Mensajero.imprimirQuery(preparedStatement.toString());

			if (preparedStatement.executeUpdate() > 0) {

				try (ResultSet dbrs = preparedStatement.executeQuery(QUERY_LAST_INSERTED_ID);) {
					dbrs.first();
					return dbrs.getInt(1);
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return 0;
		}
		return 0;
	}

	@Override
	public Boolean actualizarConsulta(ConsultaNutricion consulta, Usuario usuario, boolean modificacion) {

		if (consulta != null) {

			Date fechaProxCita = Date.valueOf(consulta.getProxCita());

			try (Connection conn = c3Pool.getConexion();
					PreparedStatement preparedStatement = conn.prepareStatement(QUERY_UPDATE_CONSULTA);) {

				preparedStatement.setDate(1, fechaProxCita);
				preparedStatement.setBigDecimal(2, consulta.getConsultaPeso());
				preparedStatement.setBigDecimal(3, consulta.getConsultaDiam());
				preparedStatement.setString(4, consulta.getTratamiento());
				preparedStatement.setNull(5, Types.INTEGER);
				preparedStatement.setInt(6, usuario.getUid());
				preparedStatement.setInt(7, consulta.getId());

				Mensajero.imprimirQuery(preparedStatement.toString());

				// Si la consulta se actualiza correctamente, guardamos la consulta en la tabla
				// de citas medicas, y en caso de esta sea una modificacion de una consulta
				// previa, actualizamos la consulta anterior.
				if (preparedStatement.executeUpdate() > 0) {

					// Si es una nueva consulta
					if (!modificacion) {
						// Actualiza la tabla de citas
						actualizarSeguimiento(consulta, usuario);
					}

					// Le asigna el id de la enmienda a la consulta anterior, para asi
					// reemplazarla
					if (consulta.getEnmienda() > 0) {
						return (enmendarConsulta(consulta, usuario));
					} else {
						return true;
					}
				}
			} catch (SQLException exc) {
				bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public Boolean eliminarConsulta(int idConsulta) {
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_DELETE_CONSULTA);) {

			preparedStatement.setInt(1, idConsulta);

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	@Override
	public Boolean actualizarSeguimiento(ConsultaNutricion consulta, Usuario usuario) throws SQLException {
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_ACTUALIZAR_SEGUIMIENTO);) {

			// Parte del insert
			preparedStatement.setInt(1, consulta.getPaciente().getId());
			preparedStatement.setDate(2, Date.valueOf(consulta.getFecha()));
			preparedStatement.setDate(3, Date.valueOf(consulta.getProxCita()));
			preparedStatement.setInt(4, usuario.getUid());
			preparedStatement.setInt(5, consulta.getId());

			// Parte del update
			preparedStatement.setDate(6, Date.valueOf(consulta.getFecha()));
			preparedStatement.setDate(7, Date.valueOf(consulta.getProxCita()));
			preparedStatement.setInt(8, usuario.getUid());
			preparedStatement.setInt(9, consulta.getId());

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	@Override
	public ConsultaNutricion obtenerConsultaID(int idConsulta) {
		ConsultaNutricion consulta = null;
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_CONSULTA_ID);) {

			preparedStatement.setInt(1, idConsulta);

			try (ResultSet dbrs = preparedStatement.executeQuery();) {

				while (dbrs.next()) {

					// Evitamos el null en proxConsulta
					LocalDate proxCita;
					if (dbrs.getDate("proxConsulta") != null) {
						proxCita = dbrs.getDate("proxConsulta").toLocalDate();
					} else {
						proxCita = LocalDate.now();
					}

					consulta = new ConsultaNutricion(dbrs.getInt("id"), null,
							dbrs.getDate("fechaConsulta").toLocalDate(), proxCita, dbrs.getInt("idEnmienda"),
							new Usuario(dbrs.getInt("idUsuario"), dbrs.getString("usuario"), dbrs.getString("nombre")),
							dbrs.getBigDecimal("peso"), dbrs.getBigDecimal("diametroCintura"),
							dbrs.getString("tratamiento"));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return null;
		}
		return consulta;
	}

	@Override
	public Boolean enmendarConsulta(ConsultaNutricion consulta, Usuario usuario) throws SQLException {
		// En este paso actualizamos la consulta anterior, y le añadimos un valor al
		// campo enmienda, esta es la consulta que reemplaza a la anterior.
		// Si esta era una modificacion, mantenemos el ID
		if (consulta.getEnmienda() > 0) {
			try (Connection conn = c3Pool.getConexion();
					PreparedStatement preparedStatement = conn.prepareStatement(QUERY_UPDATE_CONSULTA_ENMIENDA);) {
				// UPDATE pac_hc_nutric SET id_hc_nutric_enmienda = ?, id_usuario = ? WHERE
				// id_hc_nutric =?;"
				preparedStatement.setInt(1, consulta.getId());
				preparedStatement.setInt(2, usuario.getUid());
				preparedStatement.setInt(3, consulta.getEnmienda());

				Mensajero.imprimirQuery(preparedStatement.toString());

				return preparedStatement.executeUpdate() > 0;
			} catch (SQLException exc) {
				bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
				return false;
			}
		}
		return false;
	}

}
