package com.aplicacion.spring.negocio.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.paciente.Antropometricos;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaPodologica;
import com.aplicacion.spring.utilitarios.C3Pool;

@Repository("ConsultaPodologiaDAO")
public class RepositorioPodologia implements IRepositorioConsulta<ConsultaPodologica> {

	@Autowired
	C3Pool c3Pool;

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(RepositorioPodologia.class);

	private static final String DB_VISTA = "vw_lista_hc_podo";
	private static final String DB_CONSULTA = "pac_hc_podo";
	private static final String DB_PROXCITA = "pac_proxcita_podo";
	private static final String DB_ANTROPOMETRICOS = "pac_antropometricos";

	// Consultas antropometricas
	private static final String QUERY_DATOS_ANTROPOMETRICOS_SELECT = "SELECT idPaciente, numeroCalzado, formulaDigital, antecedentes, idUsuario, fechaCreacion, fechaUltActualizacion "
			+ "FROM " + DB_ANTROPOMETRICOS + " WHERE idPaciente = ?;";
	private static final String QUERY_DATOS_ANTROPOMETRICOS_DELETE = "DELETE FROM " + DB_ANTROPOMETRICOS
			+ " WHERE idPaciente = ?;";
	private static final String QUERY_DATOS_ANTROPOMETRICOS_INSERT_UPDATE = "INSERT INTO " + DB_ANTROPOMETRICOS
			+ " (idpaciente, numeroCalzado, formulaDigital, antecedentes, fechaCreacion, fechaUltActualizacion, idUsuario)"
			+ " VALUES (?, ?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, ?)"
			+ " ON DUPLICATE KEY UPDATE numeroCalzado = ?, formulaDigital = ?, antecedentes = ?, idUsuario = ?;";

	// Consultas de podologia
	private static final String QUERY_SELECT_LAST_INSERTED_ID = "SELECT LAST_INSERT_ID();";

	private static final String QUERY_SELECT_CONSULTA_ID = "SELECT idPaciente, id, fechaConsulta, pulsoPedio, pulsoTibial, texturaPiel, coloracionPiel, temperatura, dedosPie, observaciones, onicopatias, evolucion, idEnmienda, idusuario, usuario, nombre  FROM "
			+ DB_VISTA + " WHERE id = ?;";

	private static final String QUERY_SELECT_CONSULTAS = "SELECT idPaciente, id, fechaConsulta, pulsoPedio, pulsoTibial, texturaPiel, coloracionPiel, temperatura, dedosPie, observaciones, onicopatias, evolucion, idEnmienda, idusuario, usuario, nombre "
			+ "FROM " + DB_VISTA + " WHERE idPaciente = ? AND idEnmienda IS NULL;";

	private static final String QUERY_INSERT_CONSULTA = "INSERT INTO " + DB_CONSULTA
			+ " (id_hc_podo, idpaciente, fechaConsulta, id_hc_podo_enmienda, id_usuario) "
			+ "VALUES (default, ?, ?, ?, ?);";

	private static final String QUERY_UPDATE_CONSULTA = "UPDATE " + DB_CONSULTA + " SET "
			+ "fechaConsulta = ?, pulsoPedio = ?, pulsoTibial = ?, texturaPiel = ?, colorPiel = ?, tempPie = ?, dedosPie = ?, "
			+ "observaciones = ?, onicopatias = ?, evolucion = ?, id_hc_podo_enmienda = ?, id_usuario = ? WHERE id_hc_podo = ?; ";

	private static final String QUERY_DELETE_CONSULTA = "DELETE FROM " + DB_CONSULTA + " WHERE id_hc_podo=?;";

	private static final String QUERY_UPDATE_CONSULTA_ENMIENDA = "UPDATE " + DB_CONSULTA + " SET"
			+ " id_hc_clinica_enmienda = ?, id_usuario = ? WHERE id_hc_clinica = ?;";

	private static final String QUERY_ACTUALIZAR_SEGUIMIENTO = "INSERT INTO " + DB_PROXCITA
			+ "  (idPaciente, ultConsulta, proxCita, idUsuario, idUltConsulta) " + "VALUES (?, ?, ?, ?, ?) "
			+ " ON DUPLICATE KEY UPDATE " + "ultConsulta = ?, proxCita = ? , idUsuario = ?, idUltConsulta = ?;";

	/**
	 * Obtiene los Datos antropometricos del paciente
	 * 
	 * @param idPaciente
	 * @return DatosAntropometricos
	 */
	public Antropometricos obtenerAntropometricos(int idPaciente) {
		Antropometricos antropometricos = null;
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_DATOS_ANTROPOMETRICOS_SELECT);) {

			preparedStatement.setInt(1, idPaciente);

			try (ResultSet dbrs = preparedStatement.executeQuery()) {

				if (dbrs.first()) {
					antropometricos = new Antropometricos();
					antropometricos.setNumeroCalzado(dbrs.getBigDecimal("numeroCalzado"));
					antropometricos.setFormulaDigital(dbrs.getString("formulaDigital"));
					antropometricos.setAntecedentes(dbrs.getString("antecedentes"));
					antropometricos
							.setFechaCreacion(dbrs.getTimestamp("fechaCreacion").toLocalDateTime().toLocalDate());
					antropometricos
							.setFechaUltAct(dbrs.getTimestamp("fechaUltActualizacion").toLocalDateTime().toLocalDate());
				} else {
					return null;
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return null;
		}
		return antropometricos;
	}

	/**
	 * Insertar nuevos Datos si no existen, o actualizar los que ya estan
	 * antropometricos del paciente
	 * 
	 * @param datosAntropometricos
	 * @return DatosAntropometricos
	 *
	 */
	public boolean actualizarAntropometricos(Antropometricos datosAntropometricos, int idPaciente, int idUsuario) {
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn
						.prepareStatement(QUERY_DATOS_ANTROPOMETRICOS_INSERT_UPDATE);) {

			preparedStatement.setInt(1, idPaciente);
			preparedStatement.setBigDecimal(2, datosAntropometricos.getNumeroCalzado());
			preparedStatement.setString(3, datosAntropometricos.getFormulaDigital());
			preparedStatement.setString(4, datosAntropometricos.getAntecedentes());
			preparedStatement.setInt(5, idUsuario);

			preparedStatement.setBigDecimal(6, datosAntropometricos.getNumeroCalzado());
			preparedStatement.setString(7, datosAntropometricos.getFormulaDigital());
			preparedStatement.setString(8, datosAntropometricos.getAntecedentes());
			preparedStatement.setInt(9, idUsuario);

			Mensajero.imprimirQuery(preparedStatement.toString());

			// Ejecucion
			return (preparedStatement.executeUpdate() > 0);
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	/**
	 * Eliminar Datos antropometricos del paciente
	 * 
	 * @param idPaciente
	 * @return
	 */
	public boolean eliminarAntropometricos(int idPaciente) {
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_DATOS_ANTROPOMETRICOS_DELETE);) {

			preparedStatement.setInt(1, idPaciente);

			// Ejecucion
			return (preparedStatement.executeUpdate() > 0);
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	@Override
	public Integer nuevaConsulta(ConsultaPodologica consulta, Paciente paciente, Usuario usuario) {

		Date fechaConsulta;
		// Si esta es una enmienda, no cambia la fecha de la consulta
		if (consulta.getEnmienda() > 0) {
			fechaConsulta = Date.valueOf(consulta.getFecha());
		} else {
			fechaConsulta = Date.valueOf(LocalDate.now());
		}

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_INSERT_CONSULTA);) {

			preparedStatement.setInt(1, paciente.getId());
			preparedStatement.setDate(2, fechaConsulta);
			preparedStatement.setNull(3, Types.INTEGER);
			preparedStatement.setInt(4, usuario.getUid());

			Mensajero.imprimirQuery(preparedStatement.toString());

			if (preparedStatement.executeUpdate() > 0) {

				try (ResultSet dbrs = preparedStatement.executeQuery(QUERY_SELECT_LAST_INSERTED_ID);) {
					dbrs.first();
					return dbrs.getInt(1);
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return 0;
		}
		return 0;
	}

	@Override
	public Boolean actualizarConsulta(ConsultaPodologica consulta, Usuario usuario, boolean modificacion) {

		if (consulta != null) {

			Date fechaProxCita = Date.valueOf(consulta.getProxCita());

			try (Connection conn = c3Pool.getConexion();
					PreparedStatement preparedStatement = conn.prepareStatement(QUERY_UPDATE_CONSULTA);) {

				preparedStatement.setDate(1, fechaProxCita);
				preparedStatement.setString(2, consulta.getPulsoPedio());
				preparedStatement.setString(3, consulta.getPulsoTibial());
				preparedStatement.setString(4, consulta.getTexturaPiel());
				preparedStatement.setString(5, consulta.getColoracionPiel());
				preparedStatement.setString(6, consulta.getTemperatura());
				preparedStatement.setString(7, consulta.getDedos());
				preparedStatement.setString(8, consulta.getObservaciones());
				preparedStatement.setString(9, consulta.getOnicopatias());
				preparedStatement.setString(10, consulta.getEvolucion());
				preparedStatement.setNull(11, Types.INTEGER);
				preparedStatement.setInt(12, usuario.getUid());
				preparedStatement.setInt(13, consulta.getId());

				Mensajero.imprimirQuery(preparedStatement.toString());

				// Si la consulta se actualiza correctamente, guardamos la consulta en la tabla
				// de citas medicas, y en caso de esta sea una modificacion de una consulta
				// previa, actualizamos la consulta anterior.
				if (preparedStatement.executeUpdate() > 0) {

					// Si es una nueva consulta
					if (!modificacion) {
						// Actualiza la tabla de citas
						actualizarSeguimiento(consulta, usuario);
					}

					// Le asigna el id de la enmienda a la consulta anterior, para asi
					// reemplazarla
					if (consulta.getEnmienda() > 0) {
						return (enmendarConsulta(consulta, usuario));
					} else {
						return true;
					}
				}
			} catch (SQLException exc) {
				bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
				return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public Boolean actualizarSeguimiento(ConsultaPodologica consulta, Usuario usuario) throws SQLException {
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_ACTUALIZAR_SEGUIMIENTO);) {

			// Parte del insert
			preparedStatement.setInt(1, consulta.getPaciente().getId());
			preparedStatement.setDate(2, Date.valueOf(consulta.getFecha()));
			preparedStatement.setDate(3, Date.valueOf(consulta.getProxCita()));
			preparedStatement.setInt(4, usuario.getUid());
			preparedStatement.setInt(5, consulta.getId());

			// Parte del update
			preparedStatement.setDate(6, Date.valueOf(consulta.getFecha()));
			preparedStatement.setDate(7, Date.valueOf(consulta.getProxCita()));
			preparedStatement.setInt(8, usuario.getUid());
			preparedStatement.setInt(9, consulta.getId());

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	@Override
	public List<ConsultaPodologica> obtenerConsultas(Paciente paciente) {
		// Contruye la lista para retornar
		List<ConsultaPodologica> listConsultas = null;

		// Buscamos el paciente
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_CONSULTAS);) {
			preparedStatement.setInt(1, paciente.getId());

			try (ResultSet dbrs = preparedStatement.executeQuery();) {
				listConsultas = new ArrayList<>();

				while (dbrs.next()) {
					listConsultas.add(new ConsultaPodologica(dbrs.getInt("id"), paciente,
							dbrs.getDate("fechaConsulta").toLocalDate(), null, dbrs.getInt("idEnmienda"),
							new Usuario(dbrs.getInt("idUsuario"), dbrs.getString("usuario"), dbrs.getString("nombre")),
							dbrs.getString("evolucion"), dbrs.getString("pulsoPedio"), dbrs.getString("pulsoTibial"),
							dbrs.getString("texturaPiel"), dbrs.getString("coloracionPiel"),
							dbrs.getString("temperatura"), dbrs.getString("dedosPie"), dbrs.getString("onicopatias"),
							dbrs.getString("observaciones")));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return listConsultas;
		}
		return listConsultas;
	}

	@Override
	public Boolean eliminarConsulta(int idConsulta) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_DELETE_CONSULTA);) {

			preparedStatement.setInt(1, idConsulta);

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	@Override
	public ConsultaPodologica obtenerConsultaID(int idConsulta) {
		ConsultaPodologica consulta = null;
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_CONSULTA_ID);) {

			preparedStatement.setInt(1, idConsulta);

			try (ResultSet dbrs = preparedStatement.executeQuery();) {

				while (dbrs.next()) {

					// Evitamos el null en proxConsulta
					LocalDate proxCita;
					if (dbrs.getDate("proxConsulta") != null) {
						proxCita = dbrs.getDate("proxConsulta").toLocalDate();
					} else {
						proxCita = LocalDate.now();
					}

					consulta = new ConsultaPodologica(dbrs.getInt("id"), null,
							dbrs.getDate("fechaConsulta").toLocalDate(), proxCita, dbrs.getInt("idEnmienda"),
							new Usuario(dbrs.getInt("idUsuario"), dbrs.getString("usuario"), dbrs.getString("nombre")),
							dbrs.getString("evolucion"), dbrs.getString("pulsoPedio"), dbrs.getString("pulsoTibial"),
							dbrs.getString("texturaPiel"), dbrs.getString("coloracionPiel"),
							dbrs.getString("temperatura"), dbrs.getString("dedosPie"), dbrs.getString("onicopatias"),
							dbrs.getString("observaciones"));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return null;
		}
		return consulta;
	}

	@Override
	public Boolean enmendarConsulta(ConsultaPodologica consulta, Usuario usuario) throws SQLException {
		// En este paso actualizamos la consulta anterior, y le añadimos un valor al
		// campo enmienda, esta es la consulta que reemplaza a la anterior.
		// Si esta era una modificacion, mantenemos el ID
		if (consulta.getEnmienda() > 0) {
			try (Connection conn = c3Pool.getConexion();
					PreparedStatement preparedStatement = conn.prepareStatement(QUERY_UPDATE_CONSULTA_ENMIENDA);) {

				preparedStatement.setInt(1, consulta.getId());
				preparedStatement.setInt(2, usuario.getUid());
				preparedStatement.setInt(3, consulta.getEnmienda());

				Mensajero.imprimirQuery(preparedStatement.toString());

				return preparedStatement.executeUpdate() > 0;
			} catch (SQLException exc) {
				bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
				return false;
			}
		}
		return false;
	}
}
