package com.aplicacion.spring.negocio.dao;

import java.time.LocalDate;
import java.util.List;

import com.aplicacion.spring.gestion.entidades.Especialidad;
import com.aplicacion.spring.gestion.entidades.Perfil;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.turnos.Turno;

public interface IRepositorioTurnos {

	/**
	 * Metodo que permite obtener una lista de las especialidades del sistema
	 * 
	 * @return Lista de especialidades
	 */
	List<Especialidad> obtenerEspecialidades();

	/**
	 * Metodo que permite obtener una lista de las especialidades del sistema
	 * 
	 * @return Lista de especialidades
	 */
	List<Perfil> obtenerPerfiles();

	/**
	 * Metodo que permite obtener una lista de los profesionales del sistema
	 * 
	 * @return Lista de profesionales
	 */
	List<Usuario> obtenerProfesionales();

	/**
	 * Metodo para recuperar los turnos para la semana y del profesional pasados por
	 * parametro
	 * 
	 * @param profesional
	 * @param fecha
	 */
	List<Turno> obtenerTurnosFecha(int profesional, LocalDate fecha);

	/**
	 * Reserva de turnos
	 * 
	 * @param t,
	 *            turno a reservar
	 * @return turno reservado
	 */
	Boolean reservarTurno(Turno turno, Usuario usuario);

	/**
	 * Libera el turno pasado
	 * 
	 * @param t,
	 *            turno
	 * @return turno limpio
	 */
	Boolean liberarTurno(Turno turno);

	/**
	 * Concluye la cita para el turno
	 * 
	 * @param t,
	 *            turno a terminar
	 * @return turno cerrado
	 */
	Boolean consumirTurno(Turno turno);

	/**
	 * Concluye la cita para el turno
	 * 
	 * @param t,
	 *            turno a terminar
	 * @return turno cerrado
	 */
	Boolean cerrarTurno(Turno turno);

}