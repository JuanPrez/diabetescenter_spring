package com.aplicacion.spring.negocio.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaMedicina;
import com.aplicacion.spring.utilitarios.C3Pool;

@Repository("ConsultaMedicaDAO")
public class RepositorioMedicina implements IRepositorioConsulta<ConsultaMedicina> {

	@Autowired
	C3Pool c3Pool;

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(RepositorioMedicina.class);

	private static final String DB_VISTA = "vw_lista_hc_clinica";
	private static final String DB_CONSULTA = "pac_hc_clinica";
	private static final String DB_PROXCITA = "pac_proxcita_medica";

	private static final String QUERY_LAST_INSERTED_ID = "SELECT LAST_INSERT_ID();";

	private static final String QUERY_SELECT_CONSULTA_ID = "SELECT id, idpaciente, fechaConsulta, proxConsulta, peso, diametroCintura, presionSistolica, presionDiastolica, "
			+ "pulso, motivoConsulta, diagnostico, tratamiento, evolucion, complementarios, idEnmienda, tipoConsulta, idUsuario, usuario, nombre FROM "
			+ DB_VISTA + " WHERE id = ?;";

	private static final String QUERY_SELECT_CONSULTAS = "SELECT id, idpaciente, fechaConsulta, proxConsulta, peso, diametroCintura, presionSistolica, presionDiastolica, "
			+ "pulso, motivoConsulta, diagnostico, tratamiento, evolucion, complementarios, idEnmienda, tipoConsulta, idUsuario, usuario, nombre FROM "
			+ DB_VISTA + " WHERE idpaciente = ? AND idEnmienda IS NULL ORDER BY fechaConsulta DESC, id DESC;";

	private static final String QUERY_INSERT_CONSULTA = "INSERT INTO " + DB_CONSULTA
			+ "(id_hc_clinica, idpaciente, fechaConsulta, id_hc_clinica_enmienda, id_usuario, tipoConsulta) "
			+ "VALUES (default, ?, ?, ?, ?, ?);";

	private static final String QUERY_UPDATE_CONSULTA = "UPDATE " + DB_CONSULTA + " SET " + "proxConsulta=?, "
			+ "peso=?, " + "diametroCintura=?, " + "presionSistolica=?, " + "presionDiastolica=?, " + "pulso=?, "
			+ "motivoConsulta=?, " + "diagnostico=?, " + "tratamiento=?, " + "evolucion=?, " + "complementarios=?, "
			+ "id_hc_clinica_enmienda=?, " + "tipoConsulta=? " + "WHERE id_hc_clinica= ? ;";

	private static final String QUERY_DELETE_CONSULTA = "DELETE FROM " + DB_CONSULTA + " WHERE id_hc_clinica=?;";

	private static final String QUERY_UPDATE_CONSULTA_ENMIENDA = "UPDATE " + DB_CONSULTA + " SET"
			+ " id_hc_clinica_enmienda = ?, id_usuario = ? WHERE id_hc_clinica = ?;";

	private static final String QUERY_INSERT_UPDATE_SEGUIMIENTO = "INSERT INTO " + DB_PROXCITA
			+ "(idPaciente, idEspecialidad, ultConsulta, proxCita, idUsuario, idUltConsulta) "
			+ "VALUES (?, ?, ?, ?, ?, ?) " + " ON DUPLICATE KEY UPDATE "
			+ "ultConsulta = ?, proxCita = ? , idUsuario = ?, idUltConsulta = ?;";

	@Override
	public Integer nuevaConsulta(ConsultaMedicina consulta, Paciente paciente, Usuario usuario) {
		Date fechaConsulta;
		// Si esta es una enmienda, no cambia la fecha de la consulta
		if (consulta.getEnmienda() > 0) {
			fechaConsulta = Date.valueOf(consulta.getFecha());
		} else {
			fechaConsulta = Date.valueOf(LocalDate.now());
		}

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_INSERT_CONSULTA);) {

			preparedStatement.setInt(1, paciente.getId());
			preparedStatement.setDate(2, fechaConsulta);
			preparedStatement.setNull(3, Types.INTEGER);
			preparedStatement.setInt(4, usuario.getUid());
			preparedStatement.setInt(5, usuario.getEspecialidad().getId());

			Mensajero.imprimirQuery(preparedStatement.toString());

			if (preparedStatement.executeUpdate() > 0) {

				try (ResultSet dbrs = preparedStatement.executeQuery(QUERY_LAST_INSERTED_ID);) {
					dbrs.first();
					return dbrs.getInt(1);
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return 0;
		}
		return 0;
	}

	@Override
	public ConsultaMedicina obtenerConsultaID(int id) {
		ConsultaMedicina consulta = null;
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_CONSULTA_ID);) {

			preparedStatement.setInt(1, id);

			try (ResultSet dbrs = preparedStatement.executeQuery();) {
				while (dbrs.next()) {
					// Evitamos el null en proxConsulta
					LocalDate proxCita;
					if (dbrs.getDate("proxConsulta") != null) {
						proxCita = dbrs.getDate("proxConsulta").toLocalDate();
					} else {
						proxCita = LocalDate.now();
					}
					consulta = new ConsultaMedicina(dbrs.getInt("id"), null,
							dbrs.getDate("fechaConsulta").toLocalDate(), proxCita, dbrs.getBigDecimal("peso"),
							dbrs.getBigDecimal("diametroCintura"), dbrs.getInt("presionSistolica"),
							dbrs.getInt("presionDiastolica"), dbrs.getInt("pulso"), dbrs.getString("motivoConsulta"),
							dbrs.getString("diagnostico"), dbrs.getString("tratamiento"), dbrs.getString("evolucion"),
							dbrs.getString("complementarios"), dbrs.getInt("idEnmienda"),
							new Usuario(dbrs.getInt("idUsuario"), dbrs.getString("usuario"), dbrs.getString("nombre")),
							dbrs.getInt("tipoConsulta"));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return null;
		}
		return consulta;
	}

	@Override
	public List<ConsultaMedicina> obtenerConsultas(Paciente paciente) {
		// Contruye la lista para retornar
		List<ConsultaMedicina> listaConsultas = new ArrayList<>();

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_CONSULTAS);) {

			preparedStatement.setInt(1, paciente.getId());

			ConsultaMedicina consulta;

			try (ResultSet dbrs = preparedStatement.executeQuery();) {
				while (dbrs.next()) {
					LocalDate proxCita;
					if (dbrs.getDate("proxConsulta") != null) {
						proxCita = dbrs.getDate("proxConsulta").toLocalDate();
					} else {
						proxCita = LocalDate.now();
					}

					consulta = new ConsultaMedicina(dbrs.getInt("id"), paciente,
							dbrs.getDate("fechaConsulta").toLocalDate(), proxCita, dbrs.getBigDecimal("peso"),
							dbrs.getBigDecimal("diametroCintura"), dbrs.getInt("presionSistolica"),
							dbrs.getInt("presionDiastolica"), dbrs.getInt("pulso"), dbrs.getString("motivoConsulta"),
							dbrs.getString("diagnostico"), dbrs.getString("tratamiento"), dbrs.getString("evolucion"),
							dbrs.getString("complementarios"), dbrs.getInt("idEnmienda"),
							new Usuario(dbrs.getInt("idUsuario"), dbrs.getString("usuario"), dbrs.getString("nombre")),
							dbrs.getInt("tipoConsulta"));

					listaConsultas.add(consulta);
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return listaConsultas;
		}
		return listaConsultas;
	}

	@Override
	public Boolean actualizarConsulta(ConsultaMedicina consulta, Usuario usuario, boolean modificacion) {

		if (consulta != null) {

			Date fechaProxCita = Date.valueOf(consulta.getProxCita());

			try (Connection conn = c3Pool.getConexion();
					PreparedStatement preparedStatement = conn.prepareStatement(QUERY_UPDATE_CONSULTA);) {

				preparedStatement.setDate(1, fechaProxCita);
				preparedStatement.setBigDecimal(2, consulta.getPeso());
				preparedStatement.setBigDecimal(3, consulta.getDiam());
				preparedStatement.setInt(4, consulta.getSist());
				preparedStatement.setInt(5, consulta.getDias());
				preparedStatement.setInt(6, consulta.getPulso());
				preparedStatement.setString(7, consulta.getMotivo());
				preparedStatement.setString(8, consulta.getDiagn());
				preparedStatement.setString(9, consulta.getTrata());
				preparedStatement.setString(10, consulta.getEvoluc());
				preparedStatement.setString(11, consulta.getComple());
				preparedStatement.setNull(12, Types.INTEGER);
				preparedStatement.setInt(13, usuario.getEspecialidad().getId());
				preparedStatement.setInt(14, consulta.getId());

				Mensajero.imprimirQuery(preparedStatement.toString());

				// Si la consulta se actualiza correctamente, guardamos la consulta en la tabla
				// de citas medicas, y en caso de esta sea una modificacion de una consulta
				// previa, actualizamos la consulta anterior.
				if (preparedStatement.executeUpdate() > 0) {

					// Si es una nueva consulta
					if (modificacion) {
						// Genera una enmienda
						return enmendarConsulta(consulta, usuario);
					} else {
						// Actualiza la tabla de citas
						return actualizarSeguimiento(consulta, usuario);
					}
				}
			} catch (SQLException exc) {
				bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
				return false;
			}
		}
		return true;
	}

	@Override
	public Boolean actualizarSeguimiento(ConsultaMedicina consulta, Usuario usuario) throws SQLException {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_INSERT_UPDATE_SEGUIMIENTO);) {

			// Parte del insert
			preparedStatement.setInt(1, consulta.getPaciente().getId());
			preparedStatement.setInt(2, usuario.getEspecialidad().getId());
			preparedStatement.setDate(3, Date.valueOf(consulta.getFecha()));
			preparedStatement.setDate(4, Date.valueOf(consulta.getProxCita()));
			preparedStatement.setInt(5, usuario.getUid());
			preparedStatement.setInt(6, consulta.getId());

			// Parte del update
			preparedStatement.setDate(7, Date.valueOf(consulta.getFecha()));
			preparedStatement.setDate(8, Date.valueOf(consulta.getProxCita()));
			preparedStatement.setInt(9, usuario.getUid());
			preparedStatement.setInt(10, consulta.getId());

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	@Override
	public Boolean enmendarConsulta(ConsultaMedicina consulta, Usuario usuario) throws SQLException {
		// En este paso actualizamos la consulta anterior, y le añadimos un valor al
		// campo enmienda, esta es la consulta que reemplaza a la anterior.
		// Si esta era una modificacion, mantenemos el ID
		if (consulta.getEnmienda() > 0) {
			try (Connection conn = c3Pool.getConexion();
					PreparedStatement preparedStatement = conn.prepareStatement(QUERY_UPDATE_CONSULTA_ENMIENDA);) {

				preparedStatement.setInt(1, consulta.getId());
				preparedStatement.setInt(2, usuario.getUid());
				preparedStatement.setInt(3, consulta.getEnmienda());

				Mensajero.imprimirQuery(preparedStatement.toString());

				return preparedStatement.executeUpdate() > 0;
			} catch (SQLException exc) {
				bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
				return false;
			}
		}
		return false;
	}

	@Override
	public Boolean eliminarConsulta(int id) {
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_DELETE_CONSULTA);) {

			preparedStatement.setInt(1, id);

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

}
