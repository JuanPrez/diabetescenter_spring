package com.aplicacion.spring.negocio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.negocio.entidades.paciente.AnamnesisAlimenticia;
import com.aplicacion.spring.utilitarios.C3Pool;
import com.aplicacion.spring.utilitarios.Constantes;

@Repository
public class RepositorioAnamnesisAlimenticia {

	@Autowired
	C3Pool c3Pool;

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(RepositorioNutricion.class);

	private static final String QUERY_SELECT_ANAMNESIS = "SELECT desayuno,mediaManana,almuerzo,mediatarde,cena,notas,fechacreacion,ultimaAct,idpaciente,usuario "
			+ " FROM " + Constantes.DB_VISTA_ANAMNESIS + " WHERE idpaciente = ?";

	private static final String QUERY_INSERT_UPDATE_ANANMESIS_ALIMENTICIA = "INSERT INTO "
			+ Constantes.DB_PACIENTE_ANAMNESIS
			+ " (idpaciente, desayuno, mediaManana, almuerzo, mediatarde, cena, notas, fechacreacion, ultimaAct, idusuario, tipoRegistro)"
			+ " VALUES (?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), ?, '2') "
			+ " ON DUPLICATE KEY UPDATE"
			+ " desayuno = ?, mediaManana = ?, almuerzo = ?, mediatarde = ?, cena = ?, notas = ?,"
			+ " ultimaAct = CURRENT_TIMESTAMP(), idusuario = ?;";

	/**
	 * 
	 * 
	 * 
	 * @param idPaciente
	 * @return
	 */
	public AnamnesisAlimenticia obtenerAnamnesisAlimenticia(int idPaciente) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_SELECT_ANAMNESIS);) {

			AnamnesisAlimenticia anmnesisAlimenticia = new AnamnesisAlimenticia();

			preparedStatement.setInt(1, idPaciente);

			try (ResultSet dbrs = preparedStatement.executeQuery();) {

				// Recupera el primer registro encontrado
				if (dbrs.first()) {
					anmnesisAlimenticia.setIdPaciente(idPaciente);
					anmnesisAlimenticia.setDesayuno(dbrs.getString("desayuno"));
					anmnesisAlimenticia.setMediam(dbrs.getString("mediaManana"));
					anmnesisAlimenticia.setAlmuerzo(dbrs.getString("almuerzo"));
					anmnesisAlimenticia.setMediat(dbrs.getString("mediatarde"));
					anmnesisAlimenticia.setCena(dbrs.getString("cena"));
					anmnesisAlimenticia.setNotas(dbrs.getString("notas"));
					anmnesisAlimenticia.setFechaCreacion(dbrs.getDate("fechacreacion").toLocalDate());
					anmnesisAlimenticia.setFechaUltAct(dbrs.getDate("ultimaAct").toLocalDate());
					anmnesisAlimenticia.setUsuario(dbrs.getString("usuario"));
				} else {
					anmnesisAlimenticia = null;
				}

			}
			return anmnesisAlimenticia;
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return null;
		}
	}

	public boolean actualizarAnamnesisAlimenticia(AnamnesisAlimenticia anamnesisAlimenticia, int idUsuario) {
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn
						.prepareStatement(QUERY_INSERT_UPDATE_ANANMESIS_ALIMENTICIA);) {

			// Parte del insert
			preparedStatement.setInt(1, anamnesisAlimenticia.getIdPaciente());
			preparedStatement.setString(2, anamnesisAlimenticia.getDesayuno());
			preparedStatement.setString(3, anamnesisAlimenticia.getMediam());
			preparedStatement.setString(4, anamnesisAlimenticia.getAlmuerzo());
			preparedStatement.setString(5, anamnesisAlimenticia.getMediat());
			preparedStatement.setString(6, anamnesisAlimenticia.getCena());
			preparedStatement.setString(7, anamnesisAlimenticia.getNotas());
			preparedStatement.setInt(8, idUsuario);

			// Parte del update
			preparedStatement.setString(9, anamnesisAlimenticia.getDesayuno());
			preparedStatement.setString(10, anamnesisAlimenticia.getMediam());
			preparedStatement.setString(11, anamnesisAlimenticia.getAlmuerzo());
			preparedStatement.setString(12, anamnesisAlimenticia.getMediat());
			preparedStatement.setString(13, anamnesisAlimenticia.getCena());
			preparedStatement.setString(14, anamnesisAlimenticia.getNotas());
			preparedStatement.setInt(15, idUsuario);

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;

		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param idPaciente
	 * @return
	 */
	public boolean eliminarAnamnesisAlimenticia(int idPaciente) {
		// TODO PENDIENTE
		return false;
	}

}
