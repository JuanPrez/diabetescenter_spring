package com.aplicacion.spring.negocio.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.ExamenPaciente;
import com.aplicacion.spring.negocio.entidades.registros.PatologiaPaciente;
import com.aplicacion.spring.negocio.entidades.registros.patologias.Examen;
import com.aplicacion.spring.negocio.entidades.registros.patologias.Patologia;
import com.aplicacion.spring.negocio.entidades.registros.patologias.PatologiaEnlaceExamen;
import com.aplicacion.spring.utilitarios.C3Pool;
import com.aplicacion.spring.utilitarios.Constantes;

@Repository
public class RepositorioPatologias {

	@Autowired
	C3Pool c3Pool;

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(RepositorioPatologias.class);

	// private static final String QUERY_PATOLOGIAS_SELECT = "SELECT
	// idcom_patologcronica as id, nombre, abrev FROM "
	// + Constantes.DB_PATOLOGIA_CRONICA_PRINCIPAL + ";";

	private static final String QUERY_PATOLOGIAS_INSERT = "INSERT INTO " + Constantes.DB_PATOLOGIA_CRONICA_PRINCIPAL
			+ " (idcom_patologcronica, nombre, abrev) VALUES (default, ?, ?);";

	private static final String QUERY_PATOLOGIAS_UPDATE = "UPDATE " + Constantes.DB_PATOLOGIA_CRONICA_PRINCIPAL
			+ " SET nombre = ?, abrev = ? WHERE idcom_patologcronica = ?;";

	private static final String QUERY_PATOLOGIAS_DELETE = "DELETE FROM " + Constantes.DB_PATOLOGIA_CRONICA_PRINCIPAL
			+ " WHERE idcom_patologcronica = ?;";

	// private static final String QUERY_EXAMENES_SELECT = "SELECT
	// idcom_patologcron_ex as id, nombre FROM "
	// + Constantes.DB_PATOLOGIA_CRONICA_EXAMENES + ";";

	private static final String QUERY_EXAMENES_INSERT = "INSERT INTO " + Constantes.DB_PATOLOGIA_CRONICA_EXAMENES
			+ " (idcom_patologcron_ex, nombre) VALUES (default, ?);";

	private static final String QUERY_EXAMENES_UPDATE = "UPDATE " + Constantes.DB_PATOLOGIA_CRONICA_EXAMENES
			+ " SET nombre = ? WHERE idcom_patologcron_ex = ?;";

	private static final String QUERY_EXAMENES_DELETE = "DELETE FROM " + Constantes.DB_PATOLOGIA_CRONICA_EXAMENES
			+ " WHERE idcom_patologcron_ex = ?;";

	// private static final String QUERY_JUNCTION_PAT_EXA_SELECT = "SELECT idpatcron
	// AS id1, idpatcro_ex AS id2 FROM "
	// + Constantes.DB_PATOLOGIA_CRONICA_JUNCTION_PAT_EXAM + ";";

	private static final String QUERY_JUNCTION_PAT_EXA_INSERT = "INSERT INTO "
			+ Constantes.DB_PATOLOGIA_CRONICA_JUNCTION_PAT_EXAM
			+ " (idcom_patologcron_nx, idpatcron, idpatcro_ex) VALUES (default, ?, ?);";

	private static final String QUERY_JUNCTION_PAT_EXA_DELETE = "DELETE FROM "
			+ Constantes.DB_PATOLOGIA_CRONICA_JUNCTION_PAT_EXAM + " WHERE idpatcron = ? AND idpatcro_ex = ?;";

	private static final String QUERY_PATOLOGIAS_PACIENTE_SELECT = "SELECT A.idcom_patologcronica AS ID , A.abrev, A.nombre, B.origen "
			+ "FROM " + Constantes.DB_PATOLOGIA_CRONICA_PRINCIPAL + " AS A, " + Constantes.DB_PATOLOGIA_CRONICA_HC
			+ " AS B " + "WHERE B.idpatCron = A.idcom_patologcronica AND B.idpaciente = ?;";

	private static final String QUERY_PATOLOGIAS_PACIENTE_INSERT = "INSERT INTO " + Constantes.DB_PATOLOGIA_CRONICA_HC
			+ " (idcom_patologcron_hc, idpaciente, idpatCron, origen, idusuario, tipoRegistro) VALUES"
			+ " (default, ?, (SELECT idcom_patologcronica FROM " + Constantes.DB_PATOLOGIA_CRONICA_PRINCIPAL
			+ " WHERE nombre LIKE ? LIMIT 1), ?, ?, '2');";

	private static final String QUERY_PATOLOGIAS_PACIENTE_DELETE = "DELETE FROM " + Constantes.DB_PATOLOGIA_CRONICA_HC
			+ " WHERE idpatCron = ? AND idpaciente = ?;";

	private static final String QUERY_EXAMENES_PACIENTE_SELECT = "SELECT " + "A.idcom_patologcronica as idp, "
			+ "A.nombre AS pnombre, " + "C.idcom_patologcron_ex AS ide, " + "C.nombre AS enombre " + "FROM "
			+ Constantes.DB_PATOLOGIA_CRONICA_PRINCIPAL + " AS A, " + Constantes.DB_PATOLOGIA_CRONICA_JUNCTION_PAT_EXAM
			+ " AS B, " + Constantes.DB_PATOLOGIA_CRONICA_EXAMENES + " AS C, " + Constantes.DB_PATOLOGIA_CRONICA_HC
			+ " AS D " + "WHERE " + "A.idcom_patologcronica = B.idpatcron AND "
			+ "B.idpatcro_ex = C.idcom_patologcron_ex AND " + "A.idcom_patologcronica = D.idpatCron AND "
			+ "D.idpaciente = ?;";

	private static final String QUERY_RESULTADO_EXAMENES_PACIENTE_SELECT = "SELECT B.idpatCronEx AS id, A.idcom_patologcron_ex as idExamen, "
			+ "A.nombre as nombreExamen, B.fecha as fecha, B.resultado as resultado, "
			+ "C.userid as usuario, B.idusuario as idUsuario FROM " + Constantes.DB_PATOLOGIA_CRONICA_EXAMENES
			+ " AS A, " + Constantes.DB_PATOLOGIA_CRONICA_RESULTADOS + " AS B, " + Constantes.DB_USUARIO_PRINCIPAL
			+ " AS C WHERE A.idcom_patologcron_ex = B.idpatCronEx "
			+ "AND B.idusuario = C.idacc_usuarios AND B.idpaciente = ?"
			+ " ORDER BY A.idcom_patologcron_ex ASC, B.fecha DESC;";

	private static final String QUERY_RESULTADO_EXAMENES_PACIENTE_INSERT = "INSERT INTO "
			+ Constantes.DB_PATOLOGIA_CRONICA_RESULTADOS
			+ " (idcom_patologcron_re, idpaciente, idpatCronEx, resultado, fecha, idusuario, tipoRegistro) VALUES"
			+ " (default, ?, ?, ?, ?, ?, '2');";

	/**
	 * 
	 * Obtenemos la lista de patologias del paciente
	 * 
	 * @param pid
	 * @return Lista de patologias del paciente
	 * 
	 */
	public List<PatologiaPaciente> obtenerListaPatologiasPaciente(int pid) {
		List<PatologiaPaciente> listaPatolog = new LinkedList<>();

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_PATOLOGIAS_PACIENTE_SELECT);) {

			preparedStatement.setInt(1, pid);

			try (ResultSet dbrs = preparedStatement.executeQuery();) {

				while (dbrs.next()) {
					listaPatolog.add(new PatologiaPaciente(dbrs.getInt("ID"), dbrs.getString("nombre"),
							dbrs.getString("abrev"), dbrs.getString("origen")));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
		}
		return listaPatolog;
	}

	/**
	 * 
	 * Obtenemos la lista de examenes del paciente
	 * 
	 * @param pid
	 * @return la lista de examenes del paciente
	 * 
	 */
	public List<PatologiaEnlaceExamen> obtenerListaExamenesPaciente(int pid) {
		List<PatologiaEnlaceExamen> listaExamenes = new LinkedList<>();
		// Abrimos el conector de la base de datos

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_EXAMENES_PACIENTE_SELECT);) {

			preparedStatement.setInt(1, pid);

			try (ResultSet dbrs = preparedStatement.executeQuery();) {

				while (dbrs.next()) {
					listaExamenes.add(new PatologiaEnlaceExamen(
							new Patologia(dbrs.getInt("idp"), dbrs.getString("pnombre"), null),
							new Examen(dbrs.getInt("ide"), dbrs.getString("enombre"))));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
		}
		return listaExamenes;
	}

	/**
	 * 
	 * Obtenemos la lista de examenes pasados del paciente
	 * 
	 * @param pid
	 * @return la lista de examenes del paciente
	 * 
	 */
	public List<ExamenPaciente> obtenerListaHistExamenesPaciente(int pid) {
		List<ExamenPaciente> listaHistExamenes = new LinkedList<>();

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn
						.prepareStatement(QUERY_RESULTADO_EXAMENES_PACIENTE_SELECT);) {

			preparedStatement.setInt(1, pid);

			try (ResultSet dbrs = preparedStatement.executeQuery();) {

				while (dbrs.next()) {
					listaHistExamenes.add(new ExamenPaciente(dbrs.getInt("id"),
							new Examen(dbrs.getInt("idExamen"), dbrs.getString("nombreExamen")),
							dbrs.getDate("fecha").toLocalDate(), dbrs.getString("resultado"), dbrs.getString("usuario"),
							dbrs.getInt("idUsuario")));
				}
			}
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
		}
		return listaHistExamenes;
	}

	/**
	 * 
	 * 
	 * 
	 * @param nuevaPatologia
	 * @return
	 */
	public boolean sumarNuevaPatologiaPaciente(Paciente paciente, PatologiaPaciente nuevaPatologia, Usuario usuario) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_PATOLOGIAS_PACIENTE_INSERT);) {

			preparedStatement.setInt(1, paciente.getId());
			preparedStatement.setString(2, nuevaPatologia.getNombre());
			preparedStatement.setString(3, nuevaPatologia.getOrigen());
			preparedStatement.setInt(4, usuario.getUid());

			Mensajero.imprimirQuery(preparedStatement.toString());

			// Tiene que tirar una excepcion para que el controlador decida que hacer, o
			// returnar false al menos.
			// La excepcion parece el camino correcto
			return preparedStatement.executeUpdate() > 0;

		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	public boolean guardarNuevoExamenPaciente(Paciente paciente, ExamenPaciente examenNuevo, Usuario usuario) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn
						.prepareStatement(QUERY_RESULTADO_EXAMENES_PACIENTE_INSERT);) {

			preparedStatement.setInt(1, paciente.getId());
			preparedStatement.setInt(2, examenNuevo.getExamen().getId());
			preparedStatement.setString(3, examenNuevo.getResultado());
			preparedStatement.setDate(4, Date.valueOf(examenNuevo.getFechaExamen()));
			preparedStatement.setInt(5, usuario.getUid());

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	/**
	 * 
	 * Quita relacion Paciente-Patologia Cronica
	 * 
	 * @param pid
	 *            Int userid
	 * @param ps
	 *            String patologiaSeleccionada
	 * @return true si se pudo completar la query
	 */
	public boolean quitarRelacionPatologiaPaciente(Patologia patologia, Paciente paciente) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_PATOLOGIAS_PACIENTE_DELETE);) {

			preparedStatement.setInt(1, patologia.getId());
			preparedStatement.setInt(2, paciente.getId());

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param pid
	 * @param eid
	 */
	public boolean sumarRelacionPatExa(int pid, int eid) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_JUNCTION_PAT_EXA_INSERT);) {

			preparedStatement.setInt(1, pid);
			preparedStatement.setInt(2, eid);

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	/**
	 * 
	 * @param pid
	 * @param eid
	 */
	public boolean quitarRelacionPatExa(int pid, int eid) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_JUNCTION_PAT_EXA_DELETE);) {

			preparedStatement.setInt(1, pid);
			preparedStatement.setInt(2, eid);

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param nombre
	 * @param abrev
	 * @return
	 */
	// TODO, Deberia retornar la nueva patologia!!!!!
	public boolean sumarPatologia(String nombre, String abrev) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_PATOLOGIAS_INSERT);) {

			preparedStatement.setString(1, nombre);
			preparedStatement.setString(2, abrev);

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;

		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param id
	 * @param nombre
	 * @param abrev
	 * @return
	 */
	// TODO, Deberia retornar la nueva patologia!!!!!
	public boolean modificarPatologia(int id, String nombre, String abrev) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_PATOLOGIAS_UPDATE);) {

			preparedStatement.setString(1, nombre);
			preparedStatement.setString(2, abrev);
			preparedStatement.setInt(3, id);

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param id
	 * @return
	 */
	public boolean quitarPatologia(int id) {
		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_PATOLOGIAS_DELETE);) {

			preparedStatement.setInt(1, id);

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param descripcion
	 * @return
	 */
	public boolean sumarExamen(String descripcion) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_EXAMENES_INSERT);) {

			preparedStatement.setString(1, descripcion);

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param id
	 * @param descripcion
	 * @return
	 */
	public boolean modificarExamen(int id, String descripcion) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_EXAMENES_UPDATE);) {

			preparedStatement.setString(1, descripcion);
			preparedStatement.setInt(2, id);

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param id
	 * @return
	 */
	public boolean quitarExamen(int id) {

		try (Connection conn = c3Pool.getConexion();
				PreparedStatement preparedStatement = conn.prepareStatement(QUERY_EXAMENES_DELETE);) {

			preparedStatement.setInt(1, id);

			Mensajero.imprimirQuery(preparedStatement.toString());

			return preparedStatement.executeUpdate() > 0;

		} catch (SQLException exc) {
			bitacora.warn(Mensajero.DB_ERROR_CONEXION, exc);
			return false;
		}
	}
}
