/* ************************************************************************************* *
 *                                                                                       *
 *  Licensed to the Apache Software Foundation (ASF) under one                           *
 *  or more contributor license agreements.  See the NOTICE file                         *
 *  distributed with this work for additional information                                *
 *  regarding copyright ownership.  The ASF licenses this file                           *
 *  to you under the Apache License, Version 2.0 (the                                    *
 *  "License"); you may not use this file except in compliance                           *
 *  with the License.  You may obtain a copy of the License at                           *
 *                                                                                       *
 *    http://www.apache.org/licenses/LICENSE-2.0                                         *
 *                                                                                       *
 *  Unless required by applicable law or agreed to in writing,                           *
 *  software distributed under the License is distributed on an                          *
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY                               *
 *  KIND, either express or implied.  See the License for the                            *
 *  specific language governing permissions and limitations                              *
 *  under the License.                                                                   *
 *                                                                                       *
 * ************************************************************************************* */
package com.aplicacion.spring.negocio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.gestion.entidades.Especialidad;
import com.aplicacion.spring.gestion.entidades.Perfil;
import com.aplicacion.spring.gestion.entidades.Profesion;
import com.aplicacion.spring.negocio.entidades.registros.patologias.Examen;
import com.aplicacion.spring.negocio.entidades.registros.patologias.Patologia;
import com.aplicacion.spring.negocio.entidades.registros.patologias.PatologiaEnlaceExamen;
import com.aplicacion.spring.negocio.entidades.turnos.Horario;
import com.aplicacion.spring.utilitarios.C3Pool;
import com.aplicacion.spring.utilitarios.Constantes;

@Repository
@Scope(value = "singleton")
public class RepositorioEstatico {

	@Autowired
	C3Pool c3Pool;

	// Carga de la bitacora
	private static final Logger bitacora = Logger.getLogger(RepositorioEstatico.class);

	// Obtiene la lista de horarios
	private static final String selectHorarios = "SELECT idges_horarios as id, horario FROM "
			+ Constantes.DB_GESTION_HORARIOS + ";";

	// Obtiene la lista de perfiles
	private static final String selectPerfil = "SELECT idacc_perfil AS id, descripcion FROM "
			+ Constantes.DB_USUARIO_PERFIL + ";";

	// Obtiene la lista de profesiones
	private static final String selectProfesion = "SELECT idProfesion AS id, descripcion, acronimo  FROM "
			+ Constantes.DB_USUARIO_PROFESION + ";";

	// Obtiene la lista de profesiones
	private static final String selectEspecialidad = "SELECT idacc_perfil_especialidad AS id, descripcion FROM "
			+ Constantes.DB_USUARIO_ESPECIALIDAD + ";";

	// Obtiene la lista de patologias
	private static final String selectPatolog = "SELECT idcom_patologcronica as id, nombre, abrev FROM "
			+ Constantes.DB_PATOLOGIA_CRONICA_PRINCIPAL + ";";

	// Obtiene la lista de examenes
	private static final String selectExamenes = "SELECT idcom_patologcron_ex as id, nombre FROM "
			+ Constantes.DB_PATOLOGIA_CRONICA_EXAMENES + ";";

	// Obtiene la relacion patologias-examenes
	private static final String selectExaPatolog = "SELECT idpatcron AS id1, idpatcro_ex AS id2 FROM "
			+ Constantes.DB_PATOLOGIA_CRONICA_JUNCTION_PAT_EXAM + ";";

	// Repositorios
	private List<Horario> repositorioHorarios = new ArrayList<>();
	private List<Perfil> repositorioPerfiles = new ArrayList<>();
	private List<Profesion> repositorioProfesiones = new ArrayList<>();
	private List<Especialidad> repositorioEspecialidades = new ArrayList<>();
	private List<Patologia> repositorioPatologias = new ArrayList<>();
	private List<Examen> repositorioExamenes = new ArrayList<>();
	private List<PatologiaEnlaceExamen> repositorioEnlacesPatologiaExamen = new ArrayList<>();

	/* ----------------------------------------------------------------------- */
	/* ----------------------- LISTADO DE REPOSITORIOS ----------------------- */
	/* ----------------------------------------------------------------------- */

	public List<Perfil> getListaPerfiles() {
		return repositorioPerfiles;
	}

	public void addListaPerfiles(Perfil perfil) {
		this.repositorioPerfiles.add(perfil);
	}

	public void limpiarListaPerfiles() {
		this.repositorioPerfiles.clear();
	}

	public List<Profesion> getListaProfesiones() {
		return repositorioProfesiones;
	}

	public void addListaProfesiones(Profesion profesion) {
		this.repositorioProfesiones.add(profesion);
	}

	public void limpiarListaProfesiones() {
		this.repositorioProfesiones.clear();
	}

	public List<Especialidad> getListaEspecialidad() {
		return repositorioEspecialidades;
	}

	public void addListaEspecialidad(Especialidad le) {
		this.repositorioEspecialidades.add(le);
	}

	public void limpiarListaEspecialidad() {
		this.repositorioEspecialidades.clear();
	}

	public List<Horario> getListaHorarios() {
		return repositorioHorarios;
	}

	public void addListaHorarios(Horario horario) {
		this.repositorioHorarios.add(horario);
	}

	public void limpiarListaHorarios() {
		this.repositorioHorarios.clear();
	}

	public List<Patologia> getListaPatologias() {
		return repositorioPatologias;
	}

	public void addListaPatologias(Patologia lp) {
		this.repositorioPatologias.add(lp);
	}

	public void limpiarListaPatologias() {
		this.repositorioPatologias.clear();
	}

	public List<Examen> getListaExamenes() {
		return repositorioExamenes;
	}

	public void addListaExamenes(Examen le) {
		this.repositorioExamenes.add(le);
	}

	public void limpiarListaExamenes() {
		this.repositorioExamenes.clear();
	}

	public List<PatologiaEnlaceExamen> getListaEnlacesPatologiaExamen() {
		return repositorioEnlacesPatologiaExamen;
	}

	public void addListaEnlacesPatologiaExamen(PatologiaEnlaceExamen le) {
		this.repositorioEnlacesPatologiaExamen.add(le);
	}

	public void limpiarListaEnlacesPatologiaExamen() {
		this.repositorioEnlacesPatologiaExamen.clear();
	}

	/**
	 * Precarga de elementos comunes y repositorios
	 */
	public boolean iniciarRepositorios() {
		limpiarRepositorios();

		// Abre el conector de la base de datos
		try {
			try (Connection conn = c3Pool.getConexion();
					PreparedStatement preparedStatement = conn.prepareStatement(selectHorarios);
					ResultSet dbrs = preparedStatement.executeQuery();) {

				while (dbrs.next()) {
					this.addListaHorarios(new Horario(dbrs.getInt("id"), dbrs.getString("horario")));
				}
			}

			try (Connection conn = c3Pool.getConexion();
					PreparedStatement preparedStatement = conn.prepareStatement(selectPerfil);
					ResultSet dbrs = preparedStatement.executeQuery();) {
				while (dbrs.next()) {
					this.addListaPerfiles(new Perfil(dbrs.getInt("id"), dbrs.getString("descripcion")));
				}
			}
			try (Connection conn = c3Pool.getConexion();
					PreparedStatement preparedStatement = conn.prepareStatement(selectProfesion);
					ResultSet dbrs = preparedStatement.executeQuery();) {
				while (dbrs.next()) {
					this.addListaProfesiones(new Profesion(dbrs.getInt("id"), dbrs.getString("descripcion"),
							dbrs.getString("acronimo")));
				}
			}
			try (Connection conn = c3Pool.getConexion();
					PreparedStatement preparedStatement = conn.prepareStatement(selectEspecialidad);
					ResultSet dbrs = preparedStatement.executeQuery();) {

				while (dbrs.next()) {
					this.addListaEspecialidad(new Especialidad(dbrs.getInt("id"), dbrs.getString("descripcion")));
				}
			}
			try (Connection conn = c3Pool.getConexion();
					PreparedStatement preparedStatement = conn.prepareStatement(selectPatolog);
					ResultSet dbrs = preparedStatement.executeQuery();) {

				while (dbrs.next()) {
					this.addListaPatologias(
							new Patologia(dbrs.getInt("id"), dbrs.getString("nombre"), dbrs.getString("abrev")));
				}
			}
			try (Connection conn = c3Pool.getConexion();
					PreparedStatement preparedStatement = conn.prepareStatement(selectExamenes);
					ResultSet dbrs = preparedStatement.executeQuery();) {

				while (dbrs.next()) {
					this.addListaExamenes(new Examen(dbrs.getInt("id"), dbrs.getString("nombre")));
				}
			}
			try (Connection conn = c3Pool.getConexion();
					PreparedStatement preparedStatement = conn.prepareStatement(selectExaPatolog);
					ResultSet dbrs = preparedStatement.executeQuery();) {

				while (dbrs.next()) {
					this.addListaEnlacesPatologiaExamen(new PatologiaEnlaceExamen(
							new Patologia(dbrs.getInt("id1"), null, null), new Examen(dbrs.getInt("id2"), null)));
				}
			}

			iniciarPerfiles();
		} catch (Exception exc) {
			bitacora.warn(Mensajero.APP_REPOSITORIOS_NO_RECUPERADOS, exc);
			return false;
		}
		return true;
	}

	/**
	 * Inicia los valores de los perfiles
	 */
	private void iniciarPerfiles() {
		for (Perfil perfil : getListaPerfiles()) {
			switch (perfil.getDescripcion()) {
			case "Medicina":
				Perfil.setMedicina(perfil.getId());
				break;
			case "Nutricion":
				Perfil.setNutricion(perfil.getId());
				break;
			case "Podologia":
				Perfil.setPodologia(perfil.getId());
				break;
			case "Administrativo":
				Perfil.setAdministrativo(perfil.getId());
				break;
			case "Enfermeria":
				Perfil.setEnfermeria(perfil.getId());
				break;
			case "Otros":
				Perfil.setOtros(perfil.getId());
				break;
			case "Administrador":
				Perfil.setAdministrador(perfil.getId());
				break;
			default:
				break;
			}
		}
	}

	/**
	 * 
	 * Quita los repositorios que podrian estar cargados.
	 * 
	 */
	private void limpiarRepositorios() {
		// Limpia las listas
		if (this.getListaPatologias() != null)
			this.limpiarListaPatologias();
		if (this.getListaExamenes() != null)
			this.limpiarListaExamenes();
		if (this.getListaEnlacesPatologiaExamen() != null)
			this.limpiarListaEnlacesPatologiaExamen();
		if (this.getListaProfesiones() != null)
			this.limpiarListaProfesiones();
		if (this.getListaEspecialidad() != null)
			this.limpiarListaEspecialidad();
		if (this.getListaHorarios() != null)
			this.limpiarListaHorarios();
	}

	/* ----------------------------------------------------------------------- */
	/* ----------------------- LISTADO DE REPOSITORIOS ----------------------- */
	/* ----------------------------------------------------------------------- */

}
