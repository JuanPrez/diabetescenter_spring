package com.aplicacion.spring.negocio.dao;

import java.sql.SQLException;
import java.util.List;

import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.consultas.Consulta;

/**
 * Interfaz base para las consultas
 * 
 * @author Juan Ignacio Prez
 */
public interface IRepositorioConsulta<T extends Consulta> {

	/**
	 * Obtiene la lista de consultas para el paciente
	 * 
	 * @param paciente
	 * @return
	 */
	public List<T> obtenerConsultas(Paciente paciente);

	/**
	 * Obtiene la consulta pasando el id
	 * 
	 * @param idConsulta
	 * @return
	 */
	public T obtenerConsultaID(int idConsulta);

	/**
	 * Crea una nueva consulta y devuelve el ID de la misma
	 * 
	 * @param consulta
	 * @param paciente
	 * @param usuario
	 * @return
	 */
	public Integer nuevaConsulta(T consulta, Paciente paciente, Usuario usuario);

	/**
	 * Actualiza el contenido de la consulta pasada. \n Actualiza el seguimiento si
	 * es una nueva consulta o crea una enmienda si es una modificacion.
	 * 
	 * @param consulta
	 * @param usuario
	 * @param modificacion
	 * @return
	 */
	public Boolean actualizarConsulta(T consulta, Usuario usuario, boolean modificacion);

	/**
	 * Elimina la consulta
	 * 
	 * @param idConsulta
	 * @return
	 */
	public Boolean eliminarConsulta(int idConsulta);

	/**
	 * Asigna la enmienda para la consulta pasada
	 * 
	 * @param consulta
	 * @param usuario
	 * @return
	 * @throws SQLException
	 */
	Boolean enmendarConsulta(T consulta, Usuario usuario) throws SQLException;

	/**
	 * Actualiza la tabla de seguimiento
	 * 
	 * @param consulta
	 * @param usuario
	 * @return
	 * @throws SQLException
	 */
	Boolean actualizarSeguimiento(T consulta, Usuario usuario) throws SQLException;
}
