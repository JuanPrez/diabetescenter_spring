package com.aplicacion.spring.negocio.servicios;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.dao.RepositorioAntropometricos;
import com.aplicacion.spring.negocio.dao.RepositorioPodologia;
import com.aplicacion.spring.negocio.entidades.paciente.Antropometricos;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.consultas.Consulta;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaPodologica;

@Service
public class ServiciosPodologia implements IServiciosConsultas<ConsultaPodologica>, IServiciosPodologia {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(ServiciosPodologia.class);

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	RepositorioPodologia repositorioPodologia;
	@Autowired
	RepositorioAntropometricos repositorioAntropometricos;

	@Override
	public Antropometricos obtenerAntropometricos(int idPaciente) {
		return repositorioAntropometricos.obtenerAntropometricos(idPaciente);
	}

	@Override
	public Boolean guardarAntropometricos(Antropometricos datosAntropometricos, int idPaciente, int idUsuario) {
		return repositorioAntropometricos.actualizarAntropometricos(datosAntropometricos, idPaciente, idUsuario);
	}

	@Override
	public Boolean quitarAntropometricos(int idPaciente) {
		return repositorioAntropometricos.eliminarAntropometricos(idPaciente);
	}

	@Override
	public ConsultaPodologica obtenerConsultaID(int idConsulta) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ConsultaPodologica> obtenerConsultas(Paciente paciente) {
		List<ConsultaPodologica> listaConsultas = new ArrayList<>();
		for (Consulta consulta : repositorioPodologia.obtenerConsultas(paciente)) {
			listaConsultas.add((ConsultaPodologica) consulta);
		}
		return listaConsultas;
	}

	@Override
	public Integer nuevaConsulta(ConsultaPodologica consulta, Paciente paciente, Usuario usuario) {
		return repositorioPodologia.nuevaConsulta(consulta, paciente, usuario);
	}

	@Override
	public Boolean actualizarConsulta(ConsultaPodologica consulta, Usuario usuario, boolean modificacion) {
		return repositorioPodologia.actualizarConsulta(consulta, usuario, modificacion);
	}

	@Override
	public Boolean eliminarConsulta(int id) {
		return repositorioPodologia.eliminarConsulta(id);
	}

}
