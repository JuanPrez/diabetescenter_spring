package com.aplicacion.spring.negocio.servicios;

import com.aplicacion.spring.negocio.entidades.paciente.AnamnesisAlimenticia;

public interface IServiciosNutricion {
	public AnamnesisAlimenticia obtenerAnamnesisAlimenticia(int idPaciente);
	public Boolean guardarAnamnesisAlimenticia(AnamnesisAlimenticia anamnesisAlimenticia, int idUsuario);
	public Boolean quitarAnamnesisAlimenticia(int idPaciente);

}