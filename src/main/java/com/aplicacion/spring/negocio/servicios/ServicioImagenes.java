package com.aplicacion.spring.negocio.servicios;

import java.io.InputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.dao.RepositorioImagenes;
import com.aplicacion.spring.negocio.entidades.registros.ImagenPaciente;

@Service
public class ServicioImagenes {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(ServicioImagenes.class);

	@Autowired
	RepositorioImagenes repositorioImagenes;

	public int contarImagenesPaciente(int idPaciente) {
		return repositorioImagenes.contarImagenesPaciente(idPaciente);
	}

	public List<ImagenPaciente> recuperarListaImagenesPaciente(int idPaciente, int limiteMenor, int limiteMayor) {
		return repositorioImagenes.recuperarListaImagenesPaciente(idPaciente, limiteMenor, limiteMayor);
	}

	public boolean eliminarImagen(ImagenPaciente imagen) {
		return repositorioImagenes.eliminarImagen(imagen);
	}

	public ImagenPaciente guardarImagenNueva(String fileName, InputStream streamImagenPaciente, long alenght, int idPaciente,
			int tipoConsulta, int idconsulta, String coment, Usuario usuario) {
		return repositorioImagenes.guardarImagenNueva(fileName, streamImagenPaciente, alenght, idPaciente, tipoConsulta,
				idconsulta, coment, usuario);
	}

	public ImagenPaciente actualizarComentarioImagen(ImagenPaciente imagen, String comentario, Usuario usuario) {
		return repositorioImagenes.actualizarComentarioImagen(imagen, comentario, usuario);
	}
}
