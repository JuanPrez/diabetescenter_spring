package com.aplicacion.spring.negocio.servicios;

import java.time.LocalDate;
import java.util.List;

import com.aplicacion.spring.gestion.entidades.Especialidad;
import com.aplicacion.spring.gestion.entidades.Perfil;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.turnos.Turno;

public interface IServiciosTurnos {

	List<Turno> obtenerTurnos(int idProfesional, LocalDate fecha);

	List<Especialidad> obtenerEspecialidades();

	List<Perfil> obtenerPerfiles();

	List<Usuario> obtenerProfesionales();

	Boolean liberar(Turno turno);

	Boolean reservar(Turno turno, Usuario usuario);

	Boolean consumir(Turno turno);

	Boolean cerrar(Turno turno);

}