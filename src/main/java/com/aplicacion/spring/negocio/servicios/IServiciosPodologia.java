package com.aplicacion.spring.negocio.servicios;

import com.aplicacion.spring.negocio.entidades.paciente.Antropometricos;

public interface IServiciosPodologia {

	Antropometricos obtenerAntropometricos(int idPaciente);

	Boolean guardarAntropometricos(Antropometricos datosAntropometricos, int idPaciente, int idUsuario);

	Boolean quitarAntropometricos(int idPaciente);

}