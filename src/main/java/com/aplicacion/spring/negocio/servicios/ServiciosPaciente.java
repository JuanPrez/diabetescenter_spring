package com.aplicacion.spring.negocio.servicios;

import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.negocio.dao.RepositorioPaciente;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.EstadisticasPesoIMC;
import com.aplicacion.spring.negocio.entidades.registros.EstadisticasPulsoPresion;

@Service
public class ServiciosPaciente implements IServiciosPaciente {
	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(ServiciosPaciente.class);

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	RepositorioPaciente repositorioPaciente;

	/**
	 * Busqueda de pacientes por nombre (y apellido)
	 * 
	 * @param apellido
	 * @param nombre
	 * @return
	 */
	@Override
	public List<Paciente> obtenerPacientesPorNombre(String apellido, String nombre) {
		return repositorioPaciente.obtenerPacientesPorNombre(apellido, nombre);
	}

	/**
	 * Busqueda del paciente por DNI
	 * 
	 * @param buscarDNI
	 * @return paciente
	 */
	@Override
	public List<Paciente> obtenerPacientePorDNI(String buscarDNI) {
		List<Paciente> lista = new ArrayList<Paciente>();
		lista.add(repositorioPaciente.obtenerPacientePorDNI(buscarDNI));
		return lista;
	}

	/**
	 * Busqueda del paciente por ID
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public List<Paciente> obtenerPacientePorID(Integer id) {
		List<Paciente> lista = new ArrayList<Paciente>();
		lista.add(repositorioPaciente.obtenerPacientePorID(id));
		return lista;
	}

	/**
	 * Guardar datos del paciente
	 * 
	 * @param Paciente
	 *            a guardar
	 * @return
	 */
	@Override
	public Integer nuevoPaciente(Paciente paciente) {
		return repositorioPaciente.nuevoPaciente(paciente, aplicacion.getSesion().getUsuario());
	}

	/**
	 * Metodo para guardar la imagen actual del paciente.
	 * 
	 * @param sI,
	 *            Archivo de imagen
	 * @param length,
	 *            longitud del archivo
	 * @param paciente,
	 *            paciente a quien se le asigna
	 * @param absolutePath,
	 *            ubicacion del archivo
	 * @return
	 */
	@Override
	public Boolean guardarImagenPaciente(InputStream archivo, long longitud, String nombre, int pacienteId,
			int usuarioId) {
		return repositorioPaciente.guardarImagenPaciente(archivo, longitud, nombre, pacienteId, usuarioId);
	}

	/**
	 * Busqueda de listado de pacientes por fecha de nacimiento
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public List<Paciente> obtenerPacientesPorFechaNacimiento(LocalDate fechaInicial) {
		return repositorioPaciente.obtenerPacientesPorFechaNacimiento(fechaInicial);
	}

	/**
	 * Busqueda citas de pacientes por fecha
	 * 
	 * @param fechaInicial
	 * @return
	 */
	@Override
	public List<Paciente> obtenerPacientesPorCitas(LocalDate fechaInicial) {
		return repositorioPaciente.obtenerPacientesPorCitas(fechaInicial);
	}

	/**
	 * Obtener estadisticas para grafica de peso, diam abdominal e IMC
	 * 
	 * @param idPaciente
	 * @return
	 */
	@Override
	public List<EstadisticasPesoIMC> obtenerEstadisticasPesoIMC(Integer idPaciente) {
		return repositorioPaciente.obtenerEstadisticasPesoIMC(idPaciente);
	}

	/**
	 * Obtener estadisticas para grafica de pulso, sistolica y diastolica
	 * 
	 * @param idPaciente
	 * @return
	 */
	@Override
	public List<EstadisticasPulsoPresion> obtenerEstadisticasPulsoPresion(Integer idPaciente) {
		return repositorioPaciente.obtenerEstadisticasPulsoPresion(idPaciente);
	}

	/**
	 * Construye un nuevo paciente para asignarlo al turno
	 * 
	 * @param apellido
	 * @param nombre
	 * @param dni
	 * @param usuario
	 * @return Paciente
	 */
	@Override
	public Paciente nuevoPaciente(String apellido, String nombre, String dni) {
		Paciente paciente;
		int id = repositorioPaciente.nuevoPaciente(apellido, nombre, dni, aplicacion.getSesion().getUsuario());
		if (id > 0) {
			paciente = new Paciente(id, apellido, nombre, dni);
		} else {
			paciente = null;
		}
		return paciente;
	}
}
