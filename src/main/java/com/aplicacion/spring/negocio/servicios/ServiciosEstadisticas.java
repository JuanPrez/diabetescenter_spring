package com.aplicacion.spring.negocio.servicios;

import java.time.LocalDate;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aplicacion.spring.negocio.dao.RepositorioEstadisticas;
import com.aplicacion.spring.negocio.entidades.registros.consultas.EstadisticasConsultaMedicina;
import com.aplicacion.spring.negocio.entidades.registros.patologias.EstadisticasPatologia;

@Service
public class ServiciosEstadisticas {

	@Autowired
	RepositorioEstadisticas repositorioEstadisticas;

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(ServiciosEstadisticas.class);

	/**
	 * 
	 * 
	 * @param idPaciente
	 * @param fechaDesde
	 * @return
	 */
	public List<EstadisticasConsultaMedicina> obtenerEstadisticasConsultasPaciente(int idPaciente, LocalDate fechaDesde) {
		return repositorioEstadisticas.obtenerEstadisticasConsultasPaciente(idPaciente, fechaDesde);
	}

	/**
	 * 
	 * 
	 * @param idPaciente
	 * @param patologia
	 * @param fechaDesde
	 * @return
	 */
	public List<EstadisticasPatologia> obtenerEstadisticasPatologiasPaciente(int idPaciente, String patologia,
			LocalDate fechaDesde) {
		return repositorioEstadisticas.obtenerEstadisticasPatologiasPaciente(idPaciente, patologia, fechaDesde);
	}
}
