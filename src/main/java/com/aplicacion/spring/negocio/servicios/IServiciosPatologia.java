package com.aplicacion.spring.negocio.servicios;

import java.util.List;

import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.ExamenPaciente;
import com.aplicacion.spring.negocio.entidades.registros.PatologiaPaciente;
import com.aplicacion.spring.negocio.entidades.registros.patologias.Patologia;
import com.aplicacion.spring.negocio.entidades.registros.patologias.PatologiaEnlaceExamen;

public interface IServiciosPatologia {
	List<PatologiaPaciente> obtenerPatologiasPaciente(int pid);

	List<PatologiaEnlaceExamen> obtenerExamenesPaciente(int pid);

	List<ExamenPaciente> obtenerHistExamenesPaciente(int pid);

	Boolean sumarNuevaPatologiaPaciente(Paciente paciente, PatologiaPaciente patologia, Usuario usuario);

	Boolean guardarNuevoExamenPaciente(Paciente paciente, ExamenPaciente examen, Usuario usuario);

	Boolean quitarRelacionPatologiaPaciente(Patologia patologia, Paciente paciente);

	Boolean sumarRelacionPatExa(int pid, int eid);

	Boolean quitarRelacionPatExa(int pid, int eid);

	Boolean sumarPatologia(String nombre, String abrev);

	Boolean modificarPatologia(int id, String nombre, String abrev);

	Boolean quitarPatologia(int id);

	Boolean sumarExamen(String descripcion);

	Boolean modificarExamen(int id, String descripcion);

	Boolean quitarExamen(int id);

}