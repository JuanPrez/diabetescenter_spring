package com.aplicacion.spring.negocio.servicios;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.dao.RepositorioMedicina;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaMedicina;

@Service
public class ServiciosMedicina implements IServiciosConsultas<ConsultaMedicina> {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(ServiciosMedicina.class);

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	RepositorioMedicina medicinaDAO;

	@Override
	public ConsultaMedicina obtenerConsultaID(int idConsulta) {
		ConsultaMedicina consulta = medicinaDAO.obtenerConsultaID(idConsulta);
		if (aplicacion.getSesion().getPaciente() != null) {
			consulta.setPaciente(aplicacion.getSesion().getPaciente());
		}
		return consulta;
	}

	@Override
	public Integer nuevaConsulta(ConsultaMedicina consulta, Paciente paciente, Usuario usuario) {
		return medicinaDAO.nuevaConsulta(consulta, paciente, usuario);
	}

	@Override
	public Boolean actualizarConsulta(ConsultaMedicina consulta, Usuario usuario, boolean modificacion) {
		return medicinaDAO.actualizarConsulta(consulta, usuario, modificacion);
	}

	@Override
	public Boolean eliminarConsulta(int idConsulta) {
		return medicinaDAO.eliminarConsulta(idConsulta);
	}

	@Override
	public List<ConsultaMedicina> obtenerConsultas(Paciente paciente) {
		return medicinaDAO.obtenerConsultas(paciente);
	}

}
