package com.aplicacion.spring.negocio.servicios;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.dao.RepositorioAnamnesisAlimenticia;
import com.aplicacion.spring.negocio.dao.RepositorioNutricion;
import com.aplicacion.spring.negocio.entidades.paciente.AnamnesisAlimenticia;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.consultas.Consulta;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaNutricion;

@Service
public class ServiciosNutricion implements IServiciosConsultas<ConsultaNutricion>, IServiciosNutricion {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(ServiciosNutricion.class);

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	RepositorioNutricion repositorioNutricion;
	@Autowired
	RepositorioAnamnesisAlimenticia repositorioAnamnesis;

	@Override
	public AnamnesisAlimenticia obtenerAnamnesisAlimenticia(int idPaciente) {
		return repositorioAnamnesis.obtenerAnamnesisAlimenticia(idPaciente);
	}

	@Override
	public Boolean guardarAnamnesisAlimenticia(AnamnesisAlimenticia anamnesisAlimenticia, int idUsuario) {
		return repositorioAnamnesis.actualizarAnamnesisAlimenticia(anamnesisAlimenticia, idUsuario);
	}

	@Override
	public Boolean quitarAnamnesisAlimenticia(int idPaciente) {
		return repositorioAnamnesis.eliminarAnamnesisAlimenticia(idPaciente);
	}

	@Override
	public List<ConsultaNutricion> obtenerConsultas(Paciente paciente) {
		List<ConsultaNutricion> listaConsultas = new ArrayList<>();
		for (Consulta consulta : repositorioNutricion.obtenerConsultas(paciente)) {
			listaConsultas.add((ConsultaNutricion) consulta);
		}
		return listaConsultas;
	}

	@Override
	public Integer nuevaConsulta(ConsultaNutricion consulta, Paciente paciente, Usuario usuario) {
		return repositorioNutricion.nuevaConsulta(consulta, paciente, usuario);
	}

	@Override
	public Boolean actualizarConsulta(ConsultaNutricion consulta, Usuario usuario, boolean modificacion) {
		return repositorioNutricion.actualizarConsulta(consulta, usuario, modificacion);
	}

	@Override
	public Boolean eliminarConsulta(int id) {
		return repositorioNutricion.eliminarConsulta(id);
	}

	@Override
	public ConsultaNutricion obtenerConsultaID(int idConsulta) {
		return repositorioNutricion.obtenerConsultaID(idConsulta);
	}

}
