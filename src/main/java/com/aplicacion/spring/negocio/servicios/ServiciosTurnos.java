package com.aplicacion.spring.negocio.servicios;

import java.time.LocalDate;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aplicacion.spring.gestion.entidades.Especialidad;
import com.aplicacion.spring.gestion.entidades.Perfil;
import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.dao.RepositorioTurnos;
import com.aplicacion.spring.negocio.entidades.turnos.Turno;

@Service
public class ServiciosTurnos implements IServiciosTurnos {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(ServiciosTurnos.class);

	@Autowired
	RepositorioTurnos turnosDAO;

	@Override
	public List<Turno> obtenerTurnos(int idProfesional, LocalDate fecha) {
		return turnosDAO.obtenerTurnosFecha(idProfesional, fecha);
	}

	@Override
	public List<Especialidad> obtenerEspecialidades() {
		return turnosDAO.obtenerEspecialidades();
	}

	@Override
	public List<Perfil> obtenerPerfiles() {
		return turnosDAO.obtenerPerfiles();
	}

	@Override
	public List<Usuario> obtenerProfesionales() {
		return turnosDAO.obtenerProfesionales();
	}

	/**
	 * 
	 * Elimina la reservacion en la base de datos, si es exitosa, cambia el estado
	 * del turno a LIBRE(1)
	 * 
	 * @param turno
	 * @return
	 */
	@Override
	public Boolean liberar(Turno turno) {
		if (turnosDAO.liberarTurno(turno)) {
			turno.liberar();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * Crea la reservacion en la base de datos, si es exitosa, cambia el estado del
	 * turno a RESERVADO(2)
	 * 
	 * @param turno
	 * @param usuario
	 * @return
	 */
	@Override
	public Boolean reservar(Turno turno, Usuario usuario) {
		if (turnosDAO.reservarTurno(turno, usuario)) {
			turno.reservar();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * Cambia el estado a CONSUMIDO (3) en la base de datos, si es exitosa, cambia
	 * el estado en el turno pasado
	 * 
	 * @param turno
	 * @param usuario
	 * @return
	 */
	@Override
	public Boolean consumir(Turno turno) {
		if (turnosDAO.consumirTurno(turno)) {
			turno.consumir();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * Cambia el estado a CERRADO (4) en la base de datos, si es exitosa, cambia el
	 * estado en el turno pasado
	 * 
	 * @param turno
	 * @return
	 */
	@Override
	public Boolean cerrar(Turno turno) {
		if (turnosDAO.cerrarTurno(turno)) {
			turno.cerrar();
			return true;
		} else {
			return false;
		}
	}

}
