package com.aplicacion.spring.negocio.servicios;

import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;

import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.EstadisticasPesoIMC;
import com.aplicacion.spring.negocio.entidades.registros.EstadisticasPulsoPresion;

public interface IServiciosPaciente {
	List<Paciente> obtenerPacientePorID(Integer id);

	List<Paciente> obtenerPacientePorDNI(String buscarDNI);

	List<Paciente> obtenerPacientesPorNombre(String apellido, String nombre);

	List<Paciente> obtenerPacientesPorFechaNacimiento(LocalDate fechaInicial);

	List<Paciente> obtenerPacientesPorCitas(LocalDate fechaInicial);

	List<EstadisticasPesoIMC> obtenerEstadisticasPesoIMC(Integer idPaciente);

	List<EstadisticasPulsoPresion> obtenerEstadisticasPulsoPresion(Integer idPaciente);

	Integer nuevoPaciente(Paciente paciente);

	Paciente nuevoPaciente(String apellido, String nombre, String dni);

	Boolean guardarImagenPaciente(InputStream archivo, long longitud, String nombre, int pacienteId,
			int usuarioId);

}