package com.aplicacion.spring.negocio.servicios;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.dao.RepositorioPatologias;
import com.aplicacion.spring.negocio.dao.RepositorioEstatico;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.ExamenPaciente;
import com.aplicacion.spring.negocio.entidades.registros.PatologiaPaciente;
import com.aplicacion.spring.negocio.entidades.registros.patologias.Patologia;
import com.aplicacion.spring.negocio.entidades.registros.patologias.PatologiaEnlaceExamen;

@Service
public class ServiciosPatologia implements IServiciosPatologia {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(ServiciosPatologia.class);

	@Autowired
	RepositorioEstatico repositorioEstatico;
	@Autowired
	RepositorioPatologias repositorioPatologia;

	// TODO TODO TODO
	public List<String> getStringPatolog() {
		List<String> stringPat = new ArrayList<>();
		for (Patologia lp : repositorioEstatico.getListaPatologias()) {
			stringPat.add(lp.getNombre());
		}
		return stringPat;
	}

	/**
	 * 
	 * 
	 * 
	 * @param pid
	 * @return
	 */
	@Override
	public List<PatologiaPaciente> obtenerPatologiasPaciente(int pid) {
		return repositorioPatologia.obtenerListaPatologiasPaciente(pid);
	}

	/**
	 * 
	 * 
	 * 
	 * @param pid
	 * @return
	 */
	@Override
	public List<PatologiaEnlaceExamen> obtenerExamenesPaciente(int pid) {
		return repositorioPatologia.obtenerListaExamenesPaciente(pid);
	}

	/**
	 * 
	 * 
	 * 
	 * @param pid
	 * @return
	 */
	@Override
	public List<ExamenPaciente> obtenerHistExamenesPaciente(int pid) {
		return repositorioPatologia.obtenerListaHistExamenesPaciente(pid);
	}

	/**
	 * 
	 * 
	 * 
	 * @param patologia
	 * @return
	 */
	@Override
	public Boolean sumarNuevaPatologiaPaciente(Paciente paciente, PatologiaPaciente patologia, Usuario usuario) {
		return repositorioPatologia.sumarNuevaPatologiaPaciente(paciente, patologia, usuario);
	}

	/**
	 * 
	 * 
	 * 
	 * @param examen
	 * @return
	 */
	@Override
	public Boolean guardarNuevoExamenPaciente(Paciente paciente, ExamenPaciente examen, Usuario usuario) {
		return repositorioPatologia.guardarNuevoExamenPaciente(paciente, examen, usuario);
	}

	/**
	 * 
	 * 
	 * 
	 * @param patologia
	 * @return
	 */
	@Override
	public Boolean quitarRelacionPatologiaPaciente(Patologia patologia, Paciente paciente) {
		return repositorioPatologia.quitarRelacionPatologiaPaciente(patologia, paciente);
	}

	/**
	 * 
	 * 
	 * 
	 * @param pid
	 * @param eid
	 * @return
	 */
	@Override
	public Boolean sumarRelacionPatExa(int pid, int eid) {
		return repositorioPatologia.sumarRelacionPatExa(pid, eid);
	}

	/**
	 * 
	 * 
	 * 
	 * @param pid
	 * @param eid
	 * @return
	 */
	@Override
	public Boolean quitarRelacionPatExa(int pid, int eid) {
		return repositorioPatologia.quitarRelacionPatExa(pid, eid);
	}

	/**
	 * 
	 * 
	 * 
	 * @param nombre
	 * @param abrev
	 * @return
	 */
	@Override
	public Boolean sumarPatologia(String nombre, String abrev) {
		return repositorioPatologia.sumarPatologia(nombre, abrev);
	}

	/**
	 * 
	 * 
	 * 
	 * @param id
	 * @param nombre
	 * @param abrev
	 * @return
	 */
	@Override
	public Boolean modificarPatologia(int id, String nombre, String abrev) {
		return repositorioPatologia.modificarPatologia(id, nombre, abrev);
	}

	/**
	 * 
	 * 
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public Boolean quitarPatologia(int id) {
		return repositorioPatologia.quitarPatologia(id);
	}

	/**
	 * 
	 * 
	 * 
	 * @param descripcion
	 * @return
	 */
	@Override
	public Boolean sumarExamen(String descripcion) {
		return repositorioPatologia.sumarExamen(descripcion);
	}

	/**
	 *
	 * 
	 * 
	 * @param id
	 * @param descripcion
	 * @return
	 */
	@Override
	public Boolean modificarExamen(int id, String descripcion) {
		return repositorioPatologia.modificarExamen(id, descripcion);
	}

	/**
	 * 
	 * 
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public Boolean quitarExamen(int id) {
		return repositorioPatologia.quitarExamen(id);
	}

}
