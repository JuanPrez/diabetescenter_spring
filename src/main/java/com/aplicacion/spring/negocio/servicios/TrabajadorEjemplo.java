package com.aplicacion.spring.negocio.servicios;

import javafx.concurrent.Task;

public class TrabajadorEjemplo {

	public void correr() {
		Task<Void> trabajador = new Trabajador("www.google.com");

		Thread hilo = new Thread(trabajador);

		hilo.start();

	}

	public static void main(String[] args) {

		TrabajadorEjemplo ejemplo = new TrabajadorEjemplo();

		ejemplo.correr();

		System.out.println("Hilo");
	}

	public class Trabajador extends Task<Void> {

		private String url;

		public Trabajador(String url) {
			this.url = url;
		}

		@Override
		protected Void call() throws Exception {
			System.out.println(url);
			return null;
		}

		@Override
		protected void failed() {
			System.out.println("Failed");
		}

		@Override
		protected void succeeded() {
			System.out.println("Succeeded");

		}
	}
}
