package com.aplicacion.spring.negocio.controladores;

import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.gestion.controladores.ControladorHistoriaClinica;
import com.aplicacion.spring.gestion.entidades.Perfil;
import com.aplicacion.spring.negocio.entidades.paciente.AnamnesisAlimenticia;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.consultas.Consulta;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaNutricion;
import com.aplicacion.spring.negocio.servicios.ServiciosNutricion;
import com.aplicacion.spring.utilitarios.Constantes;
import com.aplicacion.spring.utilitarios.controladores.Util;

import de.felixroske.jfxsupport.FXMLController;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

@FXMLController
public class ControladorAntecedentesNutricion implements Initializable {

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	ServiciosNutricion serviciosConsultas;

	// Panel superior
	@FXML
	private BorderPane panelCentral;
	@FXML
	public Button botonGuardar;
	@FXML
	public Button botonDescartar;
	@FXML
	public Button botonNueva;
	@FXML
	public Button botonModificar;
	@FXML
	private Label idConsulta;

	// Anamnesis
	@FXML
	private VBox anamnesisVBox;
	@FXML
	private TextField campoUltUsuario;
	@FXML
	private TextField campoUltActualizacion;
	@FXML
	private TextArea areaTextoDesayuno;
	@FXML
	private TextArea areaTextoMediaManana;
	@FXML
	private TextArea areaTextoAlmuerzo;
	@FXML
	private TextArea areaTextoMediaTarde;
	@FXML
	private TextArea areaTextoCena;
	@FXML
	private TextArea areaTextoNotasAnamnesis;

	// Consulta
	@FXML
	private TextField campoPeso;
	@FXML
	private VBox columnaProxCita;
	@FXML
	private DatePicker proxCita;
	@FXML
	private Label etiquetaIMC;
	@FXML
	private TextField campoDiamCintura;
	@FXML
	private TextArea areaTextoTratamiento;
	@FXML
	public TableView<ConsultaNutricion> tablaConsultasPrevias;
	@FXML
	TableColumn<ConsultaNutricion, Integer> columnaID;
	@FXML
	TableColumn<ConsultaNutricion, LocalDate> columnaFecha;
	@FXML
	TableColumn<ConsultaNutricion, BigDecimal> columnaPeso;
	@FXML
	TableColumn<ConsultaNutricion, BigDecimal> columnaPerAbd;
	@FXML
	TableColumn<ConsultaNutricion, String> columnaTrata;
	@FXML
	TableColumn<ConsultaNutricion, String> columnaUsuario;

	ControladorHistoriaClinica controladorHistoriaClinica;
	ConsultaNutricion consulta;
	AnamnesisAlimenticia anamnesisAlimenticia = null;
	SimpleBooleanProperty editable = new SimpleBooleanProperty(false);
	ChangeListener<Paciente> listenerPaciente = null;
	ChangeListener<Boolean> listenerEditable = null;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// Listener de la propiedad editable de la propia pantalla
		listenerEditable = new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean estadoPrevio,
					Boolean estadoActual) {
				if (estadoActual)
					permitirEdicion(tablaConsultasPrevias.getSelectionModel().getSelectedItem());
				else
					bloquearEdicion();
			}
		};
		editable.addListener(listenerEditable);

		// En caso de que el listener no este activo, hace una llamada manual
		if (listenerPaciente == null && aplicacion.getSesion().getPaciente() != null)
			refrescarPantalla();

		// Implementation del listener
		listenerPaciente = new ChangeListener<Paciente>() {
			@Override
			public void changed(ObservableValue<? extends Paciente> observable, Paciente pacientePrevio,
					Paciente pacienteActual) {
				if (pacienteActual != null) {
					refrescarPantalla();
				} else {
					limpiarDatosConsultas();
					limpiarTablaConsultas();
				}
			}
		};
		aplicacion.getSesion().pacienteProperty().addListener(listenerPaciente);

		inicializarPantalla();
	}

	private void inicializarPantalla() {
		// Instanciacion de los controladores relacionados
		controladorHistoriaClinica = aplicacion.getContexto().getBean(ControladorHistoriaClinica.class);

		// Configuracion de los botones
		verBoton(botonNueva, editable.get(), Perfil.getNutricion());
		verBoton(botonModificar, editable.get(), Perfil.getNutricion());
		botonNueva.disableProperty().bind(controladorHistoriaClinica.consultaProperty().isNotNull());
		botonGuardar.visibleProperty().bind(editable);
		botonDescartar.visibleProperty().bind(editable);
		tablaConsultasPrevias.disableProperty().bind(editable);

		// Disparadores automaticos
		Util.disparadorCalculadoraIMC(etiquetaIMC,
				aplicacion.getSesion().getPaciente().getDatosClinicos().getTalla(), campoPeso);
		disparadorSeleccionConsultasPrevias();

	}

	private void verBoton(Button boton, boolean edicion, int perfil) {
		boton.setVisible(!edicion && aplicacion.getSesion().getUsuario().getPerfil().getId() == perfil);
	}

	private void refrescarPantalla() {
		// Limpia el formulario
		limpiarDatosConsultas();
		limpiarTablaConsultas();

		limpiarAnamnesisAlimenticia();
		reiniciarEditable();
		// Evita su modificación
		bloquearEdicion();

		// *** Se agregan los listeners ***
		// Si la anamnesis es nula, buscamos uno con el servicio,
		// Si aun no encontramos nada, creamos una nueva y vacia.
		anamnesisAlimenticia = serviciosConsultas
				.obtenerAnamnesisAlimenticia(aplicacion.getSesion().getPaciente().getId());
		if (anamnesisAlimenticia == null) {
			anamnesisAlimenticia = new AnamnesisAlimenticia();
		}

		aplicacion.getSesion().getPaciente().setAnamnesisAlimenticia(anamnesisAlimenticia);

		// Busca las consultas previas
		refrescarListaConsultas();

		if (!tablaConsultasPrevias.getItems().isEmpty())
			Platform.runLater(() -> tablaConsultasPrevias.getSelectionModel().select(0));
		else
			limpiarDatosConsultas();

		// Y la Anamnesis Alimenticia
		mostrarAnamnesisAlimenticia(anamnesisAlimenticia);

		// Selecciona la ultima consulta
		Platform.runLater(() -> tablaConsultasPrevias.getSelectionModel().clearAndSelect(0));
	}

	/**
	 * Obtiene las consultas previas
	 * 
	 * @param listaConsultas
	 */
	private void mostrarConsultasPrevias(List<ConsultaNutricion> listaConsultas) {
		if (listaConsultas == null)
			listaConsultas = new ArrayList<>();
		else
			tablaConsultasPrevias.setItems(FXCollections.observableArrayList(listaConsultas));
	}

	/**
	 * Refresca la lista de consultas
	 */
	public void refrescarListaConsultas() {
		mostrarConsultasPrevias(serviciosConsultas.obtenerConsultas(aplicacion.getSesion().getPaciente()));
	}

	/**
	 * Limpia el formulario para consultas
	 */
	private void limpiarDatosConsultas() {
		// Consulta
		campoPeso.clear();
		etiquetaIMC.setText("");
		campoDiamCintura.clear();
		areaTextoTratamiento.clear();
		campoUltActualizacion.clear();
	}

	/**
	 * Limpia la tabla de consultas
	 */
	private void limpiarTablaConsultas() {
		tablaConsultasPrevias.getItems().clear();
	}

	/**
	 * Limpia el formulario para anamnesis
	 */
	private void limpiarAnamnesisAlimenticia() {
		// Anamnesis
		areaTextoDesayuno.clear();
		areaTextoMediaManana.clear();
		areaTextoAlmuerzo.clear();
		areaTextoMediaTarde.clear();
		areaTextoCena.clear();
		areaTextoNotasAnamnesis.clear();
	}

	/**
	 * Completa el formulario con la consulta pasada
	 */
	private void mostrarConsulta(ConsultaNutricion consulta) {
		// Conversion de Numero a Texto
		try {
			idConsulta.setText("# " + Integer.toString(consulta.getId()));
			// Los atributos BigDecimal usan la funcion .toPlainString
			if (consulta.getConsultaPeso() != null) {
				campoPeso.setText(consulta.getConsultaPeso().toPlainString());
			}
			if (consulta.getConsultaDiam() != null) {
				campoDiamCintura.setText(consulta.getConsultaDiam().toPlainString());
			}
		} catch (NumberFormatException exc) {
		}
		areaTextoTratamiento.setText(consulta.getTratamiento());
	}

	/**
	 * Recupera los datos del objeto de persistencia
	 * 
	 * @param antropometricos
	 */
	private void mostrarAnamnesisAlimenticia(AnamnesisAlimenticia anamnesisAlimenticia) {
		if (anamnesisAlimenticia.getDesayuno() != null) {
			areaTextoDesayuno.setText(anamnesisAlimenticia.getDesayuno());
		}
		if (anamnesisAlimenticia.getMediam() != null) {
			areaTextoMediaManana.setText(anamnesisAlimenticia.getMediam());
		}
		if (anamnesisAlimenticia.getAlmuerzo() != null) {
			areaTextoAlmuerzo.setText(anamnesisAlimenticia.getAlmuerzo());
		}
		if (anamnesisAlimenticia.getMediat() != null) {
			areaTextoMediaTarde.setText(anamnesisAlimenticia.getMediat());
		}
		if (anamnesisAlimenticia.getCena() != null) {
			areaTextoCena.setText(anamnesisAlimenticia.getCena());
		}
		if (anamnesisAlimenticia.getNotas() != null) {
			areaTextoNotasAnamnesis.setText(anamnesisAlimenticia.getNotas());
		}
		if (anamnesisAlimenticia.getUsuario() != null) {
			campoUltUsuario.setText(anamnesisAlimenticia.getUsuario());
		}
		if (anamnesisAlimenticia.getFechaUltAct() != null) {
			campoUltActualizacion.setText(anamnesisAlimenticia.getFechaUltAct().format(Constantes.FECHA_DD_MM_YYYY));
		}
	}

	/**
	 * Reinicia el modo de edicion
	 */
	private void reiniciarEditable() {
		editable.set(false);
	}

	/**
	 * Permite la edicion de la consulta pasada
	 * 
	 * @param consulta
	 */
	private void permitirEdicion(ConsultaNutricion consulta) {
		// Consulta
		if (consulta != null) {
			campoPeso.setEditable(true);
			campoDiamCintura.setEditable(true);
			areaTextoTratamiento.setEditable(true);
			campoUltActualizacion.setEditable(true);
		}
	}

	/**
	 * Deshabilita el formulario
	 */
	private void bloquearEdicion() {
		// Consulta
		if (campoPeso.isEditable()) {
			campoPeso.setEditable(false);
		}
		if (campoDiamCintura.isEditable()) {
			campoDiamCintura.setEditable(false);
		}
		if (areaTextoTratamiento.isEditable()) {
			areaTextoTratamiento.setEditable(false);
		}
		if (campoUltActualizacion.isEditable()) {
			campoUltActualizacion.setEditable(false);
		}

		// Ananmesis
		if (areaTextoDesayuno.isEditable()) {
			areaTextoDesayuno.setEditable(false);
		}
		if (areaTextoMediaManana.isEditable()) {
			areaTextoMediaManana.setEditable(false);
		}
		if (areaTextoAlmuerzo.isEditable()) {
			areaTextoAlmuerzo.setEditable(false);
		}
		if (areaTextoMediaTarde.isEditable()) {
			areaTextoMediaTarde.setEditable(false);
		}
		if (areaTextoCena.isEditable()) {
			areaTextoCena.setEditable(false);
		}
		if (areaTextoNotasAnamnesis.isEditable()) {
			areaTextoNotasAnamnesis.setEditable(false);
		}
	}

	/**
	 * Escucha la seleccion de la tabla de consultas previas y muestra la consulta
	 * seleccionada
	 */
	private void disparadorSeleccionConsultasPrevias() {
		// Agrega un listener para que recupere el valor seleccionado
		tablaConsultasPrevias.getSelectionModel().selectedItemProperty().addListener((o, p, a) -> {
			if (a != null) {
				// Limpia el formulario
				limpiarDatosConsultas();
				// Muestra la consulta
				mostrarConsulta(a);
			}
		});
	}

	private ConsultaNutricion tomarDatosFormulario(ConsultaNutricion consulta) {
		consulta.setTratamiento(areaTextoTratamiento.getText());
		consulta.setConsultaPeso(Util.convertirCampoBigDecimal(campoPeso, 3));
		consulta.setConsultaDiam(Util.convertirCampoBigDecimal(campoDiamCintura, 1));
		return consulta;
	}

	@FXML
	private void nuevaConsulta() {
		controladorHistoriaClinica.setConsulta(Consulta.nuevaConsulta(Consulta.NUTRICION));
	}

	@FXML
	private void modificarConsulta() {
		editable.set(true);
	}

	@FXML
	private void guardarCambios() {
		// Prepara la consulta
		consulta = tablaConsultasPrevias.getSelectionModel().getSelectedItem();
		consulta = tomarDatosFormulario(consulta);
		consulta.setEnmienda(consulta.getId());
		consulta.setId(serviciosConsultas.nuevaConsulta(consulta, aplicacion.getSesion().getPaciente(),
				aplicacion.getSesion().getUsuario()));

		if (serviciosConsultas.actualizarConsulta(consulta, aplicacion.getSesion().getUsuario(), true)) {
			// Refresco de la lista de consultas
			refrescarListaConsultas();
			editable.set(false);
			Mensajero.setMsg("Creada el supletente # " + consulta.getId(), Mensajero.MSG_EXITO);
		} else {
			Mensajero.setMsg("Problemas al guardar los cambios", Mensajero.MSG_ALERTA);
		}
	}

	@FXML
	private void descartarCambios() {
		limpiarDatosConsultas();
		editable.set(false);
		tablaConsultasPrevias.getSelectionModel().select(0);
	}
}
