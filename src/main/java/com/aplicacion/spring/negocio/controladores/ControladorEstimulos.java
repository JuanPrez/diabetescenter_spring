package com.aplicacion.spring.negocio.controladores;

import java.net.URL;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;

import de.felixroske.jfxsupport.FXMLController;
import javafx.fxml.Initializable;

@FXMLController
public class ControladorEstimulos implements Initializable {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(ControladorEstimulos.class);

	@Autowired
	Aplicacion aplicacion;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Mensajero.setMsg(
				"Pantalla en desarrollo, algunas funciones pueden estar deshabilitadas o funcionar de forma inesperada",
				Mensajero.MSG_ALERTA);
	}

}
