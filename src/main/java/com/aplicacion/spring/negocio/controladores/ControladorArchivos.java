package com.aplicacion.spring.negocio.controladores;

import java.net.URL;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;

import de.felixroske.jfxsupport.FXMLController;
import javafx.fxml.Initializable;

@FXMLController
public class ControladorArchivos implements Initializable {

	@Autowired
	Aplicacion aplicacion;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Mensajero.setMsg(
				"Pantalla en desarrollo, algunas funciones pueden estar deshabilitadas o funcionar de forma inesperada",
				Mensajero.MSG_ALERTA);
	}

}
