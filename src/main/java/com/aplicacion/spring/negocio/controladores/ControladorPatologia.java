/* ************************************************************************************* *
 *                                                                                       *
 *  Licensed to the Apache Software Foundation (ASF) under one                           *
 *  or more contributor license agreements.  See the NOTICE file                         *
 *  distributed with this work for additional information                                *
 *  regarding copyright ownership.  The ASF licenses this file                           *
 *  to you under the Apache License, Version 2.0 (the                                    *
 *  "License"); you may not use this file except in compliance                           *
 *  with the License.  You may obtain a copy of the License at                           *
 *                                                                                       *
 *    http://www.apache.org/licenses/LICENSE-2.0                                         *
 *                                                                                       *
 *  Unless required by applicable law or agreed to in writing,                           *
 *  software distributed under the License is distributed on an                          *
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY                               *
 *  KIND, either express or implied.  See the License for the                            *
 *  specific language governing permissions and limitations                              *
 *  under the License.                                                                   *
 *                                                                                       *
 * ************************************************************************************* */

package com.aplicacion.spring.negocio.controladores;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.SortedSet;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.excepciones.ExcepcionCampoFueraDePatron;
import com.aplicacion.spring.excepciones.ExcepcionCampoMuyLargo;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.ExamenPaciente;
import com.aplicacion.spring.negocio.entidades.registros.PatologiaPaciente;
import com.aplicacion.spring.negocio.entidades.registros.patologias.Examen;
import com.aplicacion.spring.negocio.entidades.registros.patologias.Patologia;
import com.aplicacion.spring.negocio.entidades.registros.patologias.PatologiaEnlaceExamen;
import com.aplicacion.spring.negocio.servicios.ServiciosPatologia;
import com.aplicacion.spring.utilitarios.Constantes;
import com.aplicacion.spring.utilitarios.Validador;

import de.felixroske.jfxsupport.FXMLController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Side;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * @Autor Juan Ignacio Prez
 * @Version 1.0
 * @Fecha 04/09/17
 * 
 * @Uso Controlador de FormularioPatologias.fxml Pantalla para control de la
 *      Patologias
 * 
 */
@FXMLController
public class ControladorPatologia implements Initializable {

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	ServiciosPatologia serviciosPatologia;

	@FXML
	private BorderPane panelCentral;
	@FXML
	private VBox vboxPatolog;
	@FXML
	private Accordion acordion;

	// Elementos para añadir una patologia al paciente
	@FXML
	private TextField campoOrigen;
	@FXML
	private ComboBox<Patologia> selectorSumarPatologiaPaciente;
	@FXML
	private ListView<Examen> listExamenesRelacionados;
	@FXML
	private Button botonIngresarPatologiaPaciente;
	@FXML
	private Button botonQuitarPatologiaPaciente;

	// Elementos para ingresar un examen
	@FXML
	private ComboBox<Examen> selectorSumarExamenPaciente;
	@FXML
	private Button botonIngresarExamenPaciente;
	@FXML
	private DatePicker datePickerEstudioPaciente;
	@FXML
	private TextArea areaTextoResultadoPaciente;

	List<Patologia> listaPatologias = new ArrayList<>();
	List<Examen> listaExamenes = new ArrayList<>();
	List<PatologiaPaciente> listaPatologiasPaciente = new ArrayList<>();
	List<PatologiaEnlaceExamen> listaExamenesPaciente = new ArrayList<>();
	ChangeListener<Paciente> listenerPaciente = null;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		configurarFormulario();

		// Ejecucion manual
		if (listenerPaciente == null && aplicacion.getSesion().getPaciente() != null) {
			refrescarPantalla(Optional.ofNullable(aplicacion.getSesion().getPaciente()));
		}

		// Listener del paciente
		listenerPaciente = new ChangeListener<Paciente>() {
			@Override
			public void changed(ObservableValue<? extends Paciente> observable, Paciente pacientePrevio,
					Paciente pacienteActual) {
				refrescarPantalla(Optional.ofNullable(pacienteActual));
			}
		};

		// Superpone una nueva instancia del acordion de patologias
		aplicacion.getSesion().pacienteProperty().addListener(listenerPaciente);

		refrescarPantalla(Optional.ofNullable(aplicacion.getSesion().getPaciente()));

		// Se escucha el cambio en la seleccion de patologia
		selectorSumarPatologiaPaciente.valueProperty()
				.addListener((o, patologiaPrevia, patologiaActual) -> mostrarExamenesRelacionados(patologiaActual));

	}

	/**
	 * Configuracion inicial
	 */
	private void configurarFormulario() {

		// Forzar año a ser numerico
		// botonIngresarPatologiaPaciente
		// botonQuitarPatologiaPaciente
		// botonIngresarExamenPaciente
		botonIngresarPatologiaPaciente.disableProperty()
				.bind(selectorSumarPatologiaPaciente.valueProperty().isNull().or(campoOrigen.textProperty().isEmpty()));
		botonQuitarPatologiaPaciente.disableProperty().bind(selectorSumarPatologiaPaciente.valueProperty().isNull());
		botonIngresarExamenPaciente.disableProperty()
				.bind(selectorSumarExamenPaciente.valueProperty().isNull()
						.or(datePickerEstudioPaciente.valueProperty().isNull())
						.or(areaTextoResultadoPaciente.textProperty().isEmpty()));
	}

	/**
	 * Refresca la pantalla
	 * 
	 * @param paciente
	 */
	public void refrescarPantalla(Optional<Paciente> paciente) {

		limpiarFormularioPatologiaPaciente();
		if (paciente.isPresent()) {
			listaPatologiasPaciente = serviciosPatologia.obtenerPatologiasPaciente(paciente.get().getId());
			listaExamenesPaciente = serviciosPatologia.obtenerExamenesPaciente(paciente.get().getId());
			historialPatologiasCronicas(paciente.get());
		}
		// Armamos el panel para gestionar las patologias del paciente
		armarPanelDerechoPatologiasCronicas();

		// Ponemos un origen actual
		campoOrigen.setText(String.valueOf(LocalDate.now().getYear()));
	}

	/**
	 * Muestra los examenes relacionados con la patologia seleccionada
	 * 
	 * @param patologia
	 * 
	 */
	void mostrarExamenesRelacionados(Patologia patologia) {
		// Limpia la lista
		listExamenesRelacionados.getItems().clear();
		// Necesitamos tener seleccionada una patologia primero
		if (patologia != null)
			// Obtiene la lista de relaciones entre patologias y examenes
			for (PatologiaEnlaceExamen ln : aplicacion.getRepositorios().getListaEnlacesPatologiaExamen())
				if (ln.getPatologia() != null && patologia.getId() == ln.getPatologia().getId())
					// Muestra los examenes que esten relacionadas
					for (Examen le : aplicacion.getRepositorios().getListaExamenes())
						if (ln.getExamen().getId() == le.getId())
							// Añade a la lista de examenes relacionados
							listExamenesRelacionados.getItems().add(le);
	}

	/**
	 * 
	 * Contruye los Formularios, los selectores de Patologia y Examen
	 * 
	 */
	private void armarPanelDerechoPatologiasCronicas() {
		// Limpia los selectores
		selectorSumarPatologiaPaciente.getItems().clear();
		selectorSumarExamenPaciente.getItems().clear();

		// Obtiene la lista y la ordena
		ObservableSet<Patologia> setPatologias = FXCollections
				.observableSet(new TreeSet<>(aplicacion.getRepositorios().getListaPatologias()));

		SortedSet<Patologia> setPatologiasValidas = new TreeSet<>();

		// Añade todas la patologias del repositorio que contengan examenes asignados
		for (Patologia patologia : setPatologias)
			for (PatologiaEnlaceExamen enlaceExamenPatologia : aplicacion.getRepositorios()
					.getListaEnlacesPatologiaExamen())
				if (enlaceExamenPatologia.getPatologia().getId() == patologia.getId())
					setPatologiasValidas.add(patologia);

		for (Patologia patologiaValidada : setPatologiasValidas) {
			selectorSumarPatologiaPaciente.getItems().add(patologiaValidada);
		}

		// Añade los examenes al selector
		for (PatologiaEnlaceExamen enlaceExamenPatologia : listaExamenesPaciente)
			selectorSumarExamenPaciente.getItems().add(enlaceExamenPatologia.getExamen());

		// Asigna la fecha de hoy automaticamente
		datePickerEstudioPaciente.setValue(LocalDate.now());
	}

	/**
	 * Muestra el historial de Patologias Cronicas
	 */
	private void historialPatologiasCronicas(Paciente paciente) {
		armarAcordeonPatologias(paciente, listaPatologiasPaciente, listaExamenesPaciente);
		// Expande el primer titulo del acordion
		if (!acordion.getPanes().isEmpty())
			acordion.setExpandedPane(acordion.getPanes().get(0));
	}

	/**
	 * Genera el acordeon de patologias
	 * 
	 * @param listaExamenesPaciente
	 * @param listaPatologiasPaciente
	 */
	private void armarAcordeonPatologias(Paciente paciente, List<PatologiaPaciente> listaPatologiasPaciente,
			List<PatologiaEnlaceExamen> listaExamenesPaciente) {

		List<ExamenPaciente> listaHistorialExamenes = serviciosPatologia.obtenerHistExamenesPaciente(paciente.getId());

		acordion.getPanes().clear();

		// Para cada patologia crea un panel en el acordeon
		for (PatologiaPaciente patologiaPaciente : listaPatologiasPaciente) {

			// Crea un panel con pestañas laterales
			TabPane tabPane = new TabPane();
			tabPane.setSide(Side.LEFT);
			tabPane.setRotateGraphic(true);
			tabPane.getStyleClass().add("padding8, fondo-blanco");

			// Por cada tipo de examen se crea una pestaña
			for (PatologiaEnlaceExamen examenPaciente : listaExamenesPaciente)
				if (patologiaPaciente.getId() == examenPaciente.getPatologia().getId())
					// Aqui es donde se crea la tabla relacionada
					tabPane.getTabs().add(armarPestanaExamenes(examenPaciente.getExamen(), listaHistorialExamenes));

			// Inserta el panel con pestañas dentro del acordeon
			TitledPane panelConTitulo = new TitledPane(
					String.format("%s - (%s) ", patologiaPaciente.getNombre(), patologiaPaciente.getOrigen()), tabPane);

			// Añade el panel completo al acordeon
			acordion.getPanes().add(panelConTitulo);
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param panel
	 * @param examen
	 */
	private Tab armarPestanaExamenes(Examen examen, List<ExamenPaciente> listaHistExamenes) {
		Tab tabInt = new Tab();

		ObservableList<ExamenPaciente> listaHistClinicaExamenes = FXCollections.observableArrayList();
		// Colocamos
		for (ExamenPaciente examenes : listaHistExamenes) {
			if (examenes.getId() == examen.getId()) {
				listaHistClinicaExamenes.add(examenes);
			}
		}
		TableView<ExamenPaciente> tablaResultados = new TableView<>();

		// Fecha
		TableColumn<ExamenPaciente, LocalDate> fechaColumna = new TableColumn<ExamenPaciente, LocalDate>("Fecha");
		fechaColumna.prefWidthProperty().bind(tablaResultados.widthProperty().multiply(0.2));
		fechaColumna.setCellValueFactory(new PropertyValueFactory<ExamenPaciente, LocalDate>("fechaExamen"));
		// Resultado
		TableColumn<ExamenPaciente, String> resultadoColumna = new TableColumn<ExamenPaciente, String>("Resultado");
		resultadoColumna.prefWidthProperty().bind(tablaResultados.widthProperty().multiply(0.5));
		resultadoColumna.setCellValueFactory(new PropertyValueFactory<ExamenPaciente, String>("resultado"));
		// Usuario
		TableColumn<ExamenPaciente, String> usuarioColumna = new TableColumn<ExamenPaciente, String>("Usuario");
		usuarioColumna.prefWidthProperty().bind(tablaResultados.widthProperty().multiply(0.2));
		usuarioColumna.setCellValueFactory(new PropertyValueFactory<ExamenPaciente, String>("creador"));

		tablaResultados.getColumns().add(fechaColumna);
		tablaResultados.getColumns().add(resultadoColumna);
		tablaResultados.getColumns().add(usuarioColumna);

		tablaResultados.setItems(listaHistClinicaExamenes);

		if (listaHistClinicaExamenes.isEmpty()) {
			tabInt.setDisable(true);
		}
		tabInt.setClosable(false);

		// Armamos etiquetas con colores aleatorios
		Random gen = new Random();
		Label etiquetaExamen = new Label(examen.getNombre());
		etiquetaExamen.setStyle("-fx-font-size:12px; -fx-font-weight: bold;");
		etiquetaExamen.setBackground(
				new Background(new BackgroundFill(Color.rgb(gen.nextInt(256), gen.nextInt(256), gen.nextInt(256), 0.5),
						CornerRadii.EMPTY, Insets.EMPTY)));
		tabInt.setGraphic(etiquetaExamen);

		tabInt.setContent(tablaResultados);

		return tabInt;

	}

	// **************************************************************************************
	// Metodos para modificar la relacion Paciente - Patologia
	// **************************************************************************************
	/**
	 * 
	 * Añade la patologia al paciente
	 * 
	 * @param event
	 */
	@FXML
	void sumarPatologiaPaciente(ActionEvent event) {
		PatologiaPaciente patologiaNueva;
		// Obtiene la patologia de la pantalla
		if ((patologiaNueva = obtenerFormularioPatologia()) != null) {
			// Evalua si la patologia ya esta asociada
			if (!listaPatologiasPaciente.contains(patologiaNueva)) {
				// Sube la relacion patologia-paciente a la BD
				if (serviciosPatologia.sumarNuevaPatologiaPaciente(aplicacion.getSesion().getPaciente(), patologiaNueva,
						aplicacion.getSesion().getUsuario())) {
					Mensajero.setMsg("Añadida nueva patologia cronica al paciente", Mensajero.MSG_EXITO);
					limpiarFormularioPatologiaPaciente();
					refrescarPantalla(Optional.ofNullable(aplicacion.getSesion().getPaciente()));
				} else {
					Mensajero.setMsg("No se pudo añadir la patologia", Mensajero.MSG_ALERTA);
				}
			} else {
				Mensajero.setMsg("La patología ya esta asociada al paciente", Mensajero.MSG_ALERTA);
			}
		}
	}

	/**
	 * 
	 * Valida y retorna la patologia para añadir
	 * 
	 * @return
	 */
	private PatologiaPaciente obtenerFormularioPatologia() {
		// Si el origen es valido, creamos
		if (campoOrigen.getLength() > 0) {

			if (selectorSumarPatologiaPaciente.getValue() != null) {
				try {
					Validador.validarTextoPatronMaximo(campoOrigen.getText(), Constantes.PATRON_NUMEROS_ENTEROS, 10);
					return (new PatologiaPaciente(0, selectorSumarPatologiaPaciente.getValue().getNombre(),
							selectorSumarPatologiaPaciente.getValue().getAbreviatura(), campoOrigen.getText()));
				} catch (ExcepcionCampoFueraDePatron e) {
					Mensajero.setMsg("El campoOrigen solo puede contener numeros", Mensajero.MSG_INFO);
					return null;
				} catch (ExcepcionCampoMuyLargo e) {
					Mensajero.setMsg("El campoOrigen no puede tener mas de 10 caracteres", Mensajero.MSG_INFO);
					return null;
				}
			} else {
				Mensajero.setMsg("Seleccione una patalogia primero", Mensajero.MSG_ALERTA);
				return null;
			}
		} else {
			Mensajero.setMsg("El origen es requerido", Mensajero.MSG_ALERTA);
			return null;
		}
	}

	/**
	 * Limpia los
	 */
	private void limpiarFormularioPatologiaPaciente() {
		selectorSumarPatologiaPaciente.setValue(null);
		campoOrigen.clear();
	}

	/**
	 * 
	 * 
	 * 
	 * @param event
	 */
	@FXML
	void quitarPatologiaPaciente(ActionEvent event) {
		if (selectorSumarPatologiaPaciente.getValue() != null) {
			if (serviciosPatologia.quitarRelacionPatologiaPaciente(selectorSumarPatologiaPaciente.getValue(),
					aplicacion.getSesion().getPaciente())) {
				Mensajero.setMsg("Quitada la patologia cronica del paciente", Mensajero.MSG_EXITO);
				limpiarFormularioPatologiaPaciente();
				refrescarPantalla(Optional.ofNullable(aplicacion.getSesion().getPaciente()));
			}
		}
	}

	/**
	 * 
	 * 
	 * 
	 * @param event
	 */
	@FXML
	void sumarExamenPaciente(ActionEvent event) {
		ExamenPaciente examenNuevo;
		// Recupera la informacion del formulario
		if ((examenNuevo = recuperarDatosExamenPaciente()) != null) {
			// Si no hay examen asignado, lo notifica
			if (examenNuevo.getExamen() != null) {
				// Guarda el registro en la BD
				if (serviciosPatologia.guardarNuevoExamenPaciente(aplicacion.getSesion().getPaciente(), examenNuevo,
						aplicacion.getSesion().getUsuario())) {
					Mensajero.setMsg("Añadido nuevo examen al paciente", Mensajero.MSG_EXITO);
					limpiarDatosExamenPaciente();
					initialize(null, null);
				}
			} else {
				Mensajero.setMsg("Falta seleccionar el examen", Mensajero.MSG_ALERTA);
			}
		} else {
			Mensajero.setMsg("La fecha del examen es requerida", Mensajero.MSG_ALERTA);
		}
	}

	/**
	 * 
	 * Valida los datos del examen a añadir
	 * 
	 * @return
	 */
	private ExamenPaciente recuperarDatosExamenPaciente() {
		if (datePickerEstudioPaciente.getValue() != null) {
			if (selectorSumarExamenPaciente.getValue() != null) {
				return new ExamenPaciente(0, selectorSumarExamenPaciente.getValue(),
						datePickerEstudioPaciente.getValue(), areaTextoResultadoPaciente.getText().trim(),
						aplicacion.getSesion().getUsuario().getNombreUsuario(),
						aplicacion.getSesion().getUsuario().getUid());
			} else {
				Mensajero.setMsg("No hay ningun examen seleccionado", Mensajero.MSG_ALERTA);
				return null;
			}
		} else {
			Mensajero.setMsg("Falta la fecha del examen", Mensajero.MSG_ALERTA);
			return null;
		}
	}

	/**
	 * 
	 */
	private void limpiarDatosExamenPaciente() {
		selectorSumarExamenPaciente.setValue(null);
		datePickerEstudioPaciente.setValue(LocalDate.now());
		areaTextoResultadoPaciente.clear();
	}
}
