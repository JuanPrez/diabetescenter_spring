package com.aplicacion.spring.negocio.controladores;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.gestion.controladores.ControladorHistoriaClinica;
import com.aplicacion.spring.gestion.entidades.Perfil;
import com.aplicacion.spring.negocio.entidades.paciente.Antropometricos;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.consultas.Consulta;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaPodologica;
import com.aplicacion.spring.negocio.servicios.ServiciosPodologia;
import com.aplicacion.spring.utilitarios.controladores.Util;

import de.felixroske.jfxsupport.FXMLController;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

@FXMLController
public class ControladorAntecedentesPodologia implements Initializable {

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	ServiciosPodologia serviciosPodologia;

	@FXML
	private Label idConsulta;
	@FXML
	public Button botonGuardar;
	@FXML
	public Button botonDescartar;
	@FXML
	public Button botonNueva;
	@FXML
	public Button botonModificar;

	// Antropometricos
	@FXML
	private HBox hBoxAntropometricos;
	@FXML
	private TextField campoTextoNroCalzado;
	@FXML
	private TextField campoTextoFormulaDigital;
	@FXML
	private TextArea areaTextoAntecedentes;

	// Consulta
	@FXML
	private VBox vBoxodttc;
	@FXML
	private VBox vBoxPulsoObs;
	@FXML
	private VBox vBoxEvolTratamiento;
	@FXML
	private TextArea areaTextoOnicopatias;
	@FXML
	private TextArea areaTextoDedos;
	@FXML
	private TextArea areaTextoTemperatura;
	@FXML
	private TextArea areaTextoTexturaPiel;
	@FXML
	private TextArea areaTextoColoracionPiel;
	@FXML
	private TextArea areaTextoPulsoTibial;
	@FXML
	private TextArea areaTextoPulsoPedio;
	@FXML
	private TextArea areaTextoObservaciones;
	@FXML
	private TextArea areaTextoEvolTratamiento;

	@FXML
	private TableView<ConsultaPodologica> tablaConsultasPrevias;
	@FXML
	private TableColumn<ConsultaPodologica, Integer> columnaID;
	@FXML
	private TableColumn<ConsultaPodologica, LocalDate> columnaFecha;
	@FXML
	private TableColumn<ConsultaPodologica, String> columnaTratamiento;
	@FXML
	private TableColumn<ConsultaPodologica, String> columnaUsuario;

	ControladorHistoriaClinica controladorHistoriaClinica;
	ConsultaPodologica consulta;
	Antropometricos antropometricos = null;
	SimpleBooleanProperty editable = new SimpleBooleanProperty(false);
	ChangeListener<Paciente> listenerPaciente = null;
	ChangeListener<Boolean> listenerEditable = null;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// Listener de la propiedad editable de la propia pantalla
		listenerEditable = new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean estadoPrevio,
					Boolean estadoActual) {
				if (estadoActual)
					permitirEdicion(tablaConsultasPrevias.getSelectionModel().getSelectedItem());
				else
					bloquearEdicion();
			}
		};
		editable.addListener(listenerEditable);

		// En caso de que el listener no este activo, hace una llamada manual
		if (listenerPaciente == null && aplicacion.getSesion().getPaciente() != null)
			refrescarPantalla();

		// Implementation del listener
		listenerPaciente = new ChangeListener<Paciente>() {
			@Override
			public void changed(ObservableValue<? extends Paciente> observable, Paciente pacientePrevio,
					Paciente pacienteActual) {
				if (pacienteActual != null) {
					refrescarPantalla();
				} else {
					limpiarDatosConsultas();
					limpiarTablaConsultas();
				}

			}
		};
		aplicacion.getSesion().pacienteProperty().addListener(listenerPaciente);

		inicializarPantalla();
	}

	/**
	 * Primera y unica ejecucion al inicializar la pantalla
	 */
	private void inicializarPantalla() {
		// Instanciacion de los controladores relacionados
		controladorHistoriaClinica = aplicacion.getContexto().getBean(ControladorHistoriaClinica.class);

		// Configuracion de los botones
		verBoton(botonNueva, editable.get(), Perfil.getPodologia());
		verBoton(botonModificar, editable.get(), Perfil.getPodologia());
		botonNueva.disableProperty().bind(controladorHistoriaClinica.consultaProperty().isNotNull());
		botonGuardar.visibleProperty().bind(editable);
		botonDescartar.visibleProperty().bind(editable);
		tablaConsultasPrevias.disableProperty().bind(editable);

		// Disparadores automaticos
		disparadorSeleccionConsultasPrevias();
	}

	/**
	 * Ejecucion recurrente
	 */
	private void refrescarPantalla() {
		// Limpia el formulario
		limpiarDatosConsultas();
		limpiarTablaConsultas();
		limpiarAntropometricos();
		reiniciarEditable();
		// Evita su modificación
		bloquearEdicion();

		// Obtener datos antropometricos
		antropometricos = serviciosPodologia.obtenerAntropometricos(aplicacion.getSesion().getPaciente().getId());
		// Si no hay datos, construye un objecto vacio
		if (antropometricos == null)
			antropometricos = new Antropometricos();
		aplicacion.getSesion().getPaciente().setDatosAntropometricos(antropometricos);

		// Busca las consultas previas
		refrescarListaConsultas();

		if (!tablaConsultasPrevias.getItems().isEmpty()) {
			Platform.runLater(() -> tablaConsultasPrevias.getSelectionModel().select(0));
		} else {
			limpiarDatosConsultas();
		}
		// Y la Anamnesis Alimenticia
		mostrarDatosAntropometricos(antropometricos);

		// Selecciona la ultima consulta
		Platform.runLater(() -> tablaConsultasPrevias.getSelectionModel().clearAndSelect(0));
	}

	/**
	 * Hace visible los botones si el usuario es del perfil correcto
	 * 
	 * @param boton
	 * @param edicion
	 * @param perfil
	 */
	private void verBoton(Button boton, boolean edicion, int perfil) {
		boton.setVisible(!edicion && aplicacion.getSesion().getUsuario().getPerfil().getId() == perfil);
	}

	/**
	 * Muestra los datos antropometricos
	 * 
	 * @param antropometricos
	 */
	private void mostrarDatosAntropometricos(Antropometricos antropometricos) {
		campoTextoNroCalzado.setText(antropometricos.getNumeroCalzado().toString());
		campoTextoFormulaDigital.setText(antropometricos.getFormulaDigital());
		areaTextoAntecedentes.setText(antropometricos.getAntecedentes());
	}

	/**
	 * Escucha la seleccion de la tabla de consultas previas y muestra la consulta
	 * seleccionada
	 */
	private void disparadorSeleccionConsultasPrevias() {
		// Agrega un listener para que recupere el valor seleccionado
		tablaConsultasPrevias.getSelectionModel().selectedItemProperty().addListener((o, p, a) -> {
			if (a != null) {
				// Limpia el formulario
				limpiarDatosConsultas();
				// Muestra la consulta
				mostrarConsulta(a);
			}
		});
	}

	/**
	 * Muestra la consulta
	 * 
	 * @param consulta
	 */
	private void mostrarConsulta(ConsultaPodologica consulta) {
		areaTextoOnicopatias.setText(consulta.getOnicopatias());
		areaTextoDedos.setText(consulta.getDedos());
		areaTextoTemperatura.setText(consulta.getTemperatura());
		areaTextoTexturaPiel.setText(consulta.getTexturaPiel());
		areaTextoColoracionPiel.setText(consulta.getColoracionPiel());
		areaTextoPulsoTibial.setText(consulta.getPulsoTibial());
		areaTextoPulsoPedio.setText(consulta.getPulsoPedio());
		areaTextoObservaciones.setText(consulta.getObservaciones());
		areaTextoEvolTratamiento.setText(consulta.getEvolucion());
	}

	/**
	 * Obtiene los datos del formulario
	 * 
	 * @param consulta
	 * @return
	 */
	private ConsultaPodologica tomarDatosFormulario(ConsultaPodologica consulta) {
		consulta.setOnicopatias(areaTextoOnicopatias.getText());
		consulta.setDedos(areaTextoDedos.getText());
		consulta.setTemperatura(areaTextoTemperatura.getText());
		consulta.setTexturaPiel(areaTextoTexturaPiel.getText());
		consulta.setColoracionPiel(areaTextoColoracionPiel.getText());
		consulta.setPulsoTibial(areaTextoPulsoTibial.getText());
		consulta.setPulsoPedio(areaTextoPulsoPedio.getText());
		consulta.setObservaciones(areaTextoObservaciones.getText());
		consulta.setEvolucion(areaTextoEvolTratamiento.getText());
		return consulta;
	}

	/**
	 * Limpia el formulario para consultas
	 */
	private void limpiarDatosConsultas() {
		// Consulta
		Util.limpiarCampos(vBoxodttc);
		Util.limpiarCampos(vBoxPulsoObs);
		Util.limpiarCampos(vBoxEvolTratamiento);
	}

	/**
	 * Limpia la tabla de consultas
	 */
	private void limpiarTablaConsultas() {
		tablaConsultasPrevias.getItems().clear();
	}

	/**
	 * Limpia el formulario para anamnesis
	 */
	private void limpiarAntropometricos() {
		// Anamnesis
		Util.limpiarCampos(hBoxAntropometricos);
	}

	/**
	 * Obtiene las consultas previas
	 * 
	 * @param listaConsultas
	 */
	private void mostrarConsultasPrevias(List<ConsultaPodologica> listaConsultas) {
		if (listaConsultas == null)
			listaConsultas = new ArrayList<>();
		else
			tablaConsultasPrevias.setItems(FXCollections.observableArrayList(listaConsultas));
	}

	/**
	 * Refresca la lista de consultas
	 */
	public void refrescarListaConsultas() {
		mostrarConsultasPrevias(serviciosPodologia.obtenerConsultas(aplicacion.getSesion().getPaciente()));
	}

	/**
	 * Reinicia el modo de edicion
	 */
	private void reiniciarEditable() {
		editable.set(false);
	}

	/**
	 * Permite la edicion de la consulta pasada
	 * 
	 * @param consulta
	 */
	private void permitirEdicion(ConsultaPodologica consulta) {
		// Consulta
		if (consulta != null) {
			Util.habilitarCampos(hBoxAntropometricos, true);
			Util.habilitarCampos(vBoxodttc, true);
			Util.habilitarCampos(vBoxPulsoObs, true);
			Util.habilitarCampos(vBoxEvolTratamiento, true);
		}
	}

	/**
	 * Inhabilita el formulario
	 */
	private void bloquearEdicion() {
		Util.habilitarCampos(hBoxAntropometricos, false);
		Util.habilitarCampos(vBoxodttc, false);
		Util.habilitarCampos(vBoxPulsoObs, false);
		Util.habilitarCampos(vBoxEvolTratamiento, false);
	}

	@FXML
	private void nuevaConsulta() {
		controladorHistoriaClinica.setConsulta(Consulta.nuevaConsulta(Consulta.PODOLOGIA));
	}

	@FXML
	private void modificarConsulta() {
		editable.set(true);
	}

	@FXML
	private void guardarCambios() {
		// Prepara la consulta
		consulta = tablaConsultasPrevias.getSelectionModel().getSelectedItem();
		consulta = tomarDatosFormulario(consulta);
		consulta.setEnmienda(consulta.getId());
		consulta.setId(serviciosPodologia.nuevaConsulta(consulta, aplicacion.getSesion().getPaciente(),
				aplicacion.getSesion().getUsuario()));

		if (serviciosPodologia.actualizarConsulta(consulta, aplicacion.getSesion().getUsuario(), true)) {
			// Refresco de la lista de consultas
			refrescarListaConsultas();
			editable.set(false);
			Mensajero.setMsg("Creada el supletente # " + consulta.getId(), Mensajero.MSG_EXITO);
		} else {
			Mensajero.setMsg("Problemas al guardar los cambios", Mensajero.MSG_ALERTA);
		}
	}

	@FXML
	private void descartarCambios() {
		limpiarDatosConsultas();
		editable.set(false);
		tablaConsultasPrevias.getSelectionModel().select(0);
	}
}
