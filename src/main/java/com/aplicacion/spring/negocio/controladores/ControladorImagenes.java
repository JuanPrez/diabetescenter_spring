package com.aplicacion.spring.negocio.controladores;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.Ventanas;
import com.aplicacion.spring.gestion.entidades.Perfil;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.ImagenPaciente;
import com.aplicacion.spring.negocio.servicios.ServicioImagenes;
import com.aplicacion.spring.utilitarios.Constantes;
import com.aplicacion.spring.utilitarios.Validador;

import de.felixroske.jfxsupport.FXMLController;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Screen;
import javafx.stage.StageStyle;

@FXMLController
public class ControladorImagenes implements Initializable {

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	ServicioImagenes servicioImagenes;

	@FXML
	private BorderPane panelCentral;
	@FXML
	private Label idPac;
	@FXML
	private VBox mainPane;
	@FXML
	private Button botonCargarImagen;
	@FXML
	private Button botonSubirImagen;
	@FXML
	private Button botonRojoCarga;
	@FXML
	private TextArea areaTextoNuevaImagen;
	@FXML
	private ScrollPane scrollPaneImagenes;
	@FXML
	private FlowPane flowPaneImagenes;
	@FXML
	private HBox hboxCargarMas;
	@FXML
	private Hyperlink linkCargarMas;

	// Creamos los literales para los estilos
	private static final String boton = "boton";
	private static final String botonVerde = "botonVerde";
	private static final String botonRojo = "botonRojo";
	private static final String botonAzul = "botonAzul";
	private static final String botonMiniatura = "botonMiniatura";
	private static final String spacing = "spacing4";
	private static final String padding = "padding4";
	private static final String centered = "centered";
	private static final String fondo = "fondo-blanco";
	private static final String bordeGris = "borde-gris";
	private static final String bordeRedondeado = "borderRadius2";

	int i = 0;
	File nuevaImagen = null;
	// Cargamos la imagen para mostrar
	SimpleBooleanProperty subible = new SimpleBooleanProperty(false);
	ObservableList<ImagenPaciente> listaImagenes = FXCollections.observableArrayList();
	ListChangeListener<ImagenPaciente> listenerListaImagenes = null;
	ChangeListener<Paciente> listenerPaciente = null;

	// Armamos los contenedores para la ventana de imagen
	Rectangle2D bordesPantalla = Screen.getPrimary().getVisualBounds();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		// Ejecucion manual, si no hay listener instalado
		if (listenerListaImagenes == null && !listaImagenes.isEmpty()) {
			armarGaleria(listaImagenes);
		}
		// Listener para la lista de imagenes
		listenerListaImagenes = new ListChangeListener<ImagenPaciente>() {
			@Override
			public void onChanged(Change<? extends ImagenPaciente> c) {
				armarGaleria(listaImagenes);
			}
		};
		listaImagenes.addListener(listenerListaImagenes);

		// Ejecucion manual, si no hay listener instalado
		if (listenerPaciente == null && aplicacion.getSesion().getPaciente() != null) {
			refrescarPantalla(aplicacion.getSesion().getPaciente());
		}

		// Listener del paciente
		listenerPaciente = new ChangeListener<Paciente>() {
			@Override
			public void changed(ObservableValue<? extends Paciente> observable, Paciente pacientePrevio,
					Paciente pacienteActual) {
				if (pacienteActual != null) {
					refrescarPantalla(pacienteActual);
				} else {
					limpiarListaImagenes();
				}
			}

		};
		aplicacion.getSesion().pacienteProperty().addListener(listenerPaciente);

		configurarFormulario();
	}

	private void refrescarPantalla(Paciente paciente) {
		limpiarListaImagenes();
		reiniciarContadores();
		obtenerListaImagenesPaciente(paciente);

	}

	private void configurarFormulario() {
		// Deshabilita los campos mientras que subir imagen este habilitado
		botonCargarImagen.disableProperty().bind(subible);
		botonSubirImagen.disableProperty().bind(subible.not());
		areaTextoNuevaImagen.disableProperty().bind(subible.not());
		botonRojoCarga.disableProperty().bind(subible.not());
	}

	/**
	 * Limpia la lista de imagenes
	 */
	private void limpiarListaImagenes() {
		listaImagenes.clear();
	}

	private void reiniciarContadores() {
		setMinimo(0);
		setCantidad(0);
	}

	private void obtenerListaImagenesPaciente(Paciente paciente) {
		// Cuenta la cantidad de imagenes en el historial del paciente
		setCantidad(servicioImagenes.contarImagenesPaciente(paciente.getId()));

		// Carga las imagenes
		listaImagenes
				.addAll(servicioImagenes.recuperarListaImagenesPaciente(paciente.getId(), getMinimo(), getRango()));

		// Accion del panel
		linkCargarMas.setOnAction(e -> obtenerMasImagenes(listaImagenes));
	}

	/**
	 * Metodo constructor de la galeria
	 */
	private void armarGaleria(List<ImagenPaciente> listaDeImagenes) {
		// Limpia primero el panel
		flowPaneImagenes.getChildren().clear();
		hboxCargarMas.setVisible(listaImagenes.size() < getCantidad());

		// Si la lista existe y contiene elementos, construye el panel
		if (listaDeImagenes != null && !listaDeImagenes.isEmpty()) {
			for (ImagenPaciente li : listaDeImagenes) {
				construirPanelImagenes(li);
			}
		}
	}

	/**
	 * 
	 */
	private void obtenerMasImagenes(List<ImagenPaciente> listaImagenes) {
		// Mueve el rango
		setMinimo(getMinimo() + getRango());

		// Añade las nuevas imagenes a la galeria
		listaImagenes.addAll(servicioImagenes
				.recuperarListaImagenesPaciente(aplicacion.getSesion().getPaciente().getId(), getMinimo(), getRango()));
	}

	/**
	 * Metodo para mostrar las imagenes
	 * 
	 * @param lista
	 *            de imagenes
	 */
	public void construirPanelImagenes(ImagenPaciente imagen) {

		// Contenedores
		VBox vboxt = new VBox();
		HBox hboxFecha = new HBox();
		HBox hbox0 = new HBox();
		HBox hbox1 = new HBox();
		VBox vbox1 = new VBox();
		ImageView contenedorImagen = new ImageView();

		// Botones
		Button modificarI = new Button("Modificar");
		Button eliminarI = new Button("Eliminar ");
		Button guardarI = new Button("Guardar");
		Button cancelarI = new Button("Cancelar ");

		// Etiquetas
		Label etiquetaConsulta = new Label();
		Label etiquetaApellido = new Label(aplicacion.getSesion().getPaciente().getApellido());
		Label etiquetaNombre = new Label(aplicacion.getSesion().getPaciente().getNombre());
		Label etiquetaFecha = new Label("Fecha: ");

		String consulta = tipoConsulta(imagen);

		if (imagen.getIdConsulta() > 0) {
			etiquetaConsulta.setText(consulta + " " + Integer.toString(imagen.getIdConsulta()));
		} else {
			etiquetaConsulta.setText("Sin Consulta");
		}

		String fecha = "";
		if (imagen.getFecha() != null) {
			fecha = imagen.getFecha().format(Constantes.FECHA_D_M_YYYY);
		}
		Label titleFecha = new Label(fecha);
		Label imageId = new Label();

		Region separador = new Region();
		VBox.setVgrow(separador, Priority.ALWAYS);
		TextArea comentario = new TextArea();

		// Les damos un poco de estilo
		modificarI.getStyleClass().addAll(boton, botonMiniatura, botonAzul);
		eliminarI.getStyleClass().addAll(boton, botonMiniatura, botonRojo);
		guardarI.getStyleClass().addAll(boton, botonMiniatura, botonVerde);
		cancelarI.getStyleClass().addAll(boton, botonMiniatura, botonRojo);
		hbox1.getStyleClass().addAll(spacing, padding, centered);

		// Arma
		imageId.setText(String.valueOf(imagen.getId()));
		imageId.setVisible(false);

		etiquetaFecha.getStyleClass().add("bold");

		hboxFecha.getChildren().addAll(etiquetaFecha, titleFecha);

		// Ensamble
		vboxt.getChildren().addAll(etiquetaConsulta, etiquetaApellido, etiquetaNombre, hboxFecha);
		vboxt.getStyleClass().add(padding);

		// Formato del textarea comentario
		comentario.setPrefWidth(150);
		comentario.setPrefHeight(50);
		comentario.setEditable(false);
		comentario.setText(imagen.getComentarios());

		// Formato del ImageView contenedorImagen
		contenedorImagen.prefWidth(150);
		contenedorImagen.prefHeight(150);
		contenedorImagen.setFitWidth(150);
		contenedorImagen.setFitHeight(150);
		contenedorImagen.setPreserveRatio(false);

		// Ajustamos la vista previa de la imagen al tamaño de la galeria
		WritableImage imagenRecortada;
		if (imagen.getImagen() != null && imagen.getImagen().getWidth() != 0 && imagen.getImagen().getHeight() != 0) {

			// Recortamos la imagen para que se amolde al contenedor
			int x = 0;
			int y = 0;
			int w = 150;
			int h = 150;

			if (imagen.getImagen().getHeight() > 0 && imagen.getImagen().getWidth() > 0) {

				// Recortamos la imagen por el lado mas corto y la centramos
				if (imagen.getImagen().getHeight() <= imagen.getImagen().getWidth()) {
					w = (int) imagen.getImagen().getHeight();
					h = (int) imagen.getImagen().getHeight();
					x = (int) (imagen.getImagen().getWidth() - imagen.getImagen().getHeight()) / 2;
				} else {
					w = (int) imagen.getImagen().getWidth();
					h = (int) imagen.getImagen().getWidth();
					y = (int) (imagen.getImagen().getHeight() - imagen.getImagen().getWidth()) / 2;
				}
			}
			imagenRecortada = new WritableImage(imagen.getImagen().getPixelReader(), x, y, w, h);
		} else {
			imagenRecortada = null;
		}

		// Añade la imagen
		contenedorImagen.setImage(imagenRecortada);

		// Crea un dialogo de imagen cuando se hace click en una imagen
		mostrarDialogoImagen(imagen, contenedorImagen);

		// Acciones de los botones

		// Elimina la imagen
		eliminarI.setOnAction(e -> eliminarImagen(imagen));

		// Habilita el cambio en los comentarios de la imagen
		modificarI.setOnAction(e -> {
			hbox1.getChildren().clear();
			hbox1.getChildren().addAll(guardarI, cancelarI);
			comentario.setEditable(true);
		});

		// Guarda los cambios hechos en los comentarios, y deshabilita la modificacion
		guardarI.setOnAction(e -> {
			hbox1.getChildren().clear();
			hbox1.getChildren().addAll(modificarI, eliminarI);
			guardarCambioComentario(imagen, comentario.getText());
			comentario.setEditable(false);
		});

		// Descarta los cambios hechos en los comentarios, y deshabilita la modificacion
		cancelarI.setOnAction(e -> {
			hbox1.getChildren().clear();
			hbox1.getChildren().addAll(modificarI, eliminarI);
			comentario.setEditable(false);
		});

		// Agrega los elementos al contenedor
		hbox1.getChildren().addAll(modificarI, eliminarI);
		vbox1.getChildren().addAll(vboxt, separador, comentario, hbox1);

		// Agrega los elementos al contenedor
		hbox0.getChildren().addAll(contenedorImagen, vbox1);

		// Formatea el contenedor
		hbox0.getStyleClass().addAll(spacing, padding, fondo, bordeGris, bordeRedondeado);

		// Añade la imagen al conjunto
		flowPaneImagenes.getChildren().add(hbox0);

		// Agregamos el elemento al contenedor
		flowPaneImagenes.getStyleClass().addAll(fondo, padding, spacing);
	}

	/**
	 * Selecciona el
	 * 
	 * @param li
	 * @return tipo de consulta
	 */
	private String tipoConsulta(ImagenPaciente li) {

		if (li.getTipoConsulta() == Perfil.getMedicina()) {
			return "CM - ";
		} else if (li.getTipoConsulta() == Perfil.getNutricion()) {
			return "CN - ";
		} else if (li.getTipoConsulta() == Perfil.getPodologia()) {
			return "CP - ";
		} else {
			return "* - ";
		}
	}

	/**
	 * Contructor de la ventana de dialogo que muestra la imagen a tamaño completo y
	 * en una nueva ventana
	 * 
	 * @param imagenHistClinica,
	 *            imagen de la historia clinica a mostrar
	 * @param contenedorImagen
	 */
	private void mostrarDialogoImagen(ImagenPaciente imagenHistClinica, ImageView contenedorImagen) {
		// *******************************************************************************
		// Configuracion que abre la nueva ventana de dialog al hacer click en la imagen
		// *******************************************************************************
		contenedorImagen.setOnMouseClicked(e -> {

			// Crea una nueva ventana de dialogo
			Dialog<ButtonType> dialog = new Dialog<>();
			dialog.initStyle(StageStyle.UTILITY);

			// Establece las 3 formas de cerrar la ventana
			// Arma el boton de cerrar
			ButtonType cerrar = new ButtonType("Cerrar", ButtonData.CANCEL_CLOSE);
			// Accion al cerrar con la cruz roja
			dialog.setOnCloseRequest(e2 -> dialog.close());
			// Incluso si se presiona ESCAPE (Emergency exit!)
			dialog.getDialogPane().setOnKeyPressed(e3 -> {
				if (e3.getCode() == KeyCode.ESCAPE)
					dialog.close();
			});

			// Carga la imagen
			ImageView contenedorImagenOriginal = new ImageView(imagenHistClinica.getImagen());

			// Arma el contenedor
			ScrollPane panelImagen = new ScrollPane();
			panelImagen.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
			panelImagen.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
			panelImagen.setContent(contenedorImagenOriginal);

			// Arma el contenedor, Imagen y Boton
			dialog.getDialogPane().setContent(panelImagen);
			dialog.getDialogPane().getButtonTypes().addAll(cerrar);

			// Permite el cambio de tamaño
			dialog.setResizable(true);
			// Muestra la pantalla
			dialog.show();

			// Formatea la pantalla
			if (dialog.getWidth() >= bordesPantalla.getWidth() || dialog.getHeight() >= bordesPantalla.getHeight()) {
				dialog.setWidth(bordesPantalla.getWidth() * 0.9);
				dialog.setHeight(bordesPantalla.getHeight() * 0.9);
			}
		});
	}

	/**
	 * Carga de nuevas imagenes
	 */
	@FXML
	private void subirImagen() {
		ImagenPaciente imagenNueva;
		if ((imagenNueva = guardarImagenEnBD(nuevaImagen)) != null) {

			listaImagenes.add(imagenNueva);

			Collections.sort(listaImagenes);
			Collections.reverse(listaImagenes);

			// Limpieza del formulario
			areaTextoNuevaImagen.clear();
			nuevaImagen = null;
			subible.set(false);
		}
	}

	@FXML
	private void cancelarCarga() {
		// Si obtenemos una nueva imagen, intentamos subirla
		nuevaImagen = null;
		subible.set(false);
	}

	/**
	 * Guardar los cambios en los comentarios
	 * 
	 * @param id
	 *            de la imagen
	 * @param comentario
	 *            sobre la imagen
	 */
	private void guardarCambioComentario(ImagenPaciente imagen, String comentarioNuevo) {
		servicioImagenes.actualizarComentarioImagen(imagen, comentarioNuevo, aplicacion.getSesion().getUsuario());
		armarGaleria(listaImagenes);
	}

	/**
	 * Eliminar la imagen completamente
	 * 
	 * @param id
	 *            de la imagen
	 */
	private void eliminarImagen(ImagenPaciente imagen) {
		Alert alert = Ventanas.crearAlerta(AlertType.CONFIRMATION, "Eliminación",
				String.format("¿Eliminar la imagen %s?", imagen.getId()), "", null);
		Optional<ButtonType> respuesta = alert.showAndWait();

		// Confirmamos eliminacion
		if (respuesta.isPresent() && respuesta.get().equals(ButtonType.OK)) {
			if (servicioImagenes.eliminarImagen(imagen)) {
				Mensajero.setMsg("Eliminada la imagen", Mensajero.MSG_EXITO);
				for (int index = 0; index < listaImagenes.size(); index++) {
					ImagenPaciente img = listaImagenes.get(index);
					if (imagen.getId() == img.getId()) {
						listaImagenes.remove(index);
					}
				}
				Collections.sort(listaImagenes);
				Collections.reverse(listaImagenes);
			}
		}
	}

	/**
	 * 
	 * Imagen a cargar desde la PC
	 * 
	 */
	@FXML
	private void cargarImagenDesdeArchivo() {

		// Creamos el selector de imagen
		FileChooser selectorImagen = new FileChooser();
		selectorImagen.setTitle("Escoja la imagen...");

		// Aplica un filtro a la seleccion de archivos
		selectorImagen.getExtensionFilters().add(new ExtensionFilter("Imagenes", "*.jpg", "*.jpeg", "*.png", "*.bmp"));

		// Cargamos la imagen al sistema
		nuevaImagen = selectorImagen.showOpenDialog(null);

		if (Validador.validarTamanoArchivo(nuevaImagen)) {
			// Si obtenemos una nueva imagen, intentamos subirla
			if (nuevaImagen != null) {
				subible.set(true);
			}
		} else {
			Mensajero.setMsg("La imagen supera el tamaño maximo permitido", Mensajero.MSG_ALERTA);
		}
	}

	/**
	 * Imagen a cargar
	 * 
	 * @param nuevaImagen
	 * @return true si es exitoso
	 */
	private ImagenPaciente guardarImagenEnBD(File nuevaImagen) {
		ImagenPaciente imagenNueva;
		try (FileInputStream streamImagenPaciente = new FileInputStream(nuevaImagen)) {

			int tipoConsulta = 0;
			int idConsulta = 0;

			// TODO
			// Consulta consulta = sesion.getConsultaActual();
			// if (consulta != null) {
			// idConsulta = consulta.getId();
			// if (consulta instanceof ConsultaMedica) {
			// tipoConsulta = Perfil.getMedicina();
			// } else if (consulta instanceof ConsultaMedica) {
			// tipoConsulta = Perfil.getNutricion();
			// } else if (consulta instanceof ConsultaNutric) {
			// tipoConsulta = Perfil.getPodologia();
			// }
			// }
			imagenNueva = servicioImagenes.guardarImagenNueva(nuevaImagen.getAbsolutePath(), streamImagenPaciente,
					nuevaImagen.length(), aplicacion.getSesion().getPaciente().getId(), tipoConsulta, idConsulta,
					areaTextoNuevaImagen.getText(), aplicacion.getSesion().getUsuario());
		} catch (IOException exc) {
			return null;
		}
		return imagenNueva;
	}

	private SimpleIntegerProperty minimo = new SimpleIntegerProperty(0);
	private SimpleIntegerProperty rango = new SimpleIntegerProperty(10);
	private SimpleIntegerProperty cantidad = new SimpleIntegerProperty(0);

	public final SimpleIntegerProperty minimoProperty() {
		return this.minimo;
	}

	public final int getMinimo() {
		return this.minimoProperty().get();
	}

	public final void setMinimo(final int minimo) {
		this.minimoProperty().set(minimo);
	}

	public final SimpleIntegerProperty rangoProperty() {
		return this.rango;
	}

	public final int getRango() {
		return this.rangoProperty().get();
	}

	public final void setRango(final int rango) {
		this.rangoProperty().set(rango);
	}

	public final SimpleIntegerProperty cantidadProperty() {
		return this.cantidad;
	}

	public final int getCantidad() {
		return this.cantidadProperty().get();
	}

	public final void setCantidad(final int cantidad) {
		this.cantidadProperty().set(cantidad);
	}

}
