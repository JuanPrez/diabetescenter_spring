package com.aplicacion.spring.negocio.controladores;

import java.net.URL;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.Ventanas;
import com.aplicacion.spring.gestion.controladores.ControladorHistoriaClinica;
import com.aplicacion.spring.gestion.controladores.ControladorTurnos;
import com.aplicacion.spring.negocio.entidades.paciente.AnamnesisAlimenticia;
import com.aplicacion.spring.negocio.entidades.registros.consultas.Consulta;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaNutricion;
import com.aplicacion.spring.negocio.servicios.ServiciosNutricion;
import com.aplicacion.spring.utilitarios.controladores.Util;

import de.felixroske.jfxsupport.FXMLController;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

@FXMLController
public class ControladorConsultaNutricion implements Initializable {

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	ServiciosNutricion serviciosNutricion;

	// Panel Superior
	@FXML
	private BorderPane panelCentral;
	@FXML
	private Button botonLimpiarCampos;
	@FXML
	private Button botonGuardaryCerrar;
	@FXML
	private Button botonRojo;

	// Paciente
	@FXML
	private Label etiquetaIdPaciente;
	@FXML
	private Label etiquetaEdadPaciente;
	@FXML
	private Label etiquetaOsPaciente;

	// Consulta
	@FXML
	private Label etiquetaIdConsulta;
	@FXML
	private TextField campoPeso;
	@FXML
	private TextField campoDiamCintura;
	@FXML
	private Label etiquetaIMC;
	@FXML
	private TextArea areaTextoTratamiento;
	@FXML
	private DatePicker proxCita;

	// Anamnesis
	@FXML
	private TextArea areaTextoDesayuno;
	@FXML
	private TextArea areaTextoMediaManana;
	@FXML
	private TextArea areaTextoAlmuerzo;
	@FXML
	private TextArea areaTextoMediaTarde;
	@FXML
	private TextArea areaTextoCena;
	@FXML
	private TextArea areaTextoNotasAnamnesis;
	@FXML
	private TextField campoUltUsuario;
	@FXML
	private TextField campoUltActualizacion;

	ConsultaNutricion consultaNutricion;
	AnamnesisAlimenticia anamnesisAlimenticia;

	@Autowired
	ControladorAntecedentesNutricion controladorAntecedentesNutricion;
	@Autowired
	ControladorHistoriaClinica controladorHistoriaClinica;
	@Autowired
	ControladorTurnos controladorTurnos;
	ChangeListener<Consulta> listenerConsulta = null;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// Acciones de inicializacion del formulario
		inicializarFormulario();

		// Listeners
		controladorHistoriaClinica.consultaProperty().addListener((o, consultaPrevia, consultaActual) -> {
			if (consultaActual != null)
				mostrarDatosConsulta(inicializarConsulta(consultaActual));
		});

		// Cambio de paciente
		aplicacion.getSesion().pacienteProperty().addListener((o, pacientePrevio, pacienteActual) -> {
			if (pacienteActual != null)
				mostrarAnamnesisAlimenticia(inicializarAnamnesisAlimenticia(
						aplicacion.getSesion().getPaciente().getAnamnesisAlimenticia()));
		});
	}

	private void inicializarFormulario() {
		// Disparadores automaticos
		Util.disparadorCalculadoraIMC(etiquetaIMC, aplicacion.getSesion().getPaciente().getDatosClinicos().getTalla(),
				campoPeso);
		Util.disparadorReemplazoPuntoXComa(campoPeso);
		Util.disparadorReemplazoPuntoXComa(campoDiamCintura);
		Util.validacionVisualCampoBigDecimal(campoPeso, Util.CAMPO_NORMAL);
		Util.validacionVisualCampoBigDecimal(campoDiamCintura, Util.CAMPO_NORMAL);

		// Instanciacion de los controladores relacionados
		controladorHistoriaClinica = aplicacion.getContexto().getBean(ControladorHistoriaClinica.class);
		controladorAntecedentesNutricion = aplicacion.getContexto().getBean(ControladorAntecedentesNutricion.class);
		controladorTurnos = aplicacion.getContexto().getBean(ControladorTurnos.class);

		// Inicializacion manual en primera ejecucion
		mostrarDatosConsulta(inicializarConsulta(controladorHistoriaClinica.getConsulta()));
		anamnesisAlimenticia = aplicacion.getSesion().getPaciente().getAnamnesisAlimenticia();
		anamnesisAlimenticia.setIdPaciente(aplicacion.getSesion().getPaciente().getId());
		mostrarAnamnesisAlimenticia(anamnesisAlimenticia);
	}

	/**
	 * Valida que la consulta sea del tipo valido, crea una nueva en la base de
	 * datos, y le asigna un id, se la asigna al paciente, y finalmente la muestra
	 * en pantalla
	 * 
	 * @param consultaActual
	 */
	private ConsultaNutricion inicializarConsulta(Consulta consultaActual) {
		if (consultaActual != null && consultaActual instanceof ConsultaNutricion) {
			this.consultaNutricion = new ConsultaNutricion();
			// Obtiene un nuevo ID
			this.consultaNutricion.setId(serviciosNutricion.nuevaConsulta((ConsultaNutricion) consultaActual,
					aplicacion.getSesion().getPaciente(), aplicacion.getSesion().getUsuario()));

			this.consultaNutricion.setPaciente(aplicacion.getSesion().getPaciente());
			this.consultaNutricion.setUsuario(aplicacion.getSesion().getUsuario());
		}
		return this.consultaNutricion;
	}

	/**
	 * Inicializa el anamnesis alimenticia
	 * 
	 * @param anamnesisAlimenticia
	 * @return
	 */
	private AnamnesisAlimenticia inicializarAnamnesisAlimenticia(AnamnesisAlimenticia anamnesisAlimenticia) {
		if (anamnesisAlimenticia != null)
			this.anamnesisAlimenticia = anamnesisAlimenticia;
		else
			this.anamnesisAlimenticia = new AnamnesisAlimenticia();
		this.anamnesisAlimenticia.setIdPaciente(aplicacion.getSesion().getPaciente().getId());
		return this.anamnesisAlimenticia;
	}

	/**
	 * Muestra la consulta en pantalla
	 * 
	 * @param consulta
	 */
	private void mostrarDatosConsulta(ConsultaNutricion consulta) {
		if (consulta != null) {
			// Consulta
			etiquetaIdConsulta.setText(Integer.toString(consulta.getId()));
			if (consulta.getConsultaPeso() != null) {
				campoPeso.setText(consulta.getConsultaPeso().toPlainString());
			} else {
				campoPeso.setText("0");
			}
			if (consulta.getConsultaDiam() != null) {
				campoDiamCintura.setText(consulta.getConsultaDiam().toPlainString());
			} else {
				campoDiamCintura.setText("0");
			}
			areaTextoTratamiento.setText(consulta.getTratamiento());
			if (consulta.getProxCita() != null) {
				proxCita.setValue(consulta.getProxCita());
			}
		} else {
			Mensajero.setMsg("Problemas creando y mostrando la nueva consulta", Mensajero.MSG_ALERTA);
		}
	}

	/**
	 * Muestra los datos de Anamnesis Alimenticia
	 * 
	 * @param anamnesisAlimenticia
	 */
	private void mostrarAnamnesisAlimenticia(AnamnesisAlimenticia anamnesisAlimenticia) {
		if (anamnesisAlimenticia.getDesayuno() != null) {
			areaTextoDesayuno.setText(anamnesisAlimenticia.getDesayuno());
		}
		if (anamnesisAlimenticia.getMediam() != null) {
			areaTextoMediaManana.setText(anamnesisAlimenticia.getMediam());
		}
		if (anamnesisAlimenticia.getAlmuerzo() != null) {
			areaTextoAlmuerzo.setText(anamnesisAlimenticia.getAlmuerzo());
		}
		if (anamnesisAlimenticia.getMediat() != null) {
			areaTextoMediaTarde.setText(anamnesisAlimenticia.getMediat());
		}
		if (anamnesisAlimenticia.getCena() != null) {
			areaTextoCena.setText(anamnesisAlimenticia.getCena());
		}
		if (anamnesisAlimenticia.getNotas() != null) {
			areaTextoNotasAnamnesis.setText(anamnesisAlimenticia.getNotas());
		}
		if (anamnesisAlimenticia.getUsuario() != null) {
			campoUltUsuario.setText(anamnesisAlimenticia.getUsuario());
		}
		if (anamnesisAlimenticia.getFechaUltAct() != null) {
			campoUltActualizacion.setText(anamnesisAlimenticia.getFechaUltAct().toString());
		}
	}

	/**
	 * Recupera los datos del formulario, los completa en la consulta pasada y los
	 * devuelve
	 * 
	 * @param consulta
	 * @return la misma consulta
	 */
	private ConsultaNutricion tomarFormularioConsulta(ConsultaNutricion consulta) {
		consulta.setTratamiento(areaTextoTratamiento.getText());
		consulta.setConsultaPeso(Util.convertirCampoBigDecimal(campoPeso, 3));
		consulta.setConsultaDiam(Util.convertirCampoBigDecimal(campoDiamCintura, 1));
		return consulta;
	}

	/**
	 * Recupera los datos del formulario, los completa en la anamnesis alimenticia
	 * pasada y los devuelve
	 * 
	 * @param anamnesisAlimenticia
	 * @return la misma anamnesisAlimenticia
	 */
	private AnamnesisAlimenticia tomarFormularioAnamnesisAlimenticia(AnamnesisAlimenticia anamnesisAlimenticia) {
		anamnesisAlimenticia.setDesayuno(areaTextoDesayuno.getText());
		anamnesisAlimenticia.setMediam(areaTextoMediaManana.getText());
		anamnesisAlimenticia.setAlmuerzo(areaTextoAlmuerzo.getText());
		anamnesisAlimenticia.setMediat(areaTextoMediaTarde.getText());
		anamnesisAlimenticia.setCena(areaTextoCena.getText());
		anamnesisAlimenticia.setNotas(areaTextoNotasAnamnesis.getText());
		anamnesisAlimenticia.setDesayuno(areaTextoDesayuno.getText());
		if (anamnesisAlimenticia.getFechaUltAct() == null)
			anamnesisAlimenticia.setFechaUltAct(LocalDate.now());
		return anamnesisAlimenticia;
	}

	/**
	 * Llamada al servicio para guardar los datos
	 * 
	 * @param anamnesis
	 * @return
	 */
	private boolean guardarAnamnesis(AnamnesisAlimenticia anamnesis) {
		return serviciosNutricion.guardarAnamnesisAlimenticia(tomarFormularioAnamnesisAlimenticia(anamnesis),
				aplicacion.getSesion().getUsuario().getUid());
	}

	/**
	 * Llamada a validacion y al servicio para guardar los datos
	 * 
	 * @return
	 */
	private boolean guardarConsulta(ConsultaNutricion consulta) {
		return serviciosNutricion.actualizarConsulta(tomarFormularioConsulta(consulta),
				aplicacion.getSesion().getUsuario(), false);
	}

	/**
	 * Verifica las fechas, y las asigna
	 * 
	 * @return true si la consulta contiene fecha de consulta y proxima cita
	 */
	private void asignarFechas(Consulta consulta) {
		// Si la proxCita no esta configurada, lo advertimos
		if (consulta.getFecha() == null) {
			consulta.setFecha(LocalDate.now());
		}

		// Verifica la fecha antes de guardar,
		consulta.setProxCita(validarProxCita(proxCita.getValue()));
	}

	/**
	 * Verifica las fechas, y las asigna
	 */
	private LocalDate validarProxCita(LocalDate proxCita) {
		// Valida que la prox cita este programada
		if (proxCita == null) {
			Mensajero.setMsg("Ingrese una fecha para la próxima cita", Mensajero.MSG_INFO);
			return null;
		} else {
			if (Util.validarFechaFutura(proxCita)) {
				return proxCita;
			} else {
				Mensajero.setMsg("La próxima cita debe ser una fecha futura", Mensajero.MSG_INFO);
				return null;
			}
		}
	}

	@FXML
	void limpiarCampos(ActionEvent event) {
		// Cuadro de dialogo para la confirmacion
		Optional<ButtonType> respuesta = Ventanas.crearAlerta(AlertType.CONFIRMATION, "Limpiar campos", "",
				"¿Desea descartar los datos de todos los campos?", null).showAndWait();
		// Vacia los datos de los campos pasados
		if (respuesta.isPresent() && respuesta.get().equals(ButtonType.YES)) {
			campoPeso.clear();
			campoDiamCintura.clear();
			areaTextoTratamiento.clear();
		}
	}

	@FXML
	private void guardarConsulta(ActionEvent event) {
		asignarFechas(consultaNutricion);
		// Si la fecha no esta asignada, la asigna
		if (consultaNutricion.getProxCita() != null) {
			if (guardarConsulta(consultaNutricion) && guardarAnamnesis(anamnesisAlimenticia)) {
				Mensajero.setMsg("Consulta guardada", Mensajero.MSG_EXITO);
				// Actualiza la lista de consultas
				controladorAntecedentesNutricion.refrescarListaConsultas();
				aplicacion.getSesion().setTurno(controladorTurnos.cerrarTurno(aplicacion.getSesion().getTurno()));

				// Cierra la consulta
				controladorHistoriaClinica.setConsulta(null);
			} else {
				Mensajero.setMsg("No se pudo guardar la consulta y la anamnesis alimenticia", Mensajero.MSG_ALERTA);
			}
		} else {
			proxCita.requestFocus();
		}
	}

	@FXML
	private void descartarConsulta(ActionEvent event) {
		// Confirma y elimina la consulta creada recientemente
		Alert alerta = Ventanas.crearAlerta(AlertType.CONFIRMATION, "Descartar consulta", "",
				"¿Desea descartar la consulta?", null);
		Optional<ButtonType> respuesta = alerta.showAndWait();
		if (respuesta.isPresent() && respuesta.get().equals(ButtonType.OK)) {
			if (serviciosNutricion.eliminarConsulta(consultaNutricion.getId())) {
				Mensajero.setMsg("Consulta descartada", Mensajero.MSG_INFO);
			} else {
				Mensajero.setMsg("Problemas descartando la consulta", Mensajero.MSG_ALERTA);
			}
			controladorHistoriaClinica.setConsulta(null);
		}
	}
}