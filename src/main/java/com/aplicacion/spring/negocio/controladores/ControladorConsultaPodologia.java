package com.aplicacion.spring.negocio.controladores;

import java.net.URL;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.Ventanas;
import com.aplicacion.spring.gestion.controladores.ControladorHistoriaClinica;
import com.aplicacion.spring.gestion.controladores.ControladorTurnos;
import com.aplicacion.spring.negocio.entidades.paciente.Antropometricos;
import com.aplicacion.spring.negocio.entidades.registros.consultas.Consulta;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaPodologica;
import com.aplicacion.spring.negocio.servicios.ServiciosPodologia;
import com.aplicacion.spring.utilitarios.controladores.Util;

import de.felixroske.jfxsupport.FXMLController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

@FXMLController
public class ControladorConsultaPodologia implements Initializable {

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	ServiciosPodologia serviciosPodologia;

	// Panel superior
	@FXML
	private Button botonLimpiarCampos;
	@FXML
	private Button botonGuardar;
	@FXML
	private Button botonGuardaryCerrar;
	@FXML
	private Button botonRojo;
	@FXML
	private Label idConsulta;

	// Antropometricos
	@FXML
	private TextField campoTextoNroCalzado;
	@FXML
	private TextField campoTextoFormulaDigital;
	@FXML
	private TextArea areaTextoAntecedentes;

	// Consulta
	@FXML
	private TextArea areaTextoOnicopatias;
	@FXML
	private TextArea areaTextoDedos;
	@FXML
	private TextArea areaTextoTemperatura;
	@FXML
	private TextArea areaTextoTexturaPiel;
	@FXML
	private TextArea areaTextoColoracionPiel;
	@FXML
	private TextArea areaTextoPulsoTibial;
	@FXML
	private TextArea areaTextoPulsoPedio;
	@FXML
	private TextArea areaTextoObservaciones;
	@FXML
	private TextArea areaTextoEvolTratamiento;
	@FXML
	private DatePicker proxCita;

	ConsultaPodologica consultaPodologia;
	Antropometricos antropometricos;

	@Autowired
	ControladorAntecedentesPodologia controladorAntecedentesPodologia;
	@Autowired
	ControladorHistoriaClinica controladorHistoriaClinica;
	@Autowired
	ControladorTurnos controladorTurnos;
	ChangeListener<Consulta> listenerConsulta = null;

	/**
	 * 
	 * Metodo de iniciacion del formulario
	 * 
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// Acciones de inicializacion del formulario
		inicializarFormulario();

		//
		// Cambio en la consulta
		// Primera ejecucion manual
		// TODO, Refactor: remover condicion de primera consulta
		if (listenerConsulta == null && controladorHistoriaClinica.getConsulta() != null) {
			// Inicializacion de la consulta del formulario
			consultaPodologia = inicializarConsulta(controladorHistoriaClinica.getConsulta());
			// Si la consulta no es nula, muestra los datos en pantalla
			if (consultaPodologia != null)
				mostrarDatosConsulta(consultaPodologia);
		}

		listenerConsulta = new ChangeListener<Consulta>() {
			@Override
			public void changed(ObservableValue<? extends Consulta> observable, Consulta consultaPrevia,
					Consulta consultaActual) {
				if (consultaActual == null) {
					consultaPodologia = null;
				} else {
					// Inicializacion de la consulta del formulario
					consultaPodologia = inicializarConsulta(consultaActual);
				}
				// Si la consulta no es nula, muestra los datos en pantalla
				if (consultaPodologia != null)
					mostrarDatosConsulta(consultaPodologia);
			}
		};
		controladorHistoriaClinica.consultaProperty().addListener(listenerConsulta);

		//
		// Cambio de paciente
		aplicacion.getSesion().pacienteProperty().addListener((o, pacientePrevio, pacienteActual) -> {
			if (pacienteActual != null)
				mostrarAntropometricos(
						inicializarAntropometricos(aplicacion.getSesion().getPaciente().getDatosAntropometricos()));
		});

	}

	private void inicializarFormulario() {
		// Disparadores automaticos
		Util.validacionVisualCampoBigDecimal(campoTextoNroCalzado, Util.CAMPO_NORMAL);

		// Instanciacion de los controladores relacionados
		controladorHistoriaClinica = aplicacion.getContexto().getBean(ControladorHistoriaClinica.class);
		controladorAntecedentesPodologia = aplicacion.getContexto().getBean(ControladorAntecedentesPodologia.class);
		controladorTurnos = aplicacion.getContexto().getBean(ControladorTurnos.class);

		// Inicializacion manual en primera ejecucion
		mostrarDatosConsulta(inicializarConsulta(controladorHistoriaClinica.getConsulta()));
		antropometricos = aplicacion.getSesion().getPaciente().getDatosAntropometricos();
		mostrarAntropometricos(antropometricos);
	}

	/**
	 * Valida que la consulta sea del tipo valido, crea una nueva en la base de
	 * datos, y le asigna un id, se la asigna al paciente, y finalmente la muestra
	 * en pantalla
	 * 
	 * @param consultaActual
	 */
	private ConsultaPodologica inicializarConsulta(Consulta consultaActual) {
		if (consultaActual != null && consultaActual instanceof ConsultaPodologica) {
			this.consultaPodologia = new ConsultaPodologica();
			// Obtiene un nuevo ID
			this.consultaPodologia.setId(serviciosPodologia.nuevaConsulta((ConsultaPodologica) consultaActual,
					aplicacion.getSesion().getPaciente(), aplicacion.getSesion().getUsuario()));
			this.consultaPodologia.setPaciente(aplicacion.getSesion().getPaciente());
			this.consultaPodologia.setUsuario(aplicacion.getSesion().getUsuario());
		}
		return this.consultaPodologia;
	}

	/**
	 * Inicializa el anamnesis alimenticia
	 * 
	 * @param anamnesisAlimenticia
	 * @return
	 */
	private Antropometricos inicializarAntropometricos(Antropometricos antropometricos) {
		if (antropometricos != null)
			this.antropometricos = antropometricos;
		else
			this.antropometricos = new Antropometricos();
		return this.antropometricos;
	}

	/**
	 * 
	 * Recupera la consulta salvada en persistencia
	 * 
	 * @param consulta
	 */
	private void mostrarDatosConsulta(ConsultaPodologica consulta) {
		areaTextoOnicopatias.setText(consulta.getOnicopatias());
		areaTextoDedos.setText(consulta.getDedos());
		areaTextoTemperatura.setText(consulta.getTemperatura());
		areaTextoTexturaPiel.setText(consulta.getTexturaPiel());
		areaTextoColoracionPiel.setText(consulta.getColoracionPiel());
		areaTextoPulsoTibial.setText(consulta.getPulsoTibial());
		areaTextoPulsoPedio.setText(consulta.getPulsoPedio());
		areaTextoObservaciones.setText(consulta.getObservaciones());
		areaTextoEvolTratamiento.setText(consulta.getEvolucion());
		if (consulta.getProxCita() != null) {
			proxCita.setValue(consulta.getProxCita());
		}
	}

	/**
	 * Recupera los datos del objeto de persistencia
	 * 
	 * @param antropometricos
	 */
	private void mostrarAntropometricos(Antropometricos antropometricos) {
		if (antropometricos.getNumeroCalzado() != null) {
			campoTextoNroCalzado.setText(antropometricos.getNumeroCalzado().toString());
		}
		if (antropometricos.getFormulaDigital() != null) {
			campoTextoFormulaDigital.setText(antropometricos.getFormulaDigital());
		}
		if (antropometricos.getAntecedentes() != null) {
			areaTextoAntecedentes.setText(antropometricos.getAntecedentes());
		}
	}

	/**
	 * Recupera los datos del formulario, los completa en la consulta pasada y los
	 * devuelve
	 * 
	 * @param consulta
	 * @return la misma consulta
	 */
	private ConsultaPodologica tomarFormularioConsulta(ConsultaPodologica consulta) {
		consulta.setOnicopatias(areaTextoOnicopatias.getText());
		consulta.setDedos(areaTextoDedos.getText());
		consulta.setTemperatura(areaTextoTemperatura.getText());
		consulta.setTexturaPiel(areaTextoTexturaPiel.getText());
		consulta.setColoracionPiel(areaTextoColoracionPiel.getText());
		consulta.setPulsoTibial(areaTextoPulsoTibial.getText());
		consulta.setPulsoPedio(areaTextoPulsoPedio.getText());
		consulta.setObservaciones(areaTextoObservaciones.getText());
		consulta.setEvolucion(areaTextoEvolTratamiento.getText());
		return consulta;
	}

	/**
	 * Recupera los datos del formulario, los completa en la anamnesis alimenticia
	 * pasada y los devuelve
	 * 
	 * @param anamnesisAlimenticia
	 * @return la misma anamnesisAlimenticia
	 */
	private Antropometricos tomarFormularioAntropometricos(Antropometricos antropometricos) {
		antropometricos.setNumeroCalzado(Util.convertirCampoBigDecimal(campoTextoNroCalzado, 1));
		// campoTextoNroCalzado.setText(antropometricos.getNumeroCalzado().toString());
		antropometricos.setFormulaDigital(campoTextoFormulaDigital.getText());
		antropometricos.setAntecedentes(areaTextoAntecedentes.getText());
		return antropometricos;
	}

	/**
	 * Accion del boton de limpiar los campos, pide confirmacion
	 * 
	 * @param event
	 */
	@FXML
	void limpiarCampos(ActionEvent event) {
		// Cuadro de dialogo para la confirmacion
		Optional<ButtonType> respuesta = Ventanas.crearAlerta(AlertType.CONFIRMATION, "Limpiar campos", "",
				"¿Desea descartar los datos de todos los campos?", null).showAndWait();
		// Vacia los datos de los campos pasados
		if (respuesta.isPresent() && respuesta.get().equals(ButtonType.YES)) {
			campoTextoNroCalzado.clear();
			campoTextoFormulaDigital.clear();
			areaTextoAntecedentes.clear();
			areaTextoOnicopatias.clear();
			areaTextoDedos.clear();
			areaTextoTemperatura.clear();
			areaTextoTexturaPiel.clear();
			areaTextoColoracionPiel.clear();
			areaTextoPulsoTibial.clear();
			areaTextoPulsoPedio.clear();
			areaTextoObservaciones.clear();
			areaTextoEvolTratamiento.clear();
			proxCita.setValue(null);
		}
	}

	/**
	 * Accion del boton de guardar Guarda los datos de la consulta actual y cierra
	 * la misma
	 * 
	 * @param event
	 */
	@FXML
	void guardarConsulta(ActionEvent event) {
		asignarFechas(consultaPodologia);
		// Si la fecha no esta asignada, la asigna
		if (consultaPodologia.getProxCita() != null) {
			if (guardarConsulta(consultaPodologia) && guardarAntropometricos(antropometricos)) {
				Mensajero.setMsg("Consulta guardada", Mensajero.MSG_EXITO);
				// Actualiza la lista de consultas
				controladorAntecedentesPodologia.refrescarListaConsultas();
				aplicacion.getSesion().setTurno(controladorTurnos.cerrarTurno(aplicacion.getSesion().getTurno()));
				// Cierra la consulta
				controladorHistoriaClinica.setConsulta(null);
			} else {
				Mensajero.setMsg("No se pudo guardar la consulta y la anamnesis alimenticia", Mensajero.MSG_ALERTA);
			}
		} else {
			proxCita.requestFocus();
		}
	}

	/**
	 * Llamada al servicio para guardar los datos
	 * 
	 * @param anamnesis
	 * @return
	 */
	private boolean guardarAntropometricos(Antropometricos antropometricos) {
		return serviciosPodologia.guardarAntropometricos(tomarFormularioAntropometricos(antropometricos),
				aplicacion.getSesion().getPaciente().getId(), aplicacion.getSesion().getUsuario().getUid());
	}

	/**
	 * Llamada a validacion y al servicio para guardar los datos
	 * 
	 * @return
	 */
	private boolean guardarConsulta(ConsultaPodologica consulta) {
		return serviciosPodologia.actualizarConsulta(tomarFormularioConsulta(consulta),
				aplicacion.getSesion().getUsuario(), false);
	}

	/**
	 * Verifica las fechas, y las asigna
	 * 
	 * @return true si la consulta contiene fecha de consulta y proxima cita
	 */
	private void asignarFechas(Consulta consulta) {
		// Si la proxCita no esta configurada, lo advertimos
		if (consulta.getFecha() == null) {
			consulta.setFecha(LocalDate.now());
		}
		// Verifica la fecha antes de guardar,
		consulta.setProxCita(validarProxCita(proxCita.getValue()));
	}

	/**
	 * Verifica las fechas, y las asigna
	 */
	private LocalDate validarProxCita(LocalDate proxCita) {
		// Valida que la prox cita este programada
		if (proxCita == null) {
			Mensajero.setMsg("Ingrese una fecha para la próxima cita", Mensajero.MSG_INFO);
			return null;
		} else {
			if (Util.validarFechaFutura(proxCita)) {
				return proxCita;
			} else {
				Mensajero.setMsg("La próxima cita debe ser una fecha futura", Mensajero.MSG_INFO);
				return null;
			}
		}
	}

	/**
	 * 
	 * Accion del boton de Descartar Cancela la consulta, eliminandola, aunque
	 * mantiene los datos del paciente abiertos
	 * 
	 * @param event
	 */
	@FXML
	private void descartarConsulta(ActionEvent event) {
		// Confirma y elimina la consulta creada recientemente
		Alert alerta = Ventanas.crearAlerta(AlertType.CONFIRMATION, "Descartar consulta", "",
				"¿Desea descartar la consulta?", null);
		Optional<ButtonType> respuesta = alerta.showAndWait();
		if (respuesta.isPresent() && respuesta.get().equals(ButtonType.OK)) {
			if (serviciosPodologia.eliminarConsulta(consultaPodologia.getId())) {
				Mensajero.setMsg("Consulta descartada", Mensajero.MSG_INFO);
			} else {
				Mensajero.setMsg("Problemas descartando la consulta", Mensajero.MSG_ALERTA);
			}
			controladorHistoriaClinica.setConsulta(null);
		}
	}
}