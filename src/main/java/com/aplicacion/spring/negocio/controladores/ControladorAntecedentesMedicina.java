package com.aplicacion.spring.negocio.controladores;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Ventanas;
import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.gestion.controladores.ControladorHistoriaClinica;
import com.aplicacion.spring.gestion.entidades.Especialidad;
import com.aplicacion.spring.gestion.entidades.Perfil;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.consultas.Consulta;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaMedicina;
import com.aplicacion.spring.negocio.servicios.ServiciosMedicina;
import com.aplicacion.spring.utilitarios.Constantes;
import com.aplicacion.spring.utilitarios.controladores.Util;

import de.felixroske.jfxsupport.FXMLController;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

@FXMLController
public class ControladorAntecedentesMedicina implements Initializable {

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	ServiciosMedicina serviciosConsultas;

	private static final Especialidad TODAS_ESP = new Especialidad(0, "Todas");

	// Panel superior
	@FXML
	public Button botonGuardar;
	@FXML
	public Button botonDescartar;
	@FXML
	public Button botonNueva;
	@FXML
	public Button botonModificar;
	@FXML
	private ComboBox<Especialidad> selectorEspecialidad;
	@FXML
	private Label idConsulta;

	// Panel interno superior
	@FXML
	private Label idPaciente;
	@FXML
	private BorderPane panelHistClinica;
	@FXML
	private TextField campoPulso;
	@FXML
	private Label etiquetaIMC;
	@FXML
	private TextField campoPresionDias;
	@FXML
	private TextField campoPresionSist;
	@FXML
	private TextField campoTextoDiagnostico;
	@FXML
	private TextField campoTextoMotConsulta;
	@FXML
	private TextField campoPeso;
	@FXML
	private TextArea areaTextoEvolucion;
	@FXML
	private TextArea areaTextoComplementario;
	@FXML
	private TextArea areaTextoTratamiento;
	@FXML
	private TextField campoDiamCintura;
	@FXML
	private TextField campoUltimaConsultaFecha;

	@FXML
	private TableView<ConsultaMedicina> tablaConsultasPrevias;
	@FXML
	private TableColumn<ConsultaMedicina, String> columnaFecha;
	@FXML
	private TableColumn<ConsultaMedicina, String> columnaUsuario;
	@FXML
	private TableColumn<ConsultaMedicina, String> columnaEsp;

	ControladorHistoriaClinica controladorHistoriaClinica;
	ConsultaMedicina consulta;
	SimpleBooleanProperty editable = new SimpleBooleanProperty(false);
	ChangeListener<Paciente> listenerPaciente = null;
	ChangeListener<Boolean> listenerEditable = null;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		// Listener de la propiedad editable de la propia pantalla
		listenerEditable = new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean estadoPrevio,
					Boolean estadoActual) {
				if (estadoActual)
					permitirEdicion(tablaConsultasPrevias.getSelectionModel().getSelectedItem());
				else
					bloquearEdicion();
			}
		};
		editable.addListener(listenerEditable);

		// En caso de que el listener no este activo, hace una llamada manual
		if (listenerPaciente == null && aplicacion.getSesion().getPaciente() != null)
			refrescarPantalla();

		// Implementation del listener
		listenerPaciente = new ChangeListener<Paciente>() {
			@Override
			public void changed(ObservableValue<? extends Paciente> observable, Paciente pacientePrevio,
					Paciente pacienteActual) {
				if (pacienteActual != null) {
					refrescarPantalla();
				} else {
					limpiarDatosConsultas();
					limpiarTablaConsultas();
				}
			}
		};
		aplicacion.getSesion().pacienteProperty().addListener(listenerPaciente);

		inicializarPantalla();
	}

	private void inicializarPantalla() {
		// Instanciacion de los controladores relacionados
		controladorHistoriaClinica = aplicacion.getContexto().getBean(ControladorHistoriaClinica.class);

		// Acciones de los botones
		verBoton(botonNueva, editable.get(), Perfil.getMedicina());
		verBoton(botonModificar, editable.get(), Perfil.getMedicina());
		botonNueva.disableProperty().bind(controladorHistoriaClinica.consultaProperty().isNotNull());
		botonGuardar.visibleProperty().bind(editable);
		botonDescartar.visibleProperty().bind(editable);
		tablaConsultasPrevias.disableProperty().bind(editable);
		selectorEspecialidad.disableProperty().bind(editable);

		// Disparadores automaticos
		disparadorCalculadorIMC(campoPeso, etiquetaIMC);
		disparadorSeleccionConsultasPrevias();
		disparadorEspecialidad();

		// Configuraciones
		configurarTablaConsultas();
		construirSelectorEspecialidad(aplicacion.getRepositorios().getListaEspecialidad());

		// Inicializacion
		refrescarPantalla();

		Platform.runLater(() -> {
			seleccionarEspecialidad();
		});
	}

	private void verBoton(Button boton, boolean edicion, int perfil) {
		boton.setVisible(!edicion && aplicacion.getSesion().getUsuario().getPerfil().getId() == perfil);
	}

	private void refrescarPantalla() {
		// Limpia el formulario
		limpiarDatosConsultas();
		// Lispia la tabla
		limpiarTablaConsultas();
		// Reiniciar el modo
		reiniciarEditable();
		// Evita su modificación
		bloquearEdicion();
		// Refresca la lista de consultas
		refrescarConsultas();
	}

	/**
	 * Configuracion de los valores de la tabla
	 */
	private void configurarTablaConsultas() {
		columnaFecha.setCellValueFactory(
				data -> new SimpleStringProperty(data.getValue().getFecha().format(Constantes.FECHA_DD_MM_YYYY)));
		columnaEsp.setCellValueFactory(
				data -> new SimpleStringProperty(obtenerEspecialidadProfesional(data.getValue().getTipoConsulta())));
		columnaUsuario
				.setCellValueFactory(data -> new SimpleStringProperty(data.getValue().getUsuario().getNombreUsuario()));
	}

	/**
	 * Construye el selector de especialidades
	 */
	private void construirSelectorEspecialidad(List<Especialidad> especialidadesValidas) {
		// Añade la categoria
		if (!especialidadesValidas.contains(TODAS_ESP)) {
			especialidadesValidas.add(TODAS_ESP);
		}
		for (Especialidad e : especialidadesValidas) {
			if (e.getId() == 100) {
				especialidadesValidas.remove(e);
			}
		}
		selectorEspecialidad.getItems().addAll(especialidadesValidas);
	}

	/**
	 * Selecciona especialidad
	 */
	private void seleccionarEspecialidad() {
		selectorEspecialidad.getSelectionModel().select(TODAS_ESP);
	}

	/**
	 * Busca la especialidad vinculada al tipo de consulta
	 * 
	 * @param tipoConsulta
	 * @return nombre de la especialidad
	 */
	private String obtenerEspecialidadProfesional(int tipoConsulta) {
		for (Especialidad especialidad : aplicacion.getRepositorios().getListaEspecialidad()) {
			if (tipoConsulta == especialidad.getId()) {
				return especialidad.getDescripcion();
			}
		}
		return null;
	}

	/**
	 * Completa el formulario con la consulta pasada
	 */
	private void mostrarConsulta(ConsultaMedicina consulta) {
		// Completa el formulario de clinica con los datos
		// Conversion de Numero a Texto
		try {
			idConsulta.setText("#" + Integer.toString(consulta.getId()));
			// Los atributos BigDecimal usan la funcion .toPlainString
			if (consulta.getPeso() != null) {
				campoPeso.setText(consulta.getPeso().toPlainString());
			}
			if (consulta.getDiam() != null) {
				campoDiamCintura.setText(consulta.getDiam().toPlainString());
			}
			// A los atributos Integer se los convierte en text usando Integer.toString()
			campoPresionDias.setText(Integer.toString(consulta.getDias()));
			campoPresionSist.setText(Integer.toString(consulta.getSist()));
			campoPulso.setText(Integer.toString(consulta.getPulso()));
		} catch (NumberFormatException exc) {
		}
		campoTextoMotConsulta.setText(consulta.getMotivo());
		campoTextoDiagnostico.setText(consulta.getDiagn());
		areaTextoEvolucion.setText(consulta.getEvoluc());
		areaTextoTratamiento.setText(consulta.getTrata());
		areaTextoComplementario.setText(consulta.getComple());
		campoUltimaConsultaFecha.setText(consulta.getFecha().format(Constantes.FECHA_DD_MM_YYYY));
	}

	/**
	 * Reinicia el modo de edicion
	 */
	private void reiniciarEditable() {
		editable.set(false);
	}

	/**
	 * Permite la edicion de la consulta pasada
	 * 
	 * @param consulta
	 */
	private void permitirEdicion(ConsultaMedicina consulta) {
		// Consulta
		if (consulta != null) {
			campoPeso.setEditable(true);
			campoPresionDias.setEditable(true);
			campoPresionSist.setEditable(true);
			campoDiamCintura.setEditable(true);
			campoPulso.setEditable(true);
			campoTextoMotConsulta.setEditable(true);
			campoTextoDiagnostico.setEditable(true);
			areaTextoComplementario.setEditable(true);
			areaTextoTratamiento.setEditable(true);
			areaTextoEvolucion.setEditable(true);
		}
	}

	/**
	 * Deshabilita el formulario
	 */
	private void bloquearEdicion() {
		// Consulta
		if (campoPeso.isEditable())
			campoPeso.setEditable(false);
		if (campoPresionDias.isEditable())
			campoPresionDias.setEditable(false);
		if (campoPresionSist.isEditable())
			campoPresionSist.setEditable(false);
		if (campoDiamCintura.isEditable())
			campoDiamCintura.setEditable(false);
		if (campoPulso.isEditable())
			campoPulso.setEditable(false);
		if (campoTextoMotConsulta.isEditable())
			campoTextoMotConsulta.setEditable(false);
		if (campoTextoDiagnostico.isEditable())
			campoTextoDiagnostico.setEditable(false);
		if (areaTextoComplementario.isEditable())
			areaTextoComplementario.setEditable(false);
		if (areaTextoTratamiento.isEditable())
			areaTextoTratamiento.setEditable(false);
		if (areaTextoEvolucion.isEditable())
			areaTextoEvolucion.setEditable(false);
	}

	/**
	 * Limpia el formulario
	 */
	private void limpiarDatosConsultas() {
		campoPeso.clear();
		etiquetaIMC.setText("");
		campoPresionDias.clear();
		campoPresionSist.clear();
		campoDiamCintura.clear();
		campoPulso.clear();
		campoTextoMotConsulta.clear();
		campoTextoDiagnostico.clear();
		areaTextoComplementario.clear();
		areaTextoTratamiento.clear();
		areaTextoEvolucion.clear();
	}

	/**
	 * Limpia la tabla de consultas
	 */
	private void limpiarTablaConsultas() {
		tablaConsultasPrevias.getItems().clear();
	}

	/**
	 * Escucha el campo peso para calcular el IMC de acuerdo al peso y talla
	 */
	private void disparadorCalculadorIMC(TextField campo, Label etiqueta) {
		campo.textProperty().addListener((o, p, a) -> {
			try {
				// Retorna el valor calculado, o "NA" si no pudo ser calculado
				etiqueta.setText(Util.calculadorIMC(a,
						aplicacion.getSesion().getPaciente().getDatosClinicos().getTalla()));
			} catch (NullPointerException | NumberFormatException exc) {
				// Si el numero no es valido, mostramos Err
				etiqueta.setText("Err");
			}
		});
	}

	/**
	 * Escucha la seleccion de la tabla de consultas previas y muestra la consulta
	 * seleccionada
	 */
	private void disparadorSeleccionConsultasPrevias() {
		// Agrega un listener para que recupere el valor seleccionado
		tablaConsultasPrevias.getSelectionModel().selectedItemProperty().addListener((o, p, a) -> {
			if (a != null) {
				// Limpia el formulario
				limpiarDatosConsultas();
				// Muestra la consulta
				mostrarConsulta(a);
			}
		});
	}

	/**
	 * Disparador seleccion de especialidad
	 */
	private void disparadorEspecialidad() {
		selectorEspecialidad.valueProperty().addListener((o, especialidadPrevia, especialidadActual) -> {
			refrescarListaConsultas();
			if (especialidadActual != null) {
				// Usa la lista completa
				if (!especialidadActual.equals(TODAS_ESP)) {

					List<ConsultaMedicina> listaConsultas = new ArrayList<>();
					// Recorta la lista por especialidad
					for (ConsultaMedicina consulta : tablaConsultasPrevias.getItems()) {
						if (consulta.getTipoConsulta() == especialidadActual.getId()) {
							listaConsultas.add(consulta);
						}
					}
					tablaConsultasPrevias.setItems(FXCollections.observableArrayList(listaConsultas));
				}
				if (!tablaConsultasPrevias.getItems().isEmpty())
					Platform.runLater(() -> tablaConsultasPrevias.getSelectionModel().select(0));
				else
					limpiarDatosConsultas();

			}
		});

	}

	/**
	 * Refresca la lista de consultas
	 */
	public void refrescarListaConsultas() {
		mostrarConsultasPrevias(serviciosConsultas.obtenerConsultas(aplicacion.getSesion().getPaciente()));
	}

	/**
	 * Obtiene las consultas previas
	 * 
	 * @param listaConsultas
	 */
	private void mostrarConsultasPrevias(List<ConsultaMedicina> listaConsultas) {
		if (listaConsultas == null)
			listaConsultas = new ArrayList<>();
		else
			tablaConsultasPrevias.setItems(FXCollections.observableArrayList(listaConsultas));
	}

	@FXML
	private void modificarConsulta() {
		if (aplicacion.getSesion().getUsuario().getPerfil().getId() == Perfil.getMedicina()) {
			if (tablaConsultasPrevias.getSelectionModel().getSelectedItem().getUsuario().getUid() != aplicacion
					.getSesion().getUsuario().getUid()) {
				Alert confirmacion = Ventanas.crearAlerta(AlertType.CONFIRMATION, "Modificacion de registro previo",
						"Esta intentando modificar un registro previo de otro profesional",
						"Esta seguro que desea realizar la edicion, los cambios realizados, asi como los autores, seran registrados",
						null);

				Optional<ButtonType> respuesta = confirmacion.showAndWait();

				if (respuesta.isPresent()) {
					if (respuesta.get().equals(ButtonType.OK))
						editable.set(true);
				}
			} else {
				editable.set(true);
			}
		} else {
			Mensajero.setMsg("No tiene acceso para la acción", Mensajero.MSG_ALERTA);
		}
	}

	@FXML
	private void nuevaConsulta() {
		// Evalua
		controladorHistoriaClinica
				.setConsulta(!tablaConsultasPrevias.getItems().isEmpty() ? tablaConsultasPrevias.getItems().get(0)
						: Consulta.nuevaConsulta(Consulta.MEDICINA));
	}

	@FXML
	private void guardarCambios() {
		// Prepara la consulta
		consulta = tablaConsultasPrevias.getSelectionModel().getSelectedItem();
		// Pasa el ID a Enmienda
		consulta.setEnmienda(consulta.getId());
		// Crea un nuevo ID
		consulta.setId(serviciosConsultas.nuevaConsulta(consulta, aplicacion.getSesion().getPaciente(),
				aplicacion.getSesion().getUsuario()));
		// Pasa el ID
		consulta = tomarDatosFormulario(consulta);

		if (serviciosConsultas.actualizarConsulta(consulta, aplicacion.getSesion().getUsuario(), true)) {
			refrescarConsultas();
			editable.set(false);
			Mensajero.setMsg("Guardadas las modificaciones en consulta # " + consulta.getId(), Mensajero.MSG_EXITO);
		}
	}

	/**
	 * Use el panel de especialidad para recuperar las consultas nuevamente
	 */
	private void refrescarConsultas() {
		Especialidad actual = selectorEspecialidad.getSelectionModel().getSelectedItem();
		selectorEspecialidad.getSelectionModel().select(null);
		selectorEspecialidad.getSelectionModel().select(actual);
	}

	@FXML
	private void descartarCambios() {
		// Vuelve a mostrar la misma consulta
		for (ConsultaMedicina consulta : tablaConsultasPrevias.getItems()) {
			if (consulta.getId() == Integer.parseInt(idConsulta.getText().replaceAll("#", ""))) {
				mostrarConsulta(consulta);
			}
			break;
		}
		consulta = null;
		editable.set(false);
	}

	private ConsultaMedicina tomarDatosFormulario(ConsultaMedicina consulta) {
		// Cambia el estado de editabilidad de los campos
		consulta.setPeso(Util.convertirCampoBigDecimal(campoPeso, 3));
		consulta.setDiam(Util.convertirCampoBigDecimal(campoDiamCintura, 1));
		consulta.setDias(Util.evaluarCampoNumerico(campoPresionDias));
		consulta.setSist(Util.evaluarCampoNumerico(campoPresionSist));
		consulta.setPulso(Util.evaluarCampoNumerico(campoPulso));

		consulta.setMotivo(campoTextoMotConsulta.getText());
		consulta.setDiagn(campoTextoDiagnostico.getText());
		consulta.setTrata(areaTextoTratamiento.getText());
		consulta.setEvoluc(areaTextoEvolucion.getText());
		consulta.setComple(areaTextoComplementario.getText());

		return consulta;
	}

}
