/* ************************************************************************************* *
 *                                                                                       *
 *  Licensed to the Apache Software Foundation (ASF) under one                           *
 *  or more contributor license agreements.  See the NOTICE file                         *
 *  distributed with this work for additional information                                *
 *  regarding copyright ownership.  The ASF licenses this file                           *
 *  to you under the Apache License, Version 2.0 (the                                    *
 *  "License"); you may not use this file except in compliance                           *
 *  with the License.  You may obtain a copy of the License at                           *
 *                                                                                       *
 *    http://www.apache.org/licenses/LICENSE-2.0                                         *
 *                                                                                       *
 *  Unless required by applicable law or agreed to in writing,                           *
 *  software distributed under the License is distributed on an                          *
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY                               *
 *  KIND, either express or implied.  See the License for the                            *
 *  specific language governing permissions and limitations                              *
 *  under the License.                                                                   *
 *                                                                                       *
 * ************************************************************************************* */

package com.aplicacion.spring.negocio.controladores;

import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.excepciones.ExcepcionCampoFueraDePatron;
import com.aplicacion.spring.excepciones.ExcepcionCampoMuyLargo;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.Seguimiento;
import com.aplicacion.spring.negocio.servicios.ServiciosPaciente;
import com.aplicacion.spring.utilitarios.Constantes;
import com.aplicacion.spring.utilitarios.controladores.Util;
import com.aplicacion.spring.utilitarios.controladores.ejecutores.EjecutorBusquedaPacientes;
import com.aplicacion.spring.utilitarios.controladores.ejecutores.EjecutorSubirImagen;
import com.aplicacion.spring.utilitarios.controladores.personalizacion.VBoxLista;
import com.aplicacion.spring.utilitarios.controladores.personalizacion.VBoxListaPacientes;

import de.felixroske.jfxsupport.FXMLController;
import javafx.animation.Animation;
import javafx.animation.Transition;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Duration;

/**
 * @Autor Juan Ignacio Prez
 * @Version 1.0
 * @Fecha 04/07/17 // Sin definir aun
 * 
 * @Uso Controlador de Pantalla de Pacientes
 */
@FXMLController
public class ControladorPaciente implements Initializable {

	private static final String SELECCIONADO = "vbox-paciente-seleccionado";
	@Autowired
	Aplicacion aplicacion;
	@Autowired
	ServiciosPaciente serviciosPaciente;

	@FXML
	private Button botonGuardar;
	@FXML
	private Button botonRojo;
	@FXML
	private Button botonCambiarImagen;

	@FXML
	private BorderPane panelCentral;
	@FXML
	private HBox panelPaciente;
	@FXML
	private VBox panelPacientesDatosPersonales;
	@FXML
	private VBox panelDatosClinicos;

	@FXML
	private VBox panelBusquedaPaciente;
	@FXML
	private Button botonAccionador;
	@FXML
	private TextField campoBuscarPaciente;
	@FXML
	private RadioButton radioBuscarNombre;
	@FXML
	private RadioButton radioBuscarDNI;
	@FXML
	private Label etiquetaIdpaciente;
	@FXML
	private TextField campoNombre;
	@FXML
	private TextField campoApellido;
	@FXML
	private RadioButton radioMas;
	@FXML
	private RadioButton radioFem;
	@FXML
	private ScrollPane scrollPaneBusqueda;
	@FXML
	private VBoxListaPacientes<Paciente> tablaPacientes;

	@FXML
	private TextField campoTalla;

	@FXML
	private DatePicker fechaNacimiento;

	@FXML
	private TextField campoDNI;
	@FXML
	private TextField campoObraSocial;
	@FXML
	private TextField campoNroAfiliado;
	@FXML
	private ImageView imagenPaciente;

	@FXML
	private TextField campoCiudad;
	@FXML
	private TextField campoDomicilio;

	@FXML
	private TextField campoTelefono;
	@FXML
	private TextField campoTelefono2;
	@FXML
	private TextField campoEmail;
	@FXML
	private TextArea areaTextoObservaciones;

	@FXML
	private Label etiquetaEdad;
	@FXML
	private CheckBox checkBoxActivo;
	@FXML
	private Label fechaAlta;
	@FXML
	private Label fechaUltAct;
	@FXML
	private HBox hBoxUltimasConsultas;
	@FXML
	private Label etiquetaFechaUltimaConsulta;
	@FXML
	private Label etiquetaEspecialidadUltimaConsulta;
	@FXML
	private Label etiquetaProfesionalUltimaConsulta;
	@FXML
	private TextArea areaTextoStatusPatologico;
	@FXML
	private TextArea areaTextoAntecedentes;

	private ToggleGroup seleccionSexo = new ToggleGroup();
	private ToggleGroup seleccionBusqueda = new ToggleGroup();

	private double anchoPanelExpandido;
	private SimpleBooleanProperty editable = new SimpleBooleanProperty(false);
	private ObservableList<Paciente> listaPacientes = FXCollections.observableArrayList();
	private ChangeListener<Paciente> listenerPaciente = null;
	private EjecutorBusquedaPacientes ejecutorLista;
	private EjecutorSubirImagen ejecutorSubirImagen;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// Acciones del panel de busqueda
		configuracionBusqueda(radioBuscarNombre, seleccionBusqueda, campoBuscarPaciente);
		anchoPanelExpandido = panelBusquedaPaciente.getPrefWidth();

		// Configuracion de la tabla de pacientes
		configurarTablaPacientes(tablaPacientes);

		// Configuracion del formulario
		configurarFormulario();

		// En caso de que el listener no este activo, hace una llamada manual
		if (listenerPaciente == null && aplicacion.getSesion().getPaciente() != null) {
			mostrarPaciente(aplicacion.getSesion().getPaciente());
			editable.set(true);
		}

		// Implementation del listener
		listenerPaciente = new ChangeListener<Paciente>() {
			@Override
			public void changed(ObservableValue<? extends Paciente> observable, Paciente pacientePrevio,
					Paciente pacienteActual) {
				if (pacienteActual == null) {
					limpiarFormularioPaciente();
					editable.set(false);
				} else {
					mostrarPaciente(pacienteActual);
					editable.set(true);
				}
			}
		};

		aplicacion.getSesion().pacienteProperty().addListener(listenerPaciente);

		Platform.runLater(() -> {
			// Opcion por defecto
			campoBuscarPaciente.requestFocus();
		});

	}

	private void configurarFormulario() {
		// Fuerza el valor de los campos
		Util.forzarCampoNumericoEnteroLimitado(campoDNI, 10);
		Util.forzarCampoNumericoEnteroLimitado(campoTalla, 10);

		// Ayuda en la validacion visual
		Util.validacionVisualCampoFecha(fechaNacimiento, Util.CAMPO_TRANSPARENTE);

		// Selectores de sexo
		radioMas.setToggleGroup(seleccionSexo);
		radioMas.setUserData("M");
		radioFem.setToggleGroup(seleccionSexo);
		radioFem.setUserData("F");

		// Convierte todos los campos en mayusculas
		capitalizarCampos();

		// Ataduras
		botonGuardar.disableProperty().bind(editable.not().or(campoApellido.textProperty().isEmpty()
				.or(campoNombre.textProperty().isEmpty().or(fechaNacimiento.valueProperty().isNull()
						.or(seleccionSexo.selectedToggleProperty().isNull().or(campoCiudad.textProperty().isEmpty().or(
								campoDomicilio.textProperty().isEmpty().or(campoDNI.textProperty().isEmpty()))))))));
		panelPaciente.disableProperty().bind(editable.not());

	}

	/**
	 * Establece los parametros iniciales y el listener para el selector y el campo
	 * de busqueda
	 * 
	 * @param radio,
	 *            seleccionado inicialmente
	 * @param seleccion,
	 *            ToggleGroup a evaluar
	 * @param campo,
	 *            TextField a manipular
	 */
	private void configuracionBusqueda(RadioButton radio, ToggleGroup seleccion, TextField campo) {
		radioBuscarDNI.setToggleGroup(seleccionBusqueda);
		radioBuscarNombre.setToggleGroup(seleccionBusqueda);

		radio.setSelected(true);
		campo.setPromptText("Apellido, Nombre");

		seleccion.selectedToggleProperty().addListener((o, p, a) -> {
			if (a.toString().contains("radioBuscarDNI")) {
				campo.setPromptText("12345678");
			} else if (a.toString().contains("radioBuscarNombre")) {
				campo.setPromptText("Apellido, Nombre");
			}
			campo.requestFocus();
		});
	}

	/**
	 * Valida la busqueda por nombre
	 *
	 * @return
	 */
	private void validarBusquedaNombre() {
		if (campoBuscarPaciente.getText().trim().length() >= 2) {
			if (campoBuscarPaciente.getText().trim().contains("##")) {
				buscarPacienteID(campoBuscarPaciente.getText().replace("##", ""));
			} else {
				buscarPacienteNombre(campoBuscarPaciente.getText());
			}
		} else {
			Mensajero.setMsg("Ingrese al menos 3 caracteres para iniciar la busqueda", Mensajero.MSG_INFO);
		}
	}

	/**
	 * Muestra el paciente seleccionado en el formulario pacienteSeleccionado
	 * 
	 * @param Paciente
	 */
	private void seleccionarPaciente(Paciente paciente) {
		aplicacion.getSesion().setPaciente(paciente);
	}

	// -----------------------------------------------------------------------------------------------------------------
	// Metodos para la lista o tabla de pacientes
	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Configura los listeners y la configuracion inicial de la tabla
	 * 
	 * @param tablaPacientes
	 */
	private void configurarTablaPacientes(VBoxLista<Paciente> tablaPacientes) {
		tablaPacientes.setImagenIzquierda(new Image(aplicacion.getPropiedades().get("imagen.persona")));
		tablaPacientes.getStyleClass().add("lista");
		tablaPacientes.getChildren().addListener((ListChangeListener<? super Node>) cambio -> {
			cambio.next();
			for (Node boton : cambio.getAddedSubList()) {
				if (boton != null && boton instanceof Button)
					((Button) boton).setOnAction(e -> {
						for (Node node : tablaPacientes.getChildren())
							node.getStyleClass().remove(SELECCIONADO);
						boton.getStyleClass().add(SELECCIONADO);
						seleccionarPaciente((Paciente) boton.getUserData());
					});
			}
		});
	}

	/**
	 * Reinicia los datos de la lista u tabla
	 */
	private void limpiarListaPacientes() {
		listaPacientes.clear();
		tablaPacientes.limpiarElementos();
	}

	/**
	 * Carga de la lista de pacientes en el contendedor
	 * 
	 * @param lista
	 */
	private void cargarListaPacientes(List<Paciente> lista) {
		// En caso de lista nula
		if (lista == null) {
			lista = FXCollections.observableArrayList();
			Mensajero.setMsg("Problemas al recuperar la lista de pacientes", Mensajero.MSG_ALERTA);
		} else {
			// Reinicia el contenedor
			limpiarListaPacientes();
			// Carga la lista
			for (Paciente p : lista) {
				tablaPacientes.sumarElemento(p);
			}
			// Carga los mensajes
			if (lista.size() == 0) {
				Mensajero.setMsg("No se hallaron pacientes", Mensajero.MSG_INFO);
			} else {
				if (lista.size() == 1) {
					seleccionarPaciente(lista.get(0));
					Mensajero.setMsg("Hallado 1 paciente", Mensajero.MSG_EXITO);
				} else {
					Mensajero.setMsg(String.format("Hallados %s pacientes", lista.size()), Mensajero.MSG_EXITO);
				}
			}
		}
	}

	/**
	 * Busca el paciente por nombre o id
	 * 
	 * @param texto
	 * @return
	 */
	private void buscarPacienteNombre(String texto) {
		// Comienzo del servicio
		String nombreApellido;
		String nombre;
		String apellido;

		List<String> listaBusqueda = new ArrayList<>();

		if (texto.trim().contains(",")) {
			nombreApellido = texto.trim();
		} else {
			nombreApellido = texto.trim().replaceFirst(" ", ",");
		}

		// Convierte el array en lista
		for (String s : Arrays.asList(nombreApellido.split(",")))
			listaBusqueda.add(s);

		// Verifica si la lista esta vacia
		if (!listaBusqueda.isEmpty()) {
			// Si hay menos de dos parametros, continua
			if (listaBusqueda.size() <= 2) {
				apellido = listaBusqueda.get(0).trim();
				// Si solo hay un parametro, creamos un segundo vacio
				if (listaBusqueda.size() == 1) {
					nombre = "";
				} else {
					nombre = listaBusqueda.get(1).trim();
				}

				ejecutorLista = new EjecutorBusquedaPacientes(serviciosPaciente, aplicacion.getPanelCarga(),
						aplicacion.getPanelFrontal(), "nombre", Arrays.asList(apellido, nombre));

				// TODO, Poner esto en Utils
				// Mensajero.setMsg(String.format("%s: %s: Ejecutor",
				// this.getClass().getSimpleName(),
				// Thread.currentThread().getStackTrace()[1].getMethodName()));

				ejecutorLista.setOnSucceeded(e -> {
					cargarListaPacientes(ejecutorLista.getValue());
				});

				ejecutorLista.setOnFailed(e -> {
					cargarListaPacientes(Collections.emptyList());
					Mensajero.setMsg("No se hayaron pacientes", Mensajero.MSG_ALERTA);
				});

				ejecutorLista.correrServicio();

				// Obtiene el valor de la ejecucion del servicio
				// return serviciosPaciente.obtenerPacientesPorNombre(apellido, nombre);
			} else {
				Mensajero.setMsg("Demasiados parametros: Solo use apellido y nombre, separados por una coma",
						Mensajero.MSG_ALERTA);
			}
		} else {
			Mensajero.setMsg("No hay nada en el campo de busqueda", Mensajero.MSG_ALERTA);
		}
	}

	/**
	 * Busca el paciente por ID
	 * 
	 * @param id
	 * @param listaPacientes
	 * @return
	 */
	private void buscarPacienteID(String id) {
		try {
			Integer.parseInt(id);

		} catch (NumberFormatException exc) {
			Mensajero.notificar("Id invalido");
		}

		ejecutorLista = new EjecutorBusquedaPacientes(serviciosPaciente, aplicacion.getPanelCarga(),
				aplicacion.getPanelFrontal(), "id", Arrays.asList(id));

		ejecutorLista.setOnSucceeded(e -> {
			cargarListaPacientes(ejecutorLista.getValue());
		});

		ejecutorLista.setOnFailed(e -> {
			cargarListaPacientes(Collections.emptyList());
			Mensajero.setMsg("No se hayaron pacientes", Mensajero.MSG_ALERTA);
		});

		ejecutorLista.correrServicio();
	}

	/**
	 * Metodo para la busqueda de pacientes por DNI, no puede devolver mas de 1
	 * valor por busqueda
	 */
	private Paciente buscarPacienteDNI() {
		Paciente paciente = null;
		if (campoBuscarPaciente.getText().length() > 6) {

			ejecutorLista = new EjecutorBusquedaPacientes(serviciosPaciente, aplicacion.getPanelCarga(),
					aplicacion.getPanelFrontal(), "dni", Arrays.asList(campoBuscarPaciente.getText()));

			ejecutorLista.setOnSucceeded(e -> {
				cargarListaPacientes(ejecutorLista.getValue());
			});

			ejecutorLista.setOnFailed(e -> {
				cargarListaPacientes(Collections.emptyList());
				Mensajero.setMsg("No se hayaron pacientes", Mensajero.MSG_ALERTA);
			});

			ejecutorLista.correrServicio();

		} else {
			Mensajero.setMsg("Ingrese al menos 6 caracteres para iniciar la busqueda", Mensajero.MSG_INFO);
		}
		return paciente;
	}

	/**
	 * Prepara el formulario para trabajar con un nuevo paciente
	 */
	@FXML
	void nuevoPaciente(ActionEvent event) {
		aplicacion.getSesion().setPaciente(null);
		limpiarListaPacientes();
		prepararNuevoPaciente();
	}

	/**
	 * Preparacion del formulario para ingresar un nuevo paciente
	 */
	private void prepararNuevoPaciente() {
		limpiarFormularioPaciente();
		limpiarListaPacientes();
		editable.set(true);
		campoApellido.requestFocus();
		botonCambiarImagen.setDisable(true);
	}

	// -----------------------------------------------------------------------------------------------------------------
	// Acciones del formulario
	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Carga los datos del paciente en el formulario
	 * 
	 * @param precampoId
	 *            del paciente
	 */
	private void mostrarPaciente(Paciente paciente) {
		limpiarFormularioPaciente();

		// Completa el formulario con los datos
		etiquetaIdpaciente.setText(String.format("%05d", paciente.getId()));

		campoNombre.setText(paciente.getNombre().toUpperCase());
		campoApellido.setText(paciente.getApellido().toUpperCase());

		// Selecciona el sexo
		if (paciente.getDatosPersonales().getSexo() == 'M') {
			radioMas.setSelected(true);
		} else if (paciente.getDatosPersonales().getSexo() == 'F') {
			radioFem.setSelected(true);
		}

		campoTalla.setText(paciente.getDatosClinicos().getTalla());

		if (paciente.getDatosPersonales().getFechaNacimiento() != null) {
			fechaNacimiento.setValue(paciente.getDatosPersonales().getFechaNacimiento());
		} else {
			fechaNacimiento.getEditor().setText("");
		}

		etiquetaEdad.setText(String.valueOf(paciente.getDatosPersonales().getEdad()));

		checkBoxActivo.setDisable(true);
		fechaAlta.setText(paciente.getFecAlta().format(Constantes.FECHA_D_M_YYYY));
		fechaUltAct.setText(paciente.getUltimaAct().format(Constantes.FECHA_D_M_YYYY));

		// TODO, añadir valor para activo
		campoDNI.setText(paciente.getDatosPersonales().getDni());
		if (paciente.getDatosPersonales().getIdObraSocial() != null)
			campoObraSocial.setText(paciente.getDatosPersonales().getIdObraSocial().toUpperCase());
		if (paciente.getDatosPersonales().getNroAfiliado() != null)
			campoNroAfiliado.setText(paciente.getDatosPersonales().getNroAfiliado().toUpperCase());

		// Muestra la foto, si hay alguna
		imagenPaciente.setImage(Util.recortarImagen(paciente.getDatosPersonales().getFoto()));
		if (imagenPaciente.getImage() == null)
			imagenPaciente.setImage(Paciente.getImagenDefecto());
		botonCambiarImagen.setDisable(false);

		if (paciente.getDatosDemograficos().getCiudad() != null)
			campoCiudad.setText(paciente.getDatosDemograficos().getCiudad().toUpperCase());
		if (paciente.getDatosDemograficos().getDomicilio() != null)
			campoDomicilio.setText(paciente.getDatosDemograficos().getDomicilio().toUpperCase());
		if (paciente.getDatosDemograficos().getTelefono() != null)
			campoTelefono.setText(paciente.getDatosDemograficos().getTelefono().toUpperCase());
		if (paciente.getDatosDemograficos().getTelefono2() != null)
			campoTelefono2.setText(paciente.getDatosDemograficos().getTelefono2().toUpperCase());
		if (paciente.getDatosDemograficos().getEmail() != null)
			campoEmail.setText(paciente.getDatosDemograficos().getEmail().toUpperCase());
		if (paciente.getDatosDemograficos().getObservaciones() != null)
			areaTextoObservaciones.setText(paciente.getDatosDemograficos().getObservaciones().toUpperCase());

		// Muestra la ultima consulta
		mostrarUltimasConsultas(paciente);

		areaTextoStatusPatologico.setText(paciente.getDatosClinicos().getStatusPatologico());
		areaTextoAntecedentes.setText(paciente.getDatosClinicos().getAntecedentes());

	}

	/**
	 * Obtiene y valida los datos del formulario, tambien crea un objeto Paciente
	 * con los datos
	 * 
	 * @return Paciente
	 */
	private Paciente tomarFormularioPaciente(Paciente paciente) {

		if (paciente == null) {
			// Crea un paciente vacio para cargarle los datos
			paciente = new Paciente();
		}

		// Valida los campos
		if (validarCamposRequeridos()) {

			// Datos basicos
			paciente.setApellido(Util.recortarLongitudCampo(campoApellido, 120));
			paciente.setNombre(Util.recortarLongitudCampo(campoNombre, 120));
			if (campoTalla.getText() == null || campoTalla.getText().isEmpty()) {
				paciente.getDatosClinicos().setTalla("0");
			} else {
				paciente.getDatosClinicos().setTalla(campoTalla.getText());
			}

			// Datos personales
			paciente.getDatosPersonales().setSexo(((String) seleccionSexo.getSelectedToggle().getUserData()).charAt(0));
			paciente.getDatosPersonales().setFechaNacimiento(fechaNacimiento.getValue());
			paciente.getDatosPersonales().setDni(campoDNI.getText());
			paciente.getDatosPersonales().setIdObraSocial(campoObraSocial.getText().trim());
			paciente.getDatosPersonales().setNroAfiliado(
					!campoNroAfiliado.getText().trim().isEmpty() ? campoNroAfiliado.getText().trim() : "");

			// Datos demograficos
			paciente.getDatosDemograficos().setDomicilio(Util.recortarLongitudCampo(campoDomicilio, 120));
			paciente.getDatosDemograficos().setCiudad(Util.recortarLongitudCampo(campoCiudad, 120));
			paciente.getDatosDemograficos().setTelefono(Util.recortarLongitudCampo(campoTelefono, 120));
			paciente.getDatosDemograficos().setTelefono2(Util.recortarLongitudCampo(campoTelefono2, 120));
			paciente.getDatosDemograficos().setTelefono2(Util.recortarLongitudCampo(campoTelefono2, 120));
			paciente.getDatosDemograficos().setEmail(Util.recortarLongitudCampo(campoEmail, 120));

			// Observaciones
			paciente.getDatosDemograficos().setObservaciones(areaTextoObservaciones.getText());

			// Status patologico
			paciente.getDatosClinicos().setStatusPatologico(areaTextoStatusPatologico.getText());

			// Instanciamos la fecha de alta y ultima actualizacion si no existen
			if (paciente.getFecAlta() == null) {
				paciente.setFecAlta(LocalDate.now());
			}
			paciente.setUltimaAct(LocalDate.now());
		}

		// Antecedentes
		return paciente;

	}

	private boolean validarCamposRequeridos() {
		if (seleccionSexo.getSelectedToggle().getUserData() == null) {
			Mensajero.setMsg("Falta seleccionar el sexo ", Mensajero.MSG_ALERTA);
			return false;
		}
		// ** Fecha de Nacimiento
		if (fechaNacimiento.getValue() == null) {
			Mensajero.setMsg("La fecha de nacimiento es obligatoria", Mensajero.MSG_ALERTA);
			fechaNacimiento.requestFocus();
			return false;
		}

		// ** Ciudad
		if (campoCiudad.getText().trim().isEmpty()) {
			Mensajero.setMsg("El campoCiudad es obligatorio", Mensajero.MSG_ALERTA);
			campoCiudad.requestFocus();
			return false;
		}
		// ** Domicilio
		if (campoDomicilio.getText().trim().isEmpty()) {
			Mensajero.setMsg("El campo domicilio no puede estar vacio", Mensajero.MSG_ALERTA);
			campoDomicilio.requestFocus();
			return false;
		}
		// Email
		if (!campoEmail.getText().isEmpty()) {
			try {
				Util.validarTipoCampo(campoEmail, Constantes.PATRON_EMAIL, 120);
			} catch (ExcepcionCampoFueraDePatron e) {
				Mensajero.setMsg("No es un email valido", Mensajero.MSG_INFO);
				return false;
			} catch (ExcepcionCampoMuyLargo e) {
				Mensajero.setMsg("El campoEmail no puede tener mas de 120 caracteres", Mensajero.MSG_INFO);
				return false;
			}
		}

		return true;
	}

	/**
	 * Metodo que abre el dialogo de carga de imagenes de pacientes y se las pasa al
	 * cargador de la base de datos
	 */
	@FXML
	private void subirImagenNueva() {
		// Crea la ventana de carga de la imagen
		FileChooser selectorImagen = new FileChooser();

		// Aplica un filtro a la seleccion de archivos
		selectorImagen.getExtensionFilters().add(new ExtensionFilter("Imagenes", "*.jpg", "*.jpeg", "*.png", "*.bmp"));

		selectorImagen.setTitle("Escoja la imagen...");

		// Sube la imagen nueva como archivo
		File imagenNueva = selectorImagen.showOpenDialog(null);
		// Si la imagen existe y se pudo subir
		if (Util.validarImagen(imagenNueva)) {
			ejecutorSubirImagen = new EjecutorSubirImagen(serviciosPaciente, EjecutorSubirImagen.PACIENTE, imagenNueva,
					aplicacion.getSesion().getPaciente().getId(), aplicacion.getSesion().getUsuario().getUid());

			ejecutorSubirImagen.setOnSucceeded(e -> {
				if (ejecutorSubirImagen.getValue() != null) {
					cambiarImagen((Image) ejecutorSubirImagen.getValue());
					Mensajero.setMsg(Mensajero.APP_PACIENTE_IMAGEN_CARGA_EXITO, Mensajero.MSG_EXITO);
				} else {
					Mensajero.setMsg(Mensajero.APP_PACIENTE_IMAGEN_CARGA_FALLO, Mensajero.MSG_ALERTA);
				}
			});

			ejecutorSubirImagen.setOnFailed(e -> {
				cargarListaPacientes(Collections.emptyList());
				Mensajero.setMsg("No se hayaron pudo cambiar la imagen", Mensajero.MSG_ALERTA);
			});

			ejecutorSubirImagen.correrServicio();
		} else {
			Mensajero.setMsg(Mensajero.APP_PACIENTE_IMAGEN_CARGA_FALLO, Mensajero.MSG_ALERTA);
		}

	}

	/**
	 * Cambia la imagen
	 * 
	 * @param foto
	 */
	private void cambiarImagen(Image foto) {
		imagenPaciente.setImage(Util.recortarImagen(foto));
		if (imagenPaciente.getImage() == null)
			imagenPaciente.setImage(Paciente.getImagenDefecto());
		aplicacion.getSesion().getPaciente().getDatosPersonales().setFoto(foto);
	}

	/**
	 * Limpia del formulario de pacientes
	 */
	private void limpiarFormularioPaciente() {
		etiquetaIdpaciente.setText("");
		campoNombre.clear();
		campoApellido.clear();
		radioMas.setSelected(false);
		radioFem.setSelected(false);
		campoTalla.clear();

		fechaNacimiento.getEditor().clear();
		etiquetaEdad.setText("");
		campoDNI.clear();
		campoObraSocial.clear();
		campoNroAfiliado.clear();
		fechaAlta.setText("");
		fechaUltAct.setText("");

		imagenPaciente.setImage(Paciente.getImagenDefecto());

		campoCiudad.clear();
		campoDomicilio.clear();

		campoTelefono.clear();
		campoTelefono2.clear();
		campoEmail.clear();
		etiquetaFechaUltimaConsulta.setText("");
		areaTextoObservaciones.clear();

		areaTextoStatusPatologico.clear();
		areaTextoAntecedentes.clear();

		Util.quitarResaltadoDelCampo(fechaNacimiento, Util.CAMPO_TRANSPARENTE);
	}

	// -----------------------------------------------------------------------------------------------------------------
	// Acciones de paciente
	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Busca paciente en campoBuscarPaciente
	 */
	@FXML
	void buscarPaciente() {
		if (radioBuscarNombre.isSelected()) {
			validarBusquedaNombre();
		} else if (radioBuscarDNI.isSelected()) {
			buscarPacienteDNI();
		}
	}

	/**
	 * Cierra la sesion actual del paciente
	 */
	@FXML
	private void cerrarPaciente() {
		limpiarFormularioPaciente();
		aplicacion.getSesion().setPaciente(null);
	}

	/**
	 * 
	 * 
	 * @param paciente
	 */
	private void mostrarUltimasConsultas(Paciente paciente) {
		// Ultimas consultas
		// Label etiquetaUltimasConsultas = new Label();
		// etiquetaUltimasConsultas.getStyleClass().clear();
		// etiquetaUltimasConsultas.getStyleClass().add("text14",
		// "borde-superior-inferior", "borde-negro");
		// gridUltimasConsultas.setHgap(4.0);
		// gridUltimasConsultas.setVgap(4.0);

		Tooltip tooltipUltimasConsultas = new Tooltip();
		GridPane gridUltimasConsultas = new GridPane();

		tooltipUltimasConsultas.getStyleClass().add("globo-flotante");
		Seguimiento consultaMasActual = new Seguimiento();

		// Si existen consultas previas, se muestra una fila por especialidad
		if (paciente.getDatosClinicos().getConsultas() != null
				&& !paciente.getDatosClinicos().getConsultas().isEmpty()) {

			// Busca la consulta mas actual
			consultaMasActual.setUltConsulta(LocalDate.parse("01/01/1900", Constantes.FECHA_DD_MM_YYYY));

			// Recorre la lista de consultas en busca de la mas reciente
			for (int idx = 0; idx < paciente.getDatosClinicos().getConsultas().size(); idx++) {
				Seguimiento consulta = paciente.getDatosClinicos().getConsultas().get(idx);
				if (consulta.getUltConsulta().isAfter(consultaMasActual.getUltConsulta())) {
					consultaMasActual = consulta;
				}

				Label textConsultaFecha = new Label(consulta.getUltConsulta().format(Constantes.FECHA_DD_MM_YYYY));
				Label textConsultaEspecialidad = new Label(consulta.getEspecialidad());
				Label textConsultaProfesional = new Label(consulta.getProfesional());

				gridUltimasConsultas.add(textConsultaFecha, 0, idx);
				gridUltimasConsultas.add(textConsultaEspecialidad, 1, idx);
				gridUltimasConsultas.add(textConsultaProfesional, 2, idx);
			}

			etiquetaFechaUltimaConsulta.setText(consultaMasActual.getUltConsulta().format(Constantes.FECHA_DD_MM_YYYY));
			etiquetaEspecialidadUltimaConsulta.setText(consultaMasActual.getEspecialidad());
			etiquetaProfesionalUltimaConsulta.setText(consultaMasActual.getUsuario());

			// Reemplaza
			tooltipUltimasConsultas.setGraphic(gridUltimasConsultas);
			tooltipUltimasConsultas.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

			Tooltip.install(hBoxUltimasConsultas, tooltipUltimasConsultas);
		} else {
			etiquetaFechaUltimaConsulta.setText("No hay consultas previas");
			etiquetaFechaUltimaConsulta.setTooltip(new Tooltip("No hay consultas previas"));
		}

	}

	/**
	 * Guarda los cambios ingresados en el formulario
	 */
	@FXML
	private void guardarPaciente() {
		// Validacion inicial de campos base (Nombre, Apellido, DNI)
		Paciente paciente = tomarFormularioPaciente(aplicacion.getSesion().getPaciente());
		// Valida y envia los datos
		if (paciente != null) {
			if (serviciosPaciente.nuevoPaciente(paciente) > 0) {
				// Notifica
				Mensajero.setMsg("Se guardaron los datos del paciente " + paciente.getId() + " - "
						+ paciente.getApellido() + ", " + paciente.getNombre(), Mensajero.MSG_EXITO);
				// Una vez guardado, pasa a ser el paciente activo
				aplicacion.getSesion().setPaciente(paciente);
			}
		}
	}

	/**
	 * Capitaliza todos los campos de texto del formulario
	 */
	private void capitalizarCampos() {
		Util.capitalizarCampo(campoNombre);
		Util.capitalizarCampo(campoApellido);
		Util.capitalizarCampo(campoTalla);
		Util.capitalizarCampo(campoDNI);
		Util.capitalizarCampo(campoObraSocial);
		Util.capitalizarCampo(campoNroAfiliado);
		Util.capitalizarCampo(campoCiudad);
		Util.capitalizarCampo(campoDomicilio);
		Util.capitalizarCampo(campoTelefono);
		Util.capitalizarCampo(campoTelefono2);
		Util.capitalizarCampo(areaTextoObservaciones);
		Util.capitalizarCampo(areaTextoStatusPatologico);
		Util.capitalizarCampo(areaTextoAntecedentes);
	}

	/**
	 * Oculta/Muestra el panel de busqueda
	 */
	@FXML
	void ocultarBusquedaPaciente() {
		ocultarPanelBusqueda(panelBusquedaPaciente, anchoPanelExpandido, botonAccionador);
	}

	/**
	 * Ejecucion efectiva de la animacion
	 * 
	 * @param panel
	 * @param anchoInicial
	 * @param boton
	 */
	private void ocultarPanelBusqueda(Pane panel, double anchoInicial, Button boton) {
		panel.setMinWidth(0);

		// Funcion de ocultamiento
		final Animation ocultar = new Transition() {
			{
				setCycleDuration(Duration.millis(250));
			}

			@Override
			protected void interpolate(double fraccion) {
				final double anchoActual = anchoInicial * (1.0 - fraccion);
				panel.setPrefWidth(anchoActual);
				panel.setTranslateX(-anchoInicial + anchoActual);
			}
		};

		// Al finalizar la animacion de ocultamiento
		ocultar.onFinishedProperty().set(f -> {
			panel.setVisible(false);
			boton.setText("Mostrar");
		});

		// Funcion de aparicion
		final Animation mostrar = new Transition() {
			{
				setCycleDuration(Duration.millis(250));
			}

			@Override
			protected void interpolate(double fraccion) {
				final double anchoActual = anchoInicial * fraccion;
				panel.setPrefWidth(anchoActual);
				panel.setTranslateX(-anchoInicial + anchoActual);
			}
		};

		// Al finalizar la animacion de aparicion
		mostrar.onFinishedProperty().set(g -> boton.setText("Ocultar"));

		// Intercambia el efecto en funcion del estado
		if (mostrar.statusProperty().get() == Animation.Status.STOPPED
				&& ocultar.statusProperty().get() == Animation.Status.STOPPED) {
			if (panel.isVisible()) {
				mostrar.setRate(4.0);
				ocultar.play();
			} else {
				panel.setVisible(true);
				mostrar.setRate(4.0);
				mostrar.play();
			}
		}
	}

}
