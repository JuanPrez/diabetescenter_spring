package com.aplicacion.spring.negocio.controladores;

import com.aplicacion.spring.negocio.entidades.registros.consultas.Consulta;

public interface ControladorConsulta {

	/**
	 * Configura la consulta inicial
	 * 
	 * @param consulta
	 */
	public void setConsulta(Consulta consulta);
//	/**
//	 * Crea una nueva consulta y retorna la misma
//	 * 
//	 * @param consulta
//	 * @return la consulta con el nuevo id
//	 */
//	public <T extends Consulta> T crearConsulta(Consulta consulta);
//
//	/**
//	 * Guarda la consulta
//	 * 
//	 * @param consulta
//	 * @return true si tiene exito
//	 */
//	public Boolean guardarConsulta(Consulta consulta);
//
//	/**
//	 * Cancela la consulta y cierra la pantalla si no se puede crear la consulta
//	 * solicitada
//	 */
//	public void cancelarConsulta();
//
//	/**
//	 * Crea una nueva consulta y retorna la misma
//	 * 
//	 * @param consulta
//	 * @return la consulta con el nuevo id
//	 */
//	public Boolean eliminarConsulta(Integer idConsulta);
//
//	/**
//	 * Pasa la consulta a mostrar
//	 */
//	public void mostrarConsulta();
//
//	/**
//	 * Ata las propiedades de los campos con los de la consulta
//	 */
//	public void persistirConsulta();
//
//	/**
//	 * Limpia el texto de los campos pasados
//	 */
//	public void limpiarCampos(Set<TextInputControl> campos);
//
}
