package com.aplicacion.spring.negocio.controladores;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.registros.consultas.EstadisticasConsultaMedicina;
import com.aplicacion.spring.negocio.entidades.registros.patologias.EstadisticasPatologia;
import com.aplicacion.spring.negocio.entidades.registros.patologias.Patologia;
import com.aplicacion.spring.negocio.servicios.ServiciosEstadisticas;
import com.aplicacion.spring.utilitarios.controladores.Util;

import de.felixroske.jfxsupport.FXMLController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.CheckBox;

@FXMLController
public class ControladorEstadisticas implements Initializable {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(ControladorEstadisticas.class);

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	ServiciosEstadisticas serviciosEstadisticas;

	// @FXML
	// private LineChart<String, Number> datosConsultas;
	@FXML
	private ScatterChart<String, Number> datosConsultasPesoIMC;
	@FXML
	private ScatterChart<String, Number> datosConsultasPresion;
	@FXML
	private ScatterChart<String, Number> datosPatologiasHbA1c;
	@FXML
	private CheckBox comboBoxPeso;
	@FXML
	private CheckBox comboBoxIMC;
	@FXML
	private CheckBox comboBoxSist;
	@FXML
	private CheckBox comboBoxDias;
	@FXML
	private LineChart<String, Number> datosPatologias;
	@FXML
	private CheckBox comboBoxHbA1c;

	XYChart.Series<String, Number> dataPesoConsultas = new XYChart.Series<String, Number>();
	XYChart.Series<String, Number> dataIMCConsultas = new XYChart.Series<String, Number>();
	XYChart.Series<String, Number> dataSistConsultas = new XYChart.Series<String, Number>();
	XYChart.Series<String, Number> dataDiasConsultas = new XYChart.Series<String, Number>();

	XYChart.Series<String, Number> dataHb1a1cPatologias = new XYChart.Series<String, Number>();

	List<EstadisticasConsultaMedicina> listaEstadisticasConsultas = new ArrayList<>();
	List<EstadisticasPatologia> listaEstadisticasPatologias = new ArrayList<>();
	ChangeListener<Paciente> listenerPaciente = null;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		Mensajero.setMsg(
				"Pantalla en desarrollo, algunas funciones pueden estar deshabilitadas o funcionar de forma inesperada",
				Mensajero.MSG_ALERTA);
		// Ejecucion manual
		if (listenerPaciente == null && aplicacion.getSesion().getPaciente() != null) {
			obtenerDatosPaciente(Optional.ofNullable(aplicacion.getSesion().getPaciente()));
		}

		// Listener del paciente
		listenerPaciente = new ChangeListener<Paciente>() {
			@Override
			public void changed(ObservableValue<? extends Paciente> observable, Paciente pacientePrevio,
					Paciente pacienteActual) {
				obtenerDatosPaciente(Optional.ofNullable(pacienteActual));
			}
		};

		dataPesoConsultas.setName("Peso");
		dataIMCConsultas.setName("IMC");
		dataSistConsultas.setName("Sistólica");
		dataDiasConsultas.setName("Diastólica");
		dataHb1a1cPatologias.setName("HbA1c");

		datosConsultasPesoIMC.setAnimated(false);
		datosConsultasPresion.setAnimated(false);
		datosPatologiasHbA1c.setAnimated(false);

		comboBoxPeso.setOnAction(e -> {
			alternadorPeso();
		});

		comboBoxIMC.setOnAction(e -> {
			alternadorIMC();
		});

		comboBoxSist.setOnAction(e -> {
			alternadorSist();
		});

		comboBoxDias.setOnAction(e -> {
			alternadorDias();
		});

		comboBoxHbA1c.setOnAction(e -> {
			alternadorHbA1c();
		});

	}

	public void obtenerDatosPaciente(Optional<Paciente> paciente) {
		limpiarPantalla();
		if (paciente.isPresent()) {
			listaEstadisticasConsultas = serviciosEstadisticas.obtenerEstadisticasConsultasPaciente(
					aplicacion.getSesion().getPaciente().getId(), LocalDate.now().minusMonths(6));

			listaEstadisticasPatologias = serviciosEstadisticas.obtenerEstadisticasPatologiasPaciente(
					aplicacion.getSesion().getPaciente().getId(), Patologia.HEMOGLOBINA_GLICOCILADA, null);
		}
	}

	private void limpiarPantalla() {
		if (comboBoxPeso.isSelected()) {
			comboBoxPeso.setSelected(false);
		}
		if (comboBoxIMC.isSelected()) {
			comboBoxIMC.setSelected(false);
		}
		if (comboBoxSist.isSelected()) {
			comboBoxSist.setSelected(false);
		}
		if (comboBoxDias.isSelected()) {
			comboBoxDias.setSelected(false);
		}
		if (comboBoxHbA1c.isSelected()) {
			comboBoxHbA1c.setSelected(false);
		}
		datosConsultasPesoIMC.getData().clear();
		datosConsultasPresion.getData().clear();
		datosPatologiasHbA1c.getData().clear();
	}

	private void alternadorPeso() {
		if (!datosConsultasPesoIMC.getData().contains(dataPesoConsultas)) {
			for (EstadisticasConsultaMedicina estadistica : listaEstadisticasConsultas) {
				dataPesoConsultas.getData().add(
						new XYChart.Data<String, Number>(estadistica.getFecha().toString(), estadistica.getPeso()));
			}
			datosConsultasPesoIMC.getData().add(dataPesoConsultas);
		} else {
			datosConsultasPesoIMC.getData().remove(dataPesoConsultas);
		}
	}

	private void alternadorIMC() {
		if (!datosConsultasPesoIMC.getData().contains(dataIMCConsultas)) {
			for (EstadisticasConsultaMedicina estadistica : listaEstadisticasConsultas) {
				dataIMCConsultas.getData()
						.add(new XYChart.Data<String, Number>(estadistica.getFecha().toString(),
								Util.calculadorIMC(estadistica.getPeso(),
										aplicacion.getSesion().getPaciente().getDatosClinicos().getTalla())));
			}
			datosConsultasPesoIMC.getData().add(dataIMCConsultas);
		} else {
			datosConsultasPesoIMC.getData().remove(dataIMCConsultas);
		}
	}

	private void alternadorSist() {
		if (!datosConsultasPresion.getData().contains(dataSistConsultas)) {
			for (EstadisticasConsultaMedicina estadistica : listaEstadisticasConsultas) {

				dataSistConsultas.getData().add(new XYChart.Data<String, Number>(estadistica.getFecha().toString(),
						estadistica.getPresionSistolica()));
			}
			datosConsultasPresion.getData().add(dataSistConsultas);
		} else {
			datosConsultasPresion.getData().remove(dataSistConsultas);
		}
	}

	private void alternadorDias() {
		if (!datosConsultasPresion.getData().contains(dataDiasConsultas)) {
			for (EstadisticasConsultaMedicina estadistica : listaEstadisticasConsultas) {
				dataDiasConsultas.getData().add(new XYChart.Data<String, Number>(estadistica.getFecha().toString(),
						estadistica.getPresionDiastolica()));
			}
			datosConsultasPresion.getData().add(dataDiasConsultas);
		} else {
			datosConsultasPresion.getData().remove(dataDiasConsultas);
		}
	}

	private void alternadorHbA1c() {
		if (!datosPatologiasHbA1c.getData().contains(dataHb1a1cPatologias)) {
			for (EstadisticasPatologia estadistica : listaEstadisticasPatologias) {
				dataHb1a1cPatologias.getData().add(new XYChart.Data<String, Number>(estadistica.getFecha().toString(),
						estadistica.getResultado()));
			}
			datosPatologiasHbA1c.getData().add(dataHb1a1cPatologias);
		} else {
			datosPatologiasHbA1c.getData().remove(dataHb1a1cPatologias);
		}
	}

}
