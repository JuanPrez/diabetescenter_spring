package com.aplicacion.spring.negocio.controladores;

import java.net.URL;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.aplicacion.spring.Aplicacion;
import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.Ventanas;
import com.aplicacion.spring.gestion.controladores.ControladorHistoriaClinica;
import com.aplicacion.spring.gestion.controladores.ControladorTurnos;
import com.aplicacion.spring.negocio.entidades.registros.consultas.Consulta;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaMedicina;
import com.aplicacion.spring.negocio.servicios.ServiciosMedicina;
import com.aplicacion.spring.utilitarios.controladores.Util;

import de.felixroske.jfxsupport.FXMLController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

@FXMLController
public class ControladorConsultaMedicina implements Initializable {

	@Autowired
	Aplicacion aplicacion;
	@Autowired
	ServiciosMedicina serviciosMedicina;

	// Panel superior
	@FXML
	private Button botonLimpiarCampos;
	@FXML
	private Button botonGuardar;
	@FXML
	private Button botonDescartar;
	@FXML
	private Label etiquetaIdConsulta;

	// Panel interno superior
	@FXML
	private BorderPane panelHistClinica;
	@FXML
	private TextField campoPulso;
	@FXML
	private Label etiquetaIMC;
	@FXML
	private TextField campoPresionDias;
	@FXML
	private TextField campoPresionSist;
	@FXML
	private TextField campoTextoDiagnostico;
	@FXML
	private TextField campoTextoMotConsulta;
	@FXML
	private TextField campoPeso;
	@FXML
	private VBox vBoxProxCita;
	@FXML
	private DatePicker proxCita;
	@FXML
	private TextArea areaTextoEvolucion;
	@FXML
	private TextArea areaTextoComplementario;
	@FXML
	private TextArea areaTextoTratamiento;
	@FXML
	private TextField campoDiamCintura;

	private Set<TextInputControl> campos = prepararListaCampos();
	private HashMap<String, String> plantillas;

	ConsultaMedicina consultaMedicina;

	@Autowired
	ControladorAntecedentesMedicina controladorAntecedentesMedicina;
	@Autowired
	ControladorHistoriaClinica controladorHistoriaClinica;
	@Autowired
	ControladorTurnos controladorTurnos;
	ChangeListener<Consulta> listenerConsulta = null;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// Acciones de inicializacion del formulario
		inicializarFormulario();

		// Primera ejecucion manual
		if (listenerConsulta == null && controladorHistoriaClinica.getConsulta() != null) {
			// Inicializacion de la consulta del formulario
			consultaMedicina = inicializarConsulta(controladorHistoriaClinica.getConsulta());
			// Si la consulta no es nula, muestra los datos en pantalla
			if (consultaMedicina != null)
				mostrarDatosConsulta(consultaMedicina);
			// mostrarDatosConsulta(inicializarConsulta(controladorHistoriaClinica.getConsulta()));
		}

		// Listener de la consulta
		listenerConsulta = new ChangeListener<Consulta>() {
			@Override
			public void changed(ObservableValue<? extends Consulta> observable, Consulta consultaPrevia,
					Consulta consultaActual) {
				if (consultaActual == null) {
					consultaMedicina = null;
				} else {
					// Inicializacion de la consulta del formulario
					consultaMedicina = inicializarConsulta(consultaActual);
				}
				// Si la consulta no es nula, muestra los datos en pantalla
				if (consultaMedicina != null)
					mostrarDatosConsulta(consultaMedicina);
			}
		};
		controladorHistoriaClinica.consultaProperty().addListener(listenerConsulta);

	}

	/**
	 * Acciones de inicializacion del formulario Acciones de los botones,
	 * configuracion de disparadores,
	 */
	private void inicializarFormulario() {
		// Disparadores automaticos
		Util.disparadorCalculadoraIMC(etiquetaIMC, aplicacion.getSesion().getPaciente().getDatosClinicos().getTalla(),
				campoPeso);
		Util.disparadorAtaduraLigera(campoTextoMotConsulta, campoTextoDiagnostico);

		Util.disparadorReemplazoPuntoXComa(campoPeso);
		Util.disparadorReemplazoPuntoXComa(campoDiamCintura);
		Util.validacionVisualCampoBigDecimal(campoPeso, Util.CAMPO_NORMAL);
		Util.validacionVisualCampoBigDecimal(campoDiamCintura, Util.CAMPO_NORMAL);

		Util.validacionVisualCampoInteger(campoPresionSist, Util.CAMPO_NORMAL);
		Util.validacionVisualCampoInteger(campoPresionDias, Util.CAMPO_NORMAL);
		Util.validacionVisualCampoInteger(campoPulso, Util.CAMPO_NORMAL);

		// Configuracion de las plantillas
		plantillas = construirPlantillas();

	}

	/**
	 * Inicializa la consulta, valida la consulta pasada, si es valida, inicializa
	 * la consulta segun los requisitos del negocio
	 * 
	 * @param consultaActual
	 */
	private ConsultaMedicina inicializarConsulta(Consulta consultaActual) {
		// Datos de la nueva consulta
		ConsultaMedicina consulta = null;
		if (consultaActual instanceof ConsultaMedicina) {
			consulta = ((ConsultaMedicina) consultaActual).copiar();
			// Obtiene un nuevo ID
			consulta.setId(serviciosMedicina.nuevaConsulta(consulta, aplicacion.getSesion().getPaciente(),
					aplicacion.getSesion().getUsuario()));
			// Paciente
			consulta.setPaciente(aplicacion.getSesion().getPaciente());
			// y Usuario
			consulta.setUsuario(aplicacion.getSesion().getUsuario());

			// Vacia los campos
			consulta.setProxCita(null);

			// Estilo 1 para uso de plantilla
			consulta.setEvoluc("");
			consulta.setMotivo("Control");
			consulta.setDiagn("");
			consulta.setComple("");

			// Aplica las plantillas
			if (aplicacion.getSesion().getPaciente().getDatosClinicos().getStatusPatologico() != null)
				consulta.setEvoluc(aplicarPlantilla(consulta,
						aplicacion.getSesion().getPaciente().getDatosClinicos().getStatusPatologico(), plantillas));
		}
		return consulta;
	}

	/**
	 * Muestra la consulta
	 * 
	 * @param consulta
	 */
	private void mostrarDatosConsulta(ConsultaMedicina consulta) {
		if (consulta != null) {

			// Completamos el formulario de clinica con los datos
			try {
				etiquetaIdConsulta.setText(Integer.toString(consulta.getId()));
				campoPeso.setText(consulta.getPeso().toPlainString());
				campoDiamCintura.setText(consulta.getDiam().toPlainString());
			} catch (NumberFormatException | NullPointerException exc) {
			}

			campoPresionDias.setText(Integer.toString(consulta.getDias()));
			campoPresionSist.setText(Integer.toString(consulta.getSist()));
			campoPulso.setText(Integer.toString(consulta.getPulso()));

			campoTextoMotConsulta.setText(consulta.getMotivo());
			campoTextoDiagnostico.setText(consulta.getDiagn());
			areaTextoEvolucion.setText(consulta.getEvoluc());
			areaTextoTratamiento.setText(consulta.getTrata());
			areaTextoComplementario.setText(consulta.getComple());
			if (proxCita != null)
				proxCita.setValue(consulta.getProxCita());
		}
	}

	/**
	 * Recupera los datos del formulario, los completa en la consulta pasada y los
	 * devuelve
	 * 
	 * @param consulta
	 * @return la misma consulta
	 */
	private ConsultaMedicina tomarFormularioConsulta(ConsultaMedicina consulta) {
		// Completamos el formulario de clinica con los datos
		consulta.setPeso(Util.convertirCampoBigDecimal(campoPeso, 3));
		consulta.setDiam(Util.convertirCampoBigDecimal(campoDiamCintura, 1));
		consulta.setDias(Util.convertirCampoInteger(campoPresionDias.getText()));
		consulta.setSist(Util.convertirCampoInteger(campoPresionSist.getText()));
		consulta.setPulso(Util.convertirCampoInteger(campoPulso.getText()));
		consulta.setMotivo(campoTextoMotConsulta.getText());
		consulta.setDiagn(campoTextoDiagnostico.getText());
		consulta.setEvoluc(areaTextoEvolucion.getText());
		consulta.setTrata(areaTextoTratamiento.getText());
		consulta.setComple(areaTextoComplementario.getText());
		return consulta;
	}

	@FXML
	private void guardarConsulta(ActionEvent event) {
		asignarFechas(consultaMedicina);
		// Si la fecha no esta asignada, la asigna
		if (consultaMedicina.getProxCita() != null) {
			if (guardarConsulta(consultaMedicina)) {
				Mensajero.setMsg("Consulta guardada", Mensajero.MSG_EXITO);
				// Actualiza la lista de consultas
				controladorAntecedentesMedicina.refrescarListaConsultas();
				aplicacion.getSesion().setTurno(controladorTurnos.cerrarTurno(aplicacion.getSesion().getTurno()));

				// Cierra la consulta
				controladorHistoriaClinica.setConsulta(null);
			} else {
				Mensajero.setMsg("No se pudo guardar la consulta y la anamnesis alimenticia", Mensajero.MSG_ALERTA);
			}
		} else {
			proxCita.requestFocus();
		}
	}

	/**
	 * Guarda la consulta y cierra la sesion actual
	 */
	private boolean guardarConsulta(ConsultaMedicina consulta) {
		return serviciosMedicina.actualizarConsulta(tomarFormularioConsulta(consulta),
				aplicacion.getSesion().getUsuario(), false);
	}

	/**
	 * Verifica las fechas, y las asigna
	 * 
	 * @return true si la consulta contiene fecha de consulta y proxima cita
	 */
	private void asignarFechas(Consulta consulta) {
		// Si la proxCita no esta configurada, lo advertimos
		if (consulta.getFecha() == null) {
			consulta.setFecha(LocalDate.now());
		}
		// Verifica la fecha antes de guardar,
		consulta.setProxCita(validarProxCita(proxCita.getValue()));
	}

	/**
	 * Verifica las fechas, y las asigna
	 */
	private LocalDate validarProxCita(LocalDate proxCita) {
		// Valida que la prox cita este programada
		if (proxCita == null) {
			Mensajero.setMsg("Ingrese una fecha para la próxima cita", Mensajero.MSG_INFO);
			return null;
		} else {
			if (Util.validarFechaFutura(proxCita)) {
				return proxCita;
			} else {
				Mensajero.setMsg("La próxima cita debe ser una fecha futura", Mensajero.MSG_INFO);
				return null;
			}
		}
	}

	/**
	 * Cancela la consulta, eliminandola, aunque mantiene los datos del paciente
	 * abiertos
	 */
	@FXML
	private void descartarConsulta(ActionEvent event) {
		// Confirma y elimina la consulta creada recientemente
		Alert alerta = Ventanas.crearAlerta(AlertType.CONFIRMATION, "Descartar consulta", "",
				"¿Desea descartar la consulta?", null);
		Optional<ButtonType> respuesta = alerta.showAndWait();
		if (respuesta.isPresent() && respuesta.get().equals(ButtonType.OK)) {
			if (serviciosMedicina.eliminarConsulta(consultaMedicina.getId())) {
				Mensajero.setMsg("Consulta descartada", Mensajero.MSG_INFO);
			} else {
				Mensajero.setMsg("Problemas descartando la consulta", Mensajero.MSG_ALERTA);
			}
			controladorHistoriaClinica.setConsulta(null);
		}
	}

	/**
	 * Construye la colleccion de campos
	 * 
	 * @param campos
	 */
	private Set<TextInputControl> prepararListaCampos() {
		Set<TextInputControl> campos = new LinkedHashSet<>();
		campos.add(campoDiamCintura);
		campos.add(campoPulso);
		campos.add(campoTextoMotConsulta);
		campos.add(campoTextoDiagnostico);
		campos.add(areaTextoComplementario);
		campos.add(areaTextoEvolucion);
		return campos;
	}

	/**
	 * Limpia los campos
	 */
	@FXML
	private void limpiarCampos() {
		// Cuadro de dialogo para la confirmacion
		Optional<ButtonType> respuesta = Ventanas.crearAlerta(AlertType.CONFIRMATION, "Limpiar campos", "",
				"¿Desea descartar los datos de los campos Diámetro Cintura, Pulso, Motivo Consulta, Diagnóstico, Complementario y Evolución?",
				Arrays.asList(ButtonType.YES, ButtonType.NO)).showAndWait();
		// Vacia los datos de los campos pasados
		if (respuesta.isPresent() && respuesta.get().equals(ButtonType.YES)) {
			Util.vaciarCampos(campos);
		}
	}

	/**
	 * Construye las plantillas a partir de los valores pasados
	 * 
	 * @return plantillas
	 */
	private HashMap<String, String> construirPlantillas() {
		HashMap<String, String> plantillas = new HashMap<>();
		// TODO Quitar el valor hardcoded y pasarlo a la base de datos.
		// Diabetes
		plantillas.put("DBT", "Monitoreo glucemico \r\n" + "Alimentación \r\n" + "Movimiento \r\n"
				+ "-----------------------------------------------\r\n");
		return plantillas;
	}

	/**
	 * Aplica la plantilla en el campo correspondiente
	 * 
	 * @param consulta
	 * @param plantillas
	 */
	private String aplicarPlantilla(ConsultaMedicina consulta, String referencia, HashMap<String, String> plantillas) {
		// Crea el constructor del campo
		StringBuilder campoConPlantilla = new StringBuilder();
		// Recorre las plantillas para ver coincidencias
		plantillas.forEach((key, value) -> {
			if (referencia.contains(key)) {
				// Anexa la plantilla
				campoConPlantilla.append(value);
			}
		});
		// Agrega la plantilla correspondiente
		return campoConPlantilla.toString();
	}
}
