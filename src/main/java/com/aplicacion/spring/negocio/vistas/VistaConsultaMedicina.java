package com.aplicacion.spring.negocio.vistas;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

@FXMLView(value = "/spring/vistas/negocio/ConsultaMedicina.fxml", encoding = "UTF-8")
public class VistaConsultaMedicina extends AbstractFxmlView {

}
