package com.aplicacion.spring.negocio.vistas;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

@FXMLView(value = "/spring/vistas/negocio/ConsultaNutricion.fxml", encoding = "UTF-8")
public class VistaConsultaNutricion extends AbstractFxmlView {

}
