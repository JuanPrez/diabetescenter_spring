package com.aplicacion.spring.negocio.vistas;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

@FXMLView(value = "/spring/vistas/negocio/ConsultaPodologia.fxml", encoding = "UTF-8")
public class VistaConsultaPodologia extends AbstractFxmlView {

}
