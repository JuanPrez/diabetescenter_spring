package com.aplicacion.spring.negocio.entidades.turnos;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

public class TurnoProfesional {
	private Horario horario;
	private Estado estado;
	private PacienteLite paciente;
	private String observaciones;

	public Horario getHorario() {
		return horario;
	}

	public void setHorario(Horario horario) {
		this.horario = horario;
	}

	public Estado getEstado() {
		return estado;
	}

	@XmlJavaTypeAdapter(AdapterEstado.class)
	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public PacienteLite getPaciente() {
		return paciente;
	}

	public void setPaciente(PacienteLite paciente) {
		this.paciente = paciente;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((horario == null) ? 0 : horario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof TurnoProfesional))
			return false;
		TurnoProfesional other = (TurnoProfesional) obj;
		if (horario == null) {
			if (other.horario != null)
				return false;
		} else if (!horario.equals(other.horario))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TurnoProfesional [horario=" + horario + ", estado=" + estado + ", paciente=" + paciente
				+ ", observaciones=" + observaciones + "]";
	}

}
