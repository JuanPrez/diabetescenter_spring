package com.aplicacion.spring.negocio.entidades.registros.consultas;

import java.math.BigDecimal;
import java.time.LocalDate;

public class EstadisticasConsultaMedicina {
	long id;
	LocalDate fecha;
	BigDecimal peso;
	int presionSistolica;
	int presionDiastolica;

	public EstadisticasConsultaMedicina() {
	}

	public EstadisticasConsultaMedicina(long id, LocalDate fecha, BigDecimal peso, int presionSistolica,
			int presionDiastolica) {
		this.id = id;
		this.fecha = fecha;
		this.peso = peso;
		this.presionSistolica = presionSistolica;
		this.presionDiastolica = presionDiastolica;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getPeso() {
		return peso;
	}

	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}

	public int getPresionSistolica() {
		return presionSistolica;
	}

	public void setPresionSistolica(int presionSistolica) {
		this.presionSistolica = presionSistolica;
	}

	public int getPresionDiastolica() {
		return presionDiastolica;
	}

	public void setPresionDiastolica(int presionDiastolica) {
		this.presionDiastolica = presionDiastolica;
	}

	@Override
	public String toString() {
		return "EstadisticasConsultaMedicina [id=" + id + ", fecha=" + fecha + ", peso=" + peso + ", presionSistolica="
				+ presionSistolica + ", presionDiastolica=" + presionDiastolica + "]";
	}

}
