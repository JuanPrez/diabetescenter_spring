package com.aplicacion.spring.negocio.entidades.registros.consultas;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;

@Component
public class ConsultaPodologica extends Consulta implements IConsulta<ConsultaPodologica> {

	private String evolucion = "";
	private String pulsoPedio = "";
	private String pulsoTibial = "";
	private String texturaPiel = "";
	private String coloracionPiel = "";
	private String temperatura = "";
	private String dedos = "";
	private String onicopatias = "";
	private String observaciones = "";
	private String creador = "";

	public ConsultaPodologica() {
	}

	public ConsultaPodologica(
			// Consulta
			int id, Paciente paciente, LocalDate fecha, LocalDate proxCita, int enmienda, Usuario usuario,
			// Datos Consulta
			String evolucion, String pulsoPedio, String pulsoTibial, String texturaPiel, String coloracionPiel,
			String temperatura, String dedos, String onicopatias, String observaciones) {
		// Consulta
		super(id, enmienda, paciente, fecha, proxCita, usuario);
		this.creador = usuario.getNombreUsuario();
		// Datos consulta
		this.pulsoPedio = pulsoPedio;
		this.pulsoTibial = pulsoTibial;
		this.texturaPiel = texturaPiel;
		this.coloracionPiel = coloracionPiel;
		this.temperatura = temperatura;
		this.dedos = dedos;
		this.onicopatias = onicopatias;
		this.observaciones = observaciones;
		this.evolucion = evolucion;
	}

	public String getEvolucion() {
		return evolucion;
	}

	public void setEvolucion(String evolucion) {
		this.evolucion = evolucion;
	}

	public String getPulsoPedio() {
		return pulsoPedio;
	}

	public void setPulsoPedio(String pulsoPedio) {
		this.pulsoPedio = pulsoPedio;
	}

	public String getPulsoTibial() {
		return pulsoTibial;
	}

	public void setPulsoTibial(String pulsoTibial) {
		this.pulsoTibial = pulsoTibial;
	}

	public String getTexturaPiel() {
		return texturaPiel;
	}

	public void setTexturaPiel(String texturaPiel) {
		this.texturaPiel = texturaPiel;
	}

	public String getColoracionPiel() {
		return coloracionPiel;
	}

	public void setColoracionPiel(String coloracionPiel) {
		this.coloracionPiel = coloracionPiel;
	}

	public String getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}

	public String getDedos() {
		return dedos;
	}

	public void setDedos(String dedos) {
		this.dedos = dedos;
	}

	public String getOnicopatias() {
		return onicopatias;
	}

	public void setOnicopatias(String onicopatias) {
		this.onicopatias = onicopatias;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Override
	public String toString() {
		return "ConsultaPodo [evolucion=" + evolucion + ", pulsoPedio=" + pulsoPedio + ", pulsoTibial=" + pulsoTibial
				+ ", texturaPiel=" + texturaPiel + ", coloracionPiel=" + coloracionPiel + ", temperatura=" + temperatura
				+ ", dedos=" + dedos + ", onicopatias=" + onicopatias + ", observaciones=" + observaciones
				+ ", creador=" + creador + "]";
	}

	@Override
	public ConsultaPodologica copiar() {
		ConsultaPodologica consulta = new ConsultaPodologica();
		consulta.setId(this.getId());
		consulta.setEnmienda(this.getEnmienda());
		consulta.setFecha(this.getFecha());
		consulta.setProxCita(this.getProxCita());
		consulta.setUsuario(this.getUsuario());
		consulta.setPaciente(this.getPaciente());

		consulta.setPulsoPedio(this.getPulsoPedio());
		consulta.setPulsoTibial(this.getPulsoTibial());
		consulta.setTexturaPiel(this.getTexturaPiel());
		consulta.setColoracionPiel(this.getColoracionPiel());
		consulta.setTemperatura(this.getTemperatura());
		consulta.setDedos(this.getDedos());
		consulta.setOnicopatias(this.getOnicopatias());
		consulta.setObservaciones(this.getObservaciones());
		consulta.setEvolucion(this.getEvolucion());
		consulta.setCreador(this.getCreador());

		return consulta;
	}
}