package com.aplicacion.spring.negocio.entidades.turnos;

import com.aplicacion.spring.gestion.entidades.Usuario;

public class ProfesionalLite {
	int id;
	String usuario;
	String nombre;

	public ProfesionalLite() {

	}

	public ProfesionalLite(Usuario usuario) {
		this.id = usuario.getUid();
		this.usuario = usuario.getNombreUsuario();
		this.nombre = usuario.getNombre();
	}

	public ProfesionalLite(int id, String usuario, String nombre) {
		this.id = id;
		this.usuario = usuario;
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "ProfesionalLite [id=" + id + ", usuario=" + usuario + ", nombre=" + nombre + "]";
	}

}
