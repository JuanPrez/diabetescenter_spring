package com.aplicacion.spring.negocio.entidades.paciente;

public class DatosDemograficos {

	private String ciudad = "";
	private String domicilio = "";
	private int codigoPostal = 0;
	private String telefono = "";
	private String telefono2 = "";
	private String email = "";
	private String observaciones = "";

	public DatosDemograficos() {

	}

	public DatosDemograficos(String ciudad, String domicilio, int codigoPostal, String telefono,
			String telefono2, String email, String observaciones) {
		this.ciudad = ciudad;
		this.domicilio = domicilio;
		this.codigoPostal = codigoPostal;
		this.telefono = telefono;
		this.telefono2 = telefono2;
		this.email = email;
		this.observaciones = observaciones;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public int getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(int codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTelefono2() {
		return telefono2;
	}

	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Override
	public String toString() {
		return "PacienteDatosDemograficos [ciudad=" + ciudad + ", domicilio=" + domicilio + ", codigoPostal="
				+ codigoPostal + ", telefono=" + telefono + ", telefono2=" + telefono2 + ", email=" + email
				+ ", observaciones=" + observaciones + "]";
	}

}
