package com.aplicacion.spring.negocio.entidades.registros.patologias;

public class PatologiaEnlaceExamen implements Comparable<PatologiaEnlaceExamen> {
	Patologia patologia;
	Examen examen;

	@Override
	public int compareTo(PatologiaEnlaceExamen o) {
		int r = this.getPatologia().compareTo(o.getPatologia());
		if (r == 0) {
			r = this.getExamen().compareTo(o.getExamen());
		}
		return r;
	}

	public Patologia getPatologia() {
		return patologia;
	}

	public void setPatologia(Patologia patologia) {
		this.patologia = patologia;
	}

	public PatologiaEnlaceExamen() {
	}

	public Examen getExamen() {
		return examen;
	}

	public void setExamen(Examen examen) {
		this.examen = examen;
	}

	public PatologiaEnlaceExamen(Patologia patologia, Examen examen) {
		this.patologia = patologia;
		this.examen = examen;
	}

	@Override
	public String toString() {
		return "EnlacePatologiaExamen [Patologia=" + patologia + ", examen=" + examen + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((examen == null) ? 0 : examen.hashCode());
		result = prime * result + ((patologia == null) ? 0 : patologia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof PatologiaEnlaceExamen))
			return false;
		PatologiaEnlaceExamen other = (PatologiaEnlaceExamen) obj;
		if (examen == null) {
			if (other.examen != null)
				return false;
		} else if (!examen.equals(other.examen))
			return false;
		if (patologia == null) {
			if (other.patologia != null)
				return false;
		} else if (!patologia.equals(other.patologia))
			return false;
		return true;
	}
}
