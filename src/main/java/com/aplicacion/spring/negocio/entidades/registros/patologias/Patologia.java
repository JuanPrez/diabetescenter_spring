package com.aplicacion.spring.negocio.entidades.registros.patologias;

/**
 * 
 * POJO representando al objeto Patologia
 * 
 * @Atrib id
 * @Atrib nombre
 * @Atrib abreviatura
 * 
 * @author Juan Ignacio Prez
 *
 */
public class Patologia implements Comparable<Patologia> {
	public static final String HEMOGLOBINA_GLICOCILADA = "HbA1c";

	/* SimpleIntegerProperty SimpleStringProperty SimpleObjectProperty<> */
	protected int id;
	protected String nombre = "";
	protected String abreviatura = "";

	public Patologia() {
	}

	public Patologia(int id, String nombre, String abrev) {
		this.id = id;
		this.nombre = nombre;
		this.abreviatura = abrev;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}

	@Override
	public String toString() {
		return nombre;
	}

	public String print() {
		return "Examen [id=" + id + ", nombre=" + nombre + ", abreviatura=" + abreviatura + "]";
	}

	@Override
	public int compareTo(Patologia o) {
		return this.nombre.compareTo(o.nombre);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Patologia))
			return false;
		Patologia other = (Patologia) obj;
		if (id != other.id)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

}
