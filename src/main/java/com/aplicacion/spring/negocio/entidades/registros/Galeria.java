package com.aplicacion.spring.negocio.entidades.registros;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.SimpleIntegerProperty;

public class Galeria {
	private SimpleIntegerProperty minimo = new SimpleIntegerProperty(0);
	private SimpleIntegerProperty rango = new SimpleIntegerProperty(10);
	private SimpleIntegerProperty cantidad = new SimpleIntegerProperty(0);
	private List<ImagenPaciente> listaImagenes;

	public Galeria() {
		minimo.set(0);
		rango.set(10);
		cantidad.set(0);
		listaImagenes = new ArrayList<>();
	}

	public List<ImagenPaciente> getListaImagenes() {
		return listaImagenes;
	}

	public void setListaImagenes(List<ImagenPaciente> listaImagenes) {
		this.listaImagenes = listaImagenes;
	}

	public final SimpleIntegerProperty minimoProperty() {
		return this.minimo;
	}

	public final int getMinimo() {
		return this.minimoProperty().get();
	}

	public final void setMinimo(final int minimo) {
		this.minimoProperty().set(minimo);
	}

	public final SimpleIntegerProperty rangoProperty() {
		return this.rango;
	}

	public final int getRango() {
		return this.rangoProperty().get();
	}

	public final void setRango(final int rango) {
		this.rangoProperty().set(rango);
	}

	public final SimpleIntegerProperty cantidadProperty() {
		return this.cantidad;
	}

	public final int getCantidad() {
		return this.cantidadProperty().get();
	}

	public final void setCantidad(final int cantidad) {
		this.cantidadProperty().set(cantidad);
	}

}
