package com.aplicacion.spring.negocio.entidades.paciente;

import java.util.ArrayList;
import java.util.List;

import com.aplicacion.spring.negocio.entidades.registros.Seguimiento;

public class DatosClinicos {

	private String talla = "";
	private String antecedentes = "";
	private String statusPatologico = "";
	private List<Seguimiento> consultas = new ArrayList<>();
	private char difunto;

	public DatosClinicos() {
		difunto = 'N';
	}

	public DatosClinicos(String talla, String antQuirurgico, String statusPatologico, List<Seguimiento> consulta,
			char difunto) {
		this.talla = talla;
		this.antecedentes = antQuirurgico;
		this.statusPatologico = statusPatologico;
		this.consultas = consulta;
		this.difunto = difunto;
	}

	public String getTalla() {
		return talla;
	}

	public void setTalla(String talla) {
		this.talla = talla;
	}

	public String getAntecedentes() {
		return antecedentes;
	}

	public void setAntecedentes(String antecedentes) {
		this.antecedentes = antecedentes;
	}

	public String getStatusPatologico() {
		return statusPatologico;
	}

	public void setStatusPatologico(String statusPatologico) {
		this.statusPatologico = statusPatologico;
	}

	public List<Seguimiento> getConsultas() {
		return consultas;
	}

	public void setConsultas(List<Seguimiento> consultas) {
		this.consultas = consultas;
	}

	public char getDifunto() {
		return difunto;
	}

	public void setDifunto(char difunto) {
		this.difunto = difunto;
	}

}
