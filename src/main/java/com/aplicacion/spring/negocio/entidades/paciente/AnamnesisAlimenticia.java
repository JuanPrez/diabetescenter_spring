package com.aplicacion.spring.negocio.entidades.paciente;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

@Component
public class AnamnesisAlimenticia {

	private SimpleIntegerProperty idPaciente = new SimpleIntegerProperty();
	private SimpleStringProperty usuario = new SimpleStringProperty();
	private SimpleStringProperty desayuno = new SimpleStringProperty();
	private SimpleStringProperty mediam = new SimpleStringProperty();
	private SimpleStringProperty almuerzo = new SimpleStringProperty();
	private SimpleStringProperty mediat = new SimpleStringProperty();
	private SimpleStringProperty cena = new SimpleStringProperty();
	private SimpleStringProperty notas = new SimpleStringProperty();
	private LocalDate fechaCreacion = null;
	private LocalDate fechaUltAct = null;

	public AnamnesisAlimenticia() {
	}

	public AnamnesisAlimenticia(int idPaciente, String usuario, String desayuno, String mediam, String almuerzo,
			String mediat, String cena, String notas, LocalDate fechaCreacion, LocalDate fechaUltAct) {
		this.idPaciente.set(idPaciente);
		this.usuario.set(usuario);
		this.desayuno.set(desayuno);
		this.mediam.set(mediam);
		this.almuerzo.set(almuerzo);
		this.mediat.set(mediat);
		this.cena.set(cena);
		this.notas.set(notas);
		this.fechaCreacion = fechaCreacion;
		this.fechaUltAct = fechaUltAct;
	}

	public final SimpleIntegerProperty idPacienteProperty() {
		return this.idPaciente;
	}

	public final int getIdPaciente() {
		return this.idPacienteProperty().get();
	}

	public final void setIdPaciente(final int idPaciente) {
		this.idPacienteProperty().set(idPaciente);
	}

	public final SimpleStringProperty usuarioProperty() {
		return this.usuario;
	}

	public final String getUsuario() {
		return this.usuarioProperty().get();
	}

	public final void setUsuario(final String usuario) {
		this.usuarioProperty().set(usuario);
	}

	public final SimpleStringProperty desayunoProperty() {
		return this.desayuno;
	}

	public final String getDesayuno() {
		return this.desayunoProperty().get();
	}

	public final void setDesayuno(final String desayuno) {
		this.desayunoProperty().set(desayuno);
	}

	public final SimpleStringProperty mediamProperty() {
		return this.mediam;
	}

	public final String getMediam() {
		return this.mediamProperty().get();
	}

	public final void setMediam(final String mediam) {
		this.mediamProperty().set(mediam);
	}

	public final SimpleStringProperty almuerzoProperty() {
		return this.almuerzo;
	}

	public final String getAlmuerzo() {
		return this.almuerzoProperty().get();
	}

	public final void setAlmuerzo(final String almuerzo) {
		this.almuerzoProperty().set(almuerzo);
	}

	public final SimpleStringProperty mediatProperty() {
		return this.mediat;
	}

	public final String getMediat() {
		return this.mediatProperty().get();
	}

	public final void setMediat(final String mediat) {
		this.mediatProperty().set(mediat);
	}

	public final SimpleStringProperty cenaProperty() {
		return this.cena;
	}

	public final String getCena() {
		return this.cenaProperty().get();
	}

	public final void setCena(final String cena) {
		this.cenaProperty().set(cena);
	}

	public final SimpleStringProperty notasProperty() {
		return this.notas;
	}

	public final String getNotas() {
		return this.notasProperty().get();
	}

	public final void setNotas(final String notas) {
		this.notasProperty().set(notas);
	}

	public LocalDate getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDate fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public LocalDate getFechaUltAct() {
		return fechaUltAct;
	}

	public void setFechaUltAct(LocalDate fechaUltAct) {
		this.fechaUltAct = fechaUltAct;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idPaciente.get();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AnamnesisAlimenticia))
			return false;
		AnamnesisAlimenticia other = (AnamnesisAlimenticia) obj;
		if (idPaciente == null) {
			if (other.idPaciente != null)
				return false;
		} else if (idPaciente.get() != other.idPaciente.get())
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AnamnesisAlimenticia [idPaciente=" + idPaciente.get() + ", usuario=" + usuario.get() + ", desayuno="
				+ desayuno.get() + ", mediam=" + mediam.get() + ", almuerzo=" + almuerzo.get() + ", mediat="
				+ mediat.get() + ", cena=" + cena.get() + ", notas=" + notas.get() + ", fechaCreacion=" + fechaCreacion
				+ ", fechaUltAct=" + fechaUltAct + "]";
	}

}
