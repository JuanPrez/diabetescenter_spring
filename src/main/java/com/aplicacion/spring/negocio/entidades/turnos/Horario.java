package com.aplicacion.spring.negocio.entidades.turnos;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Horario implements Comparable<Horario> {

	SimpleIntegerProperty id = new SimpleIntegerProperty();
	SimpleStringProperty hora = new SimpleStringProperty();

	@Override
	public int compareTo(Horario o) {
		return this.getHora().compareTo(o.getHora());
	}

	public Horario() {

	}

	public Horario(int id, String hora) {
		this.id.set(id);
		this.hora.set(hora);
	}

	public final SimpleIntegerProperty idProperty() {
		return this.id;
	}

	public final int getId() {
		return this.idProperty().get();
	}

	public final void setId(final int id) {
		this.idProperty().set(id);
	}

	public final SimpleStringProperty horaProperty() {
		return this.hora;
	}

	public final String getHora() {
		return this.horaProperty().get();
	}

	public final void setHora(final String hora) {
		this.horaProperty().set(hora);
	}

	@Override
	public String toString() {
		return "Horario [getId()=" + getId() + ", getHorario()=" + getHora() + "]";
	}

}
