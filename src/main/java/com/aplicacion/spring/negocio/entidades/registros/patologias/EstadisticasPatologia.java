package com.aplicacion.spring.negocio.entidades.registros.patologias;

import java.time.LocalDate;

public class EstadisticasPatologia {
	long idpaciente;
	LocalDate fecha;
	Number resultado;

	public EstadisticasPatologia() {

	}

	public EstadisticasPatologia(long idpaciente, LocalDate fecha, Number resultado) {
		this.idpaciente = idpaciente;
		this.fecha = fecha;
		this.resultado = resultado;
	}

	public long getIdpaciente() {
		return idpaciente;
	}

	public void setIdpaciente(long idpaciente) {
		this.idpaciente = idpaciente;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public Number getResultado() {
		return resultado;
	}

	public void setResultado(Number resultado) {
		this.resultado = resultado;
	}

	@Override
	public String toString() {
		return "EstadisticasPatologia [idpaciente=" + idpaciente + ", fecha=" + fecha + ", resultado=" + resultado
				+ "]";
	}

}
