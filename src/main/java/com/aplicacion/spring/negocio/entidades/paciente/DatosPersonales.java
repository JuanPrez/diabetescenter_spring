package com.aplicacion.spring.negocio.entidades.paciente;

import java.time.LocalDate;

import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;

public class DatosPersonales {
	private char sexo;
	private int edad = 0;
	private LocalDate fechaNacimiento;
	private String idObraSocial = "";
	private String nroAfiliado = "";
	private String dni = "";
	private SimpleObjectProperty<Image> foto = new SimpleObjectProperty<>();

	public DatosPersonales() {

	}

	public DatosPersonales(char sexo, int edad, LocalDate fechaNacimiento, String idObraSocial,
			String nroAfiliado, String dni, Image foto) {
		this.sexo = sexo;
		this.edad = edad;
		this.fechaNacimiento = fechaNacimiento;
		this.idObraSocial = idObraSocial;
		this.nroAfiliado = nroAfiliado;
		this.dni = dni;
		this.foto.set(foto);
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getIdObraSocial() {
		return idObraSocial;
	}

	public void setIdObraSocial(String idObraSocial) {
		this.idObraSocial = idObraSocial;
	}

	public String getNroAfiliado() {
		return nroAfiliado;
	}

	public void setNroAfiliado(String nroAfiliado) {
		this.nroAfiliado = nroAfiliado;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	@Override
	public String toString() {
		return "PacienteDatosPersonales [sexo=" + sexo + ", edad=" + edad + ", fechaNacimiento=" + fechaNacimiento
				+ ", idObraSocial=" + idObraSocial + ", nroAfiliado=" + nroAfiliado + ", dni=" + dni + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		result = prime * result + edad;
		result = prime * result + ((idObraSocial == null) ? 0 : idObraSocial.hashCode());
		result = prime * result + ((nroAfiliado == null) ? 0 : nroAfiliado.hashCode());
		result = prime * result + sexo;
		return result;
	}

	public final SimpleObjectProperty<Image> fotoProperty() {
		return this.foto;
	}

	public final Image getFoto() {
		return this.fotoProperty().get();
	}

	public final void setFoto(final Image foto) {
		this.fotoProperty().set(foto);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof DatosPersonales))
			return false;
		DatosPersonales other = (DatosPersonales) obj;
		if (dni == null) {
			if (other.dni != null)
				return false;
		} else if (!dni.equals(other.dni))
			return false;
		if (edad != other.edad)
			return false;
		if (idObraSocial == null) {
			if (other.idObraSocial != null)
				return false;
		} else if (!idObraSocial.equals(other.idObraSocial))
			return false;
		if (nroAfiliado == null) {
			if (other.nroAfiliado != null)
				return false;
		} else if (!nroAfiliado.equals(other.nroAfiliado))
			return false;
		if (sexo != other.sexo)
			return false;
		return true;
	}

}
