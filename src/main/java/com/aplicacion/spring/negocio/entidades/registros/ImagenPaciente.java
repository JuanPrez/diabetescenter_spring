package com.aplicacion.spring.negocio.entidades.registros;

import java.time.LocalDate;

import javafx.scene.image.Image;

public class ImagenPaciente implements Comparable<ImagenPaciente> {
	private int id;
	private String comentarios = "";
	private LocalDate fecha = null;
	private Image imagen = null;
	private int idPaciente;
	private int tipoConsulta;
	private int idConsulta;

	public ImagenPaciente() {
	}

	public ImagenPaciente(int id, String comentarios, LocalDate fecha, Image imagen, int idPaciente,
			int tipoConsulta, int idConsulta) {
		this.id = id;
		this.comentarios = comentarios;
		this.fecha = fecha;
		this.imagen = imagen;
		this.idPaciente = idPaciente;
		this.tipoConsulta = tipoConsulta;
		this.idConsulta = idConsulta;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public Image getImagen() {
		return imagen;
	}

	public void setImagen(Image imagen) {
		this.imagen = imagen;
	}

	public int getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(int idPaciente) {
		this.idPaciente = idPaciente;
	}

	public int getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(int tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public int getIdConsulta() {
		return idConsulta;
	}

	public void setIdConsulta(int idConsulta) {
		this.idConsulta = idConsulta;
	}

	@Override
	public String toString() {
		return "ImagenHistClinica [id=" + id + ", comentarios=" + comentarios + ", fecha=" + fecha + ", imagen="
				+ imagen + ", idPaciente=" + idPaciente + ", tipoConsulta=" + tipoConsulta + ", idConsulta="
				+ idConsulta + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ImagenPaciente))
			return false;
		ImagenPaciente other = (ImagenPaciente) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public int compareTo(ImagenPaciente o) {
		return Integer.valueOf(getId()).compareTo(Integer.valueOf(o.getId()));
	}

}
