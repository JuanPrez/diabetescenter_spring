package com.aplicacion.spring.negocio.entidades.paciente;

public class ObraSocial {

	private int id;
	private String descripcion;

	public ObraSocial() {

	}

	public ObraSocial(int id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "ObraSocial [id=" + id + ", descripcion=" + descripcion + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ObraSocial))
			return false;
		ObraSocial other = (ObraSocial) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
