package com.aplicacion.spring.negocio.entidades.registros.patologias;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * 
 * POJO representando al objeto Examen
 * 
 * @Atrib id
 * @Atrib nombre
 * 
 * @author Juan Ignacio Prez
 *
 */
public class Examen implements Comparable<Examen> {
	SimpleIntegerProperty id = new SimpleIntegerProperty();
	SimpleStringProperty nombre = new SimpleStringProperty();

	public Examen() {
	}

	public Examen(int id, String nombre) {
		super();
		this.id.set(id);
		this.nombre.set(nombre);
	}

	public final SimpleIntegerProperty idProperty() {
		return this.id;
	}

	public final int getId() {
		return this.idProperty().get();
	}

	public final void setId(final int id) {
		this.idProperty().set(id);
	}

	public final SimpleStringProperty nombreProperty() {
		return this.nombre;
	}

	public final String getNombre() {
		return this.nombreProperty().get();
	}

	public final void setNombre(final String nombre) {
		this.nombreProperty().set(nombre);
	}

	@Override
	public String toString() {
		return nombre.get();
	}

	public String print() {
		return "Examen [id=" + id + ", nombre=" + nombre.get() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre.get() == null) ? 0 : nombre.get().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Examen))
			return false;
		Examen other = (Examen) obj;
		if (nombre.get() == null) {
			if (other.nombre.get() != null)
				return false;
		} else if (!nombre.get().equals(other.nombre.get()))
			return false;
		return true;
	}

	@Override
	public int compareTo(Examen o) {
		return this.getNombre().compareTo(o.getNombre());
	}

}
