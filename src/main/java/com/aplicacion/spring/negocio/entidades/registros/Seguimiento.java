package com.aplicacion.spring.negocio.entidades.registros;

import java.time.LocalDate;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Seguimiento {
	SimpleIntegerProperty idPaciente = new SimpleIntegerProperty();
	SimpleIntegerProperty idEspecialidad = new SimpleIntegerProperty();
	SimpleStringProperty especialidad = new SimpleStringProperty();
	LocalDate ultConsulta;
	LocalDate proxCita;
	SimpleIntegerProperty idUsuario = new SimpleIntegerProperty();
	SimpleStringProperty usuario = new SimpleStringProperty();
	SimpleStringProperty profesional = new SimpleStringProperty();
	SimpleIntegerProperty idUltConsulta = new SimpleIntegerProperty();

	public Seguimiento() {

	}

	public Seguimiento(int idPaciente, int idEspecialidad, String especialidad, LocalDate ultConsulta,
			LocalDate proxCita, int idUsuario, String usuario, String profesional, int idUltConsulta) {
		this.idPaciente.set(idPaciente);
		this.idEspecialidad.set(idEspecialidad);
		this.especialidad.set(especialidad);
		this.ultConsulta = ultConsulta;
		this.proxCita = proxCita;
		this.idUsuario.set(idUsuario);
		this.usuario.set(usuario);
		this.profesional.set(profesional);
		this.idUltConsulta.set(idUltConsulta);
	}

	public final SimpleIntegerProperty idPacienteProperty() {
		return this.idPaciente;
	}

	public final int getIdPaciente() {
		return this.idPacienteProperty().get();
	}

	public final void setIdPaciente(final int idPaciente) {
		this.idPacienteProperty().set(idPaciente);
	}

	public final SimpleIntegerProperty idEspecialidadProperty() {
		return this.idEspecialidad;
	}

	public final int getIdEspecialidad() {
		return this.idEspecialidadProperty().get();
	}

	public final void setIdEspecialidad(final int idEspecialidad) {
		this.idEspecialidadProperty().set(idEspecialidad);
	}

	public final SimpleIntegerProperty idUsuarioProperty() {
		return this.idUsuario;
	}

	public final int getIdUsuario() {
		return this.idUsuarioProperty().get();
	}

	public final void setIdUsuario(final int idUsuario) {
		this.idUsuarioProperty().set(idUsuario);
	}

	public final SimpleIntegerProperty idUltConsultaProperty() {
		return this.idUltConsulta;
	}

	public final int getIdUltConsulta() {
		return this.idUltConsultaProperty().get();
	}

	public final void setIdUltConsulta(final int idUltConsulta) {
		this.idUltConsultaProperty().set(idUltConsulta);
	}

	public final SimpleStringProperty especialidadProperty() {
		return this.especialidad;
	}

	public final String getEspecialidad() {
		return this.especialidadProperty().get();
	}

	public final void setEspecialidad(final String especialidad) {
		this.especialidadProperty().set(especialidad);
	}

	public final SimpleStringProperty usuarioProperty() {
		return this.usuario;
	}

	public final String getUsuario() {
		return this.usuarioProperty().get();
	}

	public final void setUsuario(final String usuario) {
		this.usuarioProperty().set(usuario);
	}

	public final SimpleStringProperty profesionalProperty() {
		return this.profesional;
	}

	public final String getProfesional() {
		return this.profesionalProperty().get();
	}

	public final void setProfesional(final String profesional) {
		this.profesionalProperty().set(profesional);
	}

	public LocalDate getUltConsulta() {
		return ultConsulta;
	}

	public void setUltConsulta(LocalDate ultConsulta) {
		this.ultConsulta = ultConsulta;
	}

	public LocalDate getProxCita() {
		return proxCita;
	}

	public void setProxCita(LocalDate proxCita) {
		this.proxCita = proxCita;
	}

	@Override
	public String toString() {
		return "CronogramaConsultas [idPaciente=" + idPaciente.get() + ", idEspecialidad=" + idEspecialidad.get()
				+ ", ultConsulta=" + ultConsulta + ", proxCita=" + proxCita + ", idUsuario=" + idUsuario.get()
				+ ", idUltConsulta=" + idUltConsulta.get() + "]";
	}

}
