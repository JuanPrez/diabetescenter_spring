package com.aplicacion.spring.negocio.entidades.paciente;

public class Afiliado {

	private int id;
	private int idOS;
	private String nroAfiliado = "";

	public Afiliado() {
	}

	public Afiliado(int id, int idOS, String nroAfiliado) {
		this.id = id;
		this.idOS = idOS;
		this.nroAfiliado = nroAfiliado;
	}

	public Afiliado(int id, String nroAfiliado) {
		this.id = id;
		this.nroAfiliado = nroAfiliado;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdOS() {
		return idOS;
	}

	public void setIdOS(int idOS) {
		this.idOS = idOS;
	}

	public String getNroAfiliado() {
		return nroAfiliado;
	}

	public void setNroAfiliado(String nroAfiliado) {
		this.nroAfiliado = nroAfiliado;
	}

	@Override
	public String toString() {
		return "Afiliado [id=" + id + ", idOS=" + idOS + ", nroAfiliado=" + nroAfiliado + "]";
	}

}
