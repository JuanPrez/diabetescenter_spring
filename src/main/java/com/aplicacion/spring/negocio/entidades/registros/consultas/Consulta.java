package com.aplicacion.spring.negocio.entidades.registros.consultas;

import java.time.LocalDate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;

public abstract class Consulta implements Comparable<Consulta> {

	public static final String MEDICINA = "Medicina";
	public static final String NUTRICION = "Nutricion";
	public static final String PODOLOGIA = "Podologia";

	protected int id;
	protected int enmienda;
	protected Paciente paciente;
	protected LocalDate fecha;
	protected LocalDate proxCita;
	protected Usuario usuario;

	@Override
	public int compareTo(Consulta o) {
		return Integer.valueOf(o.getId()).compareTo(Integer.valueOf(this.getId()));
	}

	public Consulta() {
	}

	public Consulta(int id, int enmienda, Paciente paciente, LocalDate fecha, LocalDate proxCita, Usuario usuario) {
		this.id = id;
		this.enmienda = enmienda;
		this.setPaciente(paciente);
		this.fecha = fecha;
		this.proxCita = proxCita;
		this.usuario = usuario;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEnmienda() {
		return enmienda;
	}

	public void setEnmienda(int enmienda) {
		this.enmienda = enmienda;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public LocalDate getProxCita() {
		return proxCita;
	}

	public void setProxCita(LocalDate proxCita) {
		this.proxCita = proxCita;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public String toString() {
		return "Consulta [id=" + id + ", enmienda=" + enmienda + ", paciente=" + paciente + ", fecha=" + fecha
				+ ", proxCita=" + proxCita + ", usuario=" + usuario + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((paciente == null) ? 0 : paciente.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Consulta))
			return false;
		Consulta other = (Consulta) obj;
		if (id != other.id)
			return false;
		if (paciente == null) {
			if (other.paciente != null)
				return false;
		} else if (!paciente.equals(other.paciente))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		return true;
	}

	@Bean
	@Scope("prototype")
	public static Consulta nuevaConsulta(String tipo) {
		switch (tipo) {
		case MEDICINA:
			return new ConsultaMedicina();
		case NUTRICION:
			return new ConsultaNutricion();
		case PODOLOGIA:
			return new ConsultaPodologica();
		default:
			return null;
		}
	}

}