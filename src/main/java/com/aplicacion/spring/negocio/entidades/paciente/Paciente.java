/* ************************************************************************************* *
 *                                                                                       *
 *  Licensed to the Apache Software Foundation (ASF) under one                           *
 *  or more contributor license agreements.  See the NOTICE file                         *
 *  distributed with this work for additional information                                *
 *  regarding copyright ownership.  The ASF licenses this file                           *
 *  to you under the Apache License, Version 2.0 (the                                    *
 *  "License"); you may not use this file except in compliance                           *
 *  with the License.  You may obtain a copy of the License at                           *
 *                                                                                       *
 *    http://www.apache.org/licenses/LICENSE-2.0                                         *
 *                                                                                       *
 *  Unless required by applicable law or agreed to in writing,                           *
 *  software distributed under the License is distributed on an                          *
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY                               *
 *  KIND, either express or implied.  See the License for the                            *
 *  specific language governing permissions and limitations                              *
 *  under the License.                                                                   *
 *                                                                                       *
 * ************************************************************************************* */
package com.aplicacion.spring.negocio.entidades.paciente;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.registros.HistoriaClinica;

import javafx.scene.image.Image;

@Component
public class Paciente {

	private static Image imagenDefecto;

	private int id;
	private String apellido = "";
	private String nombre = "";
	private DatosPersonales datosPersonales = new DatosPersonales();
	private DatosDemograficos datosDemograficos = new DatosDemograficos();
	private DatosClinicos datosClinicos = new DatosClinicos();
	private Antropometricos datosAntropometricos;
	private AnamnesisAlimenticia anamnesisAlimenticia;
	private HistoriaClinica historiaClinica = new HistoriaClinica();
	private LocalDate fecAlta;
	private LocalDate ultimaAct;
	private Usuario usuario = new Usuario();

	/**
	 * 
	 * Constructor generico
	 * 
	 */
	public Paciente() {
		id = 0;
		apellido = "";
		nombre = "";
		historiaClinica = new HistoriaClinica();
	}

	/**
	 * 
	 * Constructor minimo (para listas)
	 *
	 * @param id
	 * @param apellido
	 * @param nombre
	 */
	public Paciente(int id, String apellido, String nombre) {
		this.id = id;
		this.apellido = apellido;
		this.nombre = nombre;
		historiaClinica = new HistoriaClinica();
	}

	/**
	 * 
	 * Constructor minimo con fecha de nacimiento (aniversarios)
	 * 
	 * @param id
	 * @param apellido
	 * @param nombre
	 * @param fechaNacimiento
	 */

	public Paciente(int id, String apellido, String nombre, LocalDate fechaNacimiento) {
		this.id = id;
		this.apellido = apellido;
		this.nombre = nombre;
		this.datosPersonales = new DatosPersonales();
		this.datosPersonales.setFechaNacimiento(fechaNacimiento);
		historiaClinica = new HistoriaClinica();
	}

	/**
	 * 
	 * Constructor completo
	 * 
	 * @param id
	 * @param nombre
	 * @param apellido
	 * @param datosPersonales
	 * @param datosDemograficos
	 * @param datosClinicos
	 * @param fecAlta
	 * @param ultimaAct
	 * @param usuario
	 */
	public Paciente(int id, String nombre, String apellido, DatosPersonales datosPersonales,
			DatosDemograficos datosDemograficos, DatosClinicos datosClinicos, LocalDate fecAlta, LocalDate ultimaAct,
			Usuario usuario) {
		this.setId(id);
		this.setApellido(apellido);
		this.setNombre(nombre);
		this.datosPersonales = datosPersonales;
		this.datosDemograficos = datosDemograficos;
		this.datosClinicos = datosClinicos;
		this.datosAntropometricos = new Antropometricos();
		this.anamnesisAlimenticia = new AnamnesisAlimenticia();
		this.historiaClinica = new HistoriaClinica();
		this.fecAlta = fecAlta;
		this.ultimaAct = ultimaAct;
		this.usuario = usuario;
	}

	public Paciente(Paciente paciente) {
		this.setId(paciente.getId());
		this.setApellido(paciente.getApellido());
		this.setNombre(paciente.getNombre());
		this.datosPersonales = paciente.getDatosPersonales();
		this.datosDemograficos = paciente.getDatosDemograficos();
		this.datosClinicos = paciente.getDatosClinicos();
		this.datosAntropometricos = paciente.getDatosAntropometricos();
		this.anamnesisAlimenticia = paciente.getAnamnesisAlimenticia();
		this.historiaClinica = paciente.getHistoriaClinica();
		this.fecAlta = paciente.getFecAlta();
		this.ultimaAct = paciente.getUltimaAct();
		this.usuario = paciente.getUsuario();
	}

	// Contructor para turnos
	public Paciente(int id, String apellido, String nombre, String dni) {
		this();
		this.id = id;
		this.apellido = apellido;
		this.nombre = nombre;
		this.datosPersonales = new DatosPersonales();
		this.datosPersonales.setDni(dni);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public DatosPersonales getDatosPersonales() {
		return datosPersonales;
	}

	public void setDatosPersonales(DatosPersonales datosPersonales) {
		this.datosPersonales = datosPersonales;
	}

	public DatosDemograficos getDatosDemograficos() {
		return datosDemograficos;
	}

	public void setDatosDemograficos(DatosDemograficos datosDemograficos) {
		this.datosDemograficos = datosDemograficos;
	}

	public DatosClinicos getDatosClinicos() {
		return datosClinicos;
	}

	public void setDatosClinicos(DatosClinicos datosClinicos) {
		this.datosClinicos = datosClinicos;
	}

	public LocalDate getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(LocalDate fecAlta) {
		this.fecAlta = fecAlta;
	}

	public LocalDate getUltimaAct() {
		return ultimaAct;
	}

	public void setUltimaAct(LocalDate ultimaAct) {
		this.ultimaAct = ultimaAct;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Antropometricos getDatosAntropometricos() {
		return datosAntropometricos;
	}

	public void setDatosAntropometricos(Antropometricos datosAntropometricos) {
		this.datosAntropometricos = datosAntropometricos;
	}

	public AnamnesisAlimenticia getAnamnesisAlimenticia() {
		return anamnesisAlimenticia;
	}

	public void setAnamnesisAlimenticia(AnamnesisAlimenticia anamnesisAlimenticia) {
		this.anamnesisAlimenticia = anamnesisAlimenticia;
	}

	public HistoriaClinica getHistoriaClinica() {
		return historiaClinica;
	}

	public void setHistoriaClinica(HistoriaClinica historiaClinica) {
		this.historiaClinica = historiaClinica;
	}

	@Override
	public String toString() {
		return "Paciente [id=" + id + ", apellido=" + apellido + ", nombre=" + nombre + ", datosPersonales="
				+ datosPersonales + ", datosDemograficos=" + datosDemograficos + ", datosClinicos=" + datosClinicos
				+ ", datosAntropometricos=" + datosAntropometricos + ", anamnesisAlimenticia=" + anamnesisAlimenticia
				+ ", historiaClinica=" + historiaClinica + ", fecAlta=" + fecAlta + ", ultimaAct=" + ultimaAct
				+ ", usuario=" + usuario + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Paciente))
			return false;
		Paciente other = (Paciente) obj;
		return id != other.id;
	}

	public static Image getImagenDefecto() {
		return imagenDefecto;
	}

	public static void setImagenDefecto(Image imagenDefecto) {
		Paciente.imagenDefecto = imagenDefecto;
	}

}
