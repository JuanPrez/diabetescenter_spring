package com.aplicacion.spring.negocio.entidades.registros.consultas;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;

@Component
public class ConsultaMedicina extends Consulta implements IConsulta<ConsultaMedicina> {

	private BigDecimal peso = new BigDecimal(0).setScale(3, RoundingMode.HALF_EVEN);
	private BigDecimal diam = new BigDecimal(0).setScale(1, RoundingMode.HALF_EVEN);
	private int sist = 0;
	private int dias = 0;
	private int pulso = 0;
	private String motivo = "";
	private String diagn = "";
	private String trata = "";
	private String evoluc = "";
	private String comple = "";
	private String creador = "";
	private int tipoConsulta = 0;

	public ConsultaMedicina() {
	}

	public ConsultaMedicina(int id, Paciente paciente, LocalDate fecha, LocalDate proxCita, BigDecimal peso,
			BigDecimal diam, int sist, int dias, int pulso, String motivo, String diagn, String trata, String evoluc,
			String comple, int enmienda, Usuario usuario, int tipoConsulta) {
		super(id, enmienda, paciente, fecha, proxCita, usuario);
		this.peso = peso;
		this.diam = diam;
		this.sist = sist;
		this.dias = dias;
		this.pulso = pulso;
		this.motivo = motivo;
		this.diagn = diagn;
		this.trata = trata;
		this.evoluc = evoluc;
		this.comple = comple;
		this.tipoConsulta = tipoConsulta;
		this.creador = usuario.getNombreUsuario();
	}

	public BigDecimal getPeso() {
		return peso;
	}

	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}

	public BigDecimal getDiam() {
		return diam;
	}

	public void setDiam(BigDecimal diam) {
		this.diam = diam;
	}

	public int getSist() {
		return sist;
	}

	public void setSist(int sist) {
		this.sist = sist;
	}

	public int getDias() {
		return dias;
	}

	public void setDias(int dias) {
		this.dias = dias;
	}

	public int getPulso() {
		return pulso;
	}

	public void setPulso(int pulso) {
		this.pulso = pulso;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getDiagn() {
		return diagn;
	}

	public void setDiagn(String diagn) {
		this.diagn = diagn;
	}

	public String getTrata() {
		return trata;
	}

	public void setTrata(String trata) {
		this.trata = trata;
	}

	public String getEvoluc() {
		return evoluc;
	}

	public void setEvoluc(String evoluc) {
		this.evoluc = evoluc;
	}

	public String getComple() {
		return comple;
	}

	public void setComple(String comple) {
		this.comple = comple;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	public int getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(int tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	@Override
	public String toString() {
		return "ConsultaMedica [peso=" + peso + ", diam=" + diam + ", sist=" + sist + ", dias=" + dias + ", pulso="
				+ pulso + ", motivo=" + motivo + ", diagn=" + diagn + ", trata=" + trata + ", evoluc=" + evoluc
				+ ", comple=" + comple + ", creador=" + creador + ", tipoConsulta=" + tipoConsulta + "]";
	}

	@Override
	public ConsultaMedicina copiar() {
		ConsultaMedicina consulta = new ConsultaMedicina();
		consulta.setId(this.getId());
		consulta.setEnmienda(this.getEnmienda());
		consulta.setFecha(this.getFecha());
		consulta.setProxCita(this.getProxCita());
		consulta.setUsuario(this.getUsuario());
		consulta.setPaciente(this.getPaciente());

		consulta.setTipoConsulta(this.getTipoConsulta());
		consulta.setPeso(this.getPeso());
		consulta.setPulso(this.getPulso());
		consulta.setDiam(this.getDiam());
		consulta.setSist(this.getSist());
		consulta.setDias(this.getDias());

		consulta.setDiagn(this.getDiagn());
		consulta.setMotivo(this.getMotivo());
		consulta.setEvoluc(this.getEvoluc());
		consulta.setTrata(this.getTrata());
		consulta.setComple(this.getComple());
		consulta.setCreador(this.getCreador());

		return consulta;
	}

}
