package com.aplicacion.spring.negocio.entidades.registros.consultas;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;

@Component
public class ConsultaNutricion extends Consulta implements IConsulta<ConsultaNutricion> {

	private BigDecimal consultaPeso = new BigDecimal(0).setScale(3, RoundingMode.HALF_EVEN);
	private BigDecimal consultaDiam = new BigDecimal(0).setScale(1, RoundingMode.HALF_EVEN);
	private String tratamiento = "";
	private String creador = "";

	public ConsultaNutricion() {
	}

	// ,
	// Anamnesis alimenticia
	// String desayuno, String mediam, String almuerzo, String mediat, String cena,
	// String notas,
	// LocalDate fechaUltAct, LocalDate fechaCreacion

	public ConsultaNutricion(
			// Consulta
			int id, Paciente paciente, LocalDate fecha, LocalDate proxCita, int enmienda, Usuario usuario,
			// Consulta Nutricional
			BigDecimal peso, BigDecimal diam, String tratamiento) {
		// Consulta
		super(id, enmienda, paciente, fecha, proxCita, usuario);
		// Consulta Nutricional
		this.consultaPeso = peso;
		this.consultaDiam = diam;
		this.tratamiento = tratamiento;
		this.creador = usuario.getNombreUsuario();
	}

	public BigDecimal getConsultaPeso() {
		return consultaPeso;
	}

	public void setConsultaPeso(BigDecimal consultaPeso) {
		this.consultaPeso = consultaPeso;
	}

	public BigDecimal getConsultaDiam() {
		return consultaDiam;
	}

	public void setConsultaDiam(BigDecimal consultaDiam) {
		this.consultaDiam = consultaDiam;
	}

	public String getTratamiento() {
		return tratamiento;
	}

	public void setTratamiento(String tratamiento) {
		this.tratamiento = tratamiento;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	@Override
	public String toString() {
		return "ConsultaNutric [consultaPeso=" + consultaPeso + ", consultaDiam=" + consultaDiam + ", tratamiento="
				+ tratamiento + ", creador=" + creador + "]";
	}

	@Override
	public ConsultaNutricion copiar() {
		ConsultaNutricion consulta = new ConsultaNutricion();
		consulta.setId(this.getId());
		consulta.setEnmienda(this.getEnmienda());
		consulta.setFecha(this.getFecha());
		consulta.setProxCita(this.getProxCita());
		consulta.setUsuario(this.getUsuario());
		consulta.setPaciente(this.getPaciente());

		consulta.setConsultaPeso(this.getConsultaPeso());
		consulta.setConsultaDiam(this.getConsultaDiam());
		consulta.setTratamiento(this.getTratamiento());
		consulta.setCreador(this.getCreador());
		return consulta;
	}

}
