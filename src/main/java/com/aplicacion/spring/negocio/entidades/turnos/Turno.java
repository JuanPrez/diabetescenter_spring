package com.aplicacion.spring.negocio.entidades.turnos;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;

import javafx.beans.property.SimpleObjectProperty;

/**
 * 
 * @Autor Juan Ignacio Prez
 * @Version 1.0
 * @Fecha 09/07/18
 * 
 * @Uso Clase Turno, Contiene id, fecha, horario, estado, profesional, paciente,
 *      consulta, observaciones
 * 
 */
@Component
public class Turno {
	
	private LocalDate fecha;
	private Horario horario;
	private SimpleObjectProperty<Estado> estado = new SimpleObjectProperty<Estado>();
	private Usuario profesional;
	private Paciente paciente;
	private String observaciones;

	public Turno() {

	}

	public Turno(LocalDate localDate, Horario horario2, Estado libre, Usuario usuario, Paciente paciente2,
			Object object) {
	}

	public Turno(LocalDate fecha, Horario horario, Estado estado, Usuario profesional, Paciente paciente,
			String observaciones) {
		this.fecha = fecha;
		this.horario = horario;
		this.estado.set(estado);
		this.profesional = profesional;
		this.paciente = paciente;
		this.observaciones = observaciones;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public Horario getHorario() {
		return horario;
	}

	public void setHorario(Horario horario) {
		this.horario = horario;
	}

	public final SimpleObjectProperty<Estado> estadoProperty() {
		return this.estado;
	}

	public final Estado getEstado() {
		return this.estadoProperty().get();
	}

	public final void setEstado(final Estado estado) {
		this.estadoProperty().set(estado);
	}

	public Usuario getProfesional() {
		return profesional;
	}

	public void setProfesional(Usuario profesional) {
		this.profesional = profesional;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	/**
	 * 
	 * Libera el turno
	 * 
	 * @return, turno liberado
	 */
	public Turno liberar() {
		this.setEstado(Estado.LIBRE);
		this.setProfesional(new Usuario());
		this.setPaciente(new Paciente());
		this.setObservaciones(null);
		return this;
	}

	/**
	 * 
	 * Turno a reservar
	 * 
	 * @return turno reservado
	 */
	public Turno reservar() {
		this.setEstado(Estado.RESERVADO);
		return this;
	}

	/**
	 * 
	 * Turno a consumir
	 * 
	 * @return turno consumido
	 */
	public Turno consumir() {
		this.setEstado(Estado.CONSUMIDO);
		return this;
	}

	/**
	 * 
	 * Turno a cerrar
	 * 
	 * @return turno cerrado
	 */
	public Turno cerrar() {
		this.setEstado(Estado.CERRADO);
		return this;
	}

	@Override
	public String toString() {
		return "Turno [fecha=" + fecha + ", horario=" + horario + ", estado=" + estado + ", profesional=" + profesional
				+ ", paciente=" + paciente + ", observaciones=" + observaciones + "]";
	}

}
