package com.aplicacion.spring.negocio.entidades.turnos;

public enum Estado {
	LIBRE(1), RESERVADO(2), CONSUMIDO(3), CERRADO(4);

	Estado(int i) {
		valor = i;
	}

	int valor;

	public int getValor() {
		return valor;
	}

	public void setValor(int i) {
		valor = i;
	}
}