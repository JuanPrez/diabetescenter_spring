package com.aplicacion.spring.negocio.entidades.registros.consultas;

public interface IConsulta<T extends Consulta> {
	public T copiar();
}
