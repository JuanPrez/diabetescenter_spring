package com.aplicacion.spring.negocio.entidades.turnos;

import java.util.List;

public class ListadoTurnos {
	private List<TurnoProfesional> turno;

	public ListadoTurnos(List<TurnoProfesional> turno) {
		this.turno = turno;
	}

	public List<TurnoProfesional> getTurno() {
		return turno;
	}

	public void setTurno(List<TurnoProfesional> turno) {
		this.turno = turno;
	}

}
