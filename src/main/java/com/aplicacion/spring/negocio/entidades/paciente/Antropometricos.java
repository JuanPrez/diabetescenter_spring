package com.aplicacion.spring.negocio.entidades.paciente;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.aplicacion.spring.gestion.entidades.Usuario;

@Component
public class Antropometricos {
	private BigDecimal numeroCalzado = new BigDecimal(0).setScale(1);
	private String formulaDigital = null;
	private String antecedentes = null;
	private Usuario usuario = new Usuario();
	private LocalDate fechaCreacion = null;
	private LocalDate fechaUltAct = null;

	public Antropometricos() {
		numeroCalzado = new BigDecimal(0).setScale(1, RoundingMode.HALF_EVEN);
		formulaDigital = "";
		antecedentes = "";
		fechaCreacion = LocalDate.now();
		fechaUltAct = LocalDate.now();
	}

	public Antropometricos(BigDecimal numeroCalzado, String formulaDigital, String antecedentes, String observaciones,
			Usuario usuario, LocalDate fechaCreacion, LocalDate fechaUltAct) {
		this.numeroCalzado = numeroCalzado;
		this.formulaDigital = formulaDigital;
		this.antecedentes = antecedentes;
		this.usuario = usuario;
		this.fechaCreacion = fechaCreacion;
		this.fechaUltAct = fechaUltAct;
	}

	public BigDecimal getNumeroCalzado() {
		return numeroCalzado;
	}

	public void setNumeroCalzado(BigDecimal numeroCalzado) {
		this.numeroCalzado = numeroCalzado;
	}

	public String getFormulaDigital() {
		return formulaDigital;
	}

	public void setFormulaDigital(String formulaDigital) {
		this.formulaDigital = formulaDigital;
	}

	public String getAntecedentes() {
		return antecedentes;
	}

	public void setAntecedentes(String antecedentes) {
		this.antecedentes = antecedentes;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public LocalDate getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDate fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public LocalDate getFechaUltAct() {
		return fechaUltAct;
	}

	public void setFechaUltAct(LocalDate fechaUltAct) {
		this.fechaUltAct = fechaUltAct;
	}

	@Override
	public String toString() {
		return "Antropometricos [numeroCalzado=" + numeroCalzado + ", formulaDigital=" + formulaDigital
				+ ", antecedentes=" + antecedentes + ", usuario=" + usuario + ", fechaCreacion=" + fechaCreacion
				+ ", fechaUltAct=" + fechaUltAct + "]";
	}

}
