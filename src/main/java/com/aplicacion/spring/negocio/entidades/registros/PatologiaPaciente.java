package com.aplicacion.spring.negocio.entidades.registros;

import com.aplicacion.spring.negocio.entidades.registros.patologias.Patologia;

public class PatologiaPaciente extends Patologia {
	private String origen = "";

	public PatologiaPaciente() {}

	public PatologiaPaciente(int id, String nombre, String abrev, String origen) {
		super(id, nombre, abrev);
		this.setOrigen(origen);
	}

	public PatologiaPaciente(String origen) {
		this.setOrigen(origen);
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	@Override
	public String toString() {
		return "PatologiaHC [nombre=" + super.nombre + ", origen=" + origen + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((origen == null) ? 0 : origen.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof PatologiaPaciente))
			return false;
		PatologiaPaciente other = (PatologiaPaciente) obj;
		if (origen == null) {
			if (other.origen != null)
				return false;
		} else if (!origen.equals(other.origen))
			return false;
		return true;
	}


}
