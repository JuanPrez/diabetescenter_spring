package com.aplicacion.spring.negocio.entidades.turnos;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class AdapterEstado extends XmlAdapter<Integer, Estado> {

	@Override
	public Integer marshal(Estado estado) {
		return estado.getValor();
	}

	@Override
	public Estado unmarshal(Integer valor) throws Exception {
		return null;
	}

}
