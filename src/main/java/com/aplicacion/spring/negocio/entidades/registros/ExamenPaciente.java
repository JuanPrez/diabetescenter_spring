package com.aplicacion.spring.negocio.entidades.registros;

import java.time.LocalDate;

import com.aplicacion.spring.negocio.entidades.registros.patologias.Examen;

/**
 * 
 * POJO representando al objeto Resultado de Examen para el paciente
 * 
 * @Atrib id
 * @Atrib nombre
 * 
 * @author Juan Ignacio Prez
 *
 */
public class ExamenPaciente implements Comparable<ExamenPaciente> {
	private Integer id;
	private Examen examen;
	private LocalDate fechaExamen;
	private String resultado;
	private String creador;
	private Integer idCreador;

	public ExamenPaciente() {

	}

	public ExamenPaciente(Integer id, Examen examen, LocalDate fechaExamen, String resultado, String creador,
			Integer idCreador) {
		this.id = id;
		this.examen = examen;
		this.fechaExamen = fechaExamen;
		this.resultado = resultado;
		this.creador = creador;
		this.idCreador = idCreador;
	}

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Examen getExamen() {
		return examen;
	}

	public void setExamen(Examen examen) {
		this.examen = examen;
	}

	public LocalDate getFechaExamen() {
		return fechaExamen;
	}

	public void setFechaExamen(LocalDate fechaExamen) {
		this.fechaExamen = fechaExamen;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	public int getIdCreador() {
		return idCreador;
	}

	public void setIdCreador(int idCreador) {
		this.idCreador = idCreador;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ExamenPaciente))
			return false;
		ExamenPaciente other = (ExamenPaciente) obj;
		return (id != other.id);
	}

	@Override
	public int compareTo(ExamenPaciente o) {
		return this.getId().compareTo(o.getId());
	}

}
