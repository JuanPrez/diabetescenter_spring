package com.aplicacion.spring.negocio.entidades.registros;

import java.util.ArrayList;
import java.util.List;

import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaMedicina;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaNutricion;
import com.aplicacion.spring.negocio.entidades.registros.consultas.ConsultaPodologica;
import com.aplicacion.spring.negocio.entidades.registros.disciplinar.Encuesta;
import com.aplicacion.spring.negocio.entidades.registros.disciplinar.Metas;
import com.aplicacion.spring.negocio.entidades.registros.disciplinar.Objetivos;

public class HistoriaClinica {
	private Galeria galeria;
	private List<Metas> listaMetas;
	private List<Objetivos> listaObjetivos;
	private List<Encuesta> listaEncuestas;
	private List<PatologiaPaciente> listaPatologiasPaciente;
	private List<ExamenPaciente> listaExamenesPaciente;
	private List<ConsultaMedicina> listaConsultasMedicas;
	private List<ConsultaNutricion> listaConsultasNutric;
	private List<ConsultaPodologica> listaConsultasPodo;

	public HistoriaClinica() {
		galeria = new Galeria();
		listaMetas = new ArrayList<>();
		listaObjetivos = new ArrayList<>();
		listaEncuestas = new ArrayList<>();
		listaPatologiasPaciente = new ArrayList<>();
		listaExamenesPaciente = new ArrayList<>();
		listaConsultasMedicas = new ArrayList<>();
		listaConsultasNutric = new ArrayList<>();
		listaConsultasPodo = new ArrayList<>();
	}

	public Galeria getGaleria() {
		return galeria;
	}

	public void setGaleria(Galeria galeria) {
		this.galeria = galeria;
	}

	public List<Metas> getListaMetas() {
		return listaMetas;
	}

	public void setListaMetas(List<Metas> listaMetas) {
		this.listaMetas = listaMetas;
	}

	public List<Objetivos> getListaObjetivos() {
		return listaObjetivos;
	}

	public void setListaObjetivos(List<Objetivos> listaObjetivos) {
		this.listaObjetivos = listaObjetivos;
	}

	public List<Encuesta> getListaEncuestas() {
		return listaEncuestas;
	}

	public void setListaEncuestas(List<Encuesta> listaEncuestas) {
		this.listaEncuestas = listaEncuestas;
	}

	public List<PatologiaPaciente> getListaPatologiasPaciente() {
		return listaPatologiasPaciente;
	}

	public void setListaPatologiasPaciente(List<PatologiaPaciente> listaPatologiasPaciente) {
		this.listaPatologiasPaciente = listaPatologiasPaciente;
	}

	public List<ExamenPaciente> getListaExamenesPaciente() {
		return listaExamenesPaciente;
	}

	public void setListaExamenesPaciente(List<ExamenPaciente> listaExamenesPaciente) {
		this.listaExamenesPaciente = listaExamenesPaciente;
	}

	public List<ConsultaMedicina> getListaConsultasMedicas() {
		return listaConsultasMedicas;
	}

	public void setListaConsultasMedicas(List<ConsultaMedicina> listaConsultasMedicas) {
		this.listaConsultasMedicas = listaConsultasMedicas;
	}

	public List<ConsultaNutricion> getListaConsultasNutric() {
		return listaConsultasNutric;
	}

	public void setListaConsultasNutric(List<ConsultaNutricion> listaConsultasNutric) {
		this.listaConsultasNutric = listaConsultasNutric;
	}

	public List<ConsultaPodologica> getListaConsultasPodo() {
		return listaConsultasPodo;
	}

	public void setListaConsultasPodo(List<ConsultaPodologica> listaConsultasPodo) {
		this.listaConsultasPodo = listaConsultasPodo;
	}
	@Override
	public String toString() {
		return "HistoriaClinica [galeria=" + galeria + ", listaMetas=" + listaMetas + ", listaObjetivos="
				+ listaObjetivos + ", listaEncuestas=" + listaEncuestas + ", listaPatologiasPaciente="
				+ listaPatologiasPaciente + ", listaExamenesPaciente=" + listaExamenesPaciente
				+ ", listaConsultasMedicas=" + listaConsultasMedicas + ", listaConsultasNutric=" + listaConsultasNutric
				+ ", listaConsultasPodo=" + listaConsultasPodo + "]";
	}

}
