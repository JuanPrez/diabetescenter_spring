package com.aplicacion.spring;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Ventanas {

	// Constantes de los botones
	public static final String REINICIAR = "Reiniciar";
	public static final String SOLO_LECTURA = "Solo lectura";

	// Botones de la alerta
	static final ButtonType reiniciar = new ButtonType(REINICIAR);
	static final ButtonType soloLectura = new ButtonType(SOLO_LECTURA);

	/**
	 * Alerta de falta de conexion
	 * 
	 * @param mensaje
	 */
	public static String alertaConexion(String mensaje) {
		// Si hay pantalla de inicio de sesion, muesto el mensaje en pantalla
		Alert restart = Ventanas.crearAlerta(AlertType.CONFIRMATION, mensaje, mensaje,
				"Reinicie la aplicacion o cambie el modo a solo lectura", Arrays.asList(reiniciar, soloLectura));
		Optional<ButtonType> valor = restart.showAndWait();

		// Recibimos respuesta
		if (valor.isPresent() && valor.get() instanceof ButtonType)
			// A este punto evaluamos que los botones seleccionados, sean los correctos
			if (valor.get().getButtonData() == ButtonData.OTHER) {
				// Y evaluamos la accion a realizar
				return valor.get().getText();
			} else {
				System.exit(0);
			}
		return null;
	}

	/**
	 * Construye una ventana de alerta
	 * 
	 * @param tipo
	 * @param titulo
	 * @param encabezado
	 * @param contenido
	 * @param botones
	 * @return
	 */
	public static Alert crearAlerta(Alert.AlertType tipo, String titulo, String encabezado, String contenido,
			List<ButtonType> botones) {
		Alert alerta = new Alert(tipo);
		if (botones == null || botones.isEmpty()) {
			// Usa los botones por defecto para el tipo de ventana
		} else {
			alerta.getButtonTypes().clear();
			alerta.getButtonTypes().addAll(botones);
		}
		alerta.setTitle(titulo);
		alerta.setHeaderText(encabezado);
		alerta.setContentText(contenido);
		// Cambia el icono del dialogo
		Stage escenarioDialog = (Stage) alerta.getDialogPane().getScene().getWindow();
		escenarioDialog.getIcons().add(new Image(Aplicacion.getIcono()));
		return alerta;
	}

	public static Dialog<ButtonType> crearDialogo(StageStyle estilo, String titulo, Node contenido,
			ButtonType... botones) {
		Dialog<ButtonType> dialogo = new Dialog<>();

		// Estilo de dialogo
		if (estilo != null)
			dialogo.initStyle(estilo);
		else
			dialogo.initStyle(StageStyle.UTILITY);

		if (titulo != null) {
			dialogo.setTitle(titulo);
		}
		
		// Acciones de cierre facil
		dialogo.setOnCloseRequest(eventoCierre -> dialogo.close());
		dialogo.getDialogPane().setOnKeyPressed(eventoEscape -> {
			if (eventoEscape.getCode() == KeyCode.ESCAPE)
				dialogo.close();
		});

		// Arma el contenedor
		dialogo.getDialogPane().setContent(contenido);

		if (botones != null) {
			dialogo.getDialogPane().getButtonTypes().addAll(botones);
		}

		// Cambia el icono del dialogo
		Stage escenarioDialog = (Stage) dialogo.getDialogPane().getScene().getWindow();
		escenarioDialog.getIcons().add(new Image(Aplicacion.getIcono()));

		// Permite el cambio de tamaño
		dialogo.setResizable(true);
		return dialogo;
	}
}
