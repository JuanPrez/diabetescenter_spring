package com.aplicacion.spring;

import org.springframework.stereotype.Component;

import de.felixroske.jfxsupport.SplashScreen;
import javafx.scene.Parent;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

@Component
public class DCSplashScreen extends SplashScreen {
	private final String DEFAULT_IMAGE = "/images/diabetes_center_lateral.png";

	public DCSplashScreen() {
		super();
	}

	@Override
	public String getImagePath() {
		return DEFAULT_IMAGE;
	}

	@Override
	public Parent getParent() {
		final ImageView imageView = new ImageView(getClass().getResource(getImagePath()).toExternalForm());
		final ProgressBar splashProgressBar = new ProgressBar(ProgressBar.INDETERMINATE_PROGRESS);
		splashProgressBar.setPrefWidth(imageView.getImage().getWidth());

		final VBox vbox = new VBox();
		vbox.getChildren().addAll(imageView, splashProgressBar);

		return vbox;
	}

}
