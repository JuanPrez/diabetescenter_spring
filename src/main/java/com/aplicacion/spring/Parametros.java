/* ************************************************************************************* *
 *                                                                                       *
 *  Licensed to the Apache Software Foundation (ASF) under one                           *
 *  or more contributor license agreements.  See the NOTICE file                         *
 *  distributed with this work for additional information                                *
 *  regarding copyright ownership.  The ASF licenses this file                           *
 *  to you under the Apache License, Version 2.0 (the                                    *
 *  "License"); you may not use this file except in compliance                           *
 *  with the License.  You may obtain a copy of the License at                           *
 *                                                                                       *
 *    http://www.apache.org/licenses/LICENSE-2.0                                         *
 *                                                                                       *
 *  Unless required by applicable law or agreed to in writing,                           *
 *  software distributed under the License is distributed on an                          *
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY                               *
 *  KIND, either express or implied.  See the License for the                            *
 *  specific language governing permissions and limitations                              *
 *  under the License.                                                                   *
 *                                                                                       *
 * ************************************************************************************* */
package com.aplicacion.spring;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Clase para manejo de los parametros de ingreso
 * 
 * @author Juan Ignacio Prez
 *
 */
@Service
@Scope(value = "singleton")
public class Parametros {

	// Lista de Enum de ambientes
	public enum Ambiente {
		PRODUCCION(), PRUEBA(), DESARROLLO();
	}

	static List<String> parametros;
	private static final String DESARROLLO = "-desarrollo";
	private static final String SCENICVIEWER = "-depuracion";
	private static final String PRUEBA = "-prueba";

	public String AMBIENTE_PREFIX = "";

	private Ambiente ambiente;

	/**
	 * Manejo de los parametros pasados
	 * 
	 * @param parametros
	 * @return lista concatenada de parametros
	 */
	public String manejarParametros(final List<String> parametros) {

		Parametros.parametros = parametros;
		boolean desarrollo = false;
		// Manejo de parametros
		if (!parametros.isEmpty()) {
			StringBuilder param = new StringBuilder();
			for (String parametro : parametros) {
				// Version de desarrollo
				if (parametro.equals(DESARROLLO)) {
					// Cambiamos el ambiente
					ambiente = Ambiente.DESARROLLO;
					AMBIENTE_PREFIX = "_dev";
					desarrollo = true;
					param.append(parametro + " ");
				}
				if (desarrollo) {
					if (parametro.equals(SCENICVIEWER)) {
						// Activamos el Scenic Viewer
						Aplicacion.activarScenicViewer = true;
					}
				}
				// PreProduccion o test
				if (parametro.equals(PRUEBA)) {
					// Cambiamos el ambiente
					ambiente = Ambiente.PRUEBA;
					AMBIENTE_PREFIX = "_pruebas";
					param.append(parametro + " ");
				}
				// Modo monousuario (todos los privilegios)
				if (parametro.equals("-monousuario")) {
					// Incluye el parametro localDB por defecto
					// Lee el archivo de sesion para tomar los parametros del usuario
					// TODO, tomamos los datos de usuario del archivo de persistencia
					param.append(parametro + " ");
				}
			}
			return "Parametros pasados: " + param;
		} else {
			return "Sin parametros";
		}
	}

	public Ambiente getAmbiente() {
		return ambiente;
	}
}
