/* ************************************************************************************* *
 *                                                                                       *
 *  Licensed to the Apache Software Foundation (ASF) under one                           *
 *  or more contributor license agreements.  See the NOTICE file                         *
 *  distributed with this work for additional information                                *
 *  regarding copyright ownership.  The ASF licenses this file                           *
 *  to you under the Apache License, Version 2.0 (the                                    *
 *  "License"); you may not use this file except in compliance                           *
 *  with the License.  You may obtain a copy of the License at                           *
 *                                                                                       *
 *    http://www.apache.org/licenses/LICENSE-2.0                                         *
 *                                                                                       *
 *  Unless required by applicable law or agreed to in writing,                           *
 *  software distributed under the License is distributed on an                          *
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY                               *
 *  KIND, either express or implied.  See the License for the                            *
 *  specific language governing permissions and limitations                              *
 *  under the License.                                                                   *
 *                                                                                       *
 * ************************************************************************************* */
package com.aplicacion.spring;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Optional;
import java.util.Timer;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.aplicacion.spring.gestion.controladores.ControladorDiabetesCenter;
import com.aplicacion.spring.negocio.dao.RepositorioEstatico;
import com.aplicacion.spring.utilitarios.C3Pool;
import com.aplicacion.spring.utilitarios.Conexion;
import com.aplicacion.spring.utilitarios.Conexion.Disponible;
import com.aplicacion.spring.utilitarios.Conexion.Estado;
import com.aplicacion.spring.utilitarios.Monitor;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.Event;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

@Component
@Scope(value = "singleton")
public class Aplicacion {

	// Constantes
	public static final String VERSION = "0.96e";

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(Aplicacion.class);
	public Monitor monitor;
	public static boolean activarScenicViewer = false;

	@Autowired
	C3Pool c3Pool;
	@Autowired
	Parametros parametros;
	@Autowired
	Propiedades propiedades;
	@Autowired
	RepositorioEstatico repositorios;
	@Autowired
	Sesion sesion;
	@Autowired
	ApplicationContext contexto;

	// ----------------------------------------------------------------
	// Centralizacion de las configuraciones
	// ----------------------------------------------------------------
	/**
	 * Inicializa la base de datos y crea el pool de conexiones
	 * 
	 * @param tamPool
	 */
	public void iniciarBaseDeDatos(int tamPool) {
		c3Pool.inicializar(propiedades);
		c3Pool.crearBancoConexiones(tamPool);
	}

	public Propiedades getPropiedades() {
		return propiedades;
	}

	public Parametros getParametros() {
		return parametros;
	}

	public RepositorioEstatico getRepositorios() {
		return repositorios;
	}

	public Sesion getSesion() {
		return sesion;
	}

	public ApplicationContext getContexto() {
		return contexto;
	}

	// ----------------------------------------------------------------
	// Estados de respaldo con solo lectura
	// ----------------------------------------------------------------
	private boolean soloLectura;

	public boolean isSoloLectura() {
		return soloLectura;
	}

	public void setRespaldo(boolean respaldo) {
		setSoloLectura(respaldo);
		c3Pool.setRespaldo(respaldo);
	}

	private void setSoloLectura(boolean soloLectura) {
		this.soloLectura = soloLectura;
	}

	// ----------------------------------------------------------------
	// Conectividad
	// ----------------------------------------------------------------
	private SimpleBooleanProperty online = new SimpleBooleanProperty(false);

	public final SimpleBooleanProperty onlineProperty() {
		return this.online;
	}

	public final void setOnline(final boolean online) {
		this.onlineProperty().set(online);
	}

	/**
	 * Comprueba la conexion al servidor
	 */
	public boolean validarServidor() {
		// Crea el conector
		try (Socket conn = new Socket()) {
			// Intenta conectar, si tiene exito, cierra la conexion y retorna exito
			conn.connect(new InetSocketAddress(InetAddress.getByName(c3Pool.getUrl()), c3Pool.getPuerto()),
					2500); /* 2,5 segundos */
		} catch (Exception exc) {
			// Si falla la conexion, lo anuncia
			bitacora.error("No hay conexion al servidor " + exc.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * Inicializa el monitor, si no existe, y verifica si hay conexion al servicio
	 * 
	 * @return
	 */
	public final boolean verificarServicio() {
		if (monitor == null) {
			abrirMonitor(new Timer());
			bitacora.info("Inicializado el monitor");
		}
		monitor.verificarServicio();
		this.setOnline(monitor.getCalidadConexion() == Estado.BUENA_CONEXION
				|| monitor.getCalidadConexion() == Estado.MALA_CONEXION);
		return (monitor.getServicio().equals(Disponible.ACTIVO) && this.onlineProperty().get());
	}

	// ----------------------------------------------------------------
	// Monitores
	// ----------------------------------------------------------------
	/**
	 * Inicializa el monitor
	 * 
	 * @param monitorProgramado
	 */
	private void abrirMonitor(Timer monitorProgramado) {
		monitor = new Monitor(new Conexion(
				getPropiedades().get(C3Pool.DB_PREFIX + getParametros().AMBIENTE_PREFIX + C3Pool.URL), Integer.parseInt(
						getPropiedades().get(C3Pool.DB_PREFIX + getParametros().AMBIENTE_PREFIX + C3Pool.PUERTO))));
		try {
			monitorProgramado.schedule(monitor, 0, 8000);
			Thread.sleep(600);
		} catch (InterruptedException exc) {
			bitacora.warn("Error: no se pudo esperar a la comprobacion");
		}
	}

	public Monitor getMonitor() {
		return monitor;
	}

	/**
	 * Cierra el monitor de conexion
	 */
	public void cerrarMonitor() {
		if (this.getMonitor() != null)
			if (this.monitor.cancel()) 
				bitacora.info("Monitor inactivo");
			else 
				bitacora.info("Monitor activo");
	}

	/**
	 * Cierra el banco de conexiones de la base de datos (C3P0)
	 */
	private void cerrarConexionBaseDatos() {
		c3Pool.cerrarBancoConexiones();
	}

	// ----------------------------------------------------------------
	// Programa
	// ----------------------------------------------------------------
	/**
	 * Cierra el programa de forma ordenada <br>
	 * 1. Muestra confirmacion <br>
	 * 2. Cierra el monitor <br>
	 * 3. Cierra la aplicacion con gracia <br>
	 * 
	 * @param evento,
	 *            origen de la llamada
	 * @param escenario,
	 *            escenario raiz a cerrar
	 */
	public void cerrarPrograma(Event evento, Stage escenario) {
		Alert alerta = Ventanas.crearAlerta(AlertType.CONFIRMATION, "Confirmación de cierre", "Confirmación de cierre",
				"¿Desea salir de la aplicación?", null);
		Optional<ButtonType> respuesta = alerta.showAndWait();
		if (respuesta.isPresent() && respuesta.get().equals(ButtonType.OK)) {
			// Cerrar pool de la base de datos
			bitacora.info("Cerrando con gracia el programa");
			this.cerrarMonitor();
			this.cerrarConexionBaseDatos();
			escenario.close();
		}
		evento.consume();
	}

	private static String icono = null;

	public static void setIcono(String icono) {
		Aplicacion.icono = icono;
	}

	public static String getIcono() {
		return icono;
	}

	// Paneles de carga
	private VBox panelCarga;
	private BorderPane panelFrontal;

	public BorderPane getPanelFrontal() {
		return panelFrontal;
	}

	public void setPanelFrontal(BorderPane panelFrontal) {
		this.panelFrontal = panelFrontal;
	}

	public VBox getPanelCarga() {
		return panelCarga;
	}

	public void setPanelCarga(VBox panelCarga) {
		this.panelCarga = panelCarga;
	}

	ControladorDiabetesCenter pantallaPrincipal;

	public ControladorDiabetesCenter getPantallaPrincipal() {
		return pantallaPrincipal;
	}

	public void setPantallaPrincipal(ControladorDiabetesCenter pantallaPrincipal) {
		this.pantallaPrincipal = pantallaPrincipal;
	}

}
