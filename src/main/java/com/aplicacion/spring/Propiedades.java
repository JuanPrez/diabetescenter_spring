/* ************************************************************************************* *
 *                                                                                       *
 *  Licensed to the Apache Software Foundation (ASF) under one                           *
 *  or more contributor license agreements.  See the NOTICE file                         *
 *  distributed with this work for additional information                                *
 *  regarding copyright ownership.  The ASF licenses this file                           *
 *  to you under the Apache License, Version 2.0 (the                                    *
 *  "License"); you may not use this file except in compliance                           *
 *  with the License.  You may obtain a copy of the License at                           *
 *                                                                                       *
 *    http://www.apache.org/licenses/LICENSE-2.0                                         *
 *                                                                                       *
 *  Unless required by applicable law or agreed to in writing,                           *
 *  software distributed under the License is distributed on an                          *
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY                               *
 *  KIND, either express or implied.  See the License for the                            *
 *  specific language governing permissions and limitations                              *
 *  under the License.                                                                   *
 *                                                                                       *
 * ************************************************************************************* */
package com.aplicacion.spring;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.properties.EncryptableProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.aplicacion.spring.Parametros.Ambiente;

/**
 * 
 * @Autor Juan Ignacio Prez
 * @Version 1.0
 * @Fecha 13/07/17
 * 
 * @Uso Lector de configuraciones del sistema dentro del archivo de propiedades
 * 
 * @input config.properties
 * @tag Conector
 * @tag tipoDB
 * @tag url
 * @tag puerto
 * @tag schema
 * @tag usuario
 * @tag contrasena
 */
@Service
@Scope(value = "singleton")
public class Propiedades {

	final Logger bitacora = Logger.getLogger(Propiedades.class);

	private final String ARCHIVO_PROPIEDADES = "config.properties";
	private final String PASS = "diabetescenter";
	private final Properties PROPIEDADES = this.leerPropiedades(ARCHIVO_PROPIEDADES, PASS);
	private Ambiente ambiente;

	public void setAmbiente(Ambiente ambiente) {
		this.ambiente = ambiente;
	}

	public Ambiente getAmbiente() {
		return ambiente;
	}

	/**
	 * Lector de las propiedades del sistema
	 * 
	 * @param archivoConfig
	 * @param pass
	 * @return
	 */
	public Properties leerPropiedades(String archivoConfig, String pass) {
		// Desencriptado con contraseña
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setPassword(pass);

		// Crea el objeto para propiedades encriptadas
		Properties props = new EncryptableProperties(encryptor);
		try {
			props.load(Propiedades.class.getClassLoader().getResourceAsStream(archivoConfig));
		} catch (NullPointerException | EncryptionOperationNotPossibleException | IOException exc) {
			return null;
		}
		return props;
	}

	public String get(String s) {
		return PROPIEDADES.getProperty(s);
	}

	public boolean probar() {
		return (PROPIEDADES != null);
	}

	/**
	 * Imprime los datos de ejecucion.
	 */
	public String imprimir() {
		StringBuilder datos = new StringBuilder("\n");

		String separador = " ******************************************************************* \n";

		// Imprime el inicio de una nueva sesion
		if (ambiente == Ambiente.PRODUCCION) {
			  datos.append(" **************************** PRODUCCION *************************** \n");
		} else if (ambiente == Ambiente.DESARROLLO) {
			  datos.append(" *************************** DESARRROLLO *************************** \n");
		} else if (ambiente == Ambiente.PRUEBA) {
			  datos.append(" ***************************** PRUEBAS ***************************** \n");
		}

		datos.append(String.format(" - Version actual del programa: %s \n", Aplicacion.VERSION));
		datos.append(String.format(" - Java: %s \n", System.getProperty("java.vm.specification.name") + " "
				+ System.getProperty("java.vm.specification.version")));
		datos.append(String.format(" - Sistema: %s \n",
				System.getProperty("os.name") + " " + System.getProperty("os.version")));
		datos.append(String.format(" - Usuario: %s \n", System.getProperty("user.name")));

		// Nomobre del archivo de bitacora
		FileAppender fileAppender = null;
		Enumeration<?> appenders = Logger.getRootLogger().getAllAppenders();
		while (appenders.hasMoreElements()) {
			Appender currAppender = (Appender) appenders.nextElement();
			if (currAppender instanceof FileAppender) {
				fileAppender = (FileAppender) currAppender;
			}
		}
		if (fileAppender != null) {
			datos.append(String.format(" - Archivo de bitacora: %s \n", fileAppender.getFile()));
			datos.append(String.format(" - Nivel de bitacora: %s \n", Logger.getRootLogger().getLevel()));
		}
		datos.append(separador);
		return datos.toString();
	}

}
