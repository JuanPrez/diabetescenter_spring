package com.aplicacion.spring;

import org.springframework.stereotype.Component;

import com.aplicacion.spring.gestion.entidades.Usuario;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.entidades.turnos.Turno;

import javafx.beans.property.SimpleObjectProperty;

@Component
public class Sesion {

	SimpleObjectProperty<Usuario> usuario = new SimpleObjectProperty<>(null);
	SimpleObjectProperty<Paciente> paciente = new SimpleObjectProperty<>(null);
	SimpleObjectProperty<Turno> turno = new SimpleObjectProperty<>(null);

	public final SimpleObjectProperty<Usuario> usuarioProperty() {
		return this.usuario;
	}

	public final Usuario getUsuario() {
		return this.usuarioProperty().get();
	}

	public final void setUsuario(final Usuario usuario) {
		this.usuarioProperty().set(usuario);
	}

	public final SimpleObjectProperty<Paciente> pacienteProperty() {
		return this.paciente;
	}

	public final Paciente getPaciente() {
		return this.pacienteProperty().get();
	}

	public final void setPaciente(final Paciente paciente) {
		if (paciente == null) {
			this.pacienteProperty().set(paciente);
		} else {
			this.pacienteProperty().set(null);
			this.pacienteProperty().set(paciente);
		}
	}

	public final SimpleObjectProperty<Turno> turnoProperty() {
		return this.turno;
	}

	public final Turno getTurno() {
		return this.turnoProperty().get();
	}

	public final void setTurno(final Turno turno) {
		if (paciente == null) {
			this.turnoProperty().set(turno);
		} else {
			this.turnoProperty().set(null);
			this.turnoProperty().set(turno);
		}
	}

}
