package com.aplicacion.spring;

import java.util.Arrays;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.aplicacion.spring.Parametros.Ambiente;
import com.aplicacion.spring.gestion.vistas.VistaInicioSesion;
import com.aplicacion.spring.utilitarios.C3Pool;

import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

@SpringBootApplication
@ComponentScan(basePackages = { "com.aplicacion.spring" })
public class Main extends AbstractJavaFxApplicationSupport {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(Main.class);

	Aplicacion aplicacion;

	/**
	 * Inicio del programa
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		launch(Main.class, VistaInicioSesion.class, new DCSplashScreen(), args);
	}

	/**
	 * Ejecucion de los procesos de configuracion y pre inicializacion de las vistas
	 */
	@Override
	public void beforeInitialView(Stage stage, ConfigurableApplicationContext ctx) {
		super.beforeInitialView(stage, ctx);

		// Union manual del bean de aplicacion
		aplicacion = ctx.getBean(Aplicacion.class);

		// Manejo de parametros
		bitacora.info(aplicacion.getParametros().manejarParametros(getParameters().getRaw()));

		// Configuramos el ambiente a usar
		if (aplicacion.getParametros().getAmbiente() == null)
			aplicacion.getPropiedades().setAmbiente(Ambiente.PRODUCCION);
		else
			aplicacion.getPropiedades().setAmbiente(aplicacion.getParametros().getAmbiente());

		// Lectura de propiedades
		if (aplicacion.getPropiedades().probar()) {
			bitacora.info("Archivo de propiedades leido exitosamente");
			bitacora.info(aplicacion.getPropiedades().imprimir());
		} else {
			Alert sinPropiedades = Ventanas.crearAlerta(AlertType.ERROR, "Error critico",
					"Imposible leer el archivo de propiedades",
					"No se puede encontrar o abrir el archivo de propiedades", Arrays.asList(ButtonType.OK));
			Optional<ButtonType> respuesta = sinPropiedades.showAndWait();

			if (respuesta.isPresent())
				System.exit(0);
		}

		Aplicacion.setIcono(aplicacion.getPropiedades().get("imagen.icono"));
		// Validacion pre-ejecucion
		validarInicio();
	}

	/**
	 * Valida el inicio del sistema y la conexion
	 */
	private void validarInicio() {
		// Verificacion de conectividad (Base de datos)
		// if (aplicacion.isOnline() && aplicacion.verificarServicio()) {
		if (aplicacion.verificarServicio()) {
			bitacora.info(String.format(Mensajero.SERVVERIFICADO,
					aplicacion.getPropiedades()
							.get(C3Pool.DB_PREFIX + aplicacion.getParametros().AMBIENTE_PREFIX + C3Pool.URL),
					Integer.parseInt(aplicacion.getPropiedades()
							.get(C3Pool.DB_PREFIX + aplicacion.getParametros().AMBIENTE_PREFIX + C3Pool.PUERTO))));

			// Creacion del pool de conexiones
			aplicacion.iniciarBaseDeDatos(3);

			// Inicializacion de los repositorios
			aplicacion.getRepositorios().iniciarRepositorios();
		} else {
			// En caso de no haber conexion, evalua la eleccion
			switch (Ventanas.alertaConexion(Mensajero.APP_SIN_CONEXION)) {
			// Opcion en caso de seleccion reiniciar (revalidacion)
			case Ventanas.REINICIAR: {
				// Reevalua
				validarInicio();
				break;
			}
			// Opcion en caso de seleccion de solo lectura
			case Ventanas.SOLO_LECTURA: {
				// TODO, metodo provisorio
				aplicacion.setRespaldo(true);
				Alert noImplementado = Ventanas.crearAlerta(AlertType.WARNING, "Modo No Implementado",
						"Modo aun en desarrollo", "Lo sentimos, saliendo de la aplicacion", null);
				Optional<ButtonType> respuesta = noImplementado.showAndWait();

				if (respuesta.isPresent())
					System.exit(0);
				break;
			}
			//
			default:
				System.exit(0);
			}
		}
	}
}
