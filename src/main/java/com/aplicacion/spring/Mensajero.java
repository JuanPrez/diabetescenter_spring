package com.aplicacion.spring;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.aplicacion.spring.utilitarios.controladores.Notificacion;

import de.felixroske.jfxsupport.GUIState;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.stage.Window;

public class Mensajero {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(Mensajero.class);

	private static ResourceBundle cargarArchivo() {
		return ResourceBundle.getBundle("mensajes");
	}

	private static final ResourceBundle mensajes = Mensajero.cargarArchivo();

	public static final String BIENVENIDA = mensajes.getString("BIENVENIDA");

	public static final String SERVVERIFICADO = mensajes.getString("SERVVERIFICADO");

	public static final String DB_ERROR_GENERICO = mensajes.getString("DB_ERROR_GENERICO");
	public static final String DB_ERROR_RESULTSET = mensajes.getString("DB_ERROR_RESULTSET");
	public static final String DB_SINCONEXION = mensajes.getString("DB_SINCONEXION");
	public static final String DB_MALACONFIGURACION = mensajes.getString("DB_MALACONFIGURACION");
	public static final String DB_ERROR_CONEXION = mensajes.getString("DB_ERROR_CONEXION");
	public static final String QUERY_EJECUTADA = mensajes.getString("QUERY_EJECUTADA");

	public static final String USUARIO_VALIDADO = mensajes.getString("USUARIO_VALIDADO");
	public static final String USUARIO_INVALIDO = mensajes.getString("USUARIO_INVALIDO");
	public static final String USUARIO_FALLO_VERIFICACION = mensajes.getString("USUARIO_FALLO_VERIFICACION");

	public static final String APP_PACIENTE_IMAGEN_CARGA_FALLO = mensajes.getString("APP_PACIENTE_IMAGEN_CARGA_FALLO");
	public static final String APP_PACIENTE_IMAGEN_CARGA_EXITO = mensajes.getString("APP_PACIENTE_IMAGEN_CARGA_EXITO");
	public static final String APP_PACIENTE_NO_ENCONTRADO_DNI = mensajes.getString("APP_PACIENTE_NO_ENCONTRADO_DNI");
	public static final String APP_PACIENTE_NO_ENCONTRADO_ID = mensajes.getString("APP_PACIENTE_NO_ENCONTRADO_ID");
	public static final String APP_PACIENTE_CARGADO_EXITO = mensajes.getString("APP_PACIENTE_CARGADO_EXITO");
	public static final String APP_PACIENTE_CARGADO_FALLO = mensajes.getString("APP_PACIENTE_CARGADO_FALLO");
	public static final String APP_PACIENTE_STATUS_CARGADO_EXITO = mensajes
			.getString("APP_PACIENTE_STATUS_CARGADO_EXITO");
	public static final String APP_PACIENTE_STATUS_CARGADO_FALLO = mensajes
			.getString("APP_PACIENTE_STATUS_CARGADO_FALLO");
	public static final String APP_PACIENTE_SEGUIMIENTO_NO_RECUPERADO = mensajes
			.getString("APP_PACIENTE_SEGUIMIENTO_NO_RECUPERADO");
	public static final String APP_SIN_CONEXION = mensajes.getString("APP_SIN_CONEXION");
	public static final String APP_REPOSITORIOS_NO_RECUPERADOS = mensajes.getString("APP_REPOSITORIOS_NO_RECUPERADOS");

	// Tipos de mensajes
	public static final String MSG_INFO = "info";
	public static final String MSG_EXITO = "exito";
	public static final String MSG_ALERTA = "alerta";
	public static final String MSG_ERROR = "error";
	public static final String MSG_FALLO = "fallo";
	public static final String MSG_NULO = "nulo";

	public static final String EJEC_GENERICO = mensajes.getString("EJEC_GENERICO");

	private final String description;

	private Mensajero(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	/* ----------------------------------------------------------------------- */
	/* ------------------------ ELEMENTOS PERSISTENTES ----------------------- */
	/* ----------------------------------------------------------------------- */
	private static SimpleObjectProperty<Mensajero> tipoMsg = new SimpleObjectProperty<>();
	private static SimpleStringProperty msg = new SimpleStringProperty();

	public static final SimpleObjectProperty<Mensajero> tipoMsgProperty() {
		return tipoMsg;
	}

	public static void setTipoMsg(Mensajero tipoMsg) {
		tipoMsgProperty().set(tipoMsg);
	}

	public static final Mensajero getTipoMsg() {
		return tipoMsgProperty().get();
	}

	public static final SimpleStringProperty msgProperty() {
		return msg;
	}

	/**
	 * Setters
	 * 
	 * @param msg
	 */
	public static final void setMsg(final String msg) {
		notificar(msg);
	}

	public static final void setMsg(final String msg, final String tipoMsg) {
		notificar(msg, tipoMsg);
	}

	public static final void setMsg(final String msg, final Mensajero tipoMsg) {
		notificar(msg, tipoMsg.getDescription());
	}

	public static final void setMsg(final Mensajero msg, final Mensajero tipoMsg) {
		msgProperty().set(msg.getDescription());
		tipoMsgProperty().set(tipoMsg);
		notificar(msg, tipoMsg.getDescription());
	}

	public static final String getMsg() {
		return msgProperty().get();
	}

	/**
	 * Traductor de Mensajero
	 * 
	 * @param mensaje
	 * @param tipo
	 */
	public static void notificar(Mensajero mensaje, String tipo) {
		notificar(mensaje.getDescription(), tipo);
	}

	/**
	 * Simplificacion en el caso de avisos
	 * 
	 * @param mensaje
	 */
	public static void notificar(String mensaje) {
		notificar(mensaje, MSG_INFO);
	}

	/**
	 * Crea la ventana de mensaje
	 * 
	 * @param mensaje
	 * @param tipo
	 */
	public static void notificar(String mensaje, String tipo) {
		Window ventana = GUIState.getStage().getScene().getWindow();
		switch (tipo) {
		case MSG_ALERTA: {
			Notificacion.create().text(mensaje).owner(ventana).showWarning();
			break;
		}
		case MSG_ERROR: {
			Notificacion.create().text(mensaje).owner(ventana).showError();
			break;
		}
		case MSG_EXITO: {
			Notificacion.create().text(mensaje).owner(ventana).showSuccess();
			break;
		}
		case MSG_FALLO: {
			Notificacion.create().text(mensaje).owner(ventana).showError();
			break;
		}
		case MSG_INFO: {
			Notificacion.create().text(mensaje).owner(ventana).showInformation();
			break;
		}
		case MSG_NULO: {
			Notificacion.create().text(mensaje).owner(ventana).showInformation();
			break;
		}
		default: {
			Notificacion.create().text(mensaje).owner(ventana).showInformation();
			break;
		}
		}
		bitacora.debug(mensaje);
	}

	/**
	 * Formatea la query SQL antes de imprimirla
	 * 
	 * @param query
	 */
	public static void imprimirQuery(String query) {

		// Divide la cadena en partes
		LinkedList<String> strippedQuery = new LinkedList<>(
				Arrays.asList(query.split("\\[wrapping: com.mysql.cj.jdbc.ClientPreparedStatement:")));

		// Toma el ultimo elemento (la query) y quita los contenedores
		String string = strippedQuery.peekLast().replaceAll(";\\]", ";");

		// Imprime en bitacora el resultado
		bitacora.debug(String.format(Mensajero.QUERY_EJECUTADA, string));
	}
}
