/* ************************************************************************************* *
 *                                                                                       *
 *  Licensed to the Apache Software Foundation (ASF) under one                           *
 *  or more contributor license agreements.  See the NOTICE file                         *
 *  distributed with this work for additional information                                *
 *  regarding copyright ownership.  The ASF licenses this file                           *
 *  to you under the Apache License, Version 2.0 (the                                    *
 *  "License"); you may not use this file except in compliance                           *
 *  with the License.  You may obtain a copy of the License at                           *
 *                                                                                       *
 *    http://www.apache.org/licenses/LICENSE-2.0                                         *
 *                                                                                       *
 *  Unless required by applicable law or agreed to in writing,                           *
 *  software distributed under the License is distributed on an                          *
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY                               *
 *  KIND, either express or implied.  See the License for the                            *
 *  specific language governing permissions and limitations                              *
 *  under the License.                                                                   *
 *                                                                                       *
 * ************************************************************************************* */
package com.aplicacion.spring.utilitarios;

import java.net.InetAddress;
import java.net.Socket;

import org.apache.log4j.Logger;

/**
 * 
 * @Autor Juan Ignacio Prez
 * @Version 1.0
 * @Fecha 05/07/17
 * 
 * @Uso Verificador del estado del servidor
 * 
 */
public class Conexion {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(Conexion.class);
	private static final int RAPIDO = 150;
	private static final int LENTO = 600;

	public enum Estado {
		BUENA_CONEXION, MALA_CONEXION, DESCONECTADO;
	}

	public enum Disponible {
		ACTIVO, INACTIVO;
	}

	public Conexion(String url, int puerto) {
		this.url = url;
		this.puerto = puerto;
	}

	private String url = null;
	private int puerto = 0;

	/**
	 * Ping al url
	 */
	public Estado verificarPing() {
		InetAddress inet;
		try {
			// Configuramos el servidor a verificar
			inet = InetAddress.getByName(url);
			// Si el ping es menor a 200 el estado del servidor es bueno
			if (inet.isReachable(RAPIDO) == true) {
				return Estado.BUENA_CONEXION;
			} else {
				if (inet.isReachable(LENTO) == true) {
					// Si el ping esta entre 200 y 800 el servidor puede
					// estar congestionado, muy posiblemente a causa del ISP
					// Si el ping es mayor a 800 el servidor puede estar
					// experimentado demoras o estar caido
					return Estado.MALA_CONEXION;
				} else {
					return Estado.DESCONECTADO;
				}
			}
		} catch (Exception exc) {
			return Estado.DESCONECTADO;
		}
	}

	/**
	 * Verificar que el servicio es accesible
	 */
	public Disponible verificarServicio() {
		// Si no lanza excepcion, el servicio esta bien
		try (Socket socket = new Socket(url, puerto);) {
			return Disponible.ACTIVO;
		} catch (Exception exc) {
			return Disponible.INACTIVO;
		}
	}

	// Getters y Setters //
	public int getPuerto() {
		return puerto;
	}

	public String getURL() {
		return url;
	}

	public void setPuerto(int port) {
		puerto = port;
	}

	public void setURL(String url) {
		this.url = url;
	}

}
