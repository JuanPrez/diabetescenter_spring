package com.aplicacion.spring.utilitarios;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.aplicacion.spring.Parametros.Ambiente;
import com.aplicacion.spring.Propiedades;
import com.mchange.v2.c3p0.ComboPooledDataSource;

@Component
@Scope(value = "singleton")
public class C3Pool {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(C3Pool.class);

	private ComboPooledDataSource dataSource;
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String D1 = ":";
	private static final String D2 = "/";

	public static final String DB_PREFIX = "bd";
	private static final String PRUEBA_PREFIX = "_pruebas";
	private static final String RESPAL_PREFIX = "_respaldo";
	private static final String DESARR_PREFIX = "_dev";
	private static final String PRODUC_PREFIX = "";
	private static boolean respaldo = false;

	private static final String VACIO = "";

	public static final String URL = ".url";
	public static final String PUERTO = ".puerto";
	private static final String SCHEMA = ".schema";
	private static final String USUARIO = ".usuario";
	private static final String PASS = ".pass";
	private static final String PARAMETROS = ".parametros";
	private static final String CONECTOR = ".conector";
	private static final String TIPODB = ".tipodb";

	private String dbConnString;

	Propiedades propiedades;
	Ambiente ambiente;

	public void inicializar(Propiedades propiedades) {
		this.propiedades = propiedades;
		this.ambiente = propiedades.getAmbiente();
		this.dbConnString = configurarConexion();
	}

	/**
	 * Configuracion de los parametros de conexion a la BD
	 * 
	 * @return, Cadena de Conexion a la BD (sin schema, ni parametros)
	 */
	private String configurarConexion() {
		String conector = propiedades.get(DB_PREFIX + CONECTOR);
		String tipoDB = propiedades.get(DB_PREFIX + TIPODB);

		return conector + D1 + tipoDB + D1 + D2 + D2 + getUrl() + D1 + getPuerto() + D2;
	}

	/**
	 * Constructor del banco de conexiones
	 * 
	 * @param dbConnString,
	 *            connection string de la BD
	 * @param schema,
	 *            base de datos a usar
	 * @param poolInitialSize,
	 *            cantidad de conexiones inicial
	 * @param param,
	 *            parametros de la conexion
	 */
	public boolean crearBancoConexiones(int tamPool) {
		if (dataSource == null) {
			try {
				dataSource = new ComboPooledDataSource();
				dataSource.setDriverClass(DRIVER);
				dataSource.setJdbcUrl(getDbConnString() + getSchema() + getParametros());
				dataSource.setUser(getUsuario());
				dataSource.setPassword(getPass());
				dataSource.setInitialPoolSize(tamPool);
			} catch (PropertyVetoException exc) {
				bitacora.error("Imposible iniciar pool de conexiones, fallo al iniciar las propiedades", exc);
			}
		}
		// Inicializa el pool de conexiones
		try {
			if (dataSource.getConnection() != null) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException exc) {
			return false;
		}
	}

	public void cerrarBancoConexiones() {
		dataSource.close();
	}

	/**
	 * Obtiene una instancia del pool de conexiones
	 * 
	 * @return Connection (autoclosable)
	 * @throws SQLException
	 */
	public Connection getConexion() throws SQLException {
		return dataSource.getConnection();
	}

	public String getDbConnString() {
		return dbConnString;
	}

	public String getUrl() {
		return propiedades.get(configurarPorAmbiente(DB_PREFIX, URL, respaldo));
	}

	public int getPuerto() {
		return Integer.parseInt(propiedades.get(configurarPorAmbiente(DB_PREFIX, PUERTO, respaldo)));
	}

	private String getSchema() {
		return propiedades.get(configurarPorAmbiente(DB_PREFIX, SCHEMA, respaldo));
	}

	private String getUsuario() {
		return propiedades.get(configurarPorAmbiente(DB_PREFIX, USUARIO, respaldo));
	}

	private String getPass() {
		return propiedades.get(configurarPorAmbiente(DB_PREFIX, PASS, respaldo));
	}

	private String getParametros() {
		return propiedades.get(configurarPorAmbiente(DB_PREFIX, PARAMETROS, respaldo));
	}

	/**
	 * Evalua el ambiente y genera la cadena de texto para buscar en las propiedades
	 * por el valor
	 * 
	 * @param param1
	 * @param param2
	 * @param variable
	 * @return
	 */
	public String configurarPorAmbiente(String param1, String param2, boolean respaldo) {
		String variable = VACIO;
		if (respaldo) {
			variable = param1 + RESPAL_PREFIX + param2;
		} else {
			switch (ambiente) {
			case PRODUCCION: {
				variable = param1 + PRODUC_PREFIX + param2;
				break;
			}
			case DESARROLLO: {
				variable = param1 + DESARR_PREFIX + param2;
				break;
			}
			case PRUEBA: {
				variable = param1 + PRUEBA_PREFIX + param2;
				break;
			}
			default: {
				variable = VACIO;
				break;
			}
			}
		}
		// System.out.println(variable);
		return variable;
	}

	/**
	 * Cambiar
	 */
	public void setRespaldo(boolean respaldo) {
		C3Pool.respaldo = respaldo;
	}

}
