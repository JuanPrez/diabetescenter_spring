/* ************************************************************************************* *
 *                                                                                       *
 *  Licensed to the Apache Software Foundation (ASF) under one                           *
 *  or more contributor license agreements.  See the NOTICE file                         *
 *  distributed with this work for additional information                                *
 *  regarding copyright ownership.  The ASF licenses this file                           *
 *  to you under the Apache License, Version 2.0 (the                                    *
 *  "License"); you may not use this file except in compliance                           *
 *  with the License.  You may obtain a copy of the License at                           *
 *                                                                                       *
 *    http://www.apache.org/licenses/LICENSE-2.0                                         *
 *                                                                                       *
 *  Unless required by applicable law or agreed to in writing,                           *
 *  software distributed under the License is distributed on an                          *
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY                               *
 *  KIND, either express or implied.  See the License for the                            *
 *  specific language governing permissions and limitations                              *
 *  under the License.                                                                   *
 *                                                                                       *
 * ************************************************************************************* */
package com.aplicacion.spring.utilitarios;

import java.time.format.DateTimeFormatter;

import org.apache.log4j.Logger;

/**
 * 
 * @Autor Juan Ignacio Prez
 * @Version 1.0
 * @Fecha 05/07/17
 * 
 * @Uso Metodos y Constantes globales del sistema
 * 
 */
public final class Constantes {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(Constantes.class);

	// Constantes de archivos
	public static final double BYTE = 1;
	public static final double KILOBYTE = (BYTE * 1024);
	public static final double MEGABYTE = (KILOBYTE * 1024);
	public static final double GIGABYTE = (MEGABYTE * 1024);
	public static final double TAMANO_MAXIMO = KILOBYTE * 512;
	

	// ***************************************************************
	// Tablas de la Base de datos
	// ***************************************************************
	// Tablas de acceso
	public static final String DB_USUARIO_PERFIL = "acc_perfil";
	public static final String DB_USUARIO_PROFESION = "acc_profesion";
	public static final String DB_USUARIO_ESPECIALIDAD = "acc_perfil_especialidad";
	public static final String DB_USUARIO_PRINCIPAL = "acc_usuario";

	// Tablas de patologias cronicas
	public static final String DB_PATOLOGIA_CRONICA_EXAMENES = "com_patologcron_ex";
	public static final String DB_PATOLOGIA_CRONICA_HC = "com_patologcron_hc";
	public static final String DB_PATOLOGIA_CRONICA_JUNCTION_PAT_EXAM = "com_patologcron_nx";
	public static final String DB_PATOLOGIA_CRONICA_RESULTADOS = "com_patologcron_re";
	public static final String DB_PATOLOGIA_CRONICA_PRINCIPAL = "com_patologcronica";

	// Tablas de encuestas
	public static final String DB_ENCUESTA_CONF = "dia_enc_conf";
	public static final String DB_ENCUESTA_ESTIMA = "dia_enc_estim";
	public static final String DB_ENCUESTA_JUNCTION = "dia_enc_nex";
	public static final String DB_ENCUESTA_OBJETIVO = "dia_enc_objv";
	public static final String DB_ENCUESTA_PREG = "dia_enc_preg";
	public static final String DB_ENCUESTA_RESP = "dia_enc_resp";
	public static final String DB_ENCUESTA_VALOR = "dia_enc_val";

	// Tablas de gestion
	public static final String DB_GESTION_AFILIADO = "ges_afiliado";
	public static final String DB_GESTION_HORARIOS = "ges_horarios";
	public static final String DB_GESTION_OBRASOCIAL = "ges_obrasocial";
	public static final String DB_GESTION_TURNOS = "ges_turnos";

	// Tablas de Historia Clinica
	public static final String DB_PACIENTES = "pac_paciente";
	public static final String DB_PACIENTE_STATUS = "pac_status";
	public static final String DB_PACIENTE_ANTROPOMETRICOS = "pac_antropometricos";
	public static final String DB_PACIENTE_IMAGENES = "pac_imagenes";

	public static final String DB_PROXCITA_MEDICA = "pac_proxcita_medica";
	public static final String DB_PROXCITA_NUTRIC = "pac_proxcita_nutric";
	public static final String DB_PROXCITA_PODO = "pac_proxcita_podo";

	public static final String DB_HC_CONSULTAS = "pac_consulta_medica";
	public static final String DB_CONSULTA_MEDICA = "pac_hc_clinica";
	public static final String DB_CONSULTA_NUTRIC = "pac_hc_nutric";
	public static final String DB_CONSULTA_PODO = "pac_hc_podo";
	public static final String DB_PACIENTE_ANAMNESIS = "pac_nut_anamnesis";

	// Vistas para simplificar queries
	public static final String DB_VISTA_ACCESO_USUARIO = "vw_acceso_usuarios";
	public static final String DB_VISTA_PACIENTE = "vw_paciente";
	public static final String DB_VISTA_CONSULTAS = "vw_paciente_consultas";
	public static final String DB_VISTA_HC_CLINICA = "vw_lista_hc_clinica";
	public static final String DB_VISTA_HC_NUTRIC = "vw_lista_hc_nutric";
	public static final String DB_VISTA_HC_PODOLOG = "vw_lista_hc_podo";
	public static final String DB_VISTA_TURNOS = "vw_turno";
	public static final String DB_VISTA_ANAMNESIS = "vw_anamnesis";
	public static final String DB_VISTA_ANTROPOMETRICOS = "vw_antropometricos";

	// Constantes de patron
	public static final String PATRON_NUMEROS_ENTEROS = "numeros";
	public static final String PATRON_EMAIL = "email";
	public static final String PATRON_USUARIO = "usuario";
	public static final String PATRON_CONTRA = "contrasena";
	public static final String PATRON_ARCH_IMAGEN = "archivoImgn";
	public static final String PATRON_HORA24 = "hora24";
	public static final String PATRON_FECHA = "fecha";

	// Creamos las constantes para los mensajes de alerta
	public static final String TIPO_INFO = "info";
	public static final String TIPO_EXITO = "exito";
	public static final String TIPO_ALERTA = "alerta";
	public static final String TIPO_ERROR = "error";
	public static final String TIPO_FALLO = "fallo";
	public static final String TIPO_NULO = "nulo";

	// Creamos la constante para armar el formato de fecha
	public static final DateTimeFormatter FECHA_DIA_DD_MM_YY = DateTimeFormatter.ofPattern("EEEE\n(dd/MM/yy) ");
	public static final DateTimeFormatter FECHA_YYYY_MM_DD = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	public static final DateTimeFormatter FECHA_DD_MM_YYYY = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	public static final DateTimeFormatter FECHA_D_M_YYYY = DateTimeFormatter.ofPattern("d/M/yyyy");

	// Constructor vacio
	private Constantes() {
	}

}
