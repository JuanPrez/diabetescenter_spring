/* ************************************************************************************* *
 *                                                                                       *
 *  Licensed to the Apache Software Foundation (ASF) under one                           *
 *  or more contributor license agreements.  See the NOTICE file                         *
 *  distributed with this work for additional information                                *
 *  regarding copyright ownership.  The ASF licenses this file                           *
 *  to you under the Apache License, Version 2.0 (the                                    *
 *  "License"); you may not use this file except in compliance                           *
 *  with the License.  You may obtain a copy of the License at                           *
 *                                                                                       *
 *    http://www.apache.org/licenses/LICENSE-2.0                                         *
 *                                                                                       *
 *  Unless required by applicable law or agreed to in writing,                           *
 *  software distributed under the License is distributed on an                          *
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY                               *
 *  KIND, either express or implied.  See the License for the                            *
 *  specific language governing permissions and limitations                              *
 *  under the License.                                                                   *
 *                                                                                       *
 * ************************************************************************************* */
package com.aplicacion.spring.utilitarios;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import org.apache.log4j.Logger;

import javafx.beans.property.SimpleIntegerProperty;

/**
 * 
 * @Autor Juan Ignacio Prez
 * @Version 1.0
 * @Fecha 05/07/17
 * 
 * @Uso Verificador del estado del servidor
 * 
 */
public class VerifServ {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(VerifServ.class);

	private static String url = null;
	private static int puerto = 0;
	private static SimpleIntegerProperty codigoVelocidadConexion = new SimpleIntegerProperty();
	private static SimpleIntegerProperty servicioCode = new SimpleIntegerProperty();

	private VerifServ() {

	}

	// Getters y Setters //
	public static int getPuerto() {
		return puerto;
	}

	public static String getURL() {
		return url;
	}

	public static void setPuerto(int port) {
		puerto = port;
	}

	public static void setURL(String url) {
		VerifServ.url = url;
	}

	public static final SimpleIntegerProperty pingCodeProperty() {
		return VerifServ.codigoVelocidadConexion;
	}

	public static final int getPingCode() {
		return VerifServ.pingCodeProperty().get();
	}

	public static final void setPingCode(final int pingCode) {
		VerifServ.pingCodeProperty().set(pingCode);
	}

	public static final SimpleIntegerProperty servicioCodeProperty() {
		return VerifServ.servicioCode;
	}

	public static final int getServicioCode() {
		return VerifServ.servicioCodeProperty().get();
	}

	public static final void setServicioCode(final int servicioCode) {
		VerifServ.servicioCodeProperty().set(servicioCode);
	}

	/**
	 * 
	 * Metodo para hacer ping a un servidor
	 * 
	 * @Param servidor a verificar
	 */
	public static void verificarPing(String servidor) throws IOException {
		int respuestaRapida = 100;
		int respuestaLenta = 600;

		InetAddress inet;

		// Configuramos el servidor a verificar
		inet = InetAddress.getByName(servidor);

		try {
			// Si el ping es menor a 200 el estado del servidor es bueno
			if (inet.isReachable(respuestaRapida) == true) {
				setPingCode(1);
			} else {
				if (inet.isReachable(respuestaLenta) == true) {
					// Si el ping esta entre 200 y 800 el servidor puede
					// estar
					// congestionado, muy posiblemente a causa del ISP
					// Si el ping es mayor a 800 el servidor puede estar
					// experimentado demoras o estar caido
					setPingCode(2);
				} else {
					setPingCode(3);
				}
			}
		} catch (Exception exc) {
			bitacora.info(exc.getMessage());
		}
	}

	/**
	 * 
	 * Metodo para verificar un servicio
	 * 
	 * @Param URL del servicio a verificar
	 * @Param puerto del servicio a verificar
	 */
	public static void verificarServicio(String servidor, int port) {
		// Si no lanza excepcion, el servicio esta bien
		try {
			Socket socket = new Socket(servidor, port);
			setServicioCode(1);
			socket.close();
		} catch (Exception exc) {
			setServicioCode(2);
			bitacora.info(exc.getMessage());
		}
	}

}
