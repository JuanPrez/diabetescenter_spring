/* ************************************************************************************* *
 *                                                                                       *
 *  Licensed to the Apache Software Foundation (ASF) under one                           *
 *  or more contributor license agreements.  See the NOTICE file                         *
 *  distributed with this work for additional information                                *
 *  regarding copyright ownership.  The ASF licenses this file                           *
 *  to you under the Apache License, Version 2.0 (the                                    *
 *  "License"); you may not use this file except in compliance                           *
 *  with the License.  You may obtain a copy of the License at                           *
 *                                                                                       *
 *    http://www.apache.org/licenses/LICENSE-2.0                                         *
 *                                                                                       *
 *  Unless required by applicable law or agreed to in writing,                           *
 *  software distributed under the License is distributed on an                          *
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY                               *
 *  KIND, either express or implied.  See the License for the                            *
 *  specific language governing permissions and limitations                              *
 *  under the License.                                                                   *
 *                                                                                       *
 * ************************************************************************************* */
package com.aplicacion.spring.utilitarios;

import java.io.File;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.aplicacion.spring.excepciones.ExcepcionCampoFueraDePatron;
import com.aplicacion.spring.excepciones.ExcepcionCampoMuyCorto;
import com.aplicacion.spring.excepciones.ExcepcionCampoMuyLargo;

/**
 * 
 * @Autor Juan Ignacio Prez
 * @Version 1.0
 * @Fecha 05/07/17
 * 
 * @Uso Validar campos de texto usando patrones o expresiones regulares
 * 
 */
public class Validador {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(Validador.class);
	private String input = null;
	private Pattern PatronRegex = null;
	private int largo_cadena = 10;

	/**
	 * Expresiones regular de los patrones comunes
	 * 
	 * @return patron
	 */
	public static Pattern construirPatronesComunes(String s) {
		String i = null;
		// Patrones
		switch (s) {
		case Constantes.PATRON_NUMEROS_ENTEROS:
			// Numeros
			i = "([0-9])*";
			break;
		case Constantes.PATRON_EMAIL:
			// Debe haber 3 segmentos, usuario@domonio.tipodominio
			i = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			break;
		case Constantes.PATRON_USUARIO:
			// 4 a 15 caractares que contengan letras minusculas y/o numeros
			i = "^[a-z0-9_-]{3,15}$";
			break;
		case Constantes.PATRON_CONTRA:
			// 8 a 20 caractares que contengan letras minusculas, mayusculas y/o
			// numeros
			i = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})";
			break;
		case Constantes.PATRON_ARCH_IMAGEN:
			// Nombre de archivo, punto (.) y extensiones validas
			i = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)";
			break;
		case Constantes.PATRON_HORA24:
			// Hora en formato militar, 0-23:0-59
			i = "([01]?[0-9]|2[0-3]):[0-5][0-9]";
			break;
		case Constantes.PATRON_FECHA:
			// Fecha en formato dd/mm/yyyy
			i = "^\\d{1,2}/\\d{1,2}/\\d{4}$";
			break;
		case "cad_ss":
			// Cadena de caracteres, sin comas
			i = "[\\p{L}\\s]+";
			break;
		case "cad_cs":
			// Cadena de caracteres, con signos de puntuacion
			i = "[\\p{L}\\s\\ \\.\\,\\;\\:\\_\\-]+";
			break;
		default:
			// Si no encontramos el nombre, usamos la misma cadena de caracteres
			// como expresion regular
			i = s;
			break;
		}
		Pattern p = Pattern.compile(i);
		return p;
	}

	/**
	 * Validar campo de texto.
	 * 
	 * @param texto
	 * @param regExp
	 * @param longitud
	 * @param campo
	 * @return true, si la cadena cumple ambos requisitos
	 */
	public static boolean validarTextoContraRegExpMaximoCampo(String texto, String regExp, int longitud, String campo) {
		Pattern p = construirPatronesComunes(regExp);
		if (p.matcher(texto).matches()) {
			if (texto.length() <= longitud) {
				return true;
			} else {
				bitacora.info(" El texto es demasiado extenso, el limite es de " + longitud + " caracteres");
			}
		} else {
			bitacora.info(
					" El campo contiene caracteres invalidos o no cumple con el patron de texto, el campo es " + campo);
		}
		return false;
	}

	/**
	 * Validar texto, sin limite de caracteres.
	 * 
	 * @param texto
	 * @param patron
	 * @param campo
	 * @return true, si la cadena cumple los requisitos
	 */
	public static boolean validarTextoContraPatron(String texto, Pattern patron, String campo) {
		if (patron.matcher(texto).matches()) {
			return true;
		} else {
			bitacora.info(
					" El campo contiene caracteres invalidos o no cumple con el patron de texto, el campo es " + campo);
		}
		return false;
	}

	/**
	 * Validar texto, sin limite de caracteres.
	 * 
	 * @param texto
	 * @param regExp
	 * @return true, si la cadena cumple los requisitos
	 * 
	 */
	public static boolean validarTextoContraPatron(String texto, String regExp) {
		Pattern p = construirPatronesComunes(regExp);
		if (p.matcher(texto).matches()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Validar texto.
	 * 
	 * @param texto
	 * @param patron
	 * @param longitud
	 * 
	 * @return true, si la cadena cumple ambos requisitos
	 */
	public static void validarTextoPatronMaximo(String texto, String patron, int longitud)
			throws ExcepcionCampoMuyLargo, ExcepcionCampoFueraDePatron {
		if (construirPatronesComunes(patron).matcher(texto).matches()) {
			if (texto.length() >= longitud) {
				throw new ExcepcionCampoMuyLargo();
			}
		} else {
			throw new ExcepcionCampoFueraDePatron();
		}
	}

	/**
	 * 
	 * Valida que el campo sea del largo y respete el patron de ingreso
	 * 
	 * @param cadenadeTexto
	 * @param patron
	 * @param min
	 * @param max
	 * @return true, si el campo es valido
	 * @throws ExcepcionCampoMuyLargo
	 * @throws ExcepcionCampoMuyCorto
	 * @throws ExcepcionCampoFueraDePatron
	 */
	public static boolean validarTextoContraPatronMinimoMaximo(String cadenadeTexto, String patron, int min, int max)
			throws ExcepcionCampoMuyLargo, ExcepcionCampoMuyCorto, ExcepcionCampoFueraDePatron {
		Pattern p = construirPatronesComunes(patron);
		if (p.matcher(cadenadeTexto).matches()) {
			if (cadenadeTexto.length() <= max) {
				if (cadenadeTexto.length() >= min) {
					return true;
				} else {
					throw new ExcepcionCampoMuyCorto();
				}
			} else {
				throw new ExcepcionCampoMuyLargo();
			}
		} else {
			throw new ExcepcionCampoFueraDePatron();
		}
	}

	public static boolean validarTamanoArchivo(File archivo) {
		if (archivo.length() > Constantes.TAMANO_MAXIMO)
			return false;
		else
			return true;
	}

	// Setter y getters //
	public String getInput() {
		return input;
	}

	public int getLargo_cadena() {
		return largo_cadena;
	}

	public Pattern getPatronRegex() {
		return PatronRegex;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public void setLargo_cadena(int largo_cadena) {
		this.largo_cadena = largo_cadena;
	}

	public void setPatronRegex(Pattern patronRegex) {
		PatronRegex = patronRegex;
	}

}
