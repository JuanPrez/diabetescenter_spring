package com.aplicacion.spring.utilitarios;

import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.aplicacion.spring.utilitarios.Conexion.Disponible;
import com.aplicacion.spring.utilitarios.Conexion.Estado;

import javafx.beans.property.SimpleObjectProperty;

public class Monitor extends TimerTask {

	Conexion conexion;
	Estado active;
	private SimpleObjectProperty<Disponible> servicio = new SimpleObjectProperty<>();
	private SimpleObjectProperty<Estado> calidadConexion = new SimpleObjectProperty<>();

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(Monitor.class);

	public Monitor(Conexion conexion) {
		this.conexion = conexion;
	}

	public void verificarServicio() {
		servicio.set(conexion.verificarServicio());
	}

	@Override
	public void run() {
		active = conexion.verificarPing();
		calidadConexion.set(active);
	}

	@Override
	public boolean cancel() {
		bitacora.warn("Monitor apagandose");
		return super.cancel();
	}

	public Conexion getConexion() {
		return conexion;
	}

	public void setConexion(Conexion conexion) {
		this.conexion = conexion;
	}

	public final SimpleObjectProperty<Estado> calidadConexionProperty() {
		return this.calidadConexion;
	}

	public final Estado getCalidadConexion() {
		return this.calidadConexionProperty().get();
	}

	public final void setCalidadConexion(final Estado calidadConexion) {
		this.calidadConexionProperty().set(calidadConexion);
	}

	public final SimpleObjectProperty<Disponible> servicioProperty() {
		return this.servicio;
	}

	public final Disponible getServicio() {
		return this.servicioProperty().get();
	}

	public final void setServicio(final Disponible servicio) {
		this.servicioProperty().set(servicio);
	}

}