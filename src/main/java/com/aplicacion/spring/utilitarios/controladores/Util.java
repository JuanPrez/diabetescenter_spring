package com.aplicacion.spring.utilitarios.controladores;

import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Collection;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import com.aplicacion.spring.excepciones.ExcepcionCampoFueraDePatron;
import com.aplicacion.spring.excepciones.ExcepcionCampoMuyCorto;
import com.aplicacion.spring.excepciones.ExcepcionCampoMuyLargo;
import com.aplicacion.spring.utilitarios.Constantes;
import com.aplicacion.spring.utilitarios.Validador;

import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class Util {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(Util.class);

	private Util() {
	}

	// Estilos CSS
	public static final String CAMPO_TRANSPARENTE = "campo-transparente";
	public static final String CAMPO_TRANSPARENTE_VALIDO = "campo-transparente-valido";
	public static final String CAMPO_TRANSPARENTE_INVALIDO = "campo-transparente-invalido";

	public static final String CAMPO_NORMAL = "campo-normal";
	public static final String CAMPO_VALIDO = "campo-valido";
	public static final String CAMPO_INVALIDO = "campo-invalido";
	public static final String ESTILO_BORDE_IZQUIERDO = "borde-izquierdo";
	public static final String ESTILO_BORDE_DERECHO = "borde-derecho";
	public static final String ESTILO_BORDER_INFERIOR = "borde-inferior-suave";

	/**
	 * Cambia el estilos de CSS para llamar la atencion sobre el campo
	 * 
	 * @param campo
	 * @param base
	 */
	public static void resaltarCampoInvalido(Control campo, String base) {
		String campo_valido;
		String campo_invalido;
		switch (base) {
		case CAMPO_TRANSPARENTE: {
			campo_valido = CAMPO_TRANSPARENTE_VALIDO;
			campo_invalido = CAMPO_TRANSPARENTE_INVALIDO;
			break;
		}
		case CAMPO_NORMAL: {
			campo_valido = CAMPO_VALIDO;
			campo_invalido = CAMPO_INVALIDO;
			break;
		}
		default: {
			campo_valido = CAMPO_VALIDO;
			campo_invalido = CAMPO_INVALIDO;
			break;
		}
		}
		campo.getStyleClass().removeAll(base, campo_valido);
		campo.getStyleClass().add(campo_invalido);
	}

	/**
	 * Cambia el estilos de CSS para confirmar validez del campo
	 * 
	 * @param campo
	 * @param base
	 */
	public static void resaltarCampoValido(Control campo, String base) {
		String campo_valido;
		String campo_invalido;
		switch (base) {
		case CAMPO_TRANSPARENTE: {
			campo_valido = CAMPO_TRANSPARENTE_VALIDO;
			campo_invalido = CAMPO_TRANSPARENTE_INVALIDO;
			break;
		}
		case CAMPO_NORMAL: {
			campo_valido = CAMPO_VALIDO;
			campo_invalido = CAMPO_INVALIDO;
			break;
		}
		default: {
			campo_valido = CAMPO_VALIDO;
			campo_invalido = CAMPO_INVALIDO;
			break;
		}
		}
		if (!campo.getStyleClass().contains(campo_invalido)) {
			campo.getStyleClass().removeAll(base);
			campo.getStyleClass().add(campo_valido);
		}
	}

	/**
	 * Cambia el estilos de CSS para quitar la llamada de atencion
	 * 
	 * @param campo
	 * @param base
	 */
	public static void quitarResaltadoDelCampo(Control campo, String base) {
		String campo_valido;
		String campo_invalido;
		switch (base) {
		case CAMPO_TRANSPARENTE: {
			campo_valido = CAMPO_TRANSPARENTE_VALIDO;
			campo_invalido = CAMPO_TRANSPARENTE_INVALIDO;
			break;
		}
		case CAMPO_NORMAL: {
			campo_valido = CAMPO_VALIDO;
			campo_invalido = CAMPO_INVALIDO;
			break;
		}
		default: {
			campo_valido = CAMPO_VALIDO;
			campo_invalido = CAMPO_INVALIDO;
			break;
		}
		}
		campo.getStyleClass().removeAll(campo_invalido, campo_valido);
		campo.getStyleClass().add(base);
	}

	/**
	 * Fantastico metodo que hace la vida del BigDecimal mas simple
	 * 
	 * @param campo
	 *            (textfield)
	 */
	public static void reemplazoPuntoXComa(TextField campo) {
		campo.textProperty().addListener((o, p, a) -> campo.setText(campo.getText().replaceAll(",", "\\.")));
	}

	/**
	 * Valida la longitud de un campo
	 * 
	 * @param campo
	 * @param min
	 * @param max
	 * @return
	 * @throws ExcepcionCampoMuyLargo
	 * @throws ExcepcionCampoMuyCorto
	 */
	public static String validarLongitudCampo(TextInputControl campo, int min, int max)
			throws ExcepcionCampoMuyLargo, ExcepcionCampoMuyCorto {
		if (campo.getText().trim().length() < max) {
			if (campo.getText().trim().length() > min) {
				return campo.getText().trim();
			} else {
				throw new ExcepcionCampoMuyCorto();
			}
		} else {
			throw new ExcepcionCampoMuyLargo();
		}
	}

	/**
	 * Recortar hasta longitud la longitud de un campo
	 * 
	 * @param campo
	 * @param min
	 * @param max
	 * @return
	 * @throws ExcepcionCampoMuyLargo
	 * @throws ExcepcionCampoMuyCorto
	 */
	public static String recortarLongitudCampo(TextInputControl campo, int max) {
		String texto = campo.getText().trim();
		if (texto.length() > max) {
			return texto.substring(0, max - 1);
		}
		return texto;
	}

	/**
	 * Valida el campo, usando patron y longitud
	 * 
	 * @param campo,
	 *            a validar
	 * @param patron,
	 *            para validar
	 * @param i,
	 *            longitud maxima del campo
	 * @return String
	 */
	public static String validarCampoPatron(TextInputControl campo, String patron) throws ExcepcionCampoFueraDePatron {
		if (!campo.getText().trim().isEmpty()) {
			Validador.validarTextoContraPatron(campo.getText().trim(), patron);
			return campo.getText().trim();
		}
		return "0";
	}

	/**
	 * Valida el campo, usando patron y longitud
	 * 
	 * @param campo
	 * @param patron
	 * @param longitud
	 * @return String
	 */
	public static String validarTipoCampo(TextInputControl campo, String patron, int longitud)
			throws ExcepcionCampoMuyLargo, ExcepcionCampoFueraDePatron {
		if (!campo.getText().trim().isEmpty()) {
			Validador.validarTextoPatronMaximo(campo.getText().trim(), patron, longitud);
			return campo.getText().trim();
		}
		return "0";
	}

	/**
	 * Fuerza a que solo numeros puedan incluirse en el campo
	 * 
	 * @param campo
	 * @param patron
	 * @param longitud
	 */
	public static void forzarCampoNumericoEnteroLimitado(TextInputControl campo, int longitud) {
		campo.textProperty().addListener((o, textoPrevio, textoActual) -> {
			if (textoActual != null) {
				try {
					Validador.validarTextoPatronMaximo(textoActual, Constantes.PATRON_NUMEROS_ENTEROS, longitud);
				} catch (Exception exc) {
					campo.setText(textoPrevio);
				}
			}
		});
	}

	/**
	 * Resalta el campo tipo fecha y se lo asigna a la consulta pasada
	 * 
	 * @param campo
	 */
	public static void validacionVisualCampoFecha(DatePicker campo, String base) {
		campo.getEditor().focusedProperty().addListener((o, focoPrevio, focoActual) -> {
			if (focoActual) {
				Util.quitarResaltadoDelCampo(campo, base);
			} else {
				Util.quitarResaltadoDelCampo(campo, base);
				try {
					LocalDate.parse(campo.getEditor().getText(), Constantes.FECHA_D_M_YYYY);
					Util.resaltarCampoValido(campo, base);
				} catch (Exception exc) {
					Util.resaltarCampoInvalido(campo, base);
				}
			}
		});
		campo.getEditor().textProperty().addListener((o, textoPrevio, textoActual) -> {
			if (textoActual.isEmpty()) {
				Util.quitarResaltadoDelCampo(campo, base);
			} else {
				Util.quitarResaltadoDelCampo(campo, base);
				try {
					LocalDate.parse(campo.getEditor().getText(), Constantes.FECHA_D_M_YYYY);
					Util.resaltarCampoValido(campo, base);
				} catch (Exception exc) {
					Util.resaltarCampoInvalido(campo, base);
				}
			}
		});
	}

	/**
	 * Validacion visual
	 */
	public static void validacionVisualCampoBigDecimal(TextField campo, String base) {
		campo.textProperty().addListener((o, textoPrevio, textoActual) -> {
			// Si el campo esta vacio, quita el remarcado
			Util.quitarResaltadoDelCampo(campo, base);
			// Si el campo esta vacio, lo toma como 0 (cero)
			if (textoActual.isEmpty()) {
				textoActual = "0";
			}
			// Resalta el valor, en base si puede convertir el valor en BigDecimal
			try {
				new BigDecimal(textoActual);
				Util.resaltarCampoValido(campo, base);
			} catch (Exception exc) {
				Util.resaltarCampoInvalido(campo, base);
			}
		});
	}

	/**
	 * Limpia el conjunto de Campos pasado
	 * 
	 * @param panel
	 */
	public static void limpiarCampos(Pane panel) {
		for (int i = 0; i < panel.getChildrenUnmodifiable().size(); i++) {
			try {
				Util.limpiarCampo((TextInputControl) panel.getChildrenUnmodifiable().get(i));
			} catch (ClassCastException exc) {
				if (bitacora.isTraceEnabled()) {
					bitacora.trace(exc.getMessage());
				}
			}
		}
	}

	/**
	 * Limpia el campo pasado
	 * 
	 * @param textInput
	 */
	public static void limpiarCampo(TextInputControl textInput) {
		textInput.clear();
	}

	/**
	 * Inhabilita los campos de texto (TextInputControl) dentro del contenedor (Pane
	 * pasado)
	 * 
	 * @param panel
	 */
	public static void habilitarCampos(Pane panel, boolean editable) {
		for (int i = 0; i < panel.getChildrenUnmodifiable().size(); i++) {
			try {
				Util.habilitarCampo((TextInputControl) panel.getChildrenUnmodifiable().get(i), editable);
			} catch (ClassCastException exc) {
				if (bitacora.isTraceEnabled()) {
					bitacora.trace(exc.getMessage());
				}
			}
		}
	}

	/**
	 * 
	 * Inhabilita el campo pasado
	 * 
	 * @param textInput
	 */
	public static void habilitarCampo(TextInputControl textInput, boolean editable) {
		textInput.setEditable(editable);
	}

	/**
	 * Fantastico metodo que hace la vida del BigDecimal mas simple
	 * 
	 * @param campo
	 *            (textfield)
	 * @return
	 */
	public static void disparadorReemplazoPuntoXComa(TextField campo) {
		campo.textProperty().addListener((o, p, a) -> campo.setText(campo.getText().replaceAll(",", "\\.")));
	}

	/**
	 * Metodo que repite los cambios en Motivo a Diagnostico, Ingenioso uso del
	 * bind!!
	 *
	 */
	public static void disparadorAtaduraLigera(TextField campo1, TextField campo2) {
		campo1.focusedProperty().addListener((o, p, a) -> {
			if (campo1.isEditable()) {
				if (a) {
					campo2.textProperty().bind(campo1.textProperty());
				} else {
					campo2.textProperty().unbind();
				}
			}
		});
	}

	/**
	 * Calcula el IMC en base al peso y talla
	 * 
	 * @param campoIMC
	 * @param talla
	 * @param campoPeso
	 */
	public static void disparadorCalculadoraIMC(Label etiquetaIMC, String talla, TextField campoPeso) {
		campoPeso.textProperty().addListener((o, p, a) -> {
			try {
				// Retorna el valor calculado, o "NA" si no pudo ser calculado
				etiquetaIMC.setText(Util.calculadorIMC(a, talla));
			} catch (NullPointerException | NumberFormatException exc) {
				// Si el numero no es valido, mostramos Err
				etiquetaIMC.setText("Err");
			}
		});
	}

	/**
	 * 
	 * Evalua un campo numerico y lo convierte
	 * 
	 * @param campo
	 * @return
	 */
	public static int evaluarCampoNumerico(TextField campo) {
		if (campo.getText() != null && !campo.getText().isEmpty()) {
			try {
				return Integer.parseInt(campo.getText());
			} catch (NumberFormatException exc) {
				bitacora.info("El valor del campo" + campo.getId() + " no puede convertirse a numero");
				return 0;
			}
		}
		return 0;
	}

	/**
	 * Evalua un campo numerico decimal y lo convierte
	 */
	public static BigDecimal convertirCampoBigDecimal(TextField campo, int decimales) {
		if (campo.getText() != null && !campo.getText().isEmpty()) {
			try {
				return new BigDecimal(campo.getText()).setScale(decimales, RoundingMode.HALF_EVEN);
			} catch (NumberFormatException exc) {
				bitacora.debug("El valor del campo" + campo.getId() + " no puede convertirse a numero");
				return new BigDecimal(0).setScale(decimales, RoundingMode.HALF_EVEN);
			}
		} else {
			return new BigDecimal(0).setScale(decimales, RoundingMode.HALF_EVEN);
		}
	}

	/**
	 * Limpia los campos
	 */
	public static void vaciarCampos(Collection<TextInputControl> campos) {
		for (TextInputControl campo : campos) {
			campo.clear();
		}
	}

	/**
	 * Calcula el valor del IMC a partir del peso y talla
	 * 
	 * @param peso
	 * @param talla
	 * @return valor a imprimir
	 */
	public static String calculadorIMC(String peso, String talla) {
		// Usam la formula para calcular el IMC
		// (P/h**2) - Peso (kg) dividido la altura (m) al cuadrado

		// Conversion
		double pesoDouble = Double.parseDouble(peso.replace(',', '.'));
		float tallaFloat = Float.parseFloat(talla.replace(',', '.'));

		double ims = pesoDouble / (Math.pow(tallaFloat / 100, 2));

		// Convierte el valor
		if (ims > 0) {
			// Lo convierte en un formato ##.##
			return String.valueOf(ims).substring(0, 5) + "";
		} else {
			return "NA";
		}
	}

	/**
	 * Calcula el valor del IMC a partir del peso y talla
	 * 
	 * @param peso
	 * @param talla
	 * @return valor a imprimir
	 */
	public static double calculadorIMC(BigDecimal peso, String talla) {
		// Usam la formula para calcular el IMC
		// (P/h**2) - Peso (kg) dividido la altura (m) al cuadrado
		// Conversion
		try {
			float tallaFloat = Float.parseFloat(talla.replace(',', '.'));
			double imc = peso.toBigInteger().doubleValue() / (Math.pow(tallaFloat / 100, 2));
			if (imc > 0) {
				// Lo convierte en un formato ##.##
				return imc;
			} else {
				return 0;
			}
		} catch (Exception exc) {
			return 0;
		}

	}

	/**
	 * 
	 * Crea un espaciador dinamico vertical
	 * 
	 * @return
	 */
	public static Node espacioVertical() {
		final Region espaciador = new Region();
		// Se expande y encoge automaticamente verticalmente
		VBox.setVgrow(espaciador, Priority.ALWAYS);
		return espaciador;
	}

	/**
	 * Crea un espaciador dinamico horizontal
	 * 
	 * @param max,
	 *            maximo, o 0 (cero si no hay maximo)
	 * @return
	 */
	public static Node espacioHorizontal() {
		final Region espaciador = new Region();
		// Se expande y encoge automaticamente horizontal
		HBox.setHgrow(espaciador, Priority.ALWAYS);
		return espaciador;
	}

	/**
	 * Crea un espaciador dinamico horizontal
	 * 
	 * @param max,
	 *            maximo, o 0 (cero si no hay maximo)
	 * @return
	 */
	public static Node espacioHorizontal(int max) {
		final Region espaciador = new Region();
		if (max > 0) {
			espaciador.setMaxWidth(max);
		}
		// Se expande y encoge automaticamente horizontal
		HBox.setHgrow(espaciador, Priority.ALWAYS);
		return espaciador;
	}

	/**
	 * Crea un espaciador dinamico horizontal con borde derecho
	 * 
	 * @param max,
	 *            maximo, o 0 (cero si no hay maximo)
	 * @return
	 */
	public static Node espacioHLimitado(int max, String estilo) {
		final Region espaciador = new Region();
		if (max > 0) {
			espaciador.setMaxWidth(max);
		}
		espaciador.getStyleClass().add(estilo);
		// Se expande y encoge automaticamente horizontal
		HBox.setHgrow(espaciador, Priority.ALWAYS);
		return espaciador;
	}

	/**
	 * Agrega un listener que convierte mayusculas en minusculas
	 * 
	 * @param campo
	 */
	public static void capitalizarCampo(TextInputControl campo) {
		campo.textProperty().addListener((o, textoPrevio, textoActual) -> {
			if (textoActual != null)
				campo.setText(textoActual.toUpperCase());
		});
	}

	/**
	 * Validacion de imagen
	 * 
	 * @param archivo
	 * @return
	 */
	public static boolean validarImagen(InputStream archivo) {
		try {
			if (ImageIO.read(archivo) != null) {
				return true;
			} else {
				return false;
			}
		} catch (Exception exc) {
			bitacora.warn("El archivo no es una imagen valida", exc);
			return false;
		}
	}
	
	/**
	 * Validacion de imagen
	 * 
	 * @param archivo
	 * @return
	 */
	public static boolean validarImagen(File archivo) {
		try {
			if (ImageIO.read(archivo) != null) {
				return true;
			} else {
				return false;
			}
		} catch (Exception exc) {
			bitacora.warn("El archivo no es una imagen valida", exc);
			return false;
		}
	}

	/**
	 * Evalua si la fecha pasada es en el futuro
	 * 
	 * @param fecha
	 * @return
	 */
	public static boolean validarFechaFutura(LocalDate fecha) {
		return fecha.isAfter(LocalDate.now());
	}

	/**
	 * Evalua si la fecha pasada es en el pasado
	 * 
	 * @param fecha
	 * @return
	 */
	public static boolean validarFechaPasada(LocalDate fecha) {
		return LocalDate.now().isAfter(fecha);
	}

	public static int convertirCampoInteger(String texto) {
		try {
			return Integer.parseInt(texto);
		} catch (Exception exc) {
			return 0;
		}
	}

	public static void validacionVisualCampoInteger(TextField campo, String base) {
		campo.textProperty().addListener((o, textoPrevio, textoActual) -> {
			// Si el campo esta vacio, quita el remarcado
			Util.quitarResaltadoDelCampo(campo, base);
			// Si el campo esta vacio, lo toma como 0 (cero)
			if (textoActual.isEmpty()) {
				textoActual = "0";
			}
			// Resalta el valor, en base si puede convertir el valor en BigDecimal
			try {
				Integer.parseInt(textoActual);
				Util.resaltarCampoValido(campo, base);
			} catch (Exception exc) {
				Util.resaltarCampoInvalido(campo, base);
			}
		});
	}

	/**
	 * Recorta la imagen
	 * 
	 * @param imagen
	 */
	public static Image recortarImagen(Image imagen) {
		WritableImage imagenRecortada = null;
		// Toma la imagen, si existe
		if (imagen != null) {
			if (imagen.getWidth() < 1 || imagen.getHeight() < 1) {
				return null;
			} else {
				double nuevoX = 0;
				double nuevoY = 0;
				double nuevoAncho = 0;
				double nuevoAlto = 0;

				// Si el ancho y el alto son iguales, no cambia nada
				if (imagen.getHeight() == imagen.getWidth()) {
					return imagen;
				} else {
					// Si el alto es menor que el ancho, cambia el tamaño base al menor de ambos
					if (imagen.getHeight() < imagen.getWidth()) {
						nuevoAncho = imagen.getHeight();
						nuevoAlto = imagen.getHeight();
						// Mueve el origen de la imagen a la mitad de la diferencia entre los tamaños
						nuevoX = (imagen.getWidth() - imagen.getHeight()) / 2;
					} else {
						nuevoAncho = imagen.getWidth();
						nuevoAlto = imagen.getWidth();
						nuevoY = (imagen.getHeight() - imagen.getWidth()) / 2;
					}
					imagenRecortada = new WritableImage(imagen.getPixelReader(), (int) nuevoX, (int) nuevoY,
							(int) nuevoAncho, (int) nuevoAlto);
					return imagenRecortada;
				}
			}
		} else {
			return null;
		}
	}

	public static void validarFechaEditor() {
		// TODO Auto-generated method stub

	}

}
