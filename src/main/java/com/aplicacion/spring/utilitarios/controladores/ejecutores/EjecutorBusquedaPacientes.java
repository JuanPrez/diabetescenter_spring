package com.aplicacion.spring.utilitarios.controladores.ejecutores;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.negocio.servicios.ServiciosPaciente;

import javafx.concurrent.Task;
import javafx.scene.layout.Pane;

public class EjecutorBusquedaPacientes extends EjecutorLista<Paciente> {
	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(EjecutorBusquedaPacientes.class);

	String tipo;
	List<String> parametros;
	ServiciosPaciente serviciosPaciente;

	public EjecutorBusquedaPacientes(ServiciosPaciente serviciosPaciente, String tipo, List<String> parametros) {
		super();
		this.serviciosPaciente = serviciosPaciente;
		this.tipo = tipo;
		this.parametros = parametros;
	}

	public EjecutorBusquedaPacientes(ServiciosPaciente serviciosPaciente, Pane panelCarga, Pane panelFrontal,
			String tipo, List<String> parametros) {
		super(panelCarga, panelFrontal);
		this.serviciosPaciente = serviciosPaciente;
		this.tipo = tipo;
		this.parametros = parametros;
	}

	@Override
	protected Task<List<Paciente>> createTask() {
		return new Task<List<Paciente>>() {
			@Override
			protected List<Paciente> call() {
				// Mensaje Generico
				updateMessage(Mensajero.EJEC_GENERICO);
				switch (tipo) {
				case "nombre": {
					try {
						return serviciosPaciente.obtenerPacientesPorNombre(parametros.get(0), parametros.get(1));
					} catch (Exception exc) {
						return Collections.emptyList();
					}
				}
				case "dni": {
					return serviciosPaciente.obtenerPacientePorDNI(parametros.get(0));
				}
				case "id": {
					return serviciosPaciente.obtenerPacientePorID(Integer.parseInt(parametros.get(0)));
				}
				default: {
					return new ArrayList<>();
				}
				}
			}
		};
	}

}
