package com.aplicacion.spring.utilitarios.controladores.ejecutores;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.aplicacion.spring.Mensajero;
import com.aplicacion.spring.negocio.servicios.ServiciosPaciente;

import javafx.concurrent.Task;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;

public class EjecutorSubirImagen extends Ejecutor {
	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(EjecutorSubirImagen.class);
	public static final String PACIENTE = "paciente";
	public static final String HISTORIA_CLINICA = "histClinica";

	String tipo;
	File imagen;
	String nombre;
	int pacienteId;
	int usuarioId;
	ServiciosPaciente serviciosPaciente;

	private EjecutorSubirImagen(String tipo, String nombre, File imagen) {
		super();
		this.tipo = tipo;
		this.imagen = imagen;
		this.nombre = nombre;
	}

	private EjecutorSubirImagen(Pane panelCarga, Pane panelFrontal, String tipo, String nombre, File imagen) {
		super(panelCarga, panelFrontal);
		this.tipo = tipo;
		this.imagen = imagen;
		this.nombre = nombre;
	}

	public EjecutorSubirImagen(ServiciosPaciente serviciosPaciente, String tipo, String nombre, File imagen,
			int pacienteId, int usuarioId) {
		this(tipo, nombre, imagen);
		this.serviciosPaciente = serviciosPaciente;
		this.pacienteId = pacienteId;
		this.usuarioId = usuarioId;
	}

	public EjecutorSubirImagen(ServiciosPaciente serviciosPaciente, Pane panelCarga, Pane panelFrontal, String tipo,
			String nombre, File imagen, int pacienteId, int usuarioId) {
		this(panelCarga, panelFrontal, tipo, nombre, imagen);
		this.serviciosPaciente = serviciosPaciente;
		this.tipo = tipo;
		this.pacienteId = pacienteId;
		this.usuarioId = usuarioId;
	}

	public EjecutorSubirImagen(ServiciosPaciente serviciosPaciente, String tipo, File imagen, int pacienteId,
			int usuarioId) {
		this.serviciosPaciente = serviciosPaciente;
		this.pacienteId = pacienteId;
		this.usuarioId = usuarioId;
		this.imagen = imagen;
		this.tipo = tipo;
	}

	@Override
	protected Task<Object> createTask() {
		return new Task<Object>() {
			@Override
			protected Image call() throws FileNotFoundException, IOException {
				// Mensaje Generico
				updateMessage(Mensajero.EJEC_GENERICO);
				switch (tipo) {
				case PACIENTE: {
					if (serviciosPaciente.guardarImagenPaciente(new FileInputStream(imagen), imagen.length(), nombre,
							pacienteId, usuarioId))
						return new Image("file:" + imagen.getAbsolutePath(), 220, 220, true, true);
					else
						return null;
				}
				case HISTORIA_CLINICA: {

				}
				default:
					return null;
				}
			}
		};
	}

}
