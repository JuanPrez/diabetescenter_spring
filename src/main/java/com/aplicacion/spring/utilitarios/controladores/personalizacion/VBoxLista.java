package com.aplicacion.spring.utilitarios.controladores.personalizacion;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class VBoxLista<T> extends VBox {
	protected Image imagenIzquierda = null;
	protected Image imagenDerecha = null;

	/**
	 * Constructor por defecto
	 */
	public VBoxLista() {

	}

	/**
	 * Constructor con imagenes
	 */
	public VBoxLista(Image imagenIzquierda, Image imagenDerecha) {
		this.imagenIzquierda = imagenIzquierda;
		this.imagenDerecha = imagenDerecha;
	}

	/**
	 * Añade un nodo a la lista
	 * 
	 * @param t
	 * @param et
	 */
	public void sumarElemento(T t) {
		Button boton = nuevoNodo(t);

		boton.getStyleClass().clear();
		boton.getStyleClass().add("botonLista");

		boton.prefWidthProperty()
				.bind(this.widthProperty().subtract(this.getPadding().getLeft() + this.getPadding().getRight()));
		getChildren().add(boton);
	}

	/**
	 * Quita el elemento
	 * 
	 * @param t
	 */
	public void quitarElemento(T t) {
		for (Node n : getChildren()) {
			if (n.getUserData().equals(t))
				getChildren().remove(n);
		}
	}

	/**
	 * Quita todos los elementos
	 */
	public void limpiarElementos() {
		getChildren().clear();
	}

	/**
	 * Crea los botones por cada elemento de la lista
	 * 
	 * @param t
	 * @param et
	 * @return
	 */
	protected Button nuevoNodo(T t) {
		try {
			HBox contenedor = new HBox();

			contenedor.getStyleClass().add("botonListaHBox");
			contenedor.setAlignment(Pos.CENTER_LEFT);

			ImageView cimagenIzq = new ImageView();
			Label etiqueta = new Label(t.toString());
			ImageView cimagenDer = new ImageView();
			Region distanciador = new Region();
			HBox.setHgrow(distanciador, Priority.ALWAYS);

			cimagenIzq.setFitHeight(16);
			cimagenIzq.setFitWidth(16);
			cimagenIzq.setPreserveRatio(true);

			cimagenDer.setFitHeight(16);
			cimagenDer.setFitWidth(16);
			cimagenDer.setPreserveRatio(true);

			if (imagenDerecha != null) {
				cimagenDer.setImage(imagenDerecha);
			}
			if (imagenIzquierda != null) {
				cimagenIzq.setImage(imagenIzquierda);
			}

			contenedor.getChildren().addAll(cimagenIzq, etiqueta, distanciador, cimagenDer);

			Button b = new Button();
			b.setUserData(t);
			b.setGraphic(contenedor);
			b.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

			return b;

		} catch (Exception exc) {
			return null;
		}
	}

	public void setImagenIzquierda(Image imagenIzquierda) {
		this.imagenIzquierda = imagenIzquierda;
	}

	public void setImagenDerecha(Image imagenDerecha) {
		this.imagenDerecha = imagenDerecha;
	}
}
