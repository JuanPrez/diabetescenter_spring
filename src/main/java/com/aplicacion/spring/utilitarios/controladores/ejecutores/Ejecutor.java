package com.aplicacion.spring.utilitarios.controladores.ejecutores;

import javafx.concurrent.Service;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.Pane;

public abstract class Ejecutor extends Service<Object> {

	// Panel que contiene el indicador de progreso
	private Pane panelCarga;
	// Indicador de progreso
	private ProgressIndicator indicadorCarga;
	// Etiqueta que contiene el texto de progreso
	private Label textoCarga;
	// Panel que se inhabilita mientras la tarea corre
	private Pane panelFrontal;

	public Ejecutor() {
	}

	public Ejecutor(Pane panelCarga, Pane panelFrontal) {
		if (getPanelCarga() == null && panelCarga != null) {
			setPanelCarga(panelCarga);
		}
		if (getPanelFrontal() == null && panelFrontal != null) {
			setPanelFrontal(panelFrontal);
		}
	}

	/**
	 * Inicia (o reinicia) el servicio
	 * 
	 * @param servicio
	 */
	public void correrServicio() {
		if (this.getState() != State.READY) {
			this.restart();
		} else {
			this.start();
		}
	}

	@Override
	protected void running() {
		regularOpacidad(0.7);
		conectarPropiedades();
		super.running();
	}

	@Override
	protected void failed() {
		regularOpacidad(0);
		super.failed();
	}

	@Override
	protected void cancelled() {
		regularOpacidad(0);
		super.cancelled();
	}

	@Override
	protected void succeeded() {
		regularOpacidad(0);
		super.succeeded();
	}

	/**
	 * Regular la opacidad del indicador de carga
	 * 
	 * @param valor
	 */
	private void regularOpacidad(double valor) {
		if (panelCarga != null) {
			panelCarga.setOpacity(valor);
		}
	}

	/**
	 * Ata las propiedades de progreso y correr
	 */
	private void conectarPropiedades() {
		if (textoCarga != null && !textoCarga.textProperty().isBound())
			textoCarga.textProperty().bind(this.messageProperty());
		if (indicadorCarga != null && !indicadorCarga.progressProperty().isBound())
			indicadorCarga.progressProperty().bind(this.progressProperty());
		if (panelFrontal != null && !panelFrontal.disableProperty().isBound())
			panelFrontal.disableProperty().bind(this.runningProperty());
	}

	public void setPanelCarga(Pane panelCarga) {
		this.panelCarga = panelCarga;
		// Buscamos los elementos de carga, y los conectamos
		for (Node elemento : panelCarga.getChildren()) {
			if (elemento instanceof ProgressIndicator) {
				indicadorCarga = (ProgressIndicator) elemento;
			} else if (elemento instanceof Label) {
				textoCarga = (Label) elemento;
			}
		}
	}

	public void setPanelFrontal(Pane panelFrontal) {
		this.panelFrontal = panelFrontal;
	}

	public Pane getPanelCarga() {
		return panelCarga;
	}

	public Pane getPanelFrontal() {
		return panelFrontal;
	}
}
