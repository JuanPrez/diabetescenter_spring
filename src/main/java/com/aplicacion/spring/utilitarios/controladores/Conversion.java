package com.aplicacion.spring.utilitarios.controladores;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

/**
 * Clase utilitaria para conversiones entre formatos
 * 
 * @author Juan Ignacio Prez
 *
 */
public class Conversion {

	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(Conversion.class);

	public static final String XSLT_CRONOGRAMA = "templates/cronograma.xsl";

	private Conversion() {

	}

	/**
	 * Simple conversion del objeto pasado a formato XML
	 * 
	 * @param object,
	 *            objeto a guardar
	 * @param xml,
	 *            archivo donde se guardara el objeto
	 */
	public static <T> void formarXML(T object, String xml) {
		try {
			File file = new File(xml);
			JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// Formato bonito
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(object, file);

			bitacora.debug(String.format("%s convertido al archivo %s", object.getClass().getName(), xml));
		} catch (JAXBException e) {
			bitacora.info("Conversion con problemas", e);
		}
	}

	/**
	 * Convierte el archivo XML usando la plantilla XSL a formato HTML
	 * 
	 * @param xmlInput
	 * @param xslTemplate
	 * @param htmlOutput
	 */
	public static void convertirXMLaHTML(String xmlInput, String xslTemplate, String htmlOutput) {
		Source xmlSource = new StreamSource(xmlInput);

		Source xsltSource = new StreamSource(Conversion.class.getClassLoader().getResourceAsStream(xslTemplate));
		StringWriter stringWriter = new StringWriter();

		try {
			FileWriter fileWriter = new FileWriter(htmlOutput);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer(xsltSource);
			transformer.transform(xmlSource, new StreamResult(stringWriter));
			fileWriter.write(stringWriter.toString());
			fileWriter.close();

			bitacora.debug(String.format("%s convertido en %s", xmlInput, htmlOutput));

		} catch (IOException | TransformerConfigurationException e) {
			bitacora.info("Conversion con problemas", e);
		} catch (TransformerFactoryConfigurationError e) {
			bitacora.info("Conversion con problemas", e);
		} catch (TransformerException e) {
			bitacora.info("Conversion con problemas", e);
		}

		System.out.println(URI.create(xmlInput).getPath());
		System.out.println(Conversion.class.getClassLoader().getResource(xslTemplate).toExternalForm());
		System.out.println(URI.create(htmlOutput).getPath());

	}

	/**
	 * Convierte el archivo XML usando la plantilla XSL a formato HTML
	 * 
	 * @param xmlInput
	 * @param xslTemplate
	 * @param htmlOutput
	 */
	public static String convertirXMLaHTML(String xmlInput, String xslTemplate) {
		Source xmlSource = new StreamSource(xmlInput);

		Source xsltSource = new StreamSource(Conversion.class.getClassLoader().getResourceAsStream(xslTemplate));
		StringWriter stringWriter = new StringWriter();

		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer(xsltSource);
			transformer.transform(xmlSource, new StreamResult(stringWriter));

			bitacora.debug(String.format("%s convertido", xmlInput));

		} catch (TransformerConfigurationException e) {
			bitacora.info("Conversion con problemas", e);
		} catch (TransformerFactoryConfigurationError e) {
			bitacora.info("Conversion con problemas", e);
		} catch (TransformerException e) {
			bitacora.info("Conversion con problemas", e);
		}

		return stringWriter.toString();
	}

}
