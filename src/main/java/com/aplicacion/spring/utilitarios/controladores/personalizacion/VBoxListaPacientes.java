package com.aplicacion.spring.utilitarios.controladores.personalizacion;

import com.aplicacion.spring.negocio.entidades.paciente.Paciente;
import com.aplicacion.spring.utilitarios.Constantes;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class VBoxListaPacientes<T extends Paciente> extends VBoxLista<T> {

	private static final String BOTON_LISTA = "botonLista";
	private static final String BOTON_LIST_HBOX = "botonListaHBox";

	/**
	 * Añade un nodo a la lista
	 * 
	 * @param paciente
	 * @param et
	 */
	@Override
	public void sumarElemento(T paciente) {
		Button boton = nuevoNodo(paciente);

		boton.getStyleClass().clear();
		boton.getStyleClass().add(BOTON_LISTA);

		boton.prefWidthProperty()
				.bind(this.widthProperty().subtract(this.getPadding().getLeft() + this.getPadding().getRight()));

		// Datos flotantes
		StringBuilder datosPaciente = new StringBuilder();
		datosPaciente.append("- " + paciente.getApellido() + ", " + paciente.getNombre() + "\n");
		if (paciente.getDatosPersonales() != null) {
			if (paciente.getDatosPersonales().getDni() != null && !paciente.getDatosPersonales().getDni().isEmpty()) {
				datosPaciente.append(paciente.getDatosPersonales().getDni().toString() + "\n");
			} else {
				datosPaciente.append("Sin DNI\n");
			}
			if (paciente.getDatosPersonales().getFechaNacimiento() != null) {
				datosPaciente.append(
						paciente.getDatosPersonales().getFechaNacimiento().format(Constantes.FECHA_DD_MM_YYYY) + "\n");
			} else {
				datosPaciente.append("Sin Fecha de Nacimiento\n");
			}
		} else {
			datosPaciente.append("Sin DNI\n");
			datosPaciente.append("Sin Fecha de Nacimiento\n");
		}
		if (paciente.getDatosDemograficos() != null) {
			if (paciente.getDatosDemograficos().getTelefono() != null
					&& !paciente.getDatosDemograficos().getTelefono().isEmpty()) {
				datosPaciente.append(paciente.getDatosDemograficos().getTelefono() + "\n");
			} else {
				datosPaciente.append("Sin Telefono\n");
			}
		} else {
			datosPaciente.append("Sin Telefono\n");
		}
		Tooltip datos = new Tooltip(datosPaciente.toString());
		datos.getStyleClass().add("fondo-blanco");
		// Tooltip datos = new Tooltip();
		//
		// datos.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		//
		// datos.setGraphic(crearTooltip(paciente));
		//
		boton.setTooltip(datos);

		getChildren().add(boton);
	}

	private VBox crearTooltip(T paciente) {
		ImageView imagenPaciente = new ImageView(Paciente.getImagenDefecto());

		imagenPaciente.setFitHeight(64);
		imagenPaciente.setFitWidth(64);

		VBox vboxApellidoNombre = new VBox();
		Label apellido = new Label(paciente.getApellido());
		apellido.getStyleClass().add("texto16, bold");
		Label nombre = new Label(paciente.getNombre());

		vboxApellidoNombre.getStyleClass().add("padding4");
		vboxApellidoNombre.getChildren().addAll(apellido, nombre);

		HBox hboxCabecera = new HBox(4);

		hboxCabecera.getStyleClass().add("padding8");

		VBox tooltipVBox = new VBox();

		tooltipVBox.getStyleClass().addAll("fondo-blanco", "texto-negro");

		tooltipVBox.getChildren().add(hboxCabecera);

		// GridPane gridPane = null;
		//
		// if (paciente.getDatosPersonales() != null) {
		// gridPane = new GridPane();
		//
		// gridPane.add(new Label("DNI:"), 0, 0);
		// gridPane.add(new Label("Fecha:"), 0, 1);
		// // gridPane.add(new Label("Ult Consulta:"), 0, 0);
		//
		// gridPane.add(new Label(paciente.getDatosPersonales().getDni()), 1, 0);
		// gridPane.add(new
		// Label(paciente.getDatosPersonales().getFechaNacimiento().toString()), 1, 1);
		//
		// if (paciente.getDatosPersonales().getFoto() != null)
		// imagenPaciente.setImage(paciente.getDatosPersonales().getFoto());
		// }
		//
		// if (paciente.getDatosDemograficos() != null) {
		// if (gridPane != null)
		// gridPane = new GridPane();
		//
		// gridPane.add(new Label("TE:"), 0, 2);
		// gridPane.add(new Label(paciente.getDatosDemograficos().getTelefono()), 0, 2);
		//
		// }
		//
		// if (gridPane != null)
		// tooltipVBox.getChildren().add(gridPane);
		//

		return tooltipVBox;
	}

	/**
	 * Quita el elemento
	 * 
	 * @param t
	 */
	@Override
	public void quitarElemento(T t) {
		for (Node n : getChildren()) {
			if (n.getUserData().equals(t))
				getChildren().remove(n);
		}
	}

	/**
	 * Quita todos los elementos
	 */
	@Override
	public void limpiarElementos() {
		getChildren().clear();
	}

	/**
	 * Crea los botones por cada elemento de la lista
	 * 
	 * @param t
	 * @param et
	 * @return
	 */
	@Override
	protected Button nuevoNodo(T t) {
		try {
			HBox contenedor = new HBox();

			contenedor.getStyleClass().add(BOTON_LIST_HBOX);
			contenedor.setAlignment(Pos.CENTER_LEFT);

			ImageView cimagenIzq = new ImageView();
			Label etiqueta = new Label(t.getApellido() + ", " + t.getNombre());
			ImageView cimagenDer = new ImageView();
			Region distanciador = new Region();
			HBox.setHgrow(distanciador, Priority.ALWAYS);

			cimagenIzq.setFitHeight(16);
			cimagenIzq.setFitWidth(16);
			cimagenIzq.setPreserveRatio(true);

			cimagenDer.setFitHeight(16);
			cimagenDer.setFitWidth(16);
			cimagenDer.setPreserveRatio(true);

			if (imagenDerecha != null) {
				cimagenDer.setImage(imagenDerecha);
			}
			if (imagenIzquierda != null) {
				cimagenIzq.setImage(imagenIzquierda);
			}

			contenedor.getChildren().addAll(cimagenIzq, etiqueta, distanciador, cimagenDer);

			Button b = new Button();
			b.setUserData(t);
			b.setGraphic(contenedor);
			b.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

			return b;

		} catch (Exception exc) {
			return null;
		}
	}

}
