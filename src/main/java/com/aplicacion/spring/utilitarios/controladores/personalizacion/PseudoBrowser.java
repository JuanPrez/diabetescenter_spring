package com.aplicacion.spring.utilitarios.controladores.personalizacion;

import java.net.URL;

import org.apache.log4j.Logger;

import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class PseudoBrowser extends Region {
	// Carga de la bitacora
	static final Logger bitacora = Logger.getLogger(PseudoBrowser.class);
	private HBox barraDeBotones = new HBox();
	private final WebView explorador = new WebView();
	private final WebEngine motor = explorador.getEngine();

	public PseudoBrowser(String html, Button... botones) {
		explorador.setContextMenuEnabled(false);
		// apply the styles
		getStyleClass().add("browser");

		// create the toolbar
		barraDeBotones.setAlignment(Pos.CENTER);
		barraDeBotones.getStyleClass().add("browser-toolbar");

		if (botones != null) {
			for (Button b : botones) {
				if (b.getUserData().equals("Imprimir")) {
					b.setOnAction(e -> {
						// Crea la instancia del trabajo de impresion
						PrinterJob trabajoImpresion = PrinterJob.createPrinterJob();
						trabajoImpresion.getJobSettings().setJobName("Cronograma Turnos");
						if (trabajoImpresion != null && trabajoImpresion.showPrintDialog(null)) {
							motor.print(trabajoImpresion);
							trabajoImpresion.endJob();
						}
					});
				} else if (b.getUserData().equals("Exportar")) {
					b.setOnAction(e -> {
						bitacora.debug(b.getUserData());
					});
				} else {
					b.setOnAction(e -> {
						bitacora.debug(b.getUserData());
					});
				}
			}
			barraDeBotones.getChildren().addAll(botones);
		}

		barraDeBotones.getChildren().add(createSpacer());

		// Carga inicial
		motor.loadContent(html);

		// add components
		getChildren().add(barraDeBotones);
		getChildren().add(explorador);
	}

	public PseudoBrowser(URL urlInicial, Button... botones) {
		explorador.setContextMenuEnabled(false);
		// apply the styles
		getStyleClass().add("browser");

		// create the toolbar
		barraDeBotones.setAlignment(Pos.CENTER);
		barraDeBotones.getStyleClass().add("browser-toolbar");

		if (botones != null) {
			for (Button b : botones) {
				if (b.getUserData().equals("Imprimir")) {
					b.setOnAction(e -> {
						// Crea la instancia del trabajo de impresion
						PrinterJob trabajoImpresion = PrinterJob.createPrinterJob();
						if (trabajoImpresion != null && trabajoImpresion.showPrintDialog(null)) {
							motor.print(trabajoImpresion);
							trabajoImpresion.endJob();
						}
					});
				} else if (b.getUserData().equals("Exportar")) {
					b.setOnAction(e -> {
						System.out.println(b.getUserData());
					});
				} else {
					b.setOnAction(e -> {
						System.out.println(b.getUserData());
					});
				}
			}
			barraDeBotones.getChildren().addAll(botones);
		}

		barraDeBotones.getChildren().add(createSpacer());

		// Carga inicial
		motor.load(urlInicial.toString());

		// add components
		getChildren().add(barraDeBotones);
		getChildren().add(explorador);
	}

	private Node createSpacer() {
		Region spacer = new Region();
		HBox.setHgrow(spacer, Priority.ALWAYS);
		return spacer;
	}

	@Override
	protected void layoutChildren() {
		double w = getWidth();
		double h = getHeight();
		double toolBarHeight = barraDeBotones.prefHeight(w);
		layoutInArea(explorador, 0, toolBarHeight, w, h - toolBarHeight, 0, HPos.CENTER, VPos.CENTER);
		layoutInArea(barraDeBotones, 0, 0, w, toolBarHeight, 0, HPos.CENTER, VPos.CENTER);
	}

	@Override
	protected double computePrefWidth(double height) {
		return 750;
	}

	@Override
	protected double computePrefHeight(double width) {
		return 600;
	}
}